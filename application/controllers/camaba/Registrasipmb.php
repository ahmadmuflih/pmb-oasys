<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasipmb extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'camaba'){
			$array=array('page'=>'3');
		$this->load->model(array('Camaba','ConfigRekening','Batch','Fakultas','Jurusan','Pilihan'));
		$fakultas = $this->Fakultas->get(null);
		$jurusan = "<option value='0' selected disabled>-PILIH JURUSAN-</option>";
		$i = 1;
		$j = 1;
		foreach ($fakultas->result() as $row) {
			$jurusan .= "<optgroup label='".$this->numberToRomanRepresentation($j).". FAKULTAS $row->nama'>";
			$jrs = $this->Jurusan->getJenjangJurusan(array('id_fakultas'=>$row->id));
			foreach ($jrs->result() as $row2) {
				$jurusan .= "<option value='$row2->id'>$i. $row2->jenjang - $row2->nama</option>";
				$i++;
			}
			$j++;
			$jurusan .= "</optgroup>";
		}
		$data['jurusan_filter'] = $jurusan;

		$id = $_SESSION['data']['id'];
		$data['camaba'] = $this->Camaba->get(array('id'=>$id))->row();
		$data['status'] = $this->Camaba->getWhere('t_status_camaba',array('id'=>$data['camaba']->id_status))->row();
		$data['kewarganegaraan'] = $this->Camaba->getWhere('t_kewarganegaraan',null);
		$data['provinsi'] = $this->Camaba->getWhere('t_provinsi',null);
		$data['jenis_kelamin'] = $this->Camaba->getWhere('t_jenis_kelamin',null);
		$data['agama'] = $this->Camaba->getWhere('t_agama',null);
		$data['tipe'] = $this->Camaba->getWhere('t_tipe_pendaftaran',null);

		$data['config'] = $this->ConfigRekening->get(null);
		$data['batch'] = $this->Batch->get(array('id'=>$data['camaba']->id_batch))->row();


		$data['kabupaten'] = "";
		$data['id_provinsi']=0;
		$pilihan = $this->Pilihan->get(array('id_camaba'=>$id));
		$prodi_p = array();
		foreach($pilihan->result() as $row){
			$prodi_p[$row->no_pilihan] = $row->id_prodi;
		}
		$data['pilihan'] = $prodi_p;

		if(!is_null($data['camaba']->id_kabupaten)){
			$kabupaten = $this->Camaba->getWhere('t_kabupaten',array('id'=>$data['camaba']->id_kabupaten));
			if($kabupaten->num_rows()>0){
				$kabupaten = $kabupaten->row();
				$id_provinsi = $kabupaten->id_provinsi;
				$data['id_provinsi'] = $id_provinsi;
				$kab = $this->Camaba->getWhere('t_kabupaten',array('id_provinsi'=>$id_provinsi));
				$data['kabupaten'] = "<option value='0' selected disabled>-Pilih Kabupaten/Kota-</option>";;
				foreach ($kab->result() as $row) {
					if($row->id == $data['camaba']->id_kabupaten)
						$data['kabupaten'] .="<option value='$row->id' selected>$row->nama</option>";
					else
						$data['kabupaten'] .="<option value='$row->id'>$row->nama</option>";
				}
			}
		}


		$this->load->view('header_v',$array);
		$this->load->view('camaba/registrasipmb_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	function sanitize_post($item, $key)
	{
		if($key != 'pilihan')
		$clean_values[$key] = trim($item);
		//optional further cleaning ex) htmlentities
	}
	function insert(){
		$cek = $this->session->userdata('status');
		if ($cek == 'camaba'||$cek == 'adminpmb'){
			$this->load->Model(array("Camaba","Pilihan","Jurusan"));

			if(isset($_POST['id_camaba'])){
				$id_camaba = $_POST['id_camaba'];
			}
			else{
				$id_camaba = $_SESSION['data']['id'];
			}
			$camaba = $this->Camaba->get(array('id'=>$id_camaba))->row();

			if($_POST['tipe'] == 3){
				if(($_FILES['bukti_pembayaran']['name'] == "") && $camaba->bukti_pembayaran == "") {
					echo json_encode(array('status'=>'gagal','message'=>"Upload bukti pembayaran!"));
					return;
				}
				$config['upload_path'] = '././assets/files/berkas';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['max_size']  = '999999999';
				$config['max_width']  = '99999';
				$config['max_height']  = '99999';
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('bukti_pembayaran')){
					$dataupload = $this->upload->data();
					$url_foto = "assets/files/berkas/".$dataupload['file_name'];
					if($camaba->bukti_pembayaran != ''  && file_exists("././".$camaba->bukti_pembayaran)){
						unlink('././'.$camaba->bukti_pembayaran);
					}
					$data['bukti_pembayaran'] = $url_foto;


				}
				$data['id_status'] = 2;
				$this->Camaba->update(array('id'=>$camaba->id),$data);
				echo json_encode(array('status'=>'berhasil','message'=>"Berhasil melakukan upload bukti pembayaran!"));
				return;
			}

			$clean_values = array();
			array_walk($_POST, array($this, 'sanitize_post'));

			$data = array(
				'nama' => $_POST['nama'],
				'alamat' => $_POST['alamat'],
				'tempat_lahir' =>$_POST['tempat_lahir'],
				'tanggal_lahir' => date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_lahir']))))),
				'sma_asal' => $_POST['sma_asal'],
				'pt_asal' => $_POST['pt_asal'],
				'fakultas_asal' => $_POST['fakultas_asal'],
				'prodi_asal' => $_POST['prodi_asal'],
				'bakat' => $_POST['bakat'],
				'prestasi' => $_POST['prestasi']
			);
			if($_POST['tipe'] == 2){
				if(!isset($_POST['jenis_kelamin']) || !isset($_POST['id_kabupaten']) || !isset($_POST['agama']) || !isset($_POST['id_tipe_pendaftaran']) || !isset($_POST['kewarganegaraan'])){
					print_r($_POST);
					echo json_encode(array('status'=>'gagal','message'=>"Isi semua data dengan lengkap!1"));
					return;
				}
				else if($_POST['nama'] == "" || $_POST['alamat'] == "" || $_POST['tempat_lahir'] == "" || $_POST['bakat'] == "" || $_POST['prestasi'] == "" ){
					echo json_encode(array('status'=>'gagal','message'=>"Isi semua data dengan lengkap!2"));
					return;
				}
				else if($_POST['id_tipe_pendaftaran'] == "1" && $_POST['sma_asal']==""){
					echo json_encode(array('status'=>'gagal','message'=>"Isi semua data dengan lengkap!3"));
					return;
				}
				else if($_POST['id_tipe_pendaftaran'] == "2" && ($_POST['pt_asal']=="" || $_POST['fakultas_asal']==""  || $_POST['prodi_asal']=="" )){
					echo json_encode(array('status'=>'gagal','message'=>"Isi semua data dengan lengkap!4"));
					return;
				}
				if(!isset($_POST['pilihan']) || count($_POST['pilihan']) != 2){
					echo json_encode(array('status'=>'gagal','message'=>"Isi semua pilihan prodi!"));
					return;
				}
				if(($camaba->ktp == "" && $_FILES['ktp']['name'] == "") || ($camaba->ijazah == "" && $_FILES['ijazah']['name'] == "") || ($camaba->transkrip == "" && $_FILES['transkrip']['name'] == "" && $_POST['id_tipe_pendaftaran'] == "2")) {
					echo json_encode(array('status'=>'gagal','message'=>"Upload semua berkas yang wajib!"));
					return;
				}

				$data['id_status'] = 1;
			}


			if(isset($_POST['jenis_kelamin'])){
				$data['jenis_kelamin'] = $_POST['jenis_kelamin'];
			}
			if(isset($_POST['id_kabupaten'])){
				$data['id_kabupaten'] = $_POST['id_kabupaten'];
			}
			if(isset($_POST['agama'])){
				$data['agama'] = $_POST['agama'];
			}
			if(isset($_POST['id_tipe_pendaftaran'])){
				$data['id_tipe_pendaftaran'] = $_POST['id_tipe_pendaftaran'];
			}
			if(isset($_POST['kewarganegaraan'])){
				$data['kewarganegaraan'] = $_POST['kewarganegaraan'];
			}



			$config['upload_path'] = '././assets/files/berkas';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
			$config['max_size']  = '999999999';
			$config['max_width']  = '99999';
			$config['max_height']  = '99999';
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('ijazah')){
				$dataupload = $this->upload->data();
				$url_foto = "assets/files/berkas/".$dataupload['file_name'];
				if($camaba->ijazah != '' && file_exists("././".$camaba->ijazah)){
					unlink('././'.$camaba->ijazah);
				}
				$data['ijazah'] = $url_foto;
			}
			if ($this->upload->do_upload('ktp')){

				$dataupload = $this->upload->data();
				$url_foto = "assets/files/berkas/".$dataupload['file_name'];
				if($camaba->ktp != '' && file_exists("././".$camaba->ktp)){
					unlink('././'.$camaba->ktp);
				}
				$data['ktp'] = $url_foto;
			}
			if ($this->upload->do_upload('surat_imigrasi')){
				$dataupload = $this->upload->data();
				$url_foto = "assets/files/berkas/".$dataupload['file_name'];
				if($camaba->surat_imigrasi != ''  && file_exists("././".$camaba->surat_imigrasi)){
					unlink('././'.$camaba->surat_imigrasi);
				}
				$data['surat_imigrasi'] = $url_foto;
			}
			if ($this->upload->do_upload('transkrip')){
				$dataupload = $this->upload->data();
				$url_foto = "assets/files/berkas/".$dataupload['file_name'];
				if($camaba->transkrip != ''  && file_exists("././".$camaba->transkrip)){
					unlink('././'.$camaba->transkrip);
				}
				$data['transkrip'] = $url_foto;
			}


			$pilihan1='0';
			if(isset($_POST['pilihan'])){
				foreach($_POST['pilihan'] as $ke => $id_prodi){
					if($ke == '1'){
						$pilihan1 = $id_prodi;
					}
					$cek = $this->Pilihan->get(array('id_camaba'=>$camaba->id,'no_pilihan'=>$ke));
					if($cek->num_rows()>0){
						$this->Pilihan->update(array('id_camaba'=>$camaba->id,'no_pilihan'=>$ke),array('id_prodi'=>$id_prodi));
					}
					else{
						$this->Pilihan->insert(array('id_camaba'=>$camaba->id,'no_pilihan'=>$ke,'id_prodi'=>$id_prodi));
					}
				}
			}
			if(isset($data['id_status']) && $data['id_status'] == '1' && $pilihan1!='0'){
				$kodeA = array('0002','0006','0007');
				$kodeB = array('0001','0003','0004','0005','0008');
				$kodeC = array('0009');
				// echo "id prodi : $pilihan1";
				$kode_fakultas = $this->Jurusan->getJenjangJurusan(array('j.id'=>$pilihan1))->row()->kode_fakultas;
				// echo "Kode Fakultas : ".$kode_fakultas;
				if (in_array($kode_fakultas, $kodeA)){
					$kode = "A";
				}
				else if(in_array($kode_fakultas, $kodeB)){
					$kode = "B";
				}
				else{
					$kode = "C";
				}
				$last = $this->Camaba->getLastId($kode);
				if(is_null($last->row()->kode)){
					$lastId = 0;
				}else{
					$lastId = (int)$last->row()->kode;
				}
				$nextId = $lastId+1;
				$data['no_registrasi'] = $kode.sprintf("%04d", $nextId);
				// echo "No Reg : ".$data['no_registrasi'];

			}
			$this->Camaba->update(array('id'=>$camaba->id),$data);
			echo json_encode(array('status'=>'berhasil','message'=>"Berhasil menyimpan data!"));
		}
	}

	function numberToRomanRepresentation($number) {
		$map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
		$returnValue = '';
		while ($number > 0) {
			foreach ($map as $roman => $int) {
				if($number >= $int) {
					$number -= $int;
					$returnValue .= $roman;
					break;
				}
			}
		}
		return $returnValue;
	}

	public function getKab(){
		$id_provinsi = $_POST['id_provinsi'];
		$this->load->model(array('Camaba'));
		$kab = $this->Camaba->getWhere('t_kabupaten',array('id_provinsi'=>$id_provinsi));
		$data = "<option value='0' selected disabled>-Pilih Kabupaten/Kota-</option>";;
		foreach ($kab->result() as $row) {
			$data.="<option value='$row->id'>$row->nama</option>";
		}
		echo json_encode(array('status'=>'berhasil','data'=>$data));
	}
	function getBiaya(){
		//print_r($_POST);
		$this->load->model(array('BiayaPendaftaran'));
		$data['biaya'] = 0;
		if($_POST['tipe'] == "null"){
			echo json_encode(array('status'=>'berhasil','data'=>$data));
			return;
		}
		$tipe = $_POST['tipe'];
		foreach($_POST['prodi'] as $prodi){
			$biaya = $this->BiayaPendaftaran->get(array('id_jurusan'=>$prodi,'id_tipe_pendaftaran'=>$tipe));
			if($biaya->num_rows()>0){
				//echo $biaya->row()->biaya;
				if($biaya->row()->biaya > $data['biaya']){
					$data['biaya'] = $biaya->row()->biaya;
				}
			}
		}
		echo json_encode(array('status'=>'berhasil','data'=>$data));
	}

	public function cetak()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'camaba'){
			$this->load->Model(array("Camaba","Pilihan","Ujian"));
			// $array=array('page'=>'31');
		$id = $_SESSION['data']['id'];
		$data['camaba'] = $this->Camaba->get(array('id'=>$id))->row();
		$batch = $this->Camaba->getWhere('t_batch_pmb',array('id'=>$data['camaba']->id_batch))->row();
		if($batch->aktif_cetak_ujian == 1){
			$data['ujian'] = $this->Ujian->getLengkap(array('u.id'=>$data['camaba']->id_ujian))->row();
			$pilihan1 = $this->Pilihan->getPilihan($id, 1);
			$pilihan2 = $this->Pilihan->getPilihan($id, 2);
			$data['pilihan1'] = "-";
			$data['pilihan2'] = "-";
			if($pilihan1->num_rows()>0){
				$data['pilihan1'] = $pilihan1->row()->fakultas." / ".$pilihan1->row()->prodi;
			}
			if($pilihan2->num_rows()>0){
				$data['pilihan2'] = $pilihan2->row()->fakultas." / ".$pilihan2->row()->prodi;
			}
			$this->load->view('camaba/printtest_v',$data);

		}
		}else{
			header("location:".base_url());
		}
	}

	public function updateFoto(){
		$cek = $this->session->userdata('status');
		if ($cek == 'camaba'||$cek == 'adminpmb'){
			$config['upload_path'] = '././assets/images/profile/camaba';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$config['max_size']  = '999999999';
			$config['max_width']  = '99999';
			$config['max_height']  = '99999';
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('file')){
					$status = "gagal";
					$msg = $this->upload->display_errors();
			}
			else{
				if(isset($_POST['id_camaba'])){
					$id_camaba = $_POST['id_camaba'];
				}
				else{
					$id_camaba = $_SESSION['data']['id'];
				}

				$this->load->Model("Camaba");
					$dataupload = $this->upload->data();
					$url_foto = "assets/images/profile/camaba/".$dataupload['file_name'];
					if ($cek == 'camaba'){
						$_SESSION['foto'] = $url_foto;
					}
					$status = "berhasil";
					$camaba = array('id'=>$id_camaba);
					$datacamaba = $this->Camaba->get($camaba)->row();
					if($datacamaba->foto != 'assets/images/profile/default.jpg'){
						unlink('././'.$datacamaba->foto);
					}
					$this->Camaba->update($camaba,array('foto'=>$url_foto));
					$msg = "Foto profil berhasil diupload";
			}
			echo json_encode(array('status'=>$status,'message'=>$msg));
		}
	}
}
