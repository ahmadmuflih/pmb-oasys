<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unduhberkas extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'camaba'){
		$this->load->view('header_v');
		$this->load->view('camaba/unduhberkas_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
}
