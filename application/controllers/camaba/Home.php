<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'camaba'){
			$this->load->model('Camaba');
			$array=array('page'=>'1');
			$id = $_SESSION['data']['id'];
		$data['camaba'] = $this->Camaba->get(array('id'=>$id))->row();
		$data['status'] = $this->Camaba->getWhere('t_status_camaba',array('id'=>$data['camaba']->id_status))->row();
		$this->load->view('header_v',$array);
		$this->load->view('camaba/dashboard_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
}
