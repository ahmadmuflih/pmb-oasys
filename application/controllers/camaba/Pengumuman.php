<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'camaba' && $_SESSION['batch']['aktif_pengumuman_lulus'] == 1){
			$array=array('page'=>'4');
			$this->load->model(array('Camaba','Jurusan'));
			$data['camaba'] = $this->Camaba->get(array('id'=>$_SESSION['data']['id']))->row();
			$data['prodi'] = $this->Jurusan->getJenjangJurusan(array('j.id'=>$data['camaba']->pilihan_lulus));
		$this->load->view('header_v',$array);
		$this->load->view('camaba/pengumuman_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
}
