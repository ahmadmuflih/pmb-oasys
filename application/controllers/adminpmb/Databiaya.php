<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Databiaya extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'34');
		$this->load->view('header_v',$array);
		$this->load->Model('Fakultas');
		$data['fakultas'] = $this->Fakultas->get();
		$this->load->view('adminpmb/databiaya_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	function getProdi(){
		$this->load->model(array('Jurusan'));
		$id_fakultas = $_POST['id_fakultas'];
		$prodi = $this->Jurusan->getJenjangJurusan(array('id_fakultas'=>$id_fakultas));
		$data="<option value='0'>SEMUA</option>";


		foreach ($prodi->result() as $row) {
			$data.= "<option value='$row->id'>$row->jenjang $row->nama</option>";
		}


		echo json_encode(array('status'=>'berhasil','data'=>$data));
	}
	function getData(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){

			$this->load->model(array('Jurusan','Fakultas','BiayaPendaftaran'));
			$fakultas = $this->Fakultas->get(null);
			// $data = array();
			$data = "";
			foreach ($fakultas->result() as $row) {
				$i=1;
				$data .="<tr><th style='background-color:#fff !important;' colspan='4'>$row->nama</th></tr>";
				$prodi = $this->Jurusan->getJenjangJurusan(array('j.id_fakultas'=>$row->id));
				foreach ($prodi->result() as $row2) {
					$biaya = $this->BiayaPendaftaran->get(array('id_jurusan'=>$row2->id,'id_tipe_pendaftaran'=>1));
					if($biaya->num_rows()>0){
						$biaya1 = "Rp. ".$biaya->row()->biaya;
					}
					else{
						$biaya1 = "BELUM DIATUR";
					}
					$biaya = $this->BiayaPendaftaran->get(array('id_jurusan'=>$row2->id,'id_tipe_pendaftaran'=>2));
					if($biaya->num_rows()>0){
						$biaya2 = "Rp. ".$biaya->row()->biaya;
					}
					else{
						$biaya2 = "BELUM DIATUR";
					}
					
					$data .= "<tr><td>".$i++."</td><td>$row2->jenjang $row2->nama</td><td>$biaya1</td><td>$biaya2</td></tr>";
				}

			}
			echo json_encode(array('status'=>'berhasil','data'=>$data));
		}
		else{
			echo json_encode(array('status'=>'gagal','message'=>'Terjadi kesalahan!'));
		}
	}
	function setBiaya(){
		//print_r($_POST);
		$cek = $this->session->userdata('status');
		$this->load->Model(array('Jurusan','BiayaPendaftaran'));
		if ($cek == 'adminpmb'){
			$id_fakultas = $_POST['fakultas'];
			$id_prodi = $_POST['prodi'];
			$tipe = $_POST['tipe'];
			$biaya = $_POST['biaya'];
			if($biaya <= 0){
				echo json_encode(array('status'=>'gagal','message'=>'Perhatikan Biaya!'));
				return;
			}
			if($id_prodi == '0'){
				$prodi = $this->Jurusan->get(array('id_fakultas'=>$id_fakultas));
				foreach($prodi->result() as $row){
					if($tipe == '0'){
						for($i = 1; $i<=2; $i++){
							$cek = $this->BiayaPendaftaran->get(array('id_jurusan'=>$row->id,'id_tipe_pendaftaran'=>$i));
							if($cek->num_rows()==0){
								$this->BiayaPendaftaran->insert(array('id_jurusan'=>$row->id,'id_tipe_pendaftaran'=>$i,'biaya'=>$biaya));
							}
							else{
								$this->BiayaPendaftaran->update(array('id_jurusan'=>$row->id,'id_tipe_pendaftaran'=>$i),array('biaya'=>$biaya));
							}
						}
					}
					else{
						$cek = $this->BiayaPendaftaran->get(array('id_jurusan'=>$row->id,'id_tipe_pendaftaran'=>$tipe));
						if($cek->num_rows()==0){
							$this->BiayaPendaftaran->insert(array('id_jurusan'=>$row->id,'id_tipe_pendaftaran'=>$tipe,'biaya'=>$biaya));
						}
						else{
							$this->BiayaPendaftaran->update(array('id_jurusan'=>$row->id,'id_tipe_pendaftaran'=>$tipe),array('biaya'=>$biaya));
						}
					}
				}
			}else{
				if($tipe == '0'){
					for($i = 1; $i<=2; $i++){
						$cek = $this->BiayaPendaftaran->get(array('id_jurusan'=>$id_prodi,'id_tipe_pendaftaran'=>$i));
						if($cek->num_rows()==0){
							$this->BiayaPendaftaran->insert(array('id_jurusan'=>$id_prodi,'id_tipe_pendaftaran'=>$i,'biaya'=>$biaya));
						}
						else{
							$this->BiayaPendaftaran->update(array('id_jurusan'=>$id_prodi,'id_tipe_pendaftaran'=>$i),array('biaya'=>$biaya));
						}
					}
				}
				else{
					$cek = $this->BiayaPendaftaran->get(array('id_jurusan'=>$id_prodi,'id_tipe_pendaftaran'=>$tipe));
					if($cek->num_rows()==0){
						$this->BiayaPendaftaran->insert(array('id_jurusan'=>$id_prodi,'id_tipe_pendaftaran'=>$tipe,'biaya'=>$biaya));
					}
					else{
						$this->BiayaPendaftaran->update(array('id_jurusan'=>$id_prodi,'id_tipe_pendaftaran'=>$tipe),array('biaya'=>$biaya));
					}
				}
			}
			echo json_encode(array('status'=>'berhasil','message'=>'Berhasil Menyimpan!'));
		}
	}
}
