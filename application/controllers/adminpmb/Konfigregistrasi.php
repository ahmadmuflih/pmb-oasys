<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigregistrasi extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Batch'));
			$data['batch'] = $this->Batch->get(array('id'=>$_SESSION['batch']['id']))->row();

			$array=array('page'=>'62');
		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/konfigregistrasi_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	public function insert()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Batch'));
			$param = array('id'=>$_POST['id']);
			$data = array();
			//print_r($_POST);
			foreach($_POST as $name => $value){
				if($name != 'id'){
					$data[$name] = $value;
				}
			}
			$this->Batch->update($param,$data);
			echo json_encode(array('status'=>'berhasil','message'=>"Data berhasil diupdate"));
		}
	}
}
