<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Heregistrasi extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'5');
		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dataheregistrasi/heregistrasi_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	public function edit($id=null)
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb' && !is_null($id)){
			$this->load->model(array('Camaba','Jurusan'));
			
			$array=array('page'=>'5');
			$data['camaba'] = $this->Camaba->get(array('id'=>$id));
			if($data['camaba']->num_rows()==0){
				header("location:".base_url()."adminpmb/heregistrasi");
				return;
			}
			$data['camaba'] = $data['camaba']->row();
			$data['jk'] = $this->Camaba->getWhere('t_jenis_kelamin',null);
			$data['status'] = $this->Camaba->getWhere('t_status_mahasiswa',null);
			$data['jalur_masuk'] = $this->Camaba->getWhere('t_jalur_masuk',null);
			$data['jenis_pendaftaran'] = $this->Camaba->getWhere('t_jenis_pendaftaran',null);
			$data['provinsi'] = $this->Camaba->getWhere('t_provinsi',null);
			$data['jenis_tinggal'] = $this->Camaba->getWhere('t_jenis_tinggal',null);
			$data['transportasi'] = $this->Camaba->getWhere('t_alat_transportasi',null);
			$data['kewarganegaraan'] = $this->Camaba->getWhere('t_kewarganegaraan',null);
			$data['pekerjaan'] = $this->Camaba->getWhere('t_pekerjaan',null);
			$data['pendidikan'] = $this->Camaba->getWhere('t_pendidikan',null);
			$data['penghasilan'] = $this->Camaba->getWhere('t_penghasilan',null);
			$data['jurusan_sekolah'] = $this->Camaba->getWhere('t_jurusan_sekolah',null);
			$data['tipe_sekolah'] = $this->Camaba->getWhere('t_tipe_sekolah',null);
			$data['agama'] = $this->Camaba->getWhere('t_agama',null);
			
			$data['provinsi'] = $this->Camaba->getWhere('t_provinsi',null);
			$data['kabupaten'] = "";
			$data['id_provinsi']=0;
			if(!is_null($data['camaba']->id_kabupaten)){
				$kabupaten = $this->Camaba->getWhere('t_kabupaten',array('id'=>$data['camaba']->id_kabupaten));
				if($kabupaten->num_rows()>0){
					$kabupaten = $kabupaten->row();
					$id_provinsi = $kabupaten->id_provinsi;
					$data['id_provinsi'] = $id_provinsi;
					$kab = $this->Camaba->getWhere('t_kabupaten',array('id_provinsi'=>$id_provinsi));
					$data['kabupaten'] = "<option value='0' selected disabled>-Pilih Kabupaten/Kota-</option>";;
					foreach ($kab->result() as $row) {
						if($row->id == $data['camaba']->id_kabupaten)
							$data['kabupaten'] .="<option value='$row->id' selected>$row->nama</option>";
						else
							$data['kabupaten'] .="<option value='$row->id'>$row->nama</option>";
					}
				}
			}
			$data['pilihan'] = array(
				array('id'=>1, 'nama'=>'YA','status'=>'AKTIF'),
				array('id'=>0, 'nama'=>'TIDAK','status'=>'NON AKTIF'),
			);
			$data['prodi'] = $this->Jurusan->getJenjangJurusan(array('j.id'=>$data['camaba']->pilihan_lulus))->row();

		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dataheregistrasi/editheregistrasi_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url()."adminpmb/heregistrasi");
		}
	}
	public function cetak($id = null)
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb' && !is_null($id)){
			$this->load->model(array("Camaba","Jurusan"));
			$data['camaba'] = $this->Camaba->get(array('id'=>$id));
			if($data['camaba']->num_rows()==0){
				header("location:".base_url()."adminpmb/heregistrasi");
				return;
			}
			$data['camaba'] = $data['camaba']->row();
			if($data['camaba']->tanggal_lahir != '0000-00-00')
				$data['camaba']->tanggal_lahir = $this->tgl_indo($data['camaba']->tanggal_lahir);
			if($data['camaba']->tgl_lahir_ayah != '0000-00-00')
				$data['camaba']->tgl_lahir_ayah = $this->tgl_indo($data['camaba']->tgl_lahir_ayah);
			if($data['camaba']->tgl_lahir_ibu != '0000-00-00')
				$data['camaba']->tgl_lahir_ibu = $this->tgl_indo($data['camaba']->tgl_lahir_ibu);
			if($data['camaba']->tgl_lahir_ibu != '0000-00-00')
				$data['camaba']->tgl_lahir_wali = $this->tgl_indo($data['camaba']->tgl_lahir_wali);

			$data['jk'] = $this->Camaba->getWhere('t_jenis_kelamin',null);
			$data['status'] = $this->Camaba->getWhere('t_status_mahasiswa',null);
			$data['jalur_masuk'] = $this->Camaba->getWhere('t_jalur_masuk',null);
			$data['jenis_pendaftaran'] = $this->Camaba->getWhere('t_tipe_pendaftaran',null);
			$data['provinsi'] = $this->Camaba->getWhere('t_provinsi',null);
			$data['jenis_tinggal'] = $this->Camaba->getWhere('t_jenis_tinggal',null);
			$data['transportasi'] = $this->Camaba->getWhere('t_alat_transportasi',null);
			$data['kewarganegaraan'] = $this->Camaba->getWhere('t_kewarganegaraan',null);
			$data['pekerjaan'] = $this->Camaba->getWhere('t_pekerjaan',null);
			$data['pendidikan'] = $this->Camaba->getWhere('t_pendidikan',null);
			$data['penghasilan'] = $this->Camaba->getWhere('t_penghasilan',null);
			$data['agama'] = $this->Camaba->getWhere('t_agama',null);
			$data['prodi'] = $this->Jurusan->getJenjangJurusan(array('j.id'=>$data['camaba']->pilihan_lulus))->row();
			// $array=array('page'=>'31');
		$this->load->view('adminpmb/dataheregistrasi/printheregistrasi_v',$data);
		}else{
			header("location:".base_url()."adminpmb/heregistrasi");
		}
	}
	function json() {
		$this->load->library('datatables');
		$this->load->model(array('Camaba'));
		header('Content-Type: application/json');
		echo $this->Camaba->json_heregistrasi();
	}
	function tgl_indo($tanggal){
		$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun
	 
		return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
	}
}
