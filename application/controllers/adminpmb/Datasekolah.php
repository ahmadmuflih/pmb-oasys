<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datasekolah extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
		$this->load->view('header_v');
		$this->load->view('adminpmb/datasekolah_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}

	public function detail()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
		$this->load->view('header_v');
		$this->load->view('adminpmb/detailsekolah_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
}
