<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataregistrasi extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'32');

			$this->load->model(array('Fakultas','Jurusan'));
		$fakultas = $this->Fakultas->get(null);
		$jurusan = "<option value='0'>-PILIH JURUSAN-</option>";
		$i = 1;
		$j = 1;
		foreach ($fakultas->result() as $row) {
			$jurusan .= "<optgroup label='".$this->numberToRomanRepresentation($j).". FAKULTAS $row->nama'>";
			$jrs = $this->Jurusan->getJenjangJurusan(array('id_fakultas'=>$row->id));
			foreach ($jrs->result() as $row2) {
				$jurusan .= "<option value='$row2->id'>$i. $row2->jenjang - $row2->nama</option>";
				$i++;
			}
			$j++;
			$jurusan .= "</optgroup>";
		}
		$data['jurusan_filter'] = $jurusan;

		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dataregistrasi/dataregistrasi_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}

	function ubahStatus(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->Model('Camaba');
			$status = $_POST['status'];
			foreach($_POST['id_camaba'] as $id){
				$this->Camaba->update(array('id'=>$id),array('id_status'=>$status,'pilihan_lulus'=>NULL));
			}
		}
		echo json_encode(array('status'=>'berhasil','message'=>"Berhasil mengubah status!"));
	}

	public function edit($noreg = null)
	{
		$cek = $this->session->userdata('status');
		if($noreg==null){
			header("location:".base_url()."adminpmb/dataregistrasi");
		}

		if ($cek == 'adminpmb'){
			$array=array('page'=>'32');

			$this->load->model(array('Camaba','ConfigRekening','Batch','Fakultas','Jurusan','Pilihan'));
		$fakultas = $this->Fakultas->get(null);
		$jurusan = "<option value='0' selected disabled>-PILIH JURUSAN-</option>";
		$i = 1;
		$j = 1;
		foreach ($fakultas->result() as $row) {
			$jurusan .= "<optgroup label='".$this->numberToRomanRepresentation($j).". FAKULTAS $row->nama'>";
			$jrs = $this->Jurusan->getJenjangJurusan(array('id_fakultas'=>$row->id));
			foreach ($jrs->result() as $row2) {
				$jurusan .= "<option value='$row2->id'>$i. $row2->jenjang - $row2->nama</option>";
				$i++;
			}
			$j++;
			$jurusan .= "</optgroup>";
		}
		$data['jurusan_filter'] = $jurusan;

		$camaba = $this->Camaba->get(array('id'=>$noreg));
		if($camaba->num_rows()==0){
			header("location:".base_url()."adminpmb/dataregistrasi");
		}
		$data['camaba'] = $camaba->row();
		$id = $data['camaba']->id;
		$data['kewarganegaraan'] = $this->Camaba->getWhere('t_kewarganegaraan',null);
		$data['provinsi'] = $this->Camaba->getWhere('t_provinsi',null);
		$data['jenis_kelamin'] = $this->Camaba->getWhere('t_jenis_kelamin',null);
		$data['agama'] = $this->Camaba->getWhere('t_agama',null);
		$data['tipe'] = $this->Camaba->getWhere('t_tipe_pendaftaran',null);

		$data['config'] = $this->ConfigRekening->get(null);
		$data['batch'] = $this->Batch->get(array('id'=>$data['camaba']->id_batch))->row();
		$data['status'] = $this->Camaba->getWhere('t_status_camaba',array('id'=>$data['camaba']->id_status))->row();

		$data['kabupaten'] = "";
		$data['id_provinsi']=0;
		$pilihan = $this->Pilihan->get(array('id_camaba'=>$id));
		$prodi_p = array();
		foreach($pilihan->result() as $row){
			$prodi_p[$row->no_pilihan] = $row->id_prodi;
		}
		$data['pilihan'] = $prodi_p;

		if(!is_null($data['camaba']->id_kabupaten)){
			$kabupaten = $this->Camaba->getWhere('t_kabupaten',array('id'=>$data['camaba']->id_kabupaten));
			if($kabupaten->num_rows()>0){
				$kabupaten = $kabupaten->row();
				$id_provinsi = $kabupaten->id_provinsi;
				$data['id_provinsi'] = $id_provinsi;
				$kab = $this->Camaba->getWhere('t_kabupaten',array('id_provinsi'=>$id_provinsi));
				$data['kabupaten'] = "<option value='0' selected disabled>-Pilih Kabupaten/Kota-</option>";;
				foreach ($kab->result() as $row) {
					if($row->id == $data['camaba']->id_kabupaten)
						$data['kabupaten'] .="<option value='$row->id' selected>$row->nama</option>";
					else
						$data['kabupaten'] .="<option value='$row->id'>$row->nama</option>";
				}
			}
		}

		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dataregistrasi/editregistrasi_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	public function add($id = null)
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'32');
		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dataregistrasi/tambahregistrasi_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}

	function json() {
		$this->load->library('datatables');
		$id_prodi = $_POST['id_prodi'];
		$status = $_POST['status'];
		$tipe = $_POST['tipe'];
		$this->load->model(array('Pilihan'));
		header('Content-Type: application/json');
		echo $this->Pilihan->json($id_prodi,$tipe,$status);
	}


	public function cetak($id=null)
	{
		$this->load->model(array('Camaba','Ujian','Pilihan'));
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb' && !is_null($id)){
			// $array=array('page'=>'31');

		$data['camaba'] = $this->Camaba->get(array('id'=>$id));
		if($data['camaba']->num_rows()==0){
			header("location:".base_url()."adminpmb/dataregistrasi");
			return;
		}
		$data['camaba'] = $data['camaba']->row();
		$data['ujian'] = $this->Ujian->getLengkap(array('u.id'=>$data['camaba']->id_ujian))->row();
		$pilihan1 = $this->Pilihan->getPilihan($id, 1);
		$pilihan2 = $this->Pilihan->getPilihan($id, 2);
		$data['pilihan1'] = "-";
		$data['pilihan2'] = "-";
		if($pilihan1->num_rows()>0){
			$data['pilihan1'] = $pilihan1->row()->fakultas." / ".$pilihan1->row()->prodi;
		}
		if($pilihan2->num_rows()>0){
			$data['pilihan2'] = $pilihan2->row()->fakultas." / ".$pilihan2->row()->prodi;
		}
		$this->load->view('adminpmb/dataregistrasi/printtest_v',$data);
		}else{
			header("location:".base_url()."adminpmb/dataregistrasi");
		}
	}

	function numberToRomanRepresentation($number) {
		$map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
		$returnValue = '';
		while ($number > 0) {
			foreach ($map as $roman => $int) {
				if($number >= $int) {
					$number -= $int;
					$returnValue .= $roman;
					break;
				}
			}
		}
		return $returnValue;
	}
}
