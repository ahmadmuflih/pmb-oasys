<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'1');
			$this->load->model(array('Batch','Camaba','Jurusan'));
			$data['akun'] = $this->Camaba->get(array('id_batch'=>$_SESSION['batch']['id']))->num_rows();
			$data['pendaftar'] = $this->Camaba->get(array('id_batch'=>$_SESSION['batch']['id'],'id_status'=>3))->num_rows();
			$data['lulus'] = $this->Camaba->get(array('id_batch'=>$_SESSION['batch']['id'],'pilihan_lulus is not null'=>null))->num_rows();
			$data['jurusan'] = $this->Jurusan->get(array('id_status'=>1))->num_rows();
			$data['batch'] = $this->Batch->getWithTahun(null);
		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dashboard_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}

	public function setBatch(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$id = $_POST['id'];
			$this->load->model(array('Batch'));
			$_SESSION['batch'] = $this->Batch->getWithTahun(array('b.id'=>$id))->row_array();
			//print_r($_SESSION);
			echo json_encode(array('status'=>'berhasil','message'=>'Berhasil Menyimpan!'));
		}
	}
}
