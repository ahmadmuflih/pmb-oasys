<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodepmb extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'35');
		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/periodepmb_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	function json() {
		$this->load->library('datatables');
		$this->load->model(array('Tahun'));
	header('Content-Type: application/json');
	echo $this->Tahun->json();
	}
	function jsonBatch() {
		$this->load->library('datatables');
		$id_periode = $_POST['id_periode'];
		$this->load->model(array('Batch'));
	header('Content-Type: application/json');
	echo $this->Batch->json($id_periode);
	}
	function insert(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$data = $_POST;
			// $data['tanggal_mulai'] = date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_mulai'])))));
			// $data['tanggal_selesai'] = date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_selesai'])))));
			// print_r($data);
			$this->load->Model(array('Tahun'));
			if($data['tahun_awal'] == '' || $data['tahun_akhir'] == ''){
				echo json_encode(array('status'=>'gagal','message'=>"Data tidak boleh kosong"));
				return;
			}
			// if($data['tanggal_mulai'] > $data['tanggal_selesai']){
			// 	echo json_encode(array('status'=>'gagal','message'=>"Periksa tanggal terlebih dahulu"));
			// 	return;
			// }
			$cek = $this->Tahun->get(array('kode'=>$data['tahun_awal']))->num_rows();
			if($cek > 0){
				echo json_encode(array('status'=>'gagal','message'=>"Periode PMB ".$data['tahun_awal']."/".$data['tahun_akhir']." sudah ada!"));
				return;
			}

			$data['status'] = 0;
			$data['kode'] = $data['tahun_awal'];
			$this->Tahun->insert($data);
			echo json_encode(array('status'=>'berhasil','message'=>"Data berhasil diinput!"));
		}
	}

	function update(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			//$data = $_POST;
			$param['id'] = $_POST['id_periode'];
			$data['tanggal_mulai'] = date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_mulai'])))));
			$data['tanggal_selesai'] = date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_selesai'])))));
			// print_r($data);
			$this->load->Model(array('Tahun'));
			if($data['tanggal_mulai'] == ''  || $data['tanggal_selesai'] == '' ){
				echo json_encode(array('status'=>'gagal','message'=>"Data tidak boleh kosong"));
				return;
			}
			if($data['tanggal_mulai'] > $data['tanggal_selesai']){
				echo json_encode(array('status'=>'gagal','message'=>"Periksa tanggal terlebih dahulu"));
				return;
			}
			$this->Tahun->update($param,$data);
			echo json_encode(array('status'=>'berhasil','message'=>"Data berhasil diupdate!"));
		}
	}

	

	function getPeriode(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Tahun'));
			$id = $_POST['id'];
			$data = $this->Tahun->get(array('id'=>$id));
			$periode = null;
			if($data->num_rows()!=0){
				$periode = $data->row_array();
				$originalDate = "2010-03-21";
				$periode['tanggal_mulai'] = date("d/m/Y", strtotime($periode['tanggal_mulai']));
				$periode['tanggal_selesai'] = date("d/m/Y", strtotime($periode['tanggal_selesai']));
				//print_r($periode);
			}
			echo json_encode(array('status'=>'berhasil','message'=>"Periode PMB berhasil diaktifkan!", 'data'=>$periode));
		}
	}
	function setAktif(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Tahun'));
			$id = $this->input->post('id');
			$this->Tahun->update(array('status'=>1),array('status'=>0));
			$cek = $this->Tahun->update(array('id'=>$id),array('status'=>1));
			if($cek>0)
				echo json_encode(array('status'=>'berhasil','message'=>"Periode PMB berhasil diaktifkan!"));
			else
				echo json_encode(array('status'=>'gagal','message'=>"Periode PMB gagal diaktifkan!"));
		}
	}
	function delete(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Tahun'));
			$id = $this->input->post('id');
			$cek_status = $this->Tahun->get(array('id'=>$id));
			if($cek_status->row()->status=='1'){
				echo json_encode(array('status'=>'gagal','message'=>"Tidak boleh menghapus Periode aktif!"));
			}
			else{
				$cek = $this->Tahun->delete(array('id'=>$id));
				if($cek>0)
					echo json_encode(array('status'=>'berhasil','message'=>"Periode berhasil dihapus!"));
				else
					echo json_encode(array('status'=>'gagal','message'=>"Periode gagal dihapus!"));
			}
		}
	}
	
	function insertBatch(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$data = $_POST;
			$data['tanggal_mulai'] = date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_mulai'])))));
			$data['tanggal_selesai'] = date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_selesai'])))));
			// print_r($data);
			$this->load->Model(array('Batch'));
			if($data['tanggal_mulai'] == ''  || $data['tanggal_selesai'] == '' ){
				echo json_encode(array('status'=>'gagal','message'=>"Data tidak boleh kosong"));
				return;
			}
			if($data['tanggal_mulai'] > $data['tanggal_selesai']){
				echo json_encode(array('status'=>'gagal','message'=>"Periksa tanggal terlebih dahulu"));
				return;
			}
			// $cek = $this->Tahun->get(array('kode'=>$data['tahun_awal']))->num_rows();
			// if($cek > 0){
			// 	echo json_encode(array('status'=>'gagal','message'=>"Periode PMB ".$data['tahun_awal']."/".$data['tahun_akhir']." sudah ada!"));
			// 	return;
			// }

			$data['status'] = 0;
			$this->Batch->insert($data);
			echo json_encode(array('status'=>'berhasil','message'=>"Data berhasil diinput!"));
		}
	}

	function updateBatch(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			//$data = $_POST;
			$param['id'] = $_POST['id_batch'];
			$data['nama'] = $_POST['nama'];
			$data['tanggal_mulai'] = date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_mulai'])))));
			$data['tanggal_selesai'] = date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_selesai'])))));
			//$data['id_tahun'] = $_POST['id_periode'];
			// print_r($data);
			$this->load->Model(array('Batch'));
			if($data['tanggal_mulai'] == ''  || $data['tanggal_selesai'] == '' ){
				echo json_encode(array('status'=>'gagal','message'=>"Data tidak boleh kosong"));
				return;
			}
			if($data['tanggal_mulai'] > $data['tanggal_selesai']){
				echo json_encode(array('status'=>'gagal','message'=>"Periksa tanggal terlebih dahulu"));
				return;
			}
			$this->Batch->update($param,$data);
			echo json_encode(array('status'=>'berhasil','message'=>"Data berhasil diupdate!"));
		}
	}

	

	function getBatch(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Batch'));
			$id = $_POST['id'];
			$data = $this->Batch->get(array('id'=>$id));
			$batch = null;
			if($data->num_rows()!=0){
				$batch = $data->row_array();
				$batch['tanggal_mulai'] = date("d/m/Y", strtotime($batch['tanggal_mulai']));
				$batch['tanggal_selesai'] = date("d/m/Y", strtotime($batch['tanggal_selesai']));
				//print_r($periode);
			}
			echo json_encode(array('status'=>'berhasil','message'=>"", 'data'=>$batch));
		}
	}
	function setAktifBatch(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Batch','Tahun'));
			$id = $this->input->post('id');
			$batch = $this->Batch->get(array('id'=>$id))->row();
			
			$this->Batch->update(array('status'=>1),array('status'=>0,'aktif_registrasi'=>0));
			
			$cek = $this->Batch->update(array('id'=>$id),array('status'=>1,'aktif_registrasi'=>1));
			if($cek>0){
				$this->Tahun->update(array('status'=>1),array('status'=>0));
				$this->Tahun->update(array('id'=>$batch->id_tahun),array('status'=>1));
				echo json_encode(array('status'=>'berhasil','message'=>"Batch PMB berhasil diaktifkan!"));
			}
			else
				echo json_encode(array('status'=>'gagal','message'=>"Batch PMB gagal diaktifkan!"));
		}
	}
	function deleteBatch(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Batch'));
			$id = $this->input->post('id');
			$cek_status = $this->Batch->get(array('id'=>$id));
			if($cek_status->row()->status=='1'){
				echo json_encode(array('status'=>'gagal','message'=>"Tidak boleh menghapus Batch aktif!"));
			}
			else{
				$cek = $this->Batch->delete(array('id'=>$id));
				if($cek>0)
					echo json_encode(array('status'=>'berhasil','message'=>"Batch berhasil dihapus!"));
				else
					echo json_encode(array('status'=>'gagal','message'=>"Batch gagal dihapus!"));
			}
		}
    }
}
