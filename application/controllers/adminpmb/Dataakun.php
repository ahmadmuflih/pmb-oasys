<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataakun extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'31');
		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dataakun_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	function json() {
		$this->load->library('datatables');
		$this->load->model(array('Camaba'));
	header('Content-Type: application/json');
	echo $this->Camaba->json();
	}
	function getCamaba(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Camaba'));
			$id = $_POST['id'];
			$data = $this->Camaba->get(array('id'=>$id));
			$camaba = null;
			if($data->num_rows()!=0){
				$camaba = $data->row_array();
			}
			echo json_encode(array('status'=>'berhasil','message'=>"Periode PMB berhasil diaktifkan!", 'data'=>$camaba));
		}
	}
	function update(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Camaba','AkunCamaba'));
			$camaba = array(
				'nama' => $_POST['nama'],
				'no_telp' => $_POST['no_telp'],
				'email' => $_POST['email'],
				'id_batch' => $_POST['id_batch']
			);
			$param = array('id'=>$_POST['id']);
			$cek = $this->Camaba->get(array('email'=>$camaba['email']));
			if($cek->num_rows() > 0 && $cek->row()->id!=$param['id']){
				echo json_encode(array('status'=>'gagal','message'=>'Email telah diregistrasikan!'));
			}
			else{
				$this->Camaba->update($param,$camaba);
				echo json_encode(array('status'=>'berhasil','message'=>"Data berhasil diupdate"));
			}
		}
	}
	function delete(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('AkunCamaba'));
			$id = $this->input->post('id');
			$cek_status = $this->AkunCamaba->delete(array('id'=>$id));
			if($cek_status){
				echo json_encode(array('status'=>'berhasil','message'=>"Akun berhasil dihapus!"));
			}
			else{
				echo json_encode(array('status'=>'gagal','message'=>"Akun gagal dihapus!"));
			}
		}
	}

	function resetPassword(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Camaba','AkunCamaba'));
			$param = array('id'=>$_POST['id']);
			$cek = $this->Camaba->get($param);
			if($cek->num_rows() > 0){
				$data = $cek->row();
				
				$akun = $this->AkunCamaba->get(array('id'=>$data->id_akun))->row();
				$newAkun = array('password'=>password_hash($akun->username, PASSWORD_BCRYPT));
				$this->AkunCamaba->update(array('id'=>$akun->id),$newAkun);
				echo json_encode(array('status'=>'berhasil','message'=>'Password Berhasil direset!'));
			}
			else{
				echo json_encode(array('status'=>'gagal','message'=>"Gagal"));
			}
		}
	}
}
