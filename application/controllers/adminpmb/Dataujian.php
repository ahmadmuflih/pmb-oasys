<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataujian extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'33');
			$this->load->model(array('Gedung'));
			$data['gedung'] = $this->Gedung->get(null);
		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dataujian_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}

	function insert(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			//print_r($_POST);
			foreach($_POST as $data){
				if(trim($data)==''){
					echo json_encode(array('status'=>'gagal','message'=>"Isi data dengan lengkap!"));
					return;
				}
			}
			if(!isset($_POST['id_ruangan'])){
				echo json_encode(array('status'=>'gagal','message'=>"Pilih ruangan terlebih dahulu!"));
				return;
			}
			// $data = $_POST;
			$data = array(
				'no_ujian' => $_POST['no_ujian'],
				'tanggal_ujian' => date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_ujian']))))),
				'jam_mulai' => $_POST['jam_mulai'],
				'jam_selesai' => $_POST['jam_selesai'],
				'id_ruangan' => $_POST['id_ruangan'],
				'id_batch' => $_POST['id_batch']
			);
			if($data['jam_mulai'] >= $data['jam_selesai']){
				echo json_encode(array('status'=>'gagal','message'=>"Periksa waktu ujian!"));
				return;
			}

			$this->load->model('Ujian');
			$cek = $this->Ujian->get(array('no_ujian'=>$data['no_ujian'],'id_batch'=>$data['id_batch']));
			if($cek->num_rows()>0){
				echo json_encode(array('status'=>'gagal','message'=>"No ujian sudah ada!"));
				return;
			}
			$this->Ujian->insert($data);
			echo json_encode(array('status'=>'berhasil','message'=>"Berhasil menambahkan data!"));
			return;
		}
	}

	function update(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			//print_r($_POST);
			foreach($_POST as $data){
				if(trim($data)==''){
					echo json_encode(array('status'=>'gagal','message'=>"Isi data dengan lengkap!"));
					return;
				}
			}
			if(!isset($_POST['id_ruangan'])){
				echo json_encode(array('status'=>'gagal','message'=>"Pilih ruangan terlebih dahulu!"));
				return;
			}
			// $data = $_POST;
			$data = array(
				'no_ujian' => $_POST['no_ujian'],
				'tanggal_ujian' => date('Y-m-d',strtotime(implode("-", array_reverse(explode("/", $_POST['tanggal_ujian']))))),
				'jam_mulai' => $_POST['jam_mulai'],
				'jam_selesai' => $_POST['jam_selesai'],
				'id_ruangan' => $_POST['id_ruangan'],
				'id_batch' => $_POST['id_batch']
			);
			if($data['jam_mulai'] >= $data['jam_selesai']){
				echo json_encode(array('status'=>'gagal','message'=>"Periksa waktu ujian!"));
				return;
			}

			$this->load->model('Ujian');
			$cek = $this->Ujian->get(array('no_ujian'=>$data['no_ujian'],'id_batch'=>$data['id_batch'],'id !='=>$_POST['id_ujian']));
			if($cek->num_rows()>0){
				echo json_encode(array('status'=>'gagal','message'=>"No ujian sudah ada!"));
				return;
			}
			$this->Ujian->update(array('id'=>$_POST['id_ujian']),$data);
			echo json_encode(array('status'=>'berhasil','message'=>"Berhasil mengupdate data!"));
			return;
		}
	}

	function getPeserta(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Ujian'));
			$id = $_POST['id'];
			$peserta = $this->Ujian->getPeserta($id);
			if($peserta->num_rows()==0){
				$camaba = "<tr><td colspan='5'><center>Tidak ada peserta</center></td></tr>";
			}
			else{
				$camaba = "";
				$i = 1;
				foreach($peserta->result() as $row){
					$camaba .= "<tr><td>$i</td><td>$row->no_registrasi</td><td>$row->nama</td><td>$row->pilihan1</td><td>$row->pilihan2</td></tr>";
					$i++;
				}
			}
			echo json_encode(array('status'=>'berhasil','message'=>"!", 'data'=>$camaba));
		}
	}

	function getUjian(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Ujian','Ruangan'));
			$id = $_POST['id'];
			$data = $this->Ujian->get(array('id'=>$id));
			$ujian = null;
			if($data->num_rows()!=0){
				$ujian = $data->row_array();
				$ujian['tanggal_ujian'] = date("d/m/Y", strtotime($ujian['tanggal_ujian']));
				$ruangan = $this->Ruangan->get(array('id'=>$ujian['id_ruangan']));
				$ujian['id_gedung'] = 0;
				if($ruangan->num_rows()>0){
					$ujian['id_gedung'] = $ruangan->row()->id_gedung;
				}
				
			}
			echo json_encode(array('status'=>'berhasil','message'=>"Periode PMB berhasil diaktifkan!", 'data'=>$ujian));
		}
	}
	function delete(){
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Ujian'));
			$id = $this->input->post('id');
			$cek_status = $this->Ujian->get(array('id'=>$id));
			$cek = $this->Ujian->delete(array('id'=>$id));
			if($cek>0)
				echo json_encode(array('status'=>'berhasil','message'=>"Ujian berhasil dihapus!"));
			else
				echo json_encode(array('status'=>'gagal','message'=>"Ujian gagal dihapus!"));
		
		}
	}
	function sanitize_post($item, $key)
	{
		if($key != 'pilihan')
		$clean_values[$key] = trim($item);
		//optional further cleaning ex) htmlentities
	}
	function json() {
		$this->load->library('datatables');
		$this->load->model(array('Ujian'));
		header('Content-Type: application/json');
		echo $this->Ujian->json();
	}

	public function getRuangan(){
		$id_gedung = $_POST['id_gedung'];
		$this->load->model(array('Ruangan'));
		$ruangan = $this->Ruangan->get(array('id_gedung'=>$id_gedung,'id_status'=>1));
		$data = "<option value='0' selected disabled>-PILIH RUANGAN-</option>";;
		foreach ($ruangan->result() as $row) {
			$data.="<option value='$row->id'>$row->kode - $row->nama</option>";
		}
		echo json_encode(array('status'=>'berhasil','data'=>$data));
	}
}
