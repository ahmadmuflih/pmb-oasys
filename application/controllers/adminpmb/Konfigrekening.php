<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfigrekening extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$array=array('page'=>'61');
			$this->load->model(array('ConfigRekening'));
			$data['config'] = $this->ConfigRekening->get(null);	
			$this->load->view('header_v',$array);
			$this->load->view('adminpmb/konfigrekening_v',$data);
			$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	public function insert()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('ConfigRekening'));
			foreach($_POST as $name => $value){
				$this->ConfigRekening->update(array('nama'=>$name),array('value'=>$value));
			}
			echo json_encode(array('status'=>'berhasil','message'=>"Data berhasil diupdate"));
		}
	}
}
