<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Excel extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
    }



    public function importUjian(){
      $fileName = time().$_FILES['file']['name'];
      $config['upload_path'] = '././assets/'; //buat folder dengan nama assets di root folder
      $config['file_name'] = $fileName;
      $config['allowed_types'] = 'xls|xlsx|csv';
      $config['max_size'] = 10000;
      $this->load->library('upload');
      $this->upload->initialize($config);

      if(! $this->upload->do_upload('file') ){
      echo $this->upload->display_errors();
      return;
      }
      $media = $this->upload->data();
      $inputFileName = '././assets/'.$media['file_name'];
      $this->load->model(array('Ujian','Camaba'));

      $id_batch = $_POST['batch'];
      try {
              $inputFileType = IOFactory::identify($inputFileName);
              $objReader = IOFactory::createReader($inputFileType);
              $objReader->setReadDataOnly(true);
              $objPHPExcel = $objReader->load($inputFileName);
          } catch(Exception $e) {
              die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
          }

          $sheet = $objPHPExcel->getSheet(0);
          $highestRow = $sheet->getHighestRow();
          $highestColumn = $sheet->getHighestColumn();

          $count = array();
          $count['success'] = array();
          $count['failed'] = array();
          for ($row = 4; $row <= $highestRow; $row++){                  //  Read a row of data into an array
              $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                              NULL,
                                              TRUE,
                                              FALSE);
              if(is_null($rowData[0][1]) && is_null($rowData[0][2])){
                continue;
              }
              else if(is_null($rowData[0][2])){
                  array_push($count['failed'], $row);
                  continue;
              }
              $ujian = array(
                'no_ujian' => trim($rowData[0][1]),
                'id_batch' => $id_batch
              );
              $camaba = array(
                'no_registrasi' => trim($rowData[0][2]),
                'id_batch' => $id_batch
              );

              $cekUjian = $this->Ujian->get($ujian);
              $cekCamaba = $this->Camaba->get($camaba);
              if($cekCamaba->num_rows()==0){
                array_push($count['failed'], $row);
                continue;
              }
              $data['id_ujian'] = null;
              if($cekUjian->num_rows()>0){
                $data['id_ujian'] = $cekUjian->row()->id;
              }
              else if(trim($rowData[0][1])!=""){ // kalau no ujian tidak valid
                array_push($count['failed'], $row);
                continue;
              }
              $param['id'] = $cekCamaba->row()->id;
              $this->Camaba->update($param,$data);
              array_push($count['success'], $row);

          }
          unlink($inputFileName);
          $response = array();
          $response['success'] = "Insert Berhasil : ".count($count['success']);
          $response['failed'] = "Insert Gagal : ".count($count['failed']);
          if(count($count['failed'])>0){
              $response['failed'] .= " Line number : ".json_encode($count['failed']);
          }
          echo json_encode($response);
  }
  public function importKelulusan(){
    $fileName = time().$_FILES['file']['name'];
    $config['upload_path'] = '././assets/'; //buat folder dengan nama assets di root folder
    $config['file_name'] = $fileName;
    $config['allowed_types'] = 'xls|xlsx|csv';
    $config['max_size'] = 10000;
    $this->load->library('upload');
    $this->upload->initialize($config);

    if(! $this->upload->do_upload('file') ){
    echo $this->upload->display_errors();
    return;
    }
    $media = $this->upload->data();
    $inputFileName = '././assets/'.$media['file_name'];
    $this->load->model(array('Camaba','Pilihan'));

    $id_batch = $_POST['batch'];
    try {
            $inputFileType = IOFactory::identify($inputFileName);
            $objReader = IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $count = array();
        $count['success'] = array();
        $count['failed'] = array();
        for ($row = 4; $row <= $highestRow; $row++){                  //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                            NULL,
                                            TRUE,
                                            FALSE);
            if(is_null($rowData[0][2]) && is_null($rowData[0][6])){
              continue;
            }
            else if(is_null($rowData[0][2])){
                array_push($count['failed'], $row);
                continue;
            }
            $camaba = array(
              'no_registrasi' => trim($rowData[0][2]),
              'id_batch' => $id_batch
            );



            $cekCamaba = $this->Camaba->get($camaba);
            if($cekCamaba->num_rows()==0){
              array_push($count['failed'], $row);
              continue;
            }
            $pilihan_ke = $rowData[0][6];
            // echo "pilihan $pilihan_ke";
            if(is_null($pilihan_ke) || $pilihan_ke == '-'){
              $data['pilihan_lulus'] = null;
            }else{

              $pilihan = $this->Pilihan->get(array('id_camaba'=>$cekCamaba->row()->id,'no_pilihan'=>$pilihan_ke));
              if($pilihan->num_rows()==0){
                array_push($count['failed'], $row);
                continue;
              }else{

                $data['pilihan_lulus'] = $pilihan->row()->id_prodi;
              }
            }
            $param['id'] = $cekCamaba->row()->id;
            $this->Camaba->update($param,$data);
            array_push($count['success'], $row);

        }
        unlink($inputFileName);
        $response = array();
        $response['success'] = "Insert Berhasil : ".count($count['success']);
        $response['failed'] = "Insert Gagal : ".count($count['failed']);
        if(count($count['failed'])>0){
            $response['failed'] .= " Line number : ".json_encode($count['failed']);
        }
        echo json_encode($response);
}


    public function exportRegistrasi(){
      $cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
      $this->load->model(array('Pilihan'));
       $id_batch = $_GET['batch'];
       $prodi = $_GET['prodi'];
       $ambildata = $this->Pilihan->export_filter($id_batch,$prodi,-1);
       if(!is_null($ambildata)){
        $inputFileName = '././assets/files/excel/FormatRegistrasi.xlsx';
        $inputFileType = IOFactory::identify($inputFileName);
        $objReader = IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
          $baris  = 4;
          foreach ($ambildata as $frow){
            $styleArray = array(
            'font'  => array(
                'color' => array('rgb' => '000000'),
                'size'  => 12,
                'name'  => 'Calibri'
            ),
            'borders' => array(
              'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
              )
            )

          );
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();
            $objget->getStyle('A'.$baris.':N'.$baris)->applyFromArray($styleArray);
             //pemanggilan sesuaikan dengan nama kolom tabel
              $objset->setCellValue("A".$baris, ($baris-3)); //membaca data nama
              $objset->setCellValue("B".$baris, $frow->no_registrasi);
              $objset->setCellValue("C".$baris, $frow->nama); //membaca data alamat
              $objset->setCellValue("D".$baris, $frow->alamat);
              $objset->setCellValue("E".$baris, $frow->tempat_lahir); //membaca data alamat
              $objset->setCellValue("F".$baris, $frow->tanggal_lahir);
              $objset->setCellValue("G".$baris, $frow->provinsi);
              $objset->setCellValue("H".$baris, $frow->kabupaten);
              $objset->setCellValue("I".$baris, $frow->jenis_kelamin);
              $objset->setCellValue("J".$baris, $frow->agama);
              $objset->setCellValue("K".$baris, $frow->jenis);
              $objset->setCellValue("L".$baris, $frow->kewarganegaraan);
              $objset->setCellValue("M".$baris, $frow->pilihan1);
              $objset->setCellValue("N".$baris, $frow->pilihan2);
              $baris++;
          }


          /*check point*/

          // Read the existing excel file




          $title = "Data_Registrasi_".$_SESSION['batch']['nama'];
          $objPHPExcel->getActiveSheet()->setTitle($title);

          $objPHPExcel->setActiveSheetIndex(0);
          $filename = urlencode($title.".xlsx");

            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

          $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
          $objWriter->save('php://output');
        }
      }
  }
  public function exportUjian(){
    $cek = $this->session->userdata('status');
  if ($cek == 'adminpmb'){
    $this->load->model(array('Pilihan'));
    $id_batch = $_GET['batch'];
    $ambildata = $this->Pilihan->export($id_batch);
    if(!is_null($ambildata)){
      $inputFileName = '././assets/files/excel/FormatUjian.xlsx';
      $inputFileType = IOFactory::identify($inputFileName);
      $objReader = IOFactory::createReader($inputFileType);
      $objPHPExcel = $objReader->load($inputFileName);
        $baris  = 4;
        foreach ($ambildata as $frow){
          $styleArray = array(
          'font'  => array(
              'color' => array('rgb' => '000000'),
              'size'  => 12,
              'name'  => 'Calibri'
          ),
          'borders' => array(
            'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
            )
          )

        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
          $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
          $objget = $objPHPExcel->getActiveSheet();
          $objget->getStyle('A'.$baris.':F'.$baris)->applyFromArray($styleArray);
           //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, ($baris-3)); //membaca data nama
            $objset->setCellValue("B".$baris, $frow->no_ujian);
            $objset->setCellValue("C".$baris, $frow->no_registrasi); //membaca data alamat
            $objset->setCellValue("D".$baris, $frow->nama);
            $objset->setCellValue("E".$baris, $frow->pilihan1);
            $objset->setCellValue("F".$baris, $frow->pilihan2);
            $baris++;
        }


        /*check point*/

        // Read the existing excel file




        $title = "Data_Ujian_".$_SESSION['batch']['nama'];
        $objPHPExcel->getActiveSheet()->setTitle($title);

        $objPHPExcel->setActiveSheetIndex(0);
        $filename = urlencode($title.".xlsx");

          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache

        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
        // $objWriter->save('php://output');
        saveViaTempFile($objWriter);
      }
    }
}
public function exportKelulusan(){
  $cek = $this->session->userdata('status');
if ($cek == 'adminpmb'){
  $this->load->model(array('Pilihan'));
  $id_batch = $_GET['batch'];
  $status = $_GET['status'];
  $prodi = $_GET['prodi'];
  $ambildata = $this->Pilihan->export_filter($id_batch,$prodi,$status);
   if(!is_null($ambildata)){
    $inputFileName = '././assets/files/excel/FormatKelulusan.xlsx';
    $inputFileType = IOFactory::identify($inputFileName);
    $objReader = IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
      $baris  = 4;
      foreach ($ambildata as $frow){
        $styleArray = array(
        'font'  => array(
            'color' => array('rgb' => '000000'),
            'size'  => 12,
            'name'  => 'Calibri'
        ),
        'borders' => array(
          'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
          )
        )

      );
      $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
        $objget = $objPHPExcel->getActiveSheet();
        $objget->getStyle('A'.$baris.':H'.$baris)->applyFromArray($styleArray);
         //pemanggilan sesuaikan dengan nama kolom tabel
          $objset->setCellValue("A".$baris, ($baris-3)); //membaca data nama
          $objset->setCellValue("B".$baris, $frow->no_ujian);
          $objset->setCellValue("C".$baris, $frow->no_registrasi); //membaca data alamat
          $objset->setCellValue("D".$baris, $frow->nama);
          $objset->setCellValue("E".$baris, $frow->pilihan1);
          $objset->setCellValue("F".$baris, $frow->pilihan2);
          if(is_null($frow->pilihan_lulus)){
            $objset->setCellValue("G".$baris, "-");
            $objset->setCellValue("H".$baris, "TIDAK LULUS");
          }
          else{
            $objset->setCellValue("G".$baris, $frow->pilihan_lulus);
            $objset->setCellValue("H".$baris, "LULUS");
          }
          $baris++;
      }


      /*check point*/

      // Read the existing excel file




      $title = "Data_Kelulusan_".$_SESSION['batch']['nama'];
      $objPHPExcel->getActiveSheet()->setTitle($title);

      $objPHPExcel->setActiveSheetIndex(0);
      $filename = urlencode($title.".xlsx");

        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

      $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
      // $objWriter->save('php://output');
      saveViaTempFile($objWriter);
    }
}
}
public function exportHeregistrasi(){
        $this->load->model(array('Camaba'));
         $id_batch = $_SESSION['batch']['id'];
         $ambildata = $this->Camaba->export($id_batch);
         $count = count($ambildata);

        if(count($ambildata)>0){
          $inputFileName = '././assets/files/excel/FormatMahasiswa.xlsx';
          $inputFileType = IOFactory::identify($inputFileName);
          $objReader = IOFactory::createReader($inputFileType);
          $objPHPExcel = $objReader->load($inputFileName);
            $baris  = 2;
            foreach ($ambildata as $frow){
              $styleArray = array(
              'font'  => array(
                  'color' => array('rgb' => '000000'),
                  'size'  => 8,
                  'name'  => 'Arial'
              ),
              'borders' => array(
                'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
                )
              )

            );
              $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
              $objget = $objPHPExcel->getActiveSheet();
              $objget->getStyle('A'.$baris.':AT'.$baris)->applyFromArray($styleArray);
               //pemanggilan sesuaikan dengan nama kolom tabel
               $objPHPExcel->getActiveSheet()->getStyle("A".$baris)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
               $objPHPExcel->getActiveSheet()->getStyle("F".$baris)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
                $objset->setCellValue("A".$baris, ""); //nik
                $objset->setCellValue("B".$baris, $frow->nama);
                $objset->setCellValue("C".$baris, $frow->tempat_lahir); //membaca data alamat
                $objset->setCellValue("D".$baris, $frow->tanggal_lahir);
                $objset->setCellValue("E".$baris, $frow->jenis_kelamin); //membaca data alamat
                $objset->setCellValue("F".$baris, $frow->nik);
                $objset->setCellValue("G".$baris, $frow->agama);
                $objset->setCellValue("H".$baris, $frow->nisn);
                $objset->setCellValue("I".$baris, "5"); // jalur masuk
                $objset->setCellValue("J".$baris, $frow->npwp);
                $objset->setCellValue("K".$baris, $frow->kewarganegaraan);
                $objset->setCellValue("L".$baris, $frow->id_tipe_pendaftaran);
                $objset->setCellValue("M".$baris, ""); // tgl masuk kuliah
                $objset->setCellValue("N".$baris, $frow->kode);
                $objset->setCellValue("O".$baris, $frow->jalan);
                $objset->setCellValue("P".$baris, $frow->rt);
                $objset->setCellValue("Q".$baris, $frow->rw);
                $objset->setCellValue("R".$baris, $frow->dusun);
                $objset->setCellValue("S".$baris, $frow->kelurahan);
                $objset->setCellValue("T".$baris, $frow->kecamatan);
                $objset->setCellValue("U".$baris, $frow->kode_pos);
                $objset->setCellValue("V".$baris, $frow->jenis_tinggal);
                $objset->setCellValue("W".$baris, $frow->alat_transportasi);
                $objset->setCellValue("X".$baris, ""); // telp rumah
                $objset->setCellValue("Y".$baris, $frow->no_telp);
                $objset->setCellValue("Z".$baris, $frow->email);
                $objset->setCellValue("AA".$baris, $frow->terima_kps);
                $objset->setCellValue("AB".$baris, $frow->no_kps);
                $objset->setCellValue("AC".$baris, $frow->nik_ayah);
                $objset->setCellValue("AD".$baris, $frow->nama_ayah);
                $objset->setCellValue("AE".$baris, $frow->tgl_lahir_ayah);
                $objset->setCellValue("AF".$baris, $frow->pendidikan_ayah);
                $objset->setCellValue("AG".$baris, $frow->pekerjaan_ayah);
                $objset->setCellValue("AH".$baris, $frow->penghasilan_ayah);
                $objset->setCellValue("AI".$baris, ""); //nik ibu
                $objset->setCellValue("AJ".$baris, $frow->nama_ibu);
                $objset->setCellValue("AK".$baris, $frow->tgl_lahir_ibu);
                $objset->setCellValue("AL".$baris, $frow->pendidikan_ibu);
                $objset->setCellValue("AM".$baris, $frow->pekerjaan_ibu);
                $objset->setCellValue("AN".$baris, $frow->penghasilan_ibu);
                $objset->setCellValue("AO".$baris, $frow->nama_wali);
                $objset->setCellValue("AP".$baris, $frow->tgl_lahir_wali);
                $objset->setCellValue("AQ".$baris, $frow->pendidikan_wali);
                $objset->setCellValue("AR".$baris, $frow->pekerjaan_wali);
                $objset->setCellValue("AS".$baris, $frow->penghasilan_wali);
                $objset->setCellValue("AT".$baris, $frow->prodi);
                $baris++;
                // echo $frow->nama;
            }

            /*check point*/

            // Read the existing excel file


            $title = 'Data_Mahasiswa';

            $objPHPExcel->getActiveSheet()->setTitle($title);

            $objPHPExcel->setActiveSheetIndex(0);
            $filename = urlencode($title.".xlsx");
            //
              header('Content-Type: application/vnd.ms-excel'); //mime type
              header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
              header('Cache-Control: max-age=0'); //no cache
            //
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
        }else{
            echo "<script>alert('Tidak ada data!');</script>";
        }
    }


}

function saveViaTempFile($objWriter){
  $filePath = '' . rand(0, getrandmax()) . rand(0, getrandmax()) . ".tmp";
  $objWriter->save($filePath);
  readfile($filePath);
  unlink($filePath);
  exit;
}