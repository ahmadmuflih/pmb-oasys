<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataseleksi extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('status');
		if ($cek == 'adminpmb'){
			$this->load->model(array('Fakultas','Jurusan'));
		$fakultas = $this->Fakultas->get(null);
		$jurusan = "<option value='0'>-PILIH JURUSAN-</option>";
		$i = 1;
		$j = 1;
		foreach ($fakultas->result() as $row) {
			$jurusan .= "<optgroup label='".$this->numberToRomanRepresentation($j).". FAKULTAS $row->nama'>";
			$jrs = $this->Jurusan->getJenjangJurusan(array('id_fakultas'=>$row->id));
			foreach ($jrs->result() as $row2) {
				$jurusan .= "<option value='$row2->id'>$i. $row2->jenjang - $row2->nama</option>";
				$i++;
			}
			$j++;
			$jurusan .= "</optgroup>";
		}
		$data['jurusan_filter'] = $jurusan;
			$array=array('page'=>'4');
		$this->load->view('header_v',$array);
		$this->load->view('adminpmb/dataseleksi_v',$data);
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
	function numberToRomanRepresentation($number) {
		$map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
		$returnValue = '';
		while ($number > 0) {
			foreach ($map as $roman => $int) {
				if($number >= $int) {
					$number -= $int;
					$returnValue .= $roman;
					break;
				}
			}
		}
		return $returnValue;
	}
	function json() {
		$this->load->library('datatables');
		$id_prodi = $_POST['id_prodi'];
		$status = $_POST['status'];
		$this->load->model(array('Pilihan'));
		header('Content-Type: application/json');
		echo $this->Pilihan->json_lulus($id_prodi,$status);
	}
}
