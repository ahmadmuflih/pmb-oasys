<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswarekomendasi extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('username');
		if ($cek == 'sekolah'){
		$this->load->view('header_v');
		$this->load->view('sekolah/siswarekomendasi_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
}
