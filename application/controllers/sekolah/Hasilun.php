<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasilun extends CI_Controller {

	public function index()
	{
		$cek = $this->session->userdata('username');
		if ($cek == 'sekolah'){
		$this->load->view('header_v');
		$this->load->view('sekolah/hasilun_v');
		$this->load->view('footer_v');
		}else{
			header("location:".base_url());
		}
	}
}
