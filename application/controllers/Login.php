<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
	parent::__construct();
		// session_start();
		$this->load->model('M_login');
	}
	public function index(){
		$this->load->library('session');
		$cek = $this->session->userdata('status');
		if(empty($cek)) {
			$this->load->model(array('Batch'));
			$data['batch'] = $this->Batch->getWithTahun(array('b.aktif_registrasi'=>1));
			$this->load->view('headerhome_v',$data);
			$this->load->view('login_v');
		}else{
			$st = $this->session->userdata('status');
			if ($st == 'adminpmb')
				header("location:".base_url().'adminpmb/home');
			else if($st == 'camaba')
				header("location:".base_url().'camaba/home');

			else
				header('location:'.base_url());
		}

	}
	function login(){
		$username = $_POST['username'];
		$password = $_POST['password'];
		$ip = $_POST['ip'];
		$browser = $_POST['browser'];

		if(trim($username)=='' || $password==''){
			echo json_encode(array('status'=>'gagal','message'=>'Data tidak boleh kosong!'));
		}
		else{
			$this->load->model(array('LoginModel'));
			$result = $this->LoginModel->login($username,$password,$ip,$browser);
			//echo "Result : $result";
			if($result==1){
				reset($_SESSION['roles']);
				$first_key = key($_SESSION['roles']);

				ob_start();
				$this->selectRole($first_key,null);
				ob_end_clean();
				// //print_r($_SESSION);
				echo json_encode(array('status'=>'berhasil','message'=>'Berhasil login'));
			}
			else if($result==0){
				echo json_encode(array('status'=>'welcome','message'=>'Ubah password'));
			}
			else if($result==-1){
				echo json_encode(array('status'=>'reset','message'=>'Reset akun'));
			}
			else{
				echo json_encode(array('status'=>'gagal','message'=>'Username atau password salah!'));
			}
		}
	}

	public function registsekolah()
	{
		$cek = $this->session->userdata('username');
		if (empty($cek)){
		$this->load->view('headerhome_v');
		$this->load->view('registsekolah_v');
		}else{
			header("location:".base_url());
		}
	}

	public function registmandiri()
	{
		$cek = $this->session->userdata('username');
		if (empty($cek)){
		$this->load->view('headerhome_v');
		$this->load->view('registmandiri_v');
		}else{
			header("location:".base_url());
		}
	}
	function selectRole($kode,$id){
		$_SESSION['data'] = $_SESSION['roles'][$kode]['data'];
		$_SESSION['status'] = $_SESSION['roles'][$kode]['status'];
		$_SESSION['status_name'] = $_SESSION['roles'][$kode]['status_name'];
		if(is_null($id))
		{
			$list = $_SESSION['roles'][$kode]['data']['list'];
			if(!is_null($list))
				$id=0;
		}
		if(!is_null($id)){
			if(isset($_SESSION['data']['list'][$id]))
			foreach ($_SESSION['data']['list'][$id] as $key => $value) {
				$_SESSION['data'][$key] = $value;
			}
		}
		$_SESSION['data']['id_list'] = $id;


		echo json_encode(array('status'=>'berhasil','message'=>'Berhasil mengganti role'));
	}

	function register(){

		$this->load->model(array('Camaba','AkunCamaba'));
		if(!isset($_POST['id_batch'])){
			echo json_encode(array('status'=>'gagal','message'=>'Tidak ada periode registrasi yang terbuka!'));
			return;
		}
		$camaba = array(
			'nama' => $_POST['nama'],
			'no_telp' => $_POST['no_telp'],
			'email' => $_POST['email'],
			'id_batch' => $_POST['id_batch']
		);
		$kodeA = array('0002','0006','0007');
		$kodeB = array('0001','0003','0004','0005','0008');
		$cek = $this->Camaba->get(array('email'=>$camaba['email'],'id_batch'=>$camaba['id_batch']));
		if($cek->num_rows() > 0){
			echo json_encode(array('status'=>'gagal','message'=>'Email telah diregistrasikan!'));
		}
		else{
			$password = $this->randString(6);
			$akun = array(
				'username' => $this->randString(8,'0123456789'),
				'password' => password_hash($password, PASSWORD_BCRYPT)
			);
			$id_akun = $this->AkunCamaba->insert($akun);
			$camaba['id_akun'] = $id_akun;
			$camaba['no_registrasi'] = "-";
			$this->Camaba->insert($camaba);
			ob_start();
			?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<style>
	/* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
	margin: 0;
	padding: 0;
	font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
	box-sizing: border-box;
	font-size: 14px;
}

img {
	max-width: 100%;
}

body {
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: none;
	width: 100% !important;
	height: 100%;
	line-height: 1.6;
}

/* Let's make sure all tables have defaults */
table td {
	vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
	background-color: #f6f6f6;
}

.body-wrap {
	background-color: #f6f6f6;
	width: 100%;
}

.container {
	display: block !important;
	max-width: 600px !important;
	margin: 0 auto !important;
	/* makes it centered */
	clear: both !important;
}

.content {
	max-width: 600px;
	margin: 0 auto;
	display: block;
	padding: 20px;
}

/* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
.main {
	background: #fff;
	border: 1px solid #e9e9e9;
	border-radius: 3px;
}

.content-wrap {
	padding: 20px;
}

.content-block {
	padding: 0 0 20px;
}

.header {
	width: 100%;
	margin-bottom: 20px;
}

.footer {
	width: 100%;
	clear: both;
	color: #999;
	padding: 20px;
}

.footer a {
	color: #999;
}

.footer p,
.footer a,
.footer unsubscribe,
.footer td {
	font-size: 12px;
}

/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1,
h2,
h3 {
	font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
	color: #000;
	margin: 40px 0 0;
	line-height: 1.2;
	font-weight: 400;
}

h1 {
	font-size: 32px;
	font-weight: 500;
}

h2 {
	font-size: 24px;
}

h3 {
	font-size: 18px;
}

h4 {
	font-size: 14px;
	font-weight: 600;
}

p,
ul,
ol {
	margin-bottom: 10px;
	font-weight: normal;
}

p li,
ul li,
ol li {
	margin-left: 5px;
	list-style-position: inside;
}

/* -------------------------------------
    LINKS & BUTTONS
------------------------------------- */
a {
	color: #1ab394;
	text-decoration: underline;
}

.btn-primary {
	text-decoration: none;
	color: #FFF;
	background-color: #1ab394;
	border: solid #1ab394;
	border-width: 5px 10px;
	line-height: 2;
	font-weight: bold;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 5px;
	text-transform: capitalize;
}

/* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
.last {
	margin-bottom: 0;
}

.first {
	margin-top: 0;
}

.aligncenter {
	text-align: center;
}

.alignright {
	text-align: right;
}

.alignleft {
	text-align: left;
}

.clear {
	clear: both;
}

/* -------------------------------------
    ALERTS
    Change the class depending on warning email, good email or bad email
------------------------------------- */
.alert {
	font-size: 16px;
	color: #fff;
	font-weight: 500;
	padding: 20px;
	text-align: center;
	border-radius: 3px 3px 0 0;
}

.alert a {
	color: #fff;
	text-decoration: none;
	font-weight: 500;
	font-size: 16px;
}

.alert.alert-warning {
	background: #f8ac59;
}

.alert.alert-bad {
	background: #ed5565;
}

.alert.alert-good {
	background: #1ab394;
}

/* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
.invoice {
	margin: 40px auto;
	text-align: left;
	width: 80%;
}

.invoice td {
	padding: 5px 0;
}

.invoice .invoice-items {
	width: 100%;
}

.invoice .invoice-items td {
	border-top: #eee 1px solid;
}

.invoice .invoice-items .total td {
	border-top: 2px solid #333;
	border-bottom: 2px solid #333;
	font-weight: 700;
}

/* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (max-width: 640px) {

	h1,
	h2,
	h3,
	h4 {
		font-weight: 600 !important;
		margin: 20px 0 5px !important;
	}

	h1 {
		font-size: 22px !important;
	}

	h2 {
		font-size: 18px !important;
	}

	h3 {
		font-size: 16px !important;
	}

	.container {
		width: 100% !important;
	}

	.content,
	.content-wrap {
		padding: 10px !important;
	}

	.invoice {
		width: 100% !important;
	}
}
.btn-whatever,
.btn-whatever:active,
.btn-whatever:visited,
.btn-whatever:focus {
	background-color: #FFF !important;
	border-color: #8064A2 !important;
	color: black !important;
}

.btn-whatever:hover {
	background-color: #8064A2 !important;
	color: white !important;
}


.form-whatever:focus {
	border-color: #8064A2 !important;
}

.nav-whatever,
.nav-whatever:hover,
.nav-whatever:active {
	color: #8064A2 !important;
}

.full-tabs {
	width: 50% !important;
	text-align: center !important;
}

.btn-email {
	text-decoration: none;
	color: #FFF;
	background-color: #1347ad;
	border: solid #1347ad;
	border-width: 5px 10px;
	line-height: 2;
	font-weight: bold;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 5px;
	text-transform: capitalize;
}

.btn-email:hover {
	text-decoration: none;
	color: #FFF;
	background-color: rgb(11, 40, 99) !important;
	border: solid rgb(11, 40, 99);
	border-width: 5px 10px;
	line-height: 2;
	font-weight: bold;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 5px;
	text-transform: capitalize;
}

	</style>
</head>

<body>

	<table class="body-wrap">
		<tr>
			<td></td>
			<td class="container" width="600">
				<div class="content">
					<table class="main" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="content-wrap">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<img class="img-responsive" src="https://i.imgur.com/WuDKsWA.png" />
										</td>
									</tr>
									<tr>
										<td class="content-block">
											<center>
												<h3>Terima kasih telah melakukan pendaftaran online pada OASYS</h3>
											</center>
										</td>
									</tr>
									<tr>
										<td class="content-block">
											Berikut data username dan password anda:
										</td>
									</tr>
									<tr>
										<td width="10%" class="content-block">
											<table cellpadding="0" cellspacing="0" width="100%">
												<tr>
													<td width="40%">Username</td>
													<td>:</td>
													<td><b><?php echo $akun['username'] ?></b></td>
												</tr>
												<tr>
													<td width="40%">Password</td>
													<td>:</td>
													<td><b><?php echo $password ?></b></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td class="content-block">
											Silahkan melakukan login sebagai pendaftar calon mahasiswa baru dengan menggunakan username dan password
											di atas.
										</td>
									</tr>
									<tr>
										<td class="content-block aligncenter">
											<a href="<?php echo base_url() ?>" class="btn-primary btn-email" style="color:#fff !important;">Klik untuk Login</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</td>
			<td></td>
		</tr>
	</table>

</body>

</html>


			<?php
			$message = ob_get_clean();
			$this->load->library('emailer');
			$this->emailer->setReceipent($camaba['email']);
			$this->emailer->setSubject("Info Registrasi OASYS");
			$this->emailer->setMessage($message);
			$this->emailer->send();

			$msg_email = "\nUsername : ".$akun['username']."\nPassword\t\t : ".$password;
			echo json_encode(array('status'=>'berhasil','message'=>'Anda berhasil registrasi! Silahkan cek email anda!'));
		}

	}
	function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
	{
		$str = '';
		$count = strlen($charset);
		while ($length--) {
			$str .= $charset[mt_rand(0, $count-1)];
		}
		return $str;
	}
}
