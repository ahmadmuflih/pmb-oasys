<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ujian extends CI_Model {
  private $tableujian;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tableujian = 't_ujian_pmb';
      $this->db->query("set lc_time_names='id_ID'");
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tableujian, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tableujian, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tableujian, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tableujian);
      }
      public function getLengkap($parameterfilter=array()){
        $this->db->select("u.*, DATE_FORMAT(u.tanggal_ujian, '%W %e %M %Y') as tanggal, r.nama as ruangan, g.nama as gedung");
        $this->db->from($this->tableujian." u");
        $this->db->join('t_ruangan r', 'r.id = u.id_ruangan');
        $this->db->join('t_gedung g','g.id = r.id_gedung');
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get();
      }

      function getPeserta($id_ujian){
        $this->db->select("c.id, c.no_registrasi,c.nama,c.id_status as status,
        (select concat(je.nama,' ', j.nama) from t_pilihan_prodi p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 1) as pilihan1,
        (select concat(je.nama,' ', j.nama) from t_pilihan_prodi p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 2) as pilihan2");
          $this->db->from('t_camaba c');
          $this->db->where('c.id_ujian',$id_ujian);
          return $this->db->get();
      }

      function json() {
        $this->datatables->select("u.id, u.no_ujian, DATE_FORMAT(u.tanggal_ujian, '%W %e %M %Y') as tanggal, concat(r.kode,' - ',r.nama) as ruangan, concat(jam_mulai,' - ',jam_selesai) as jam,(select count(*) from t_camaba c where c.id_ujian = u.id ) as peserta");
        $this->datatables->from($this->tableujian.' u');
        $this->datatables->join('t_ruangan r','u.id_ruangan = r.id','left');
        $this->datatables->where('u.id_batch',$_SESSION['batch']['id']);
        $this->datatables->add_column('view', "<center>
        <button type='button' onclick='peserta($1)' class='btn btn-info btn-xs' title='Data Peserta Ujian'><span class='fa fa-users'></span></button>
        <button type='button' onclick='edit($1)' class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></button>
        <button type='button' onclick='hapus($1)' class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></button>
        </center>", 'id');
         return $this->datatables->generate();
    }
      
}
