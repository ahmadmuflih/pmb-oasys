<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tahun extends CI_Model {
  private $tabletahun;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tabletahun = 't_tahun_pmb';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tabletahun, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tabletahun, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tabletahun, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tabletahun);
      }
      function json() {
          $this->datatables->select("t.id, t.kode, concat(t.tahun_awal,'/',t.tahun_akhir) as periode, DATE_FORMAT(t.tanggal_mulai, '%d/%m/%Y') as tanggal_mulai, DATE_FORMAT(t.tanggal_selesai, '%d/%m/%Y') as tanggal_selesai, s.nama as status");
          $this->datatables->from($this->tabletahun.' t');
          $this->datatables->join('t_status_user s','t.status = s.id');
          // $this->datatables->add_column('view', "<center><button class='btn btn-success btn-xs' onclick='setAktif($1)' title='Set Aktif'><span class='glyphicon glyphicon-check'></span></button>
          //   <button type='button' onclick='updateJadwal($1)' class='btn btn-warning btn-xs' title='Edit Periode' ><span class='fa fa-calendar'></span></button> 
          //   <button class='btn btn-danger btn-xs' title='Hapus Semester' onclick='hapus($1)'><span class='fa fa-remove'></span></button></center>", 'id');
            $this->datatables->add_column('view', "<center>
            
            <button class='btn btn-danger btn-xs' title='Hapus Periode' onclick='hapus($1)'><span class='fa fa-remove'></span></button></center>", 'id');
          return $this->datatables->generate();
      }
}
