<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class BiayaPendaftaran extends CI_Model {
  private $tablebiaya;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tablebiaya = 't_biaya_pendaftaran';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tablebiaya, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tablebiaya, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tablebiaya, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tablebiaya);
      }

        // function json() {
        //     $this->datatables->select('g.id, g.kode, (select count(*) from t_ruangan where id_camaba=g.id) as jumlah,g.nama');
        //     $this->datatables->from($this->tablebiaya.' g');
        //     $this->datatables->where('g.id_status',1);
        //       $this->datatables->add_column('view', "<center><button class='btn btn-success btn-xs' title='Edit camaba' onclick='edit($1)'><span class='glyphicon glyphicon-edit'></span></button>
        //     <button class='btn btn-danger btn-xs' title='Hapus camaba' onclick='hapus($1)''><span class='glyphicon glyphicon-trash'></span></button></center>", 'id');
        //     return $this->datatables->generate();
        // }
}
