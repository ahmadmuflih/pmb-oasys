<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Camaba extends CI_Model {
  private $tablecamaba;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tablecamaba = 't_camaba';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tablecamaba, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tablecamaba, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tablecamaba, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tablecamaba);
      }
      public function getWhere($tablename,$parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($tablename);
      }
      public function getLastId($kode){
        return $this->db->query("SELECT right(max(no_registrasi),4) as kode from t_camaba where LEFT(no_registrasi, 1) = '$kode'");
      }
      function json() {
            $this->datatables->select('c.id, a.id as id_akun, c.no_registrasi,c.nama, c.no_telp, c.alamat,c.email');
            $this->datatables->from($this->tablecamaba.' c');
            $this->datatables->join('t_akun_pmb a','c.id_akun = a.id');
            $this->datatables->where('c.id_batch',$_SESSION['batch']['id']);
              $this->datatables->add_column('view', "<center>
              <button type='button' class='btn btn-warning btn-xs' title='Edit Data' onclick='edit($1)'><span class='glyphicon glyphicon-edit'></span></button>
                                            <button type='button' class='btn btn-danger btn-xs' title='Hapus Data' onclick='hapus($2)'><span class='glyphicon glyphicon-remove'></span></button>
              </center>", 'id, id_akun');
            return $this->datatables->generate();
        }
        public function json_heregistrasi(){
          $this->datatables->select("c.id, c.no_stambuk,c.nama,c.no_telp,c.email, t.tipe as jenis");
            $this->datatables->from('t_camaba c');
            $this->datatables->join('t_tipe_pendaftaran t','t.id = c.id_tipe_pendaftaran','left');
            $this->datatables->where('c.id_batch',$_SESSION['batch']['id']);
            $this->datatables->where("c.id_status = 3 AND c.pilihan_lulus IS NOT NULL");
            $url = base_url()."adminpmb/heregistrasi/edit/";
            $this->datatables->add_column('view', "<center><a href='$url$1' target='_blank' class='btn btn-warning btn-xs' title='Edit heregistrasi' ><span class='glyphicon glyphicon-edit'></span></a></center>", 'id');
            return $this->datatables->generate();
        }
        function export($id_batch){
        $this->db->select("c.*,j.uuid as prodi, concat(t.kode,'1') as kode");
        $this->db->from($this->tablecamaba.' c');
        $this->db->join('t_jurusan j',"c.pilihan_lulus = j.id");
        $this->db->join('t_batch_pmb b',"c.id_batch = b.id");
        $this->db->join('t_tahun_pmb t',"b.id_tahun = t.id");
        $this->db->where("c.id_batch =  $id_batch and c.id_status = 3");
        $query = $this->db->get();
        if($query->num_rows() > 0){
              foreach($query->result() as $data){
                  $hasil[] = $data;
              }
              return $hasil;
          }
          return [];
      }
        // function json() {
        //     $this->datatables->select('g.id, g.kode, (select count(*) from t_ruangan where id_camaba=g.id) as jumlah,g.nama');
        //     $this->datatables->from($this->tablecamaba.' g');
        //     $this->datatables->where('g.id_status',1);
        //       $this->datatables->add_column('view', "<center><button class='btn btn-success btn-xs' title='Edit camaba' onclick='edit($1)'><span class='glyphicon glyphicon-edit'></span></button>
        //     <button class='btn btn-danger btn-xs' title='Hapus camaba' onclick='hapus($1)''><span class='glyphicon glyphicon-trash'></span></button></center>", 'id');
        //     return $this->datatables->generate();
        // }
}
