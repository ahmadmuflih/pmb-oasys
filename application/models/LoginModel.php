<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class LoginModel extends CI_Model {
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tableakun = 't_akun';
      $this->tableakuncamaba = 't_akun_pmb';
      $this->tablemahasiswa = 't_mahasiswa';
      $this->tablecamaba = 't_camaba';
      $this->tablepegawai = 't_pegawai';
      $this->tabledosen = 't_dosen';
      $this->tableadmin = 't_admin';
      $this->tableroleadmin = 't_role_admin';
    }
    public function login($usr, $pwd,$ip,$browser){
      $usr = trim($usr);
      $usr = $this->db->escape_str($usr);
      $cek_login =  $this->db->get_where($this->tableakun,array('username' => $usr));
      $cek = $cek_login->num_rows();


      if($cek>0){
        $akun = $cek_login->row();
        $history = array(
          'id_akun' => $akun->id,
          'browser' => $browser,
          'ip_address' => $ip
        );
        if (password_verify($pwd, $akun->password) && $akun->id_status==1) {
          $history['id_status'] = 1;
          $this->db->insert('t_riwayat_login', $history);
          $sess = array(
            'id' => $akun->id,
            'username' => $akun->username
          );

          $pgw = $this->cekPegawai(array('id_akun'=>$akun->id));
          if($pgw->num_rows()>0){

            $pgw = $pgw->row();
            $sess['foto'] = $pgw->foto;
            $foto = $pgw->foto;
            $nama = $pgw->nama;
            $sess['email'] = $pgw->email;
            $this->load->model(array('Role','Batch'));


            $admin = $this->cekAdmin(array('id_pegawai'=>$pgw->id));
            if($admin->num_rows()>0){
              $admin = $admin->row();
              $roles = $this->Role->getRoleAdmin($pgw->id);
              if($roles->num_rows()>0){
                foreach ($roles->result() as $row) {
                  if($row->kode == 'smb'){
                    $sess['roles'][$row->kode]['data'] = array();
                    $sess['roles'][$row->kode]['data']['tipe'] = $row->nama;
                    $sess['roles'][$row->kode]['data']['list'] = null;
                    $sess['roles'][$row->kode]['status'] = 'adminpmb';
                    $sess['roles'][$row->kode]['status_name'] = 'Admin';
                    $sess['roles'][$row->kode]['data']['id'] = $admin->id;
                    $sess['roles'][$row->kode]['data']['id_pegawai'] = $pgw->id;
                    $sess['roles'][$row->kode]['data']['nip'] = $pgw->nip;
                    $sess['roles'][$row->kode]['data']['nama'] = $pgw->nama;
                    $sess['batch'] = $this->Batch->getWithTahun(array('b.status'=>1))->row_array();
                    $this->session->set_userdata($sess);
                    return 1;
                  }
                }
              }
            }
            return -2;
          }

          return -4;
        }
        else{
          $history['id_status'] = 0;
          $this->db->insert('t_riwayat_login', $history);
          return -5;
        }
        }

        $cek_login =  $this->db->get_where($this->tableakuncamaba,array('username' => $usr));
        $cek = $cek_login->num_rows();

        if($cek>0){
          $akun = $cek_login->row();

          if (password_verify($pwd, $akun->password) && $akun->id_status==1) {
            $sess = array(
              'id' => $akun->id,
              'username' => $akun->username
            );
            $this->load->model(array('Role','Batch'));

              $mhs = $this->cekCamaba(array('id_akun'=>$akun->id));
              if($mhs->num_rows()>0){

                $mhs = $mhs->row();
                $sess['foto'] = $mhs->foto;
                //$foto = $mhs->foto;
                $sess['email'] = $mhs->email;
                $sess['roles']['camaba'] = array();
                $sess['roles']['camaba']['status'] = 'camaba';
                $sess['roles']['camaba']['status_name'] = 'Camaba';
                $sess['roles']['camaba']['data']['id'] = $mhs->id;
                $sess['roles']['camaba']['data']['tipe'] = 'Mahasiswa';
                $sess['roles']['camaba']['data']['list'] = null;
                $sess['roles']['camaba']['data']['no_registrasi'] = $mhs->no_registrasi;
                $sess['roles']['camaba']['data']['nama'] = $mhs->nama;
                $sess['roles']['camaba']['data']['pilihan_lulus'] = $mhs->pilihan_lulus;

                //$sess['roles']['camaba']['data']['id_prodi'] = $mhs->id_prodi;
                $nama = $mhs->nama;
                $sess['batch'] = $this->Batch->getWithTahun(array('b.id'=>$mhs->id_batch))->row_array();
                $this->session->set_userdata($sess);
                return 1;
              }
              return -2;

          }

        }
        return -3;
      }


    private function cekPegawai($parameter = array()){
      $this->db->select('p.id,p.nip,p.nama, p.foto, p.email');
      $this->db->from($this->tablepegawai.' p');
      $parameter['id_status'] = 1;
      $this->db->where($parameter);
      return $this->db->get();
    }
    private function cekCamaba($parameter = array()){
      $this->db->select('c.*');
      $this->db->from($this->tablecamaba.' c');
      // $parameter['id_status'] = 1;
      $this->db->where($parameter);
      return $this->db->get();
    }
    // private function cekDosen($parameter = array()){
    //   $this->db->select('p.id,d.id as id_dosen,p.nip,p.nama');
    //   $this->db->from($this->tablepegawai.' p');
    //   $this->db->join($this->tabledosen.' d','p.id = d.id_pegawai');
    //   $this->db->where($parameter);
    //   return $this->db->get();
    // }
    // private function cekDosen($parameter = array()){
    //   $this->db->select('id');
    //   $this->db->from($this->tabledosen);
    //   $parameter['id_status'] = 1;
    //   $this->db->where($parameter);
    //   return $this->db->get();
    // }

    // private function cekDekan($parameter = array()){
    //   $this->db->select('id');
    //   $this->db->from('t_dekan');
    //   $parameter['id_status'] = 1;
    //   $this->db->where($parameter);
    //   return $this->db->get();
    // }
    // private function cekKaprodi($parameter = array()){
    //   $this->db->select('id');
    //   $this->db->from('t_kaprodi');
    //   $parameter['id_status'] = 1;
    //   $this->db->where($parameter);
    //   return $this->db->get();
    // }
    // // private function cekRektor($parameter = array()){
    // //   $this->db->select('p.id,d.id as id_rektor,p.nip,p.nama');
    // //   $this->db->from($this->tablepegawai.' p');
    // //   $this->db->join('t_rektor d','p.id = d.id_pegawai');
    // //   //$parameter['d.id_status'] = 1;
    // //   $this->db->where($parameter);
    // //   return $this->db->get();
    // // }
    // private function cekRektor($parameter = array()){
    //   $this->db->select('id');
    //   $this->db->from('t_rektor');
    //   $parameter['id_status'] = 1;
    //   $this->db->where($parameter);
    //   return $this->db->get();
    // }

    // // private function cekAdmin($parameter = array()){
    // //   $this->db->select('p.id,a.id as id_admin,p.nip,p.nama');
    // //   $this->db->from($this->tablepegawai.' p');
    // //   $this->db->join($this->tableadmin.' a','p.id = a.id_pegawai');
    // //   $this->db->join('t_role_admin tr','a.id = tr.id_admin');
    // //   $parameter['tr.id_role'] = 1;
    // //   $parameter['tr.id_status'] = 1;
    // //   $this->db->where($parameter);
    // //   return $this->db->get();
    // // }
    private function cekAdmin($parameter = array()){
      $this->db->select('id');
      $this->db->from($this->tableadmin);
      $parameter['id_status'] = 1;
      $this->db->where($parameter);
      return $this->db->get();
    }
    // private function cekAdminFakultas($parameter = array()){
    //     $this->db->select('af.id,af.id_fakultas, f.nama as fakultas');
    //     $this->db->from('t_admin_fakultas af');
    //     $this->db->join('t_fakultas f','af.id_fakultas = f.id');
    //     $this->db->where($parameter);
    //     return $this->db->get();
    // }
    // private function getDekanFakultas($parameter = array()){
    //     $this->db->select('rd.*, f.nama as fakultas');
    //     $this->db->from('t_role_dekan rd');
    //     $this->db->join('t_fakultas f','rd.id_fakultas = f.id');
    //     $this->db->where($parameter);
    //     return $this->db->get();
    // }
    // private function getKaprodi($parameter = array()){
    //     $this->db->select('rk.*, j.nama as jurusan');
    //     $this->db->from('t_role_kaprodi rk');
    //     $this->db->join('t_jurusan j','rk.id_jurusan = j.id');
    //     $this->db->where($parameter);
    //     return $this->db->get();
    // }
    // private function cekAdminJurusan($parameter = array()){
    //     $this->db->select('aj.id,aj.id_jurusan, j.nama as jurusan');
    //     $this->db->from('t_admin_jurusan aj');
    //     $this->db->join('t_jurusan j','aj.id_jurusan = j.id');
    //     $this->db->where($parameter);
    //     return $this->db->get();
    // }
    // function json_riwayat($id_akun) {
    //     $this->datatables->select('r.id, r.ip_address, r.browser, r.waktu, s.nama as status');
    //     $this->datatables->from('t_riwayat_login r');
    //     $this->datatables->join('t_status_login s','r.id_status=s.id');
    //     $this->datatables->where("r.id_akun =  $id_akun");
    //     $this->db->order_by("r.waktu","desc");
    //     return $this->datatables->generate();
    // }
    // public function getRiwayat($parameterfilter=array()){
    //   $this->db->select("*, DATE_FORMAT(waktu,'%d %b %Y %H:%i:%s') as waktu2");
    //   $this->db->from('t_riwayat_login');
    //   if($parameterfilter!=null)
    //     $this->db->where($parameterfilter);
    //   $this->db->order_by("waktu","desc");
    //   return $this->db->get();
    // }
}
