<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Batch extends CI_Model {
  private $tablebatch;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tablebatch = 't_batch_pmb';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tablebatch, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tablebatch, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tablebatch, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tablebatch);
      }
      public function getWithTahun($parameterfilter=array()){
        $this->db->select("b.*, concat(t.tahun_awal,'/',t.tahun_akhir) as periode");
        $this->db->from($this->tablebatch." b");
        $this->db->join("t_tahun_pmb t","t.id = b.id_tahun");
        $this->db->order_by("tahun_awal,b.id","desc");
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get();
      }
      function json($id_periode) {
          $this->datatables->select("t.id, t.nama,  DATE_FORMAT(t.tanggal_mulai, '%d/%m/%Y') as tanggal_mulai, DATE_FORMAT(t.tanggal_selesai, '%d/%m/%Y') as tanggal_selesai, s.nama as status");
          $this->datatables->from($this->tablebatch.' t');
          $this->datatables->join('t_status_user s','t.status = s.id');
          $this->datatables->where('t.id_tahun',$id_periode);
          $this->datatables->add_column('view', "<center><button class='btn btn-success btn-xs' onclick='setAktifBatch($1)' title='Set Aktif'><span class='glyphicon glyphicon-check'></span></button>
            <button type='button' onclick='updateBatch($1)' class='btn btn-warning btn-xs' title='Edit Batch' ><span class='fa fa-calendar'></span></button> 
            <button class='btn btn-danger btn-xs' title='Hapus Batch' onclick='hapusBatch($1)'><span class='fa fa-remove'></span></button></center>", 'id');
            // $this->datatables->add_column('view', "<center>
            
            // <button class='btn btn-danger btn-xs' title='Hapus Semester' onclick='hapus($1)'><span class='fa fa-remove'></span></button></center>", 'id');
          return $this->datatables->generate();
      }
}
