<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pilihan extends CI_Model {
  private $tablepilihan;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tablepilihan = 't_pilihan_prodi';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tablepilihan, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tablepilihan, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tablepilihan, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tablepilihan);
      }
      public function getWhere($tablename,$parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($tablename);
      }

      public function export($id_batch){
        $this->db->select("c.id, c.no_registrasi,c.nama,c.alamat,c.tempat_lahir,c.tanggal_lahir,u.no_ujian,
        k.nama as kabupaten,p.nama as provinsi,c.jenis_kelamin, a.nama as agama,c.kewarganegaraan, c.id_status as status, t.tipe as jenis, (select no_pilihan from $this->tablepilihan pl where pl.id_camaba=c.id and pl.id_prodi=c.pilihan_lulus) pilihan_lulus,
        (select concat(je.nama,' ', j.nama) from $this->tablepilihan p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 1) as pilihan1,
        (select concat(je.nama,' ', j.nama) from $this->tablepilihan p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 2) as pilihan2");
          $this->db->from('t_camaba c');
          $this->db->join('t_tipe_pendaftaran t','t.id = c.id_tipe_pendaftaran','left');
          $this->db->join('t_agama a','a.id = c.agama','left');
          $this->db->join('t_kabupaten k','k.id = c.id_kabupaten','left');
          $this->db->join('t_provinsi p','p.id = k.id_provinsi','left');
          $this->db->join('t_ujian_pmb u','u.id = c.id_ujian','left');
          $this->db->where(array('c.id_batch'=>$id_batch,'c.id_status'=>3));
          $query = $this->db->get();

          if($query->num_rows() > 0){
              foreach($query->result() as $data){
                  $hasil[] = $data;
              }
              return $hasil;
          }
          return null;
      }
      public function export_filter($id_batch,$id_prodi,$status){
        $this->db->select("c.id, c.no_registrasi,c.nama,c.alamat,c.tempat_lahir,c.tanggal_lahir,u.no_ujian,
        k.nama as kabupaten,p.nama as provinsi,c.jenis_kelamin, a.nama as agama,c.kewarganegaraan, c.id_status as status, t.tipe as jenis, (select no_pilihan from $this->tablepilihan pl where pl.id_camaba=c.id and pl.id_prodi=c.pilihan_lulus) pilihan_lulus,
        (select concat(je.nama,' ', j.nama) from $this->tablepilihan p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 1) as pilihan1,
        (select concat(je.nama,' ', j.nama) from $this->tablepilihan p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 2) as pilihan2");
          $this->db->from('t_camaba c');
          $this->db->join('t_tipe_pendaftaran t','t.id = c.id_tipe_pendaftaran','left');
          $this->db->join('t_agama a','a.id = c.agama','left');
          $this->db->join('t_kabupaten k','k.id = c.id_kabupaten','left');
          $this->db->join('t_provinsi p','p.id = k.id_provinsi','left');
          $this->db->join('t_ujian_pmb u','u.id = c.id_ujian','left');
          $this->db->where(array('c.id_batch'=>$id_batch,'c.id_status'=>3));
          if($id_prodi!='0'){
            $this->db->join($this->tablepilihan." pl",'c.id = pl.id_camaba');
            $this->db->where('pl.id_prodi',$id_prodi);
          }
          if($status == '1'){
            $this->db->where('c.pilihan_lulus is not null');
          }
          else if($status == '0'){
            $this->db->where('c.pilihan_lulus is null');
          }

          $query = $this->db->get();

          if($query->num_rows() > 0){
              foreach($query->result() as $data){
                  $hasil[] = $data;
              }
              return $hasil;
          }
          return null;
      }
      public function getPilihan($id_camaba,$pilihan){
        $this->db->select("p.id, concat(je.nama, ' ', j.nama) as prodi, f.nama as fakultas");
          $this->db->from('t_pilihan_prodi p');
          $this->db->join('t_camaba c','c.id = p.id_camaba');
          $this->db->join('t_jurusan j','j.id = p.id_prodi');
          $this->db->join('t_jenjang_jurusan je','je.id = j.id_jenjang');
          $this->db->join('t_fakultas f','f.id = j.id_fakultas');
          $this->db->where(array('p.id_camaba'=>$id_camaba,'no_pilihan'=>$pilihan));
          return $this->db->get();
      }
      public function json($id_prodi,$type,$status){
        $this->datatables->select("c.id, c.no_registrasi,c.nama,c.id_status as status, t.tipe as jenis, pilihan_lulus,
        (select concat(je.nama,' ', j.nama) from $this->tablepilihan p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 1) as pilihan1,
        (select concat(je.nama,' ', j.nama) from $this->tablepilihan p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 2) as pilihan2,
        (select concat(je.nama,' ', j.nama) from t_jurusan j join t_jenjang_jurusan je on je.id = j.id_jenjang where j.id = c.pilihan_lulus) as lulus");
          $this->datatables->from('t_camaba c');
          $this->datatables->join('t_tipe_pendaftaran t','t.id = c.id_tipe_pendaftaran','left');
          $this->datatables->where('c.id_batch',$_SESSION['batch']['id']);
          if($status!='-1'){
            $this->datatables->where("c.id_status",$status);
          }
          else if($type=='1'){
            $this->datatables->where("(c.id_status = 0 or c.id_status = 1)");
          }
          else if($type=='2'){
            $this->datatables->where("(c.id_status = 2 or c.id_status = 3)");
          }


          if($id_prodi!='0'){
            $this->datatables->join($this->tablepilihan." p",'c.id = p.id_camaba');
            $this->datatables->where('p.id_prodi',$id_prodi);
          }

          $this->datatables->add_column('check', "<center><input type='checkbox' value='$1' name='checkstatus'></input></center>", 'id');
          $url = base_url()."adminpmb/dataregistrasi/edit/";
          if($type == '1'){
            $this->datatables->add_column('view', "<center>
            <a href='$url$1' title='Edit Data' target='_blank' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-edit'></span></a>
            </center>", 'id,no_registrasi');

          }
          else{
            $this->datatables->add_column('view', "<center>
            <a href='$url$1' title='Edit Data' target='_blank' class='btn btn-warning btn-xs'><span class='glyphicon glyphicon-edit'></span></a>
            <button type='button' onclick='reset($1)' class='btn btn-danger btn-xs' title='Reset Data'><span class='glyphicon glyphicon-remove'></span></button>
            </center>", 'id,no_registrasi');
          }
          return $this->datatables->generate();
      }
      public function json_lulus($id_prodi,$status){
        $this->datatables->select("c.id, c.no_registrasi,c.nama,c.id_status as status, t.tipe as jenis, pilihan_lulus,
        (select concat(je.nama,' ', j.nama) from $this->tablepilihan p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 1) as pilihan1,
        (select concat(je.nama,' ', j.nama) from $this->tablepilihan p join t_jurusan j on j.id = p.id_prodi join t_jenjang_jurusan je on je.id = j.id_jenjang where p.id_camaba = c.id and no_pilihan = 2) as pilihan2,
        (select concat(je.nama,' ', j.nama) from t_jurusan j join t_jenjang_jurusan je on je.id = j.id_jenjang where j.id = c.pilihan_lulus) as lulus");
          $this->datatables->from('t_camaba c');
          $this->datatables->join('t_tipe_pendaftaran t','t.id = c.id_tipe_pendaftaran','left');
          $this->datatables->where('c.id_batch',$_SESSION['batch']['id']);
          $this->datatables->where("c.id_status = 3");
          if($id_prodi!='0'){
            $this->datatables->join($this->tablepilihan." p",'c.id = p.id_camaba');
            $this->datatables->where('p.id_prodi',$id_prodi);
          }
          if($status == '1'){
            $this->datatables->where('c.pilihan_lulus is not null');
          }
          else if($status == '0'){
            $this->datatables->where('c.pilihan_lulus is null');
          }
          return $this->datatables->generate();
      }
}
