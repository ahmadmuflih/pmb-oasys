<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ConfigRekening extends CI_Model {
  private $tableconfig;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tableconfig = 't_config_rekening_pmb';
    }
    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tableconfig, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tableconfig, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tableconfig, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tableconfig);
      }

}
