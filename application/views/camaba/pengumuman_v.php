
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Pengumuman</h2>
                </div>
                <!-- <div class="col-sm-8">
                    <div class="title-action">
                        <a href="<?php echo base_url()."adminpmb/dataregistrasi/add"?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div> -->
            </div>
            <style media="screen">
              .crop {
                width: 240px;
                height: 320px;
                overflow: hidden;
              }

              /* img {
                  max-width: 100%;
                  max-height: 100%;
              } */
            </style>
            <div class="wrapper wrapper-content">
            <div class="row">
              <!-- <div class="col-lg-12"> -->
                <div class="ibox-title">
                  <div class="row">
                    <!-- <div class="col-lg-12"> -->
                      <!-- <div class="form-group">
                        <label class="control-label">Filter Tujuan:</label>
                        <div>
                          <select id="tujuan" class="form-control">
                            <option value="0" selected>SEMUA</option>
                          </select>
                        </div>
                      </div> -->

                      <div class="col-lg-12">
                        <center>
                          <h4>PENGUMUMAN HASIL SELEKSI PMB UNIVERSITAS OASYS PERIODE <?php echo $_SESSION['batch']['periode']." - ".$_SESSION['batch']['nama'] ?></h4>
                        </center>
                      </div>
                      <br><br>
                      <?php if(is_null($camaba->pilihan_lulus)){ ?>
                      <!-- caption ditolak -->
                      <div class="col-lg-12">
                        <p>Berdasarkan keputusan dari pimpinan OASYS, telah ditetap daftar hasil seleksi calon mahasiswa(i) baru Universitas OASYS. Kami dari seluruh anggota civitas kampus mengucapkan mohon maaf anda <b>belum lulus</b> seleksi PMB dan terima kasih atas partisipasinya.</a></p>
                      </div>
                     
                      
                      <?php }else{
                        $prodi = $prodi->row();
                        ?>
                      <!-- end caption diterima -->
                      <div class="col-lg-12">
                        <p>Berdasarkan keputusan dari pimpinan Universitas OASYS, telah ditetap daftar hasil seleksi calon mahasiswa(i) baru OASYS. Kami dari seluruh anggota civitas kampus mengucapkan selamat anda dinyatakan <b>lulus</b> seleksi dan diterima pada <b><?php echo "FAKULTAS $prodi->fakultas / JURUSAN $prodi->jenjang $prodi->nama";  ?></b>. Silahkan melakukan pendaftaran ulang melalui menu Her Registrasi atau klik <a href="<?php echo base_url()."camaba/heregistrasi"?>">disini</a></p>
                      </div>
                      
                     
                    <!-- </div> -->
                  </div>
                </div>
                <div class="ibox-content">
                   <center>
                    <img class="crop" src="<?php echo base_url().$_SESSION['foto']?>" alt="foto_profil" width="100px"><br>
                    <h4>No. Reg: <?php echo $camaba->no_registrasi?></h4>
                    <h4><?php echo $camaba->nama?></h4>
                    <h4><?php 
                    
                    echo "FAKULTAS $prodi->fakultas / JURUSAN $prodi->jenjang $prodi->nama"; 
                    ?></h4>
                   </center>
                   
                    <!-- <center>
                      <button type="button" class="btn btn-md btn-primary" name="button"><i class="fa fa-send"></i> TERBITKAN</button>
                        <button type="button" class="btn btn-md btn-warning" name="button" onclick="window.histroy.back()"><i class="fa fa-reply"></i> KEMBALI</button>
                    </center> -->
                  </div>
                  <?php } ?>
              <!-- </div> -->
            </div>
          </div><br><br>
