<!-- content -->
<link href="<?php echo base_url()."assets" ?>/css/modal-img.css" rel="stylesheet">
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>Registrasi PMB</h2>
		<br>
		<center>
			<p>SELAMAT DATANG DI HALAMAN REGISTRASI PMB</p>
		</center>
		<p>Silahkan melakukan pengisian form registrasi di bawah ini, pastikan data yang telah diisi sesuai sebelum melakukan
			submit. Jika terdapat kendala atau permasalah silahkan hubungi bagian admin OASYS.</p>

	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-3">
			<div class="ibox-content">
				<center>
					<img id="display" src="<?php echo base_url().$_SESSION['foto']?>" style="width:200px" alt="foto_profil" />
				</center><br>
				<center>
					<form id="form_foto">
						<label title="Upload image file" for="inputImage" class="btn btn-sm btn-primary" style="color:white">
							<input type="file" accept="image/*" name="file" id="inputImage" class="hide"><i class="fa fa-camera"></i>
							Ubah Foto
						</label>
					</form>
				</center>

				<div <?php if($camaba->id_status != 1){ echo "class='hidden'";}?> >
				<hr>
				<p>Silahkan Melakukan Pembayaran Pendaftaran Melalui Rekening </p> <br>
				 <table>
				 	<tbody>
					 	<tr>
						 <?php
						 	$nama = ""; $norek = ""; $penerima="";
							foreach($config->result() as $row){
								if($row->nama == "nama")
									$nama = $row->value;
								else if($row->nama == "norek")
									$norek = $row->value;
								else if($row->nama == "penerima")
									$penerima = $row->value;
							}
						 ?>
						 <td>Nama Bank</td>
						 <td width="5%">:</td>
						 <td><b><?php echo $nama;?></b></td>
						</tr>
						<tr>
						 <td>No. Rekening</td>
						 <td>:</td>
						 <td><b><?php echo $norek;?></b></td>
						</tr>
						<tr>
						 <td>Penerima</td>
						 <td>:</td>
						 <td><b><?php echo $penerima;?></b></td>
						</tr>
						<tr>
						 <td>Jumlah Bayar</td>
						 <td>:</td>
						 <td><b id="jumlah_bayar">Rp 0</b></td>
						</tr>
					 </tbody>
				 </table> <br>
				<p> Lalu upload foto bukti pembayaran pendaftaran MABA <br> Silahkan <a onclick="scrollWin()">Klik disini</a></p>
				<!-- <button class="btn btn-info btn-sm" onclick="scrollWin()">KLIK TO UPLOAD</button> -->
				</div>
				<script>
					function scrollWin() {
						$('html, body').animate({
							scrollTop: '+=1000'
						}, 2000);
					}
				</script>
				<hr>
				<center>
					<p><b>STATUS PENGAJUAN</b></p>
					<?php
						if($status->id == 0 || $status->id == 1){
							$label = 'label-default';
						}
						else if($status->id == 2){
							$label = 'label-warning';
						}
						else{
							$label = 'label-primary';
						}

					?>
					<span class="label <?php echo $label?>"><span class="fa fa-pencil"></span> <?php echo $status->nama ?></span><br>
					<!-- <span class="label label-warning"><span class="fa fa-clock-o"></span> MENUNGGU ACC</span><br> -->
					<!-- <span class="label label-primary"><span class="fa fa-check"></span> TELAH ACC</span> -->
				</center>

				<?php if($batch->aktif_cetak_ujian==1 && $camaba->id_status==3){ ?>
				<hr>
				<center>
					<p><b>SILAHKAN MELAKUKAN CETAK KARTU TEST</b></p>
					<a href="<?php echo base_url()."camaba/registrasipmb/cetak" ?>" class="btn btn-sm btn-info"><i class="fa fa-print"></i> CETAK KARTU TEST</a>
				</center>
				<?php } ?>
			</div>
		</div>
		<div class="col-lg-9">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>No.Registrasi : <?php echo $camaba->no_registrasi;?></h5>
				</div>
				<form id="form" class="form-horizontal">
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-6">
								<h3>A. Identitas Diri</h3>
							</div>
							<div class="col-lg-12">
								<?php
									$st = "";
									if($camaba->id_status != 0){
										$st = "disabled";
									}
									$st_bukti="";
									if($camaba->id_status != 1){
										$st_bukti = "disabled";
									}
								?>
								<div class="form-group"><label class="col-lg-2 control-label">Nama</label>
									<div class="col-lg-10"><input type="text" name="nama" placeholder="Masukkan Nama" value="<?php echo $camaba->nama;?>" class="form-control" required <?php echo $st; ?>></div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Alamat</label>
									<div class="col-lg-10"><input type="text" name="alamat" placeholder="Masukkan Alamat" value="<?php echo $camaba->alamat?>" class="form-control" <?php echo $st; ?>></div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Tempat Lahir</label>
									<div class="col-lg-10"><input type="text" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" value="<?php echo $camaba->tempat_lahir?>" class="form-control" <?php echo $st; ?>></div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Tanggal Lahir</label>
									<div class="col-lg-10"><input type="text" name="tanggal_lahir" id="datepicker" placeholder="Masukkan Tanggal Lahir" value="<?php echo date("d/m/Y", strtotime($camaba->tanggal_lahir));?>" class="form-control"
									<?php echo $st; ?>></div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Provinsi</label>
									<div class="col-lg-10">
										<select class="form-control" id="provinsi" <?php echo $st; ?>>
											<option value="0" selected disabled>-PILIH Provinsi-</option>
											<?php foreach ($provinsi->result() as $row){
													if($row->id==$id_provinsi){
														echo "<option value='$row->id' selected>$row->nama</option>";
													}
													else{
														echo "<option value='$row->id'>$row->nama</option>";
													}


											} ?>
										</select>
									</div>
								</div>

								<div class="form-group"><label class="col-lg-2 control-label">Kab/Kota</label>
									<div class="col-lg-10">

										<select class="form-control" id="kabupaten" name="id_kabupaten" <?php echo $st; ?>>
											<?php echo $kabupaten ?> 
										</select>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Jenis Kelamin</label>
									<div class="col-lg-10">
										<select class="form-control" id="jenis_kelamin" name="jenis_kelamin" <?php echo $st; ?>>
											<option value="0" selected disabled>-PILIH JENIS KELAMIN-</option>
											<?php foreach ($jenis_kelamin->result() as $row){
												if($row->id == $camaba->jenis_kelamin){
													echo "<option value='$row->id' selected>$row->nama</option>";
												}
												else{
													echo "<option value='$row->id'>$row->nama</option>";
												}
											} ?>
										</select>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Agama</label>
									<div class="col-lg-10">
										<select class="form-control" name="agama" <?php echo $st; ?>>
											<option value="0" selected disabled>-PILIH AGAMA-</option>
											<?php foreach ($agama->result() as $row){
												if($row->id == $camaba->agama){
													echo "<option value='$row->id' selected>$row->nama</option>";
												}
												else{
													echo "<option value='$row->id'>$row->nama</option>";
												}
											} ?>
										</select>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Jenis Pendaftar</label>
									<div class="col-lg-10">
										<select class="form-control" name="id_tipe_pendaftaran" id="status" <?php echo $st; ?>>
											<option value="0" selected disabled>-PILIH JENIS PENDAFTAR-</option>
											<?php foreach ($tipe->result() as $row){
													echo "<option value='$row->id'>$row->tipe</option>";
											} ?>
										</select>
									</div>
								</div>
								<div class="hidden" id="baru">
									<div class="form-group"><label class="col-lg-2 control-label">Asal SLTA/SMA</label>
										<div class="col-lg-10"><input type="text" id="sma_asal" name="sma_asal" placeholder="Masukkan Asal SLTA/SMA dan Seerajat" value="<?php echo $camaba->sma_asal?>"  class="form-control"
										<?php echo $st; ?>></div>
									</div>
								</div>
								<div class="hidden pindahan">
									<div class="form-group"><label class="col-lg-2 control-label">Asal PTN/PTS</label>
										<div class="col-lg-10"><input type="text" id="pt_asal" name="pt_asal" placeholder="Masukkan Asal PTN/PTS" value="<?php echo $camaba->pt_asal?>"  class="form-control" <?php echo $st; ?>></div>
									</div>
									<div class="form-group"><label class="col-lg-2 control-label">Fakultas</label>
										<div class="col-lg-10"><input type="text" id="fakultas_asal" name="fakultas_asal" placeholder="Masukkan Asal Fakultas" value="<?php echo $camaba->fakultas_asal?>"  class="form-control" <?php echo $st; ?>></div>
									</div>
									<div class="form-group"><label class="col-lg-2 control-label">Jurusan/Prodi</label>
										<div class="col-lg-10"><input type="text" id="prodi_asal" name="prodi_asal" placeholder="Masukkan Asal Jurusan / Program Studi" value="<?php echo $camaba->prodi_asal?>"  class="form-control" <?php echo $st; ?>></div>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Kewarganegaraan</label>
									<div class="col-lg-10">
										<select class="form-control" name="kewarganegaraan" <?php echo $st; ?>>
											<option value="0" selected disabled>-PILIH KEWARGANEGARAAN-</option>
											<?php foreach ($kewarganegaraan->result() as $row){
												if($row->id == $camaba->kewarganegaraan){
													echo "<option value='$row->id' selected>$row->id</option>";
												}
												else{
													echo "<option value='$row->id'>$row->id</option>";
												}
											} ?>
										</select>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Bakat/Minat</label>
									<div class="col-lg-10"><input type="text" name="bakat" placeholder="Masukkan Minat/Bakat" class="form-control" value="<?php echo $camaba->bakat?>"  <?php echo $st; ?>></div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Prestasi</label>
									<div class="col-lg-10"><input type="text" name="prestasi" placeholder="Masukkan Prestasi yang pernah diraih" value="<?php echo $camaba->prestasi?>"  class="form-control"
									<?php echo $st; ?>></div>
								</div>
							</div>
							<div class="col-lg-6">
								<h3>B. Pilih Jurusan</h3>
							</div>
							<div class="col-lg-12">
								<div class="form-group"><label class="col-lg-2 control-label">Pilihan 1</label>
									<div class="col-lg-10">
										<select class="form-control" id="pilihan1" name="pilihan[1]" <?php echo $st; ?>>
											<!-- <optgroup label="I. FAKULTAS EKONOMI">
												<option value="1">1. S1 Manajemen</option>
											</optgroup>
											<optgroup label="II. FAKULTAS TEKNIK">
												<option value="2">2. S1 Teknik Sipil</option>
											</optgroup>
											<optgroup label="III. FKIP">
												<option value="3">3. S1 Pendidikan Bhs. Inggris</option>
												<option value="4">4. S1 Pendidikan Luar Sekolah</option>
												<option value="5">5. S1 PAUD</option>
											</optgroup>
											<optgroup label="IV. FAKULTAS HUKUM">
												<option value="6">6. S1 Ilmu-Ilmu Hukum</option>
											</optgroup>
											<optgroup label="V. FISIP">
												<option value="7">7. S1 Sosiologi</option>
												<option value="8">8. S1 Ilmu Administrasi Negara</option>
											</optgroup>
											<optgroup label="VI. FAKULTAS PERTANIAN">
												<option value="9">9. S1 Agribisnis</option>
												<option value="10">10. S1 Kehutanan</option>
											</optgroup>
											<optgroup label="VII. FAKULTAS KESEHATAN MASYARAKAT">
												<option value="11">11. S1 Ilmu Kesehatan Masyarakat</option>
											</optgroup>
											<optgroup label="VIII. FAKULTAS AGAMA ISLAM">
												<option value="12">12. S1 Dakwah/Penerangan Penyiaran Agama Islam</option>
												<option value="13">13. S1 Tarbiyah/Pendidikan Agama Islam</option>
												<option value="14">14. S1 Tarbiyah/Pendidikan Agama Islam</option>
												<option value="15">15. S1 PIAUD</option>
												<option value="16">16. S1 Hukum Ekonomi Syariah</option>
											</optgroup>
											<optgroup label="IX. PASCASARJANA">
												<option value="17">17. S2 Manajemen Pendidikan</option>
											</optgroup> -->
											<?php echo $jurusan_filter?>
										</select>
									</div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Pilihan 2</label>
									<div class="col-lg-10">
										<select class="form-control" id="pilihan2" name="pilihan[2]" <?php echo $st; ?>>
										<?php echo $jurusan_filter?>
										</select>
									</div>
								</div>
							</div>

							<style>
								tr.spaceUnder>td {
									padding-bottom: 1em;
								}
							</style>

							<div class="col-lg-6">
								<h3>C. Kelengkapan Berkas</h3>
							</div>
							<div class="col-lg-12">
								<table width="100%">
									<tbody>
									<tr class="spaceUnder">
										<td align="center">
										<!-- <img src="<?php echo base_url()."assets/img/profile3x4.jpg"?>" class="myImg" style="width:100px" alt="ijazah"> -->
										<span style="font-size:6em;" class="fa fa-file-pdf-o"></span><br><br>
										<?php
										$check = "";
										if($camaba->ijazah == ""){
											$check = 'disabled';
										}
										?>
										<button onclick="window.open('<?php echo base_url().$camaba->ijazah ?>')" target='_blank' type="button" class="btn btn-primary" <?php echo $check ?>><span class="fa fa-file-pdf-o"></span> Lihat File</button>
										</td>
										<td>
										<label for="">Upload Foto Ijazah PDF (Asli)</label>
										<input name="ijazah" type="file" placeholder="Masukkan foto ijazah" accept="application/pdf,image/x-png,image/gif,image/jpeg" class="form-control"
										<?php echo $st; ?>></td>
									</tr>
									<tr class="spaceUnder">
										<td align="center">
											<!-- <img src="<?php echo base_url()."assets/img/profile3x4.jpg"?>" class="myImg" style="width:100px" alt="KTP"> -->
										<span style="font-size:6em;" class="fa fa-file-pdf-o"></span><br><br>
										<?php
										$check = "";
										if($camaba->ktp == ""){
											$check = 'disabled';
										}
										?>
										<button onclick="window.open('<?php echo base_url().$camaba->ktp ?>')" target='_blank' type="button" class="btn btn-primary" <?php echo $check ?>><span class="fa fa-file-pdf-o"></span> Lihat File</button>
										</td>
										<td>
										<label for="">Upload Foto KTP / Keterangan Domisili PDF (Asli)</label>
										<input name="ktp" type="file" placeholder="Masukkan foto ktp" accept="application/pdf,image/x-png,image/gif,image/jpeg" class="form-control"
										<?php echo $st; ?>></td>
									</tr>
									<tr class="spaceUnder">
										<td align="center">
											<!-- <img src="<?php echo base_url()?>" class="myImg" style="width:100px" alt="Surat Kemigrasian"> -->
											<span style="font-size:6em;" class="fa fa-file-pdf-o"></span><br><br>
											<?php
											$check = "";
											if($camaba->surat_imigrasi == ""){
												$check = 'disabled';
											}
											?>
											<button onclick="window.open('<?php echo base_url().$camaba->surat_imigrasi ?>')" target='_blank' type="button" class="btn btn-primary" <?php echo $check ?>><span class="fa fa-file-pdf-o"></span> Lihat File</button>
											</td>
										<td>
										<label for="">Upload Foto Surat Kemigrasian PDF (Opsional)</label>
										<input name="surat_imigrasi" type="file" placeholder="Masukkan foto surat migrasi" accept="application/pdf,image/x-png,image/gif,image/jpeg" class="form-control"
										<?php echo $st; ?>>
										<small>*Khusus Pendaftar Asing</small>
										</td>
									</tr>

									<!-- additional -->
									<tr class="spaceUnder pindahan hidden">
										<td align="center">
											<!-- <img src="<?php echo base_url()?>" class="myImg" style="width:100px" alt="Surat Kemigrasian"> -->
											<span style="font-size:6em;" class="fa fa-file-pdf-o"></span><br><br>
											<?php
											$check = "";
											if($camaba->transkrip == ""){
												$check = 'disabled';
											}
											?>
											<button onclick="window.open('<?php echo base_url().$camaba->transkrip ?>')" target='_blank' type="button" class="btn btn-primary" <?php echo $check ?>><span class="fa fa-file-pdf-o"></span> Lihat File</button>
											</td>
										<td>
										<label for="">Upload Foto Transkrip Nilai PDF</label>
										<input name="transkrip" type="file" placeholder="Masukkan file transkrip nilai" accept="application/pdf,image/x-png,image/gif,image/jpeg" class="form-control"
										<?php echo $st; ?>>
										<small>*Khusus Jalur Pindahan/Alih Jenjang/Lintas Jalur</small>
										</td>
									</tr>
									<!-- end additional -->

									<?php if($camaba->id_status != 0){
										$bukti = "assets/img/noimage.jpg";
										if($camaba->bukti_pembayaran!=""){
											$bukti = $camaba->bukti_pembayaran;
										}
										?>
									<tr class="spaceUnder">
										<td align="center"><img src="<?php echo base_url().$bukti?>" class="myImg" style="width:100px" alt="Bukti Pembayaran PMB"></td>
										<td>
										<label for="">Upload Foto Bukti Pembayaran PMB (Asli)</label>
										<input name="bukti_pembayaran" type="file" placeholder="Masukkan foto pembayaran" accept="image/*" class="form-control"
										<?php echo $st_bukti; ?>></td>
									</tr>
									<?php } ?>
									</tbody>
								</table>
								<!-- <div class="form-group"><label class="col-lg-2 control-label">Upload Foto Ijazah</label>
									<div class="col-lg-10"><input type="file" placeholder="Masukkan foto ijazah" accept="image/*" class="form-control"
										 requied></div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Upload Foto KTP/Keterangan Domisili</label>
									<div class="col-lg-10"><input type="file" placeholder="Masukkan foto ktp" accept="image/*" class="form-control"
										 requied></div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Upload Foto Surat Kemigrasian (Mahasiswa Asing)</label>
									<div class="col-lg-10"><input type="file" placeholder="Masukkan foto migrasi" accept="image/*" class="form-control"
										 requied></div>
								</div>
								<div class="form-group"><label class="col-lg-2 control-label">Upload Foto Bukti Pembayaran Pendaftaran</label>
									<div class="col-lg-10"><input type="file" placeholder="Masukkan foto bukti pembayaran" accept="image/*" class="form-control"
										 requied></div>
								</div> -->
							</div>
							<div class="col-lg-12"></div>
						</div>
					</div>
					<style>
						.btn-success.active.focus, .btn-success.active:focus, .btn-success.active:hover, .btn-success:active.focus, .btn-success:active:focus, .btn-success:active:hover, .open>.dropdown-toggle.btn-success.focus, .open>.dropdown-toggle.btn-success:focus, .open>.dropdown-toggle.btn-success:hover {
							background-color:#3766b2 !important;
						}
					</style>
					<div class="ibox-footer">
						<center>
							<?php if($camaba->id_status == 1){ ?>
							<button id="button" onclick="simpan(3)" type="button" class="btn btn-md btn-primary" name="button"><i class="fa fa-send"></i>
								AJUKAN</button>
							<?php }?>
							<?php if($camaba->id_status == 0){ ?>
							<div class="btn-group">
								<button data-toggle="dropdown" class="btn btn-success btn-sc2 dropdown-toggle"><i class="fa fa-save"></i> SIMPAN <span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li onclick='simpan(1)'><a href="#">SIMPAN DATA</a></li>
									<li class="divider"></li>
									<li><a href="#" class="font-bold pembayaran">SIMPAN DAN LANJUT PEMBAYARAN</a></li>
								</ul>
							</div>
							<?php }?>
								<!-- <button id="button" type="button" class="btn btn-md btn-success" name="button"><i class="fa fa-save"></i>
								SIMPAN</button> -->
							<button type="button" class="btn btn-md btn-warning" name="button" onclick="window.history.back()"><i class="fa fa-reply"></i>
								KEMBALI</button>
						</center>
					</div>
				</form>
			</div>
		</div>
	</div>
	<br><br>
</div>

<!-- The Modal -->
<div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
            <div id="caption"></div>
          </div>

          <script>
            // Get the modal
              var modal = document.getElementById('myModal');

              // Get the image and insert it inside the modal - use its "alt" text as a caption
              var img = $('.myImg');
              var modalImg = $("#img01");
              var captionText = document.getElementById("caption");
              $('.myImg').click(function(){
                  modal.style.display = "block";
                  var newSrc = this.src;
                  modalImg.attr('src', newSrc);
                  captionText.innerHTML = this.alt;
              });

              // Get the <span> element that closes the modal
              var span = document.getElementsByClassName("close")[0];

              // When the user clicks on <span> (x), close the modal
              span.onclick = function() {
                modal.style.display = "none";
              }
          </script>

<script type="text/javascript">

	<?php
	if(!is_null($camaba->id_tipe_pendaftaran)){
		echo "$('#status').val($camaba->id_tipe_pendaftaran);";
	}
	if ($camaba->id_tipe_pendaftaran == 1): ?>
		$('#pt_asal').val('');
		$('#fakultas_asal').val('');
		$('#prodi_asal').val('');
		$('#baru').removeClass('hidden');
	<?php elseif($camaba->id_tipe_pendaftaran == 2): ?>
		$('#sma_asal').val('');
		$('.pindahan').removeClass('hidden');
	<?php endif; ?>
var status = $('#status').val(),prov,pilihan1=0,pilihan2=0;
	<?php
		foreach($pilihan as $ke => $id_prodi){
			echo "$('#pilihan$ke').val($id_prodi);\n";
			echo "pilihan$ke = $id_prodi;\n";
		}
	?>


	function readURL(input) {

   if (input.files && input.files[0]) {
       var reader = new FileReader();

       reader.onload = function (e) {
           $('#display').attr('src', e.target.result);
       }

       reader.readAsDataURL(input.files[0]);
   }
   else{
	alert('hahaha');
   }
}

$("#inputImage").change(function(){
   var dataFoto = this;
  var form = $('#form_foto')[0]; // You need to use standart javascript object here
  var formData = new FormData(form);
  $.ajax({
    url: '<?php echo base_url("camaba/Registrasipmb/updateFoto");?>',
    data: formData,
    type: 'POST',
    // THIS MUST BE DONE FOR FILE UPLOADING
    contentType: false,
    processData: false,
    dataType: "JSON",
    success: function(data){
      // alert(data);
      console.log(data);
      if (data.status=='berhasil') {
        swal("Berhasil!", data.message, "success");
		readURL(dataFoto);
      }else {
        swal("Gagal!", data.message, "error");
      }
    },
  error: function(jqXHR, textStatus, errorThrown)
  {
console.log(jqXHR);
console.log(textStatus);
console.log(errorThrown);
}
  })
        return false;
});

	$('#provinsi').change(function(){
	  prov = $(this).val();
	  $.ajax({
	       url : "<?php echo site_url('camaba/Registrasipmb/getKab')?>",
	       type: "POST",
	       data: {"id_provinsi":prov},
	       dataType: "JSON",
	       success: function(data)
	       {
	         if (data.status=='berhasil') {
	           $('#kabupaten').html(data.data);
	         }
	       },
	           error: function (jqXHR, textStatus, errorThrown)
	           {
	             console.log(jqXHR);
	       console.log(textStatus);
	       console.log(errorThrown);
	           }
	     });
	});
	$('#status').change(function () {
		status = $('#status').val();
		if (status == '1') {
			$('#baru').removeClass('hidden');
			$('.pindahan').addClass('hidden');
		} else {
			$('.pindahan').removeClass('hidden');
			$('#baru').addClass('hidden');
		}
		getBiaya();
	});
	$('#pilihan1').change(function(){
	  pilihan1 = $(this).val();
	  getBiaya();
	});
	$('#pilihan2').change(function(){
	  pilihan2 = $(this).val();
	  getBiaya();
	});
	getBiaya();
	function getBiaya(){
		var formData = new FormData();
		formData.append("tipe",status);
		formData.append("prodi[]",pilihan1);
		formData.append("prodi[]",pilihan2);
		$.ajax({
	       url : "<?php echo site_url('camaba/Registrasipmb/getBiaya')?>",
	       type: "POST",
	       data: formData,
	       dataType: "JSON",
		   contentType: false,
    	processData: false,
	       success: function(data)
	       {
			   console.log(data);
	         if (data.status=='berhasil') {
	           $('#jumlah_bayar').html('Rp. '+data.data.biaya);
	         }
	       },
	           error: function (jqXHR, textStatus, errorThrown)
	           {
	             console.log(jqXHR);
	       console.log(textStatus);
	       console.log(errorThrown);
	           }
	     });

	}

	function getBiaya(){
		var formData = new FormData();
		formData.append("tipe",status);
		formData.append("prodi[]",pilihan1);
		formData.append("prodi[]",pilihan2);
		$.ajax({
	       url : "<?php echo site_url('camaba/Registrasipmb/getBiaya')?>",
	       type: "POST",
	       data: formData,
	       dataType: "JSON",
		   contentType: false,
    	processData: false,
	       success: function(data)
	       {
			   console.log(data);
	         if (data.status=='berhasil') {
	           $('#jumlah_bayar').html('Rp. '+data.data.biaya);
	         }
	       },
	           error: function (jqXHR, textStatus, errorThrown)
	           {
	             console.log(jqXHR);
	       console.log(textStatus);
	       console.log(errorThrown);
	           }
	     });

	}

	function simpan(tipe){
		var form = $('#form')[0];
		var formData = new FormData(form);
		formData.append("tipe",tipe);
		$.ajax({
	       url : "<?php echo site_url('camaba/Registrasipmb/insert')?>",
	       type: "POST",
	       data: formData,
	       dataType: "JSON",
		   contentType: false,
    	processData: false,
	       success: function(data)
	       {
			if (data.status=='berhasil') {
                  swal("Berhasil!", data.message, "success");
                  location.reload();
                }else {
                  swal("Gagal!", data.message, "error");
                }
	       },
	           error: function (jqXHR, textStatus, errorThrown)
	           {
	             console.log(jqXHR);
	       console.log(textStatus);
	       console.log(errorThrown);
	           }
	     });
	}

</script>

<script type="text/javascript">
      function set(tipe){
      if(tipe == '-1'){
        $('#submit').removeClass('btn-primary');
        $('#submit').addClass('btn-default');
        $('#submit').addClass('disabled');
        $('#submit').html('<i class="fa fa-check"></i> Telah Submit');
      }
      else{
      }
    };
  </script>

  <script>
  $('.pembayaran').click(function () {
            swal({
                title: "PERHATIAN",
                text: "Dengan melanjutkan ke pembayaran, data registrasi PMB anda tidak bisa diubah. Anda yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4a8ad3",
                confirmButtonText: "Ya, Saya yakin!",
                closeOnConfirm: false
            }, function () {
                simpan(2);
            });
        });
  </script>
