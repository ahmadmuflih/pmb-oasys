
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-12">
                    <h2>Heregistrasi</h2>
                    <br>
                    <center>
                    <p>SELAMAT DATANG DI HALAMAN HEREGISTRASI</p>
                    </center>
                    <p>Silahkan melakukan pengisian form Herregistrasi di bawah ini, pastikan data yang telah diisi sesuai sebelum melakukan submit. Jika terdapat kendala atau permasalah silahkan hubungi bagian admin OASYS.</p>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="col-lg-3">
                <div class="ibox-content">
                <center>
                  <img id="display" src="<?php echo base_url().$_SESSION['foto']?>" style="width:200px" alt="foto_profil" />
                </center><br>
                <center>
                  <form id="form_foto">
                    <label title="Upload image file" for="inputImage" class="btn btn-sm btn-primary" style="color:white">
                      <input type="file" accept="image/*" name="file" id="inputImage" class="hide"><i class="fa fa-camera"></i>
                      Ubah Foto
                    </label>
                  </form>
                </center>
                  <hr>
                  <center>
                    <p><b>SILAHKAN MELAKUKAN CETAK HEREGISTRASI</b></p>
                    <a href="<?php echo base_url()."camaba/heregistrasi/cetak" ?>" class="btn btn-sm btn-info"><i class="fa fa-print"></i> CETAK HEREGISTRASI</a>
                  </center>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="tabs-container">
                       <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" href="#tab-1"> Identitas Diri</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-2">Data Alamat</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-3">Status Pendaftaran</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-4">Data Orang Tua/Wali</a></li>
                       </ul>
                       <form id="form" class="form-horizontal">
                       <div class="tab-content">
                           <div id="tab-1" class="tab-pane active">
                               <div class="panel-body">
                                   <div class="row">
                                     <div class="col-lg-12">
                                       <div class="form-group"><label class="col-lg-2 control-label">Nama</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama" class="form-control" name="nama" value="<?php echo $camaba->nama?>"></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Tempat Lahir</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Tempat Lahir" class="form-control" name="tempat_lahir" value="<?php echo $camaba->tempat_lahir?>"></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Tanggal Lahir</label>
                                           <div class="col-lg-10"><input type="text" id="datepicker" placeholder="Masukkan Tanggal Lahir" class="form-control" name="tanggal_lahir" value="<?php echo date("d/m/Y", strtotime($camaba->tanggal_lahir));?>"></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Jenis Kelamin</label>
                                           <div class="col-lg-10">
                                             <select class="form-control" name="jenis_kelamin" required id="jenis_kelamin">
                                               <option value="0" selected disabled>-PILIH JENIS KELAMIN-</option>
                                               <option value="L">LAKI-LAKI</option>
                                               <option value="P">PEREMPUAN</option>
                                             </select>
                                           </div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Agama</label>
                                           <div class="col-lg-10">
                                             <select class="form-control" name="agama" required>
                                               <option value="0" selected disabled>-PILIH AGAMA-</option>
                                               <?php
                                                foreach($agama->result() as $row){
                                                  if($row->id == $camaba->agama){
                                                    echo "<option value='$row->id' selected>$row->nama</option>";
                                                  }
                                                  else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                  }
                                                }
                                               ?>
                                             </select>
                                           </div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Hobi/Minat</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Hobi/Minat/Bakat/Prestasi" class="form-control" name="bakat" value="<?php echo $camaba->bakat?>"></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Prestasi</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Hobi/Minat/Bakat/Prestasi" class="form-control" name="prestasi" value="<?php echo $camaba->prestasi?>"></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Alamat Sekarang</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Alamat Sekarang" class="form-control" name="alamat" value="<?php echo $camaba->alamat?>"></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Pekerjaan/instansi</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Pekerjaan/Instansi" class="form-control" name="pekerjaan" value="<?php echo $camaba->pekerjaan?>"></div>
                                       </div>
                                     </div>
                                   </div>
                               </div>
                           </div>
                           <div id="tab-2" class="tab-pane">
                               <div class="panel-body">
                                  <div class="row">
                                    <div class="col-lg-12">
                                      <div class="form-group"><label class="col-lg-2 control-label">NIK</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan NIK" class="form-control" name="nik" value="<?php echo $camaba->nik?>">
                                          <small>*Nomor KTP Tanpa Tanda Baca, Isikan Nomor Paspor Untuk Warga Negara Asing</small></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">NISN</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan NISN" class="form-control" name="nisn" value="<?php echo $camaba->nisn?>"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">NPWP</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan NPWP" class="form-control" name="npwp" value="<?php echo $camaba->npwp?>"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Kewarganegaraan</label>
                                          <div class="col-lg-10">
                                            <select class="form-control" name="kewarganegaraan" required>
                                              <option value="0" selected disabled>-PILIH KEWARGANEGARAAN-</option>
                                              <?php
                                                foreach($kewarganegaraan->result() as $row){
                                                  if($row->id == $camaba->kewarganegaraan){
                                                    echo "<option value='$row->id' selected>$row->id</option>";
                                                  }
                                                  else{
                                                  echo "<option value='$row->id'>$row->id</option>";
                                                  }
                                                }
                                               ?>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Provinsi</label>
                                          <div class="col-lg-10">
                                            <select id="provinsi" class="form-control">
                                            <option value="0" selected disabled>-PILIH Provinsi-</option>
                                              <?php foreach ($provinsi->result() as $row){
                                                if($row->id==$id_provinsi){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }


                                            } ?>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Kota/Kabupaten</label>
                                          <div class="col-lg-10">
                                            <select name="id_kabupaten" id="kabupaten" class="form-control">
                                              <?php echo $kabupaten ?>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Kecamatan</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama Kecamatan" class="form-control" name="kecamatan" value="<?php echo $camaba->kecamatan?>"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Kelurahan</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama Kelurahan" class="form-control" name="kelurahan" value="<?php echo $camaba->kelurahan?>"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Jalan</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama Jalan" class="form-control" name="jalan" value="<?php echo $camaba->jalan?>"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Dusun/Desa</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama Dusun/Desa" class="form-control" name="dusun" value="<?php echo $camaba->dusun?>"></div>
                                      </div>
                                        <div class="form-group">
                                        <label class="col-lg-2 control-label">RT</label>
                                            <div class="col-lg-5"><input type="text" placeholder="Masukkan RT" class="form-control" name="rt" value="<?php echo $camaba->rt?>"></div>
                                        </div>
                                        <div class="form-group">
                                        <label class="col-lg-2 control-label">RW</label>
                                            <div class="col-lg-5"><input type="text" placeholder="Masukkan RW" class="form-control" name="rw" value="<?php echo $camaba->rw?>"></div>
                                        </div>
                                        <div class="form-group">
                                        <label class="col-lg-2 control-label">Kode POS</label>
                                            <div class="col-lg-5"><input type="text" placeholder="Masukkan Kode POS" class="form-control" name="kode_pos" value="<?php echo $camaba->kode_pos?>"></div>
                                        </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Jenis Tinggal</label>
                                          <div class="col-lg-10">
                                            <select class="form-control" name="jenis_tinggal" required>
                                              <option value="0" selected disabled>-PILIH JENIS TINGGAL-</option>
                                              <?php foreach ($jenis_tinggal->result() as $row){
                                                if($row->id==$camaba->id_jenis_tinggal){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }
                                            } ?>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Transportasi</label>
                                          <div class="col-lg-10">
                                            <select class="form-control" name="alat_transportasi" required>
                                              <option value="0" selected disabled>-PILIH TRANSPORTASI-</option>
                                              <?php foreach ($transportasi->result() as $row){
                                                if($row->id==$camaba->alat_transportasi){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }
                                            } ?>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">No. Telepon</label>
                                          <div class="col-lg-10"><input type="number" min="0" placeholder="Masukkan No. HP" class="form-control" name="no_telp" value="<?php echo $camaba->no_telp?>"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Email</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Email" class="form-control" name="email" value="<?php echo $camaba->email?>"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Penerima KPS?</label>
                                          <div class="col-lg-10">
                                            <select id="kps" class="form-control" name="terima_kps" required>
                                            <?php
                                          foreach (array_reverse($pilihan) as $row) {
                                            if($camaba->terima_kps==$row['id'])
                                            echo "<option value='".$row['id']."' selected>".$row['nama']."</option>";
                                            else
                                            echo "<option value='".$row['id']."'>".$row['nama']."</option>";
                                          }
                                          ?>
                                            </select>
                                          </div>
                                      </div>
                                      <?php
                                      $hiddenkps = "";
                                      if($camaba->terima_kps == 0){
                                        $hiddenkps = "hidden";
                                      }
                                      ?>
                                      <div id="terimakps" class="form-group <?php echo $hiddenkps?>"><label class="col-lg-2 control-label">No. KPS</label>
                                          <div class="col-lg-10"><input type="textr" placeholder="Masukkan Nomor KPS" class="form-control" name="no_kps" value="<?php echo $camaba->no_kps?>"></div>
                                      </div>
                                    </div>
                                  </div>
                               </div>
                           </div>
                           <div id="tab-3" class="tab-pane">
                             <div class="panel-body">
                               <div class="row">
                                 <div class="col-lg-12">
                                 <div class="form-group"><label class="col-lg-2 control-label">Mulai Semester</label>
                                     <div class="col-lg-10">
                                       <select class="form-control" name="fakultas" disabled>
                                         <option value="0" selected disabled>-PILIH SEMESTER</option>
                                         <option value="1">20181</option>
                                         <option value="2">20171</option>
                                       </select>
                                     </div>
                                   </div>
                                   <div class="form-group"><label class="col-lg-2 control-label">Terdaftar di Fakultas</label>
                                     <div class="col-lg-10">
                                       <select class="form-control">
                                        <?php echo "<option value=''>$prodi->fakultas</option>" ?>
                                       </select>
                                     </div>
                                   </div>
                                   <div class="form-group"><label class="col-lg-2 control-label">No. Stambuk</label>
                                       <div class="col-lg-10"><input type="text" placeholder="" value="<?php echo $camaba->no_stambuk?>" class="form-control" disabled></div>
                                   </div>
                                   <div class="form-group"><label class="col-lg-2 control-label">Jurusan/Prodi</label>
                                       <div class="col-lg-10">
                                         <select class="form-control">
                                         <?php echo "<option value=''>$prodi->jenjang $prodi->nama</option>" ?>
                                         </select>
                                       </div>
                                   </div>
                                   <div class="form-group"><label class="col-lg-2 control-label">Status Kemahasiswaan</label>
                                       <div class="col-lg-10">
                                         <select class="form-control" >
                                         <?php foreach ($jenis_pendaftaran->result() as $row){
                                                if($row->id==$camaba->id_tipe_pendaftaran){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                            } ?>
                                         </select>
                                       </div>
                                   </div>
                                   <!-- <div id="baru" class="hidden">
                                     <div class="form-group"><label class="col-lg-2 control-label">Asal Sekolah</label>
                                         <div class="col-lg-10"><input type="text" placeholder="Masukkan Asal Sekolah" class="form-control" requied></div>
                                     </div>
                                   </div>
                                   <div id="pindahan" class="hidden">
                                   <div class="form-group">
                                    <label class="col-lg-2 control-label">Mulai Dari:</label>
                                      <div class="col-lg-10">
                                        <label><small>PTN/PTS</small></label><input type="text" placeholder="Masukkan PTN/PTS" name="ptnpts" class="form-control" requied><br>
                                         <label><small>Fakultas</small></label><input type="text" placeholder="Masukkan Fakultas" name="fakultas" class="form-control" required><br>
                                         <label><small>Jurusan/Prodi</small></label><input type="text" placeholder="Masukkan Jurusan/Prodi" name="jurusan" class="form-control" required><br>
                                      </div>
                                    </div>
                                    <div class="form-group"><label class="col-lg-2 control-label">Alasan Mutasi</label>
                                        <div class="col-lg-10"><input type="text" placeholder="Masukkan Alasan Mutasi" class="form-control" requied></div>
                                    </div>
                                  </div> -->
                                 </div>
                               </div>
                             </div>
                           </div>
                           <div id="tab-4" class="tab-pane">
                             <div class="panel-body">
                               <div class="row">
                                 <div class="col-lg-12">
                                   <div class="form-group">
                                    <label class="col-lg-2 control-label">Data Ayah:</label>
                                      <div class="col-lg-10">
                                        <label><small>Nama</small></label><input type="text" placeholder="Masukkan Nama Ayah" name="nama_ayah" value="<?php echo $camaba->nama_ayah?>" class="form-control"><br>
                                        <label><small>Tempat Lahir</small></label><input type="text" placeholder="Masukkan Tempat Lahir" name="tempat_lahir_ayah" value="<?php echo $camaba->tempat_lahir_ayah?>" class="form-control"><br>
                                         <label><small>Tanggal Lahir</small></label><input type="text" placeholder="Masukkan Tanggal Lahir" id="datepicker2" name="tgl_lahir_ayah" value="<?php echo date("d/m/Y", strtotime($camaba->tgl_lahir_ayah));?>"class="form-control"><br>
                                         <label><small>Pendidikan Terakhir</small></label>
                                         <select name="pendidikan_ayah" class="form-control">
                                          <option value="0" selected disabled>-Pilih Pendidikan Terakhir-</option>
                                          <?php foreach ($pendidikan->result() as $row){
                                                if($row->id==$camaba->pendidikan_ayah){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Pekerjaan</small></label>
                                         <select  name="pekerjaan_ayah" class="form-control">
                                          <option value="0" selected disabled>-Pilih Pekerjaan-</option>
                                          <?php foreach ($pekerjaan->result() as $row){
                                                if($row->id==$camaba->pekerjaan_ayah){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Penghasilan</small></label>
                                         <select  name="penghasilan_ayah" class="form-control">
                                          <option value="0" selected disabled>-Pilih Penghasilan-</option>
                                          <?php foreach ($penghasilan->result() as $row){
                                                if($row->id==$camaba->penghasilan_ayah){
                                                  echo "<option value='$row->id' selected>$row->jumlah</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->jumlah</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Alamat</small></label><input type="text" placeholder="Masukkan Alamat Ayah" name="alamat_ayah" value="<?php echo $camaba->alamat_ayah?>" class="form-control"><br>
                                         <label><small>No. HP</small></label><input type="text" placeholder="Masukkan No. HP Ayah" name="no_telp_ayah" value="<?php echo $camaba->no_telp_ayah?>" class="form-control"><br>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                    <label class="col-lg-2 control-label">Data Ibu:</label>
                                      <div class="col-lg-10">
                                        <label><small>Nama</small></label><input type="text" placeholder="Masukkan Nama Ibu" name="nama_ibu" value="<?php echo $camaba->nama_ibu?>" class="form-control"><br>
                                        <label><small>Tempat Lahir</small></label><input type="text" placeholder="Masukkan Tempat Lahir" name="tempat_lahir_ibu" value="<?php echo $camaba->tempat_lahir_ibu?>" class="form-control"><br>
                                         <label><small>Tanggal Lahir</small></label><input type="text" placeholder="Masukkan Tanggal Lahir" id="datepicker3" name="tgl_lahir_ibu" value="<?php echo date("d/m/Y", strtotime($camaba->tgl_lahir_ibu));?>"class="form-control"><br>
                                         <label><small>Pendidikan Terakhir</small></label>
                                         <select name="pendidikan_ibu" class="form-control">
                                          <option value="0" selected disabled>-Pilih Pendidikan Terakhir-</option>
                                          <?php foreach ($pendidikan->result() as $row){
                                                if($row->id==$camaba->pendidikan_ibu){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Pekerjaan</small></label>
                                         <select  name="pekerjaan_ibu" class="form-control">
                                          <option value="0" selected disabled>-Pilih Pekerjaan-</option>
                                          <?php foreach ($pekerjaan->result() as $row){
                                                if($row->id==$camaba->pekerjaan_ibu){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Penghasilan</small></label>
                                         <select  name="penghasilan_ibu" class="form-control">
                                          <option value="0" selected disabled>-Pilih Penghasilan-</option>
                                          <?php foreach ($penghasilan->result() as $row){
                                                if($row->id==$camaba->penghasilan_ibu){
                                                  echo "<option value='$row->id' selected>$row->jumlah</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->jumlah</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Alamat</small></label><input type="text" placeholder="Masukkan Alamat Ibu" name="alamat_ibu" value="<?php echo $camaba->alamat_ibu?>" class="form-control"><br>
                                         <label><small>No. HP</small></label><input type="text" placeholder="Masukkan No. HP Ibu" name="no_telp_ibu" value="<?php echo $camaba->no_telp_ibu?>" class="form-control"><br>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                    <label class="col-lg-2 control-label">Data Wali:</label>
                                      <div class="col-lg-10">
                                        <label><small>Nama</small></label><input type="text" placeholder="Masukkan Nama Wali" name="nama_wali" value="<?php echo $camaba->nama_wali?>" class="form-control"><br>
                                        <label><small>Tempat Lahir</small></label><input type="text" placeholder="Masukkan Tempat Lahir" name="tempat_lahir_wali" value="<?php echo $camaba->tempat_lahir_wali?>" class="form-control"><br>
                                         <label><small>Tanggal Lahir</small></label><input type="text" placeholder="Masukkan Tanggal Lahir" id="datepicker4" name="tgl_lahir_wali" value="<?php echo date("d/m/Y", strtotime($camaba->tgl_lahir_wali));?>"class="form-control"><br>
                                         <label><small>Pendidikan Terakhir</small></label>
                                         <select name="pendidikan_wali" class="form-control">
                                          <option value="0" selected disabled>-Pilih Pendidikan Terakhir-</option>
                                          <?php foreach ($pendidikan->result() as $row){
                                                if($row->id==$camaba->pendidikan_wali){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Pekerjaan</small></label>
                                         <select  name="pekerjaan_wali" class="form-control">
                                          <option value="0" selected disabled>-Pilih Pekerjaan-</option>
                                          <?php foreach ($pekerjaan->result() as $row){
                                                if($row->id==$camaba->pekerjaan_wali){
                                                  echo "<option value='$row->id' selected>$row->nama</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->nama</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Penghasilan</small></label>
                                         <select  name="penghasilan_wali" class="form-control">
                                          <option value="0" selected disabled>-Pilih Penghasilan-</option>
                                          <?php foreach ($penghasilan->result() as $row){
                                                if($row->id==$camaba->penghasilan_wali){
                                                  echo "<option value='$row->id' selected>$row->jumlah</option>";
                                                }
                                                else{
                                                  echo "<option value='$row->id'>$row->jumlah</option>";
                                                }
                                            } ?>
                                         </select><br>
                                         <label><small>Alamat</small></label><input type="text" placeholder="Masukkan Alamat Wali" name="alamat_wali" value="<?php echo $camaba->alamat_wali?>" class="form-control"><br>
                                         <label><small>No. HP</small></label><input type="text" placeholder="Masukkan No. HP Wali" name="no_telp_wali" value="<?php echo $camaba->no_telp_wali?>" class="form-control"><br>
                                      </div>
                                    </div>
                               </div>
                             </div>
                           </div>
                       </div>
                       <div class="ibox-footer">
                         <center>
                           <button id="submit" type="submit" class="btn btn-md btn-primary" name="button"><i class="fa fa-save"></i> SIMPAN</button>
                           <button type="button" class="btn btn-md btn-warning" name="button" onclick="window.history.back()"><i class="fa fa-reply"></i> KEMBALI</button>
                         </center>
                       </div>
                       <br><br>
                     </form>
                   </div>
              </div>
            </div>
            <br><br>
          </div>

  <script type="text/javascript">
  $('#jenis_kelamin').val('<?php echo $camaba->jenis_kelamin ?>');
  $('#status').change(function(){
  status = $('#status').val();
  if(status=='1'){
    $('#baru').removeClass('hidden');
    $('#pindahan').addClass('hidden');
  }
  else{
    $('#pindahan').removeClass('hidden');
    $('#baru').addClass('hidden');
  }
});
function readURL(input) {

if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
        $('#display').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
}
else{
alert('hahaha');
}
}

$("#inputImage").change(function(){
var dataFoto = this;
var form = $('#form_foto')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
$.ajax({
 url: '<?php echo base_url("camaba/Registrasipmb/updateFoto");?>',
 data: formData,
 type: 'POST',
 // THIS MUST BE DONE FOR FILE UPLOADING
 contentType: false,
 processData: false,
 dataType: "JSON",
 success: function(data){
   // alert(data);
   console.log(data);
   if (data.status=='berhasil') {
     swal("Berhasil!", data.message, "success");
 readURL(dataFoto);
   }else {
     swal("Gagal!", data.message, "error");
   }
 },
error: function(jqXHR, textStatus, errorThrown)
{
console.log(jqXHR);
console.log(textStatus);
console.log(errorThrown);
}
})
     return false;
});
$('#form').submit(function(e){
  e.preventDefault();
  var form = $('#form')[0]; // You need to use standart javascript object here
  var formData = new FormData(form);
  $.ajax({
    url: '<?php echo base_url("camaba/Heregistrasi/update");?>',
    data: formData,
    type: 'POST',
    // THIS MUST BE DONE FOR FILE UPLOADING
    contentType: false,
    processData: false,
    dataType: "JSON",
    success: function(data){
      // alert(data);
      console.log(data);
      if (data.status=='berhasil') {
        swal("Berhasil!", data.message, "success");
      }else {
        swal("Gagal!", data.message, "error");
      }
    },
  error: function(jqXHR, textStatus, errorThrown)
  {
console.log(jqXHR);
console.log(textStatus);
console.log(errorThrown);
}
  })
        return false;
    });
  </script>

<script type="text/javascript">
  $('#kps').change(function(){
  kps = $('#kps').val();
  if(kps=='1'){
    $('#terimakps').removeClass('hidden');
  }
  else{
    $('#terimakps').addClass('hidden');
  }
});

  </script>

  <!-- <script type="text/javascript">
      function set(tipe){
      if(tipe == '-1'){
        $('#submit').removeClass('btn-primary');
        $('#submit').addClass('btn-default');
        $('#submit').addClass('disabled');
        $('#submit').html('<i class="fa fa-check"></i> Telah Submit');
      }
      else{
      }
    };
  </script> -->
