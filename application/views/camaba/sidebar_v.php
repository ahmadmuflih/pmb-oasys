<div id="wrapper">

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="<?php echo base_url().$_SESSION['foto']?>" width="100px"/>
                         </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $_SESSION['data']['nama']; ?></strong>
                        </span> <span class="text-muted text-xs block">Camaba <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="profile.html">Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    PMB
                </div>
            </li>
            <li <?php if(substr($page,0,1)=='1')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."camaba/home"?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
            </li>
            <li <?php if(substr($page,0,1)=='2')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."camaba/ubahpassword"?>"><i class="fa fa-lock"></i> <span class="nav-label">Ubah Password</span></a>
            </li>
            <!-- <li <?php if(substr($page,0,1)=='2')echo "class='active'"; ?>>
                <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Akun</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li <?php if(substr($page,0,2)=='21')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."camaba/ubahpassword"?>">Ubah Password</a>
                  </li>
                  <li <?php if(substr($page,0,2)=='22')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."camaba/riwayatlogin"?>">Riwayat Login</a>
                  </li>
                </ul>
            </li> -->
            <!-- <li <?php if(substr($page,0,1)=='3')echo "class='active'"; ?>>
                <a href="#"><i class="fa fa-pencil"></i> <span class="nav-label">Registrasi</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li <?php if(substr($page,0,2)=='31')echo "class='active'"; ?>>
                        <a href="<?php echo base_url()."camaba/registrasipmb"?>">Registrasi PMB</a>
                    </li>
                    <li <?php if(substr($page,0,2)=='32')echo "class='active'"; ?>>
                        <a href="<?php echo base_url()."camaba/heregistrasi"?>">HER Registrasi</a>
                    </li>
                    <li>
                        <a href="#">Bidik Misi</a>
                    </li>
                </ul>
            </li> -->
            <li <?php if(substr($page,0,1)=='3')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."camaba/registrasipmb"?>"><i class="fa fa-pencil"></i> <span class="nav-label">Registrasi PMB</span></a>
            </li>
            <?php if($_SESSION['batch']['aktif_pengumuman_lulus'] == 1){ ?>
            <li <?php if(substr($page,0,1)=='4')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."camaba/pengumuman"?>"><i class="fa fa-bullhorn"></i> <span class="nav-label">Pengumuman</span></a>
            </li>
            <?php } ?>
            <?php if($_SESSION['batch']['aktif_cetak_herregistrasi'] == 1 && !is_null($_SESSION['data']['pilihan_lulus'])){ ?>
            <li <?php if(substr($page,0,1)=='5')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."camaba/heregistrasi"?>"><i class="fa fa-users"></i> <span class="nav-label">Heregistrasi</span></a>
            </li>
        </ul>
        <?php } ?>
    </div>
</nav>
