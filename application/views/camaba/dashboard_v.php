
  <!-- content -->
    <style media="screen">
      .icon-size{
        font-size: 30px !important;
        opacity: 0.6;
      }
    </style>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                    </ol>
                </div>
            </div>

            <style media="screen">
            .crop {
              width: 240px;
              height: 320px;
              overflow: hidden;
            }

            /* img {
                max-width: 100%;
                max-height: 100%;
            } */
            </style>

            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Data</h5>
                        </div>
                        <div class="ibox-content">
                          <!-- <div class="sk-spinner sk-spinner-double-bounce">
                            <div class="sk-double-bounce1"></div>
                            <div class="sk-double-bounce2"></div>
                          </div> -->
                          <div class="row">
                          <div class="col-md-5">
                            <img class="crop" src="<?php echo base_url().$_SESSION['foto'] ?>" alt="foto_profil">
                          </div>
                          <div class="col-md-7">
                            <h2><?php echo $camaba->nama; ?></h2>
                            <h4>No. Registrasi: <?php echo $camaba->no_registrasi; ?></h4></p>
                            <h4><?php echo $camaba->email; ?></h4>
                            <?php
                            if($status->id == 0 || $status->id == 1){
                                $label = 'label-default';
                            }
                            else if($status->id == 2){
                                $label = 'label-warning';
                            }
                            else{
                                $label = 'label-primary';
                            }
                            ?>
                            <h4><span class="label <?php echo $label ?>"><?php echo $status->nama ?></span></h4>
                            <br ><br >
                            <!-- <div class="form-group">
                              <label class="control-label">Upload</label>
                              <input type="file" class="form-control" name="file" accept="image/jpeg,image/x-png" required>
                              <small>Silahkan upload bukti pembayaran PMB dengan format file jpg/png max file 1 mb</small>
                            </div>
                            <center>
                              <button id="submit" onclick="set(-1)" class="btn btn-sm btn-primary" type="button" name="button"><i class="fa fa-send"></i> SUBMIT</button>
                            </center> -->
                          </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <!-- <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                      <div class="ibox-title">
                        <h5>Aktivitas PMB</h5>
                      </div>
                      <div class="ibox-content">
                        <div class="activity-stream">
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-pencil bg-primary"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Arian Nurrifqhi</span>
                                                        <span class="date">27/12/2018 Jam 09:00 </span>
                                                    </a>
                                                </div>
                                                Telah Mengisi Registrasi PMB
                                            </div>
                                        </div>

                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-bullhorn bg-warning"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Admin PMB</span>
                                                        <span class="date">28/12/2018 Jam 09:00</span>
                                                    </a>
                                                </div>
                                                Meng-ACC Pengajuan Registrasi PMB
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-print bg-info"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Arian Nurrifqhi</span>
                                                        <span class="date">28/12/2018 Jam 15:21 </span>
                                                    </a>
                                                </div>
                                                Telah Melakukan Cetak Kartu Test
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-bullhorn bg-warning"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Admin PMB</span>
                                                        <span class="date">01/01/2019 Jam 07:05 </span>
                                                    </a>
                                                </div>
                                                Telah Publish Pengumuman Kelulusan PMB
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-pencil bg-primary"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Arian Nurrifqhi</span>
                                                        <span class="date">02/01/2019 Jam 10:02 </span>
                                                    </a>
                                                </div>
                                                Telah Mengisi HER Registrasi
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-print bg-info"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Arian Nurrifqhi</span>
                                                        <span class="date">02/01/2019 Jam 13:45 </span>
                                                    </a>
                                                </div>
                                                Telah Mencetak HER Registrasi
                                            </div>
                                        </div>
                                </div>
                      </div>
                    </div>
                  </div> -->
                </div>
            </div>

            <!-- modal window -->
            <div class="modal inmodal fade" id="modalberita" tabindex="-1" role="dialog"  aria-hidden="true">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                  <h4>PEMBEKALAN PENGAWAS UJIAN TENGAH SEMESTER (UTS) SEMESTER GENAP 2017/2018 FAKULTAS TEKNIK SIPIL</h4><br >
                              </div>
                              <div class="modal-body">
                                <p>PEMBEKALAN PENGAWAS UJIAN TENGAH SEMESTER (UTS) SEMESTER GENAP 2017/2018 FAKULTAS TEKNIK ELEKTRO Seluruh Pengawas Ujian Tengah Semester (UTS) Genap 2017/2018 Fakultas Teknik Elektro wajib menghadiri Pembekalan Pengawas Ujian yang akan dilaksanakan pada : Hari, Tanggal : Jum???at, 2 Maret 2018 Waktu	: 15.30 ??? 17.30 WIB Tempat	: Gedung Barung Ruang N314 Sehubungan dengan pentingnya acara ini, diharapkan dapat hadir tepat waktu . Demikian untuk diperhatikan dan dilaksanakan.</p>
                                <div class="lightBoxGallery">
                                <a href="<?php echo base_url()."assets" ?>/img/thumbnail-file.png" title="attachment" data-gallery="">
                                <center><img src="<?php echo base_url()."assets" ?>/img/thumbnail-file.png" alt="attachment_img"></a>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>

<script>
      $('#ibox1').children('.ibox-content').toggleClass('sk-loading');
      setTimeout(function () {
        $('#ibox1').children('.ibox-content').toggleClass('sk-loading').visible();
      }, 10000);
        // $(function(){
            // $('#toggleSpinners').on('click', function(){
            // })
        // })
</script>

<!-- <script type="text/javascript">
    function set(tipe){
    if(tipe == '-1'){
      $('#submit').removeClass('btn-primary');
      $('#submit').addClass('btn-default');
      $('#submit').addClass('disabled');
      $('#submit').html('<i class="fa fa-check"></i> TELAH SUBMIT');
    }
  };
</script> -->
