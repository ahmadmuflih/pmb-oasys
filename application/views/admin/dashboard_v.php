
  <!-- content -->
    <style media="screen">
      .icon-size{
        font-size: 30px !important;
        opacity: 0.6;
      }
    </style>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins animated fadeInDown">
                            <div class="ibox-title">
                                <h5> Pendaftar</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">56</h1>
                                <div class="pull-right"><i class="fa fa-user icon-size"></i></div>
                                <small>Total Pendaftar</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins animated fadeInDown ">
                            <div class="ibox-title">
                                <h5>Akun Aktif</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">1542</h1>
                                <div class="pull-right"><i class="fa fa-group icon-size"></i></div>
                                <small>Total Akun Aktif</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins animated fadeInDown">
                            <div class="ibox-title">
                                <h5>Kelas</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">27</h1>
                                <div class="pull-right"><i class="fa fa-building icon-size"></i></div>
                                <small>Total Kelas</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins animated fadeInDown">
                            <div class="ibox-title">
                                <h5>Jurusan</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">22</h1>
                                <div class="pull-right"><i class="fa fa-tags icon-size"></i></div>
                                <small>Total Jurusan</small>
                            </div>
                        </div>
                      </div>
                </div>
              <div class="row">
                      <div class="col-lg-6">
                          <div class="ibox float-e-margins">
                              <div class="ibox-title">
                                  <h5>Terakhir Login</h5>
                              </div>
                              <div class="ibox-content">
                                  <table class="table table-hover no-margins">
                                      <thead>
                                      <tr>
                                          <th>Akun</th>
                                          <th>Date</th>
                                          <th>User</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td><small>Dosen</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 12:01am</td>
                                          <td>11223</td>
                                      </tr>
                                      <tr>
                                          <td><small>Mahasiswa</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 11:24am</td>
                                          <td>112245</td>
                                      </tr>
                                      <tr>
                                          <td><small>Mahasiswa</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 10:38am</td>
                                          <td>112239</td>
                                      </tr>
                                      <tr>
                                          <td><small>Mahasiswa</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 08:13am</td>
                                          <td>112334</td>
                                      </tr>
                                      <tr>
                                          <td><small>Mahasiswa</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 11:39pm</td>
                                          <td>112299</td>
                                      </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="ibox float-e-margins">
                              <div class="ibox-title">
                                  <h5>Kalender Akademik </h5>
                              </div>
                              <div class="ibox-content">
                                  <div id="calendar"></div>
                              </div>
                          </div>
                      </div>
                </div>
            </div>
