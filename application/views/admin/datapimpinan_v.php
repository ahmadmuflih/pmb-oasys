
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Data Pimpinan</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="ibox-content col-lg-12">
                <form class="form-horizontal">
                <div class="row">
                  <div class="col-lg-12">
                      <h2>Data</h2>
                      <div class="hr-line-dashed"></div>
                    </div>
                  <div class="col-lg-6">
                      <!-- <form class="form-horizontal"> -->
                      <div class="form-group">
                        <label class="col-lg-4 control-label">Tahun Akademik:</label>
                        <div class="col-lg-7"><select class="form-control" name="tahun">
                                      <option>1718/1</option>
                                      <option>1718/2</option>
                                      <option>1819/1</option>
                                  </select>
                          </div>
                        </div>
                      <div class="form-group">
                        <label class="col-lg-4 control-label">Ketua:</label>
                          <div class="col-lg-7"><input type="text" class="form-control"></div>
                        </div>
                      <div class="form-group">
                        <label class="col-lg-4 control-label">Sekretaris:</label>
                          <div class="col-lg-7"><input type="text" class="form-control"></div>
                        </div>
                      <div class="form-group">
                        <label class="col-lg-4 control-label">Bendahara:</label>
                          <div class="col-lg-7"><input type="text" class="form-control"></div>
                        </div>
                      <div class="form-group">
                        <label class="col-lg-4 control-label">Tanggal: </label>
                          <div class="col-lg-3">
                            <input type="text" id="datepicker" name="datepicker" class="form-control" placeholder="Tanggal" required>
                          </div>
                          <div class="col-xs-1">
                            <p style="margin-top:5px;margin-right:10px;">s/d</p>
                          </div>
                            <!-- <font size="2"><b>s&sol;d</b></font> -->
                          <div class="col-lg-3">
                            <input type="text" id="datepicker2" name="datepicker2" class="form-control" placeholder="Tanggal" required>
                          </div>
                      </div>
                      <!-- </form> -->
                  </div>
                  <div class="col-lg-6">
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Rektor/Ketua:</label>
                          <div class="col-lg-5">
                                  <select class="form-control">
                                        <option>Dosen 1</option>
                                        <option>Dosen 2</option>
                                        <option>Dosen 3</option>
                                    </select>
                            </div>
                          </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Wakil Bidang I:</label>
                          <div class="col-lg-5">
                                  <select class="form-control">
                                        <option>Dosen 1</option>
                                        <option>Dosen 2</option>
                                        <option>Dosen 3</option>
                                    </select>
                            </div>
                          </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Wakil Bidang II:</label>
                          <div class="col-lg-5">
                                  <select class="form-control">
                                        <option>Dosen 1</option>
                                        <option>Dosen 2</option>
                                        <option>Dosen 3</option>
                                    </select>
                            </div>
                          </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Wakil Bidang III:</label>
                          <div class="col-lg-5">
                                  <select class="form-control">
                                        <option>Dosen 1</option>
                                        <option>Dosen 2</option>
                                        <option>Dosen 3</option>
                                    </select>
                            </div>
                          </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Wakil Bidang IV:</label>
                          <div class="col-lg-5">
                                  <select class="form-control">
                                        <option>Dosen 1</option>
                                        <option>Dosen 2</option>
                                        <option>Dosen 3</option>
                                    </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-lg-3 control-label">Wakil Bidang V:</label>
                            <div class="col-lg-5">
                                    <select class="form-control">
                                          <option>Dosen 1</option>
                                          <option>Dosen 2</option>
                                          <option>Dosen 3</option>
                                      </select>
                              </div>
                            </div>
                        <hr >
                        <div class="form-group">
                          <label class="col-lg-3 control-label">No. SK Rektor:</label>
                            <div class="col-lg-5"><input type="text" class="form-control"></div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-3 control-label">Tanggal: </label>
                            <div class="col-lg-3">
                              <input type="text" id="datepicker3" name="datepicker3" class="form-control" placeholder="Tanggal" required>
                            </div>
                            <div class="col-xs-1">
                              <p style="margin-top:5px;margin-right:10px;">s/d</p>
                            </div>
                            <div class="col-lg-3">
                              <input type="text" id="datepicker4" name="datepicker4" class="form-control" placeholder="Tanggal" required>
                            </div>
                        </div>
                  </div>
                </div> <br >
                <br >
                <center>
                  <button type="submit" class="btn btn-w-m btn-primary" name="button"><i class="fa fa-save"></i> Simpan</button>
                </center>
              </form>
              </div>
            </div> <br >
          </div>
