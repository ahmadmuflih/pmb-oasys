
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Tambah Program Studi Baru</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
                <!-- <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#myModal4"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div> -->
            </div>

            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="ibox-content col-lg-12">
                <form class="form-horizontal">
                      <h2>Form</h2>
                      <div class="hr-line-dashed"></div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Kode Program Studi:</label>
                          <div class="col-lg-6"><input type="number" class="form-control"></div>
                        </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Nama Program Studi:</label>
                          <div class="col-lg-6"><input type="text" class="form-control"></div>
                        </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Jenjang:</label>
                          <div class="col-lg-6"><input type="text" class="form-control"></div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">Gelar Akademik:</label>
                            <div class="col-lg-6"><input type="text" class="form-control"></div>
                          </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Total SKS:</label>
                          <div class="col-lg-6"><input type="number" class="form-control"></div>
                        </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">Tanggal Berdiri:</label>
                            <div class="col-lg-6"><input type="text" class="form-control"></div>
                          </div>
                        <div class="form-group">
                          <label class="col-lg-2 control-label">Mulai Semester:</label>
                            <div class="col-lg-6"><input type="text" class="form-control"></div>
                          </div>
                          <div class="hr-line-dashed"></div><br >
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Ketua Prodi:</label>
                              <div class="col-lg-6"><input type="text" class="form-control"></div>
                            </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">No HP Ketua:</label>
                              <div class="col-lg-6"><input type="number" class="form-control"></div>
                            </div>
                            <div class="hr-line-dashed"></div><br >
                          <div class="form-group">
                            <label class="col-lg-2 control-label">No SK Dikti:</label>
                              <div class="col-lg-6"><input type="number" class="form-control"></div>
                            </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Tanggal SK Dikti:</label>
                              <div class="col-lg-6"><input type="number" class="form-control"></div>
                            </div>
                            <div class="form-group">
                              <label class="col-lg-2 control-label">Tgl Berakhir SK Dikti:</label>
                                <div class="col-lg-6"><input type="number" class="form-control"></div>
                              </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">No SK BAN:</label>
                              <div class="col-lg-6"><input type="number" class="form-control"></div>
                            </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Tanggal SK BAN:</label>
                              <div class="col-lg-6"><input type="number" class="form-control"></div>
                            </div>
                            <div class="form-group">
                              <label class="col-lg-2 control-label">Tgl Berakhir SK BAN:</label>
                                <div class="col-lg-6"><input type="number" class="form-control"></div>
                              </div>
                            <div class="form-group">
                              <label class="col-lg-2 control-label">Akreditasi:</label>
                                <div class="col-lg-6"><input type="number" class="form-control"></div>
                              </div>
                <br >
                <center>
                  <button type="submit" class="btn btn-w-m btn-primary" name="button"><i class="fa fa-save"></i> Simpan</button>
                </center>
              </form>
              </div>
            </div> <br >
          </div>
