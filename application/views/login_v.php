  <body src>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <div class="wrapper">
      <div class="container">
        <nav class="navbar navbar-expand-md nav-white navbar-light bg-light fixed-top">
          <div class="container">
          <style>
            @media only screen and (max-width: 767px) {
              #logo2{
                display:none !important;
              }
              #logo1{
                display:block !important;
              }
              .logo-size {
                width: 8%;
              }
              .footer-r{
                margin-top:-100px;
              }
              #img{
                order: 2;
              }
              
              /* .d2{
                visibility:hidden !important;
              } */
            }
            @media only screen and (min-width: 768px) {
              body {
                  overflow-y:hidden !important;
              }
              #logo1{
                display:none !important;
              }
              #logo2{
                display:block !important;
              }
              .logo-size {
                width: 3%;
                margin-top:2%;
              }
              .title1{
                position:absolute;
                margin-top:1.5%;
                margin-left:3%;
              }
              .title1:hover{
                color:#271c31 !important;
              }
              .footer-f{
                margin-top:-160px;
              }

              .modal-xl {
                  width: 90%;
                max-width:1200px;
                }
              
              /* .d1{
                visibility:hidden !important;
              } */
            }

            .hidden {
              display: none
            }
          </style>
          <img id="logo1" class="img-fluid logo-size" src="<?php echo base_url(). "assets/bootstrap4/images/oasys-logo.png" ?>" alt="logo">
          <img id="logo2" class="img-fluid logo-size" src="<?php echo base_url(). "assets/bootstrap4/images/oasys-logo-white.png" ?>" alt="logo">&nbsp;<a class="navbar-brand title1" href="#">OASYS Admission</a>
          <!-- <div class="d2"><img class="img-fluid logo-size" src="<//?php echo base_url(). "assets/bootstrap4/images/oasys-logo.png" ?>" alt="logo">&nbsp;<a class="navbar-brand" href="#">OASYS Admission</a></div> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto navbar-right">
                  <!-- <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#main">Beranda</a></li> -->
                  <!-- <li><button id="startTourBtn" class="btn btn-primary">Start Tour</button></li> -->
                  <li class="nav-item"><a id="startTourBtn" data-toggle="modal" data-target="#largeModal" class="nav-link js-scroll-trigger" href="#">Bantuan</a></li>
                  <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#oasys">Profil</a></li>
                  <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#pricing">Biaya</a></li>
                  <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#ulasan">Ulasan</a></li>
                </ul>
            </div>
          </div>
        </nav>
      </div>

      <style>
          .formee-login{
              border-radius: 8px !important;
              font-family: Sans-serif;
              font-size:1em;
          }
          .btn-login {
              padding: 10px 24px !important;
              background-color: #e78d3d !important;
              border: 2px solid !important;
              border-color: #e78d3d !important;
              border-radius: 100px !important;
              box-shadow: 0 3px 15px 0 rgba(0, 0, 0, .25) !important;
              font-family: 'Montserrat' !important;
              font-size: 12px !important;
              font-weight: bold !important;
              text-transform: uppercase !important;
              letter-spacing: 1px;
              color:#fff !important;
              margin: 25px 10px 0 0 !important;
              -webkit-transition: 0.2s !important;
              -moz-transition: 0.2s !important;
              transition: 0.2s !important;
            }
          .nav-custom{
            font-family: 'Montserrat';
            color:#0f378a;
          }
          .nav-custom:hover{
            font-family: 'Montserrat';
            color:#ffc947;
          }

          .a-link{
            font-family: 'Montserrat';
            font-size:0.9em;
            color:#fff;
          }
          
          a.active{
              background: transparent !important;
              color: #fff;
               border-bottom: 3px solid #fff; 
               border-radius: 0px !important;
          }
          li.nav-item>div.line-round{
               border-bottom: 5px solid #fff !important; 
              border-radius: 24px !important;
          }
          .hero-bg {
            background-image: linear-gradient(to right, rgba(106, 17, 203, 0.45), rgba(37, 117, 252, 0.45)), url('assets/img/background.jpg');
            /* background:; */
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center top;
        background-size: cover;
        }
        .img-style{
          color: linear-gradient(to right, rgba(106, 17, 203, 0.45), rgba(37, 117, 252, 0.45)) !important;
          border-radius: 10px !important;
          -webkit-box-shadow: 0px 1px 1px 1px rgba(0,0,0,0.15);
          -moz-box-shadow: 0px 1px 1px 1px rgba(0,0,0,0.15);
          box-shadow: 0px 1px 1px 1px rgba(0,0,0,0.15);
        }

        /* Style buttons */
        .buttonload {
          background-color: #4CAF50; /* Green background */
          border: none; /* Remove borders */
          color: white; /* White text */
          padding: 12px 16px; /* Some padding */
          font-size: 16px /* Set a font size */
        }

      </style>

      <div id="main" class="main">
        <div class="features home-2">
          <div class="container">
            <div class="row align-center">
              <div id="img" class="col-md-12 col-lg-6">
                <img class="img-fluid2 img-container" src="<?php echo base_url(). "assets/bootstrap4/images/login-asset.png" ?>" alt="Home">
              </div>
              <div class="col-md-12 col-lg-5 offset-lg-1">
                <div class="hero-content wow fadeInDown">
                  <h1>Welcome to Admission</h1>
                  <p>Silahkan melakukan registrasi akun terlebih dahulu jika anda belum memiliki akun.</p>
                    <div class="subform subform-alt">
                        <ul style="margin-top:-30px" class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li id="content1" class="nav-item">
                                <a class="nav-link nav-custom active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Login</a>
                                <!-- <div class="line-round" active></div> -->
                            </li>
                            <li id="content" class="nav-item">
                                <!-- <a class="nav-link nav-custom hide red-tooltip" title="Registrasi Akun Di sini" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" >Register</a> -->
                                <a class="nav-link nav-custom hide red-tooltip"  id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" >Register</a>
                                
                                <!-- <div class="line-round2"></div> -->
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <form id="form" class="formee formee-alt" action="assets/php/subscribe.php" method="post">
                                    <input class="form-control formee-login" type="text" name="username" id="username" placeholder="Username" required="" />
                                    <br />
                                    <input class="form-control formee-login" type="password" name="password" id="password" placeholder="Password" required="" />
                                    <div class="row">
                                      <div class="col-lg-8">
                                      <button id="btn_login" type="submit" name="masuk" class="btn btn-action btn-edge btn-login fadeInLeft">Login</span></button>
                                      <button id="btn_loading_login" class="btn btn-action btn-edge btn-login fadeInLeft hidden" disabled><i class="fa fa-refresh fa-spin"></i> Loading</span></button>
                                      </div>
                                      <div class="col-lg-4 mb-3 pull-right">
                                      <br /><br />
                                        <a class="a-link" style="color:white;" href="#">Lupa Password <span class="fa fa-question"></span></a>
                                      </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <form id="formregister" class="formee formee-alt" action="assets/php/subscribe.php" method="post">
                                        <input class="form-control formee-login" type="text" name="nama" placeholder="Nama Lengkap" required="" />
                                        <!-- <br /> -->
                                        <div class="row">
                                                <div class="col-lg-6 col-md-12 col-xs-12">
                                                    <br />
                                                    <input id="content2" class="form-control formee-login" type="number" name="no_telp" min="0" placeholder="Nomor Kontak" required="" />
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-xs-12">
                                                        <br />
                                                    <input class="form-control formee-login" type="email" name="email" placeholder="Email" required="" />
                                                </div>
                                        </div>
                                        <br />
                                        <select name="id_batch" id="" class="form-control formee-login form-whatever">
                                            <option value="0" disabled>-PILIH BATCH-</option>
                                            <?php foreach($batch->result() as $row){
                                                echo "<option value='$row->id'>$row->periode - $row->nama</option>";
                                            }?>
                                        </select>
                                    <!-- <button type="submit" class="btn btn-action btn-edge btn-login fadeInLeft">Register</span></button> -->
                                    <button id="btn_register" type="submit" class="btn btn-action btn-edge btn-login fadeInLeft"></i> Register</button>
                                    <button id="btn_loading_register" class="btn btn-action btn-edge btn-login fadeInLeft hidden" disabled><i class="fa fa-refresh fa-spin"></i> Loading</span></button>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="footer">
           <span> <center>developed by: 4nesia</span>
          </div> -->
        </div>

        <style>
        @media only screen and (max-width: 600px) {

        }
        .tooltip{
            /* position: absolute !important;
            top:40% !important;
            left: 70% !important; */
            /* z-index:10000 !important; */
          }
          .tooltip > .tooltip-inner {
            background-color: #fff !important;
            color: #2e7eed !important;
            font-family: 'Lato-bold' !important;
            }
            .bs-tooltip-auto[x-placement^=right] .arrow::before, .bs-tooltip-right .arrow::before {
                border-right-color: #fff !important;
            }
            
        </style>

        <script type="text/javascript">
          $(document).ready(function(){   
              $('[data-toggle="pill"]').tooltip({
                  trigger: 'show',
                  placement: 'right',
                  animation: true,
                  delay: 300,
                  container: 'body',
              });
              $('[data-toggle="pill"]').tooltip("show");
              // $('[data-toggle="pill"]').on('shown.bs.tooltip', function () {
              //     $('.tooltip').addClass('animated shake');
              //   });
          });
          // $(function () {
                // $('[data-toggle="pill"]').tooltip("show");
              // $('.tooltip').addClass("animated shake");
            // });
            setTimeout(function() {
              $(".tooltip").fadeOut("slow");
            }, 4000);
        </script>

        <style>
          .img-custom{
            background-color: rgba(0,0,0,0) !important;
            /* box-shadow:rgba(0,0,0,0) !important; */
          }

          .img-fluid{
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            box-shadow: none !important;
          }
          .img-container{
            max-width: 100%;
            height: auto;
          }
        </style>
        

        <!---About Section----->
        <!-- <div id="about" class="about">
          <div class="container">
            <div class="row text-center">
              <div class="col-md-8 offset-md-2">
                <div class="about-intro">
                  <h1>Cara Mendaftar Melalui Sistem PMB UNISMUH Palu</h1>
                </div>
                <div class="about-img img-custom">
                      <img class="img-fluid" src="<//?php echo base_url(). "assets/bootstrap4/images/registrasi.png" ?>" alt="About">
                </div>
              </div>
              <div class="col-md-4">
                <div class="about-txt-inner">
                  <div class="num" style="background-color:#75cfd6">
                    1
                  </div>
                  <div class="about-txt">
                    <p>Lakukan Registrasi Akun, dan dapatkan akun melalui email pendaftar.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="about-txt-inner">
                  <div class="num" style="background-color:#FF9999">
                    2
                  </div>
                  <div class="about-txt">
                    <p>Lengkapi data pendaftar setelah login ke sistem PMB dan lakukan pembayaran sesuai pilihan program studi</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="about-txt-inner">
                  <div class="num" style="background-color:#4f77ff">
                    3
                  </div>
                  <div class="about-txt">
                    <p>Upload bukti pembayaran dan tunggu verifikasi dari admin. Setelah itu anda sudah dapat melakukan cetak Kartu Ujian</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        <!-- <div class="product-tour features lbl-services">
          <div class="hero-inner flex-split">
          <div class="container">
            <div class="row align-center">
              <div class="col-md-5">
                  <div class="f-left">
                    <div class="left-content wow fadeInLeft" data-wow-delay="0s">
                      <h2>Dr Rajindra SE, MM</h2>
                      <p style="margin-top:-2px">Rektor Universitas Muhammadiyah Palu</p>
                      <blockquote style="font-family:'Lato'"><small>&quot;Improve the quality of lecturers, stated in Catur Dharma Unismuh Palu, namely Education, Teaching, Research, Service, and Islam Kemuhammadiyaan.&quot;</small></blockquote>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 offset-md-1">
                  <div class="f-right">
                      <img class="img-fluid img-style" style="color: linear-gradient(to right, rgba(106, 17, 203, 0.45), rgba(37, 117, 252, 0.45)) !important;" src="<?php echo base_url(). "assets/img/rektor.png" ?>" alt="Hero Image">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        <!-- 2 -->
        <!-- <div id="features" class="features lbl-services">
          <div class="flex-split">
            <div class="container">
              <div class="row align-center">
              <div class="col-md-5">
                  <div class="f-left">
                    <div class="left-content wow fadeInLeft" data-wow-delay="0s">
                      <blockquote style="font-family:'Lato'"><p style="font-size:16pt !important;color:#007bff">&quot;Unismuh Palu kedepan dapat menjadi universitas terdepan yang menerapkan nilai-nilai Islam berdasarkan Islam dan Kemuhammadiyaan di Sulawesi Tengah&quot;</p></blockquote>
                      <h4>Dr Rajindra SE, MM</h4>
                      <p style="margin-top:-2px">Rektor Universitas Muhammadiyah Palu</p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 offset-md-1">
                  <div class="f-right">
                      <img class="img-fluid img-style" style="color: linear-gradient(to right, rgba(106, 17, 203, 0.45), rgba(37, 117, 252, 0.45)) !important;" src="<?php echo base_url(). "assets/img/rektor.png" ?>" alt="Hero Image">
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div> -->

          <!-- 3 -->
        <!-- <div id="unismuh" class="cta-big hero-bg">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-lg-6">
                <div class="cta-big-inner wow fadeInRight">
                  <h4>Kampus Biru</h4>
                  <h1>Visi Universitas Muhammadiyah Palu:</h1>
                  <p>“Menjadi Perguruan Tinggi yang Unggul dalam Pembelajaran, Penelitian dan Pengabdian Kepada Masyarakat yang Berwawasan Islam Pada Tahun 2025”. </p>
                  <a href="#" class="btn btn-action btn-white">Baca Selengkapnya</a>
                </div>
              </div>
            </div>
          </div>
        </div> -->

          <!-- 4 -->
        <!-- <div id="pricing" class="pricing-sm">
          <div class="container">
            <div class="row text-center">
              <div class="col-sm-12">
              <h1 class="text-center">Biaya Registrasi</h1>
            </div>
              <div class="col-sm-4 offset-sm-2">
                <div class="plan">
                  <h4>Sarjana</h4>
                  <div class="pricing-icon">
                    <img class="rounded-circle" src="<?php echo base_url(). "assets/bootstrap4/icons/pricing-3.png" ?>" width="70" alt="Pricing">
                  </div>
                  <div class="price">
                    Rp 350.000
                    <br>
                  </div>
                  <hr>
                  <ul class="pricing-list">
                    <li>Berlaku untuk pendaftar baru/pindahan/lanjutan</li>
                    <li>Biaya Registrasi khusus Sarjana (S1)</li>
                  </ul>
                  <a href="#" class="btn btn-action btn-edge wow fadeInLeft" data-wow-delay="0.2s">Baca Selengkapnya</a>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="plan">
                  <h4>Pascasarjana</h4>
                  <div class="pricing-icon">
                    <img class="rounded-circle" src="<//?php echo base_url(). "assets/bootstrap4/icons/pricing-2.png" ?>"  width="70" alt="Pricing">
                  </div>
                  <div class="price">
                    Rp 500.000
                    <br>
                  </div>
                  <hr>
                  <ul class="pricing-list">
                    <li>Berlaku untuk pendaftar baru/<s>pindahan/lanjutan</s></li>
                    <li>Biaya Registrasi khusus Pascasarjana (S2)</li>
                  </ul>
                  <a href="#" class="btn btn-action btn-edge wow fadeInLeft" data-wow-delay="0.2s">Baca Selengkapnya</a>
                </div>
              </div>
            </div>
          </div>
        </div> -->


          <!-- 5 -->
        <!-- <div id="ulasan" class="section-r">
          <div class="container">
            <div class="row">
              <div class="col-lg-5">
                <div class="review-pitch">
                  <h2>Statistik dan Kata Alumni</h2>
                  <p>Berikut adalah data statistik civitas Universitas Muhammadiyah Palu tahun ajaran 2018/2019</p>
                </div>
                <div class="counter-section">
            			<div class="row text-center">
              			<div class="col-sm-3 col-xs-6">
              				<div class="counter-up">
              					<div class="counter-icon">
                          <img src="<?php echo base_url(). "assets/bootstrap4/icons/downloads.png" ?>" width="60" alt="Press">
              					</div>
                        <h3><span class="counter">3</span>K+</h3>
              					<div class="counter-text">
              						<h4>Mahasiswa Aktif</h4>
              					</div>
              				</div>
              			</div>
              			<div class="col-sm-3 col-xs-6">
              				<div class="counter-up">
              					<div class="counter-icon">
                          <img src="<?php echo base_url(). "assets/bootstrap4//icons/projects.png" ?>" width="60" alt="Projects">
              					</div>
                        <h3><span class="counter">134</span></h3>
              					<div class="counter-text">
              						<h4>Tenaga Pengajar</h4>
              					</div>
              				</div>
                		</div>
                    <div class="col-sm-3 col-xs-6">
              				<div class="counter-up">
              					<div class="counter-icon">
                          <img src="<?php echo base_url(). "assets/bootstrap4//icons/awards.png" ?>" width="60" alt="Press">
              					</div>
                        <h3><span class="counter">17</span></h3>
              					<div class="counter-text">
              						<h4>Program Studi</h4>
              					</div>
              				</div>
                		</div>
                    <div class="col-sm-3 col-xs-6">
              				<div class="counter-up">
              					<div class="counter-icon">
              						<img src="<?php echo base_url(). "assets/bootstrap4//icons/press.png" ?>" width="60" alt="Press">
              					</div>
                        <h3><span class="counter">9</span></h3>
              					<div class="counter-text">
              						<h4>Fakultas</h4>
              					</div>
              				</div>
                		</div>
                	</div>
                </div>
              </div>
              <div class="col-lg-6 offset-lg-1">
                <div class="review-section">
                  <div id="reviews" class="review-section">
                    <div class="container">
                      <div class="row text-center">
                        <div class="col-sm-12">
                          <div class="reviews owl-carousel owl-theme">
                            <div class="review-single">
                              <img class="rounded-circle" src="<?php echo base_url(). "assets/bootstrap4//icons/review-1.png" ?>" alt="Client Testimonoal" />
                              <div class="review-text wow fadeInDown" data-wow-delay="0.2s">
                                <p>Saya sangat senang kuliah disini banyak pengalaman yang saya peroleh disamping itu saya juga bisa mengembangkan bakat saya dengan adanya fasilitas yang tersedia</p>
                                <h3>Anonymous</h3>
                                <h4>Alumni</h4>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                              </div>
                            </div>
                            <div class="review-single">
                              <img class="rounded-circle" src="<?php echo base_url(). "assets/bootstrap4//icons/review-2.png" ?>" alt="Client Testimonoal" />
                              <div class="review-text">
                                <p>Belajar sangat mengasyikkan ditambah banyaknya kegiatan kemahasiswaan yang membuat saya semakin percaya diri</p>
                                <h3>Anonymous</h3>
                                <h4>Alumni</h4>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-ios-star-half"></i>
                              </div>
                            </div>
                            <div class="review-single">
                              <img class="rounded-circle" src="<?php echo base_url(). "assets/bootstrap4//icons/review-3.png" ?>" alt="Client Testimonoal" />
                              <div class="review-text">
                                <p>Suasana yang nyaman membuat saya menyukai kampus ini, tenaga pengajarnya juga begitu professional</p>
                                <h3>Anonymous</h3>
                                <h4>Alumni</h4>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-star"></i>
                                <i class="ion ion-ios-star-half"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        <!-- <div class="footer">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <h2>Admission OASYS</h2>
                <p> Jalan Rusa no 1</p>
                <ul class="footer-sub">
                  <li>&copy; 2020 | Developed by <a href="https://4nesia.com">4nesia</a>.</li>
                  <li><a href="#">Privacy</a></li>
                  <li><a href="#">Legal</a></li>
                </ul>
              </div>
              <div class="col-md-6 text-right d-none d-md-block">
                <ul class="social">
                  <li><a href="#"><img src="<?php echo base_url(). "assets/bootstrap4/icons/facebook.png" ?>" width="35" alt="Social"></a></li>
                  <li><a href="#"><img src="<?php echo base_url(). "assets/bootstrap4/icons/twitter.png" ?>" width="35" alt="Social"></a></li>
                  <li><a href="#"><img src="<?php echo base_url(). "assets/bootstrap4/icons/youtube.png" ?>" width="35" alt="Social"></a></li>
                  <li><a href="#"><img src="<?php echo base_url(). "assets/bootstrap4/icons/instagram.png" ?>" width="35" alt="Social"></a></li>
                </ul>
                <ul class="footer-nav">
                  <li><a href="#">About</a></li>
                  <li><a href="#">Support</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
                <a class="f-mail" href="tel:180032472349">Call: 085-399-055-225</a>
                <br>
                <a class="f-mail" href="mailto:info@label.co">hi@4nesia.com</a>
              </div>
            </div>
          </div>
        </div> -->

        <style>
          .footer {
            position:relative;
            font-family: 'Montserrat';
            left: 0;
            bottom: 0;
            height:100%;
            width: auto;
            /* background-color: #51c4d3; */
            background-color: #0a1931;
            color: white;
            text-align: center;
          }

          .popover.right .arrow:after {
  border-right-color: white !important;
}

          </style>

          <div class="footer footer-f footer-r">
           <span> <center>developed by: 4nesia</span>
          </div>


        <!-- Scroll To Top -->
          <a id="back-top" class="back-to-top js-scroll-trigger" href="#main">
            <i class="ion-android-navigate"></i>
          </a>
        <!-- Scroll To Top Ends-->
      </div> <!-- Main -->
    </div><!-- Wrapper -->
    

  </body>
</html>

<script>
var ip, browser;
$.getJSON("http://jsonip.com?callback=?", function (data) {
    ip = data.ip;
});
browser = navigator.userAgent;
$('#form').submit(function(){
    var formData = new FormData;
    var username = $('#username').val();
    var password = $('#password').val();

    formData.append('username',username);
    formData.append('password',password);
    formData.append('ip',ip);
    formData.append('browser',browser);

    $('#btn_login').addClass('hidden')
    $('#btn_loading_login').removeClass('hidden')
    $.ajax({
      url: '<?php echo base_url("Login/login");?>',
      data: formData,
      type: 'POST',
      // THIS MUST BE DONE FOR FILE UPLOADING
      contentType: false,
      processData: false,
      dataType: "JSON",
      success: function(data){
        console.log(data.message);
        if(data.status=='berhasil'){
          location.reload();
          //alert(data);
        }
      else if(data.status=='welcome')
        location.href = '<?php echo base_url().'welcome' ?>';
      else if(data.status=='reset')
          location.href = '<?php echo base_url().'reset' ?>';
      else
          swal("Gagal!", data.message, "error");
        $('#btn_login').removeClass('hidden')
      $('#btn_loading_login').addClass('hidden')
      },
    error: function(jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
      $('#btn_login').removeClass('hidden')
  $('#btn_loading_login').addClass('hidden')
    }
          });

    return false;
  });
  $('#formregister').submit(function(){

var form = $('#formregister')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
$('#btn_register').addClass('hidden')
$('#btn_loading_register').removeClass('hidden')
$.ajax({
  url: '<?php echo base_url("Login/register");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
      $('#formregister')[0].reset();
    }else {
      swal("Gagal!", data.message, "error");
    }
    $('#btn_register').removeClass('hidden')
  $('#btn_loading_register').addClass('hidden')
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
  $('#btn_register').removeClass('hidden')
  $('#btn_loading_register').addClass('hidden')
}
})

return false;
});

//Tour

var tour = new Tour();

// Add your steps. Not too many, you don't really want to get your users sleepy
tour.addSteps([{
  element: "#content", // string (jQuery selector) - html element next to which the step popover should be shown
  title: "Hint", // string - title of the popover
  content: "Klik disini untuk Registrasi Akun", // string - content of the popover
  placement: "right",
  animation: "true",
  template: "<div class='popover tour animated shake' style=' z-index: 1 !important; border: none !important; border-radius:16px !important; background-color:rgb(255, 255, 255) !important'><button type='button' style='margin-right:10px;' class='close' data-role='end' aria-label='Close'><span aria-hidden='true'>&times;</span></button><div class='arrow'></div><center><img class='img-fluid' style='height:auto;width:25%;margin-left:20px !important;margin-top:-3px !important' src='<?php echo base_url(). 'assets/img/id-card.png' ?>' alt='logo'><center><div class='popover-content' style='font-family: Montserrat !important;'></div><div class='popover-navigation'></div>"
}]);

// tour.init();
// tour.start();
tour.init();
tour.start();
// tour.restart();
// $("#startTourBtn").click(function() {
//   tour.restart();
// });
</script>

<!-- modal -->
<div class="modal inmodal fade" id="largeModal" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <!-- <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> -->
                                           <center> <h4 class="modal-title">Tata Cara Registrasi Akun</h4><center>
                                            <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                                        </div>
                                        <div class="modal-body">
                                        <div class="container-fluid">
                                        <div class="row">
                                          <div class="col-md-4"><center><img class='img-fluid' style='height:auto;width:25%;margin-left:20px !important;margin-top:-3px !important' src='<?php echo base_url(). 'assets/img/laptop.png' ?>' alt='logo'>
                                           <br><br> <p>Jika anda belum memiliki akun Admission, Silahkan melakukan registrasi akun terlebih dahulu</p>
                                           <center> <br><hr></div>
                                           <div class="col-md-4"><center><img class='img-fluid' style='height:auto;width:25%;margin-left:20px !important;margin-top:-3px !important' src='<?php echo base_url(). 'assets/img/checklist.png' ?>' alt='logo'>
                                           <br><br> <p>Lengkapi data pendaftar setelah login ke sistem PMB dan lakukan pembayaran sesuai pilihan program studi</p>
                                           <center> <br><hr></div>
                                           <div class="col-md-4"><center><img class='img-fluid' style='height:auto;width:25%;margin-left:20px !important;margin-top:-3px !important' src='<?php echo base_url(). 'assets/img/curriculum.png' ?>' alt='logo'>
                                           <br><br> <p>Upload bukti pembayaran dan tunggu verifikasi dari admin. Setelah itu anda sudah dapat melakukan cetak Kartu Ujian</p>
                                           <center> <br><hr></div>
                                          <!-- <div class="col-md-4 ml-auto">.col-md-4 .ml-auto</div> -->
                                        </div>
                                        </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-login" style="background-color: #c2c2c2 !important; border: 2px solid !important;border-color: #c2c2c2 !important;" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

