<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Maintenance</title>

    <link href=<?php echo base_url(). "assets/css/bootstrap.min.css" ?> rel="stylesheet">
    <link href=<?php echo base_url(). "assets/font-awesome/css/font-awesome.css" ?> rel="stylesheet">

    <link href=<?php echo base_url(). "assets/css/animate.css" ?> rel="stylesheet">
    <link href=<?php echo base_url(). "assets/css/style.css" ?> rel="stylesheet">

</head>

<body class="white-bg">
    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-lg-8">
                <h1 style="font-size:12em"><b>Oops!</b></h1>
                <h1 class="font-bold">We are sorry</h1>

                <div class="error-desc">
                    The website is under maintenance.<br/>
                    You can go back for a few more moments <br/>
                </div>
                <br><br>
                <a href="http://4nesia.com" target="_blank" style="color:#33669B">www.4nesia.com</a>
            </div>
            <div class="col-lg-4">
                <img width="60%" src="<?php echo base_url(). "assets" ?>/img/maintenance.png" alt="maintenance">
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src=<?php echo base_url(). "assets/js/jquery-3.1.1.min.js" ?>></script>
    <script src=<?php echo base_url(). "assets/js/bootstrap.min.js" ?>></script>

</body>

</html>
