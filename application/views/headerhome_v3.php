<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PMB UNISMUH PALU</title>

    <link href="<?php echo base_url(). "assets/css/bootstrap.min.css" ?>" rel="stylesheet">
    <link href="<?php echo base_url(). "assets/font-awesome/css/font-awesome.css" ?>" rel="stylesheet">

    <link href="<?php echo base_url(). "assets/menu-style.css" ?>" rel="stylesheet">

    <link href="<?php echo base_url(). "assets" ?>/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <link href="<?php echo base_url(). "assets/css/animate.css" ?>" rel="stylesheet">
    <link href="<?php echo base_url(). "assets/css/style.css" ?>" rel="stylesheet">
    <link href="<?php echo base_url(). "assets/css/ui-assets.css" ?>" rel="stylesheet">
    <link href="<?php echo base_url()."assets" ?>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()."assets" ?>/img/logounismuh.png"/>

     <!-- sweealert -->
     <script src="<?php echo base_url()."assets" ?>/js/plugins/sweetalert/sweetalert.min.js"></script>

    <style media="screen">
      body > .container{
        padding-top: 10px !important;
      }
      body {
        background-image: url('assets/img/background.jpg');
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center top;
        background-size: cover;
      }

      @media
          only screen and (-webkit-min-device-pixel-ratio: 1.5),
          only screen and (-o-min-device-pixel-ratio: 3/2),
          only screen and (min--moz-device-pixel-ratio: 1.5),
          only screen and (min-device-pixel-ratio: 1.5){

            html,
            body{
              width:100%;
              overflow-x:hidden;
            }

          }
          .carousel-control.left, .carousel-control.right {
           background-image:none !important;
           filter:none !important;
        }
        /* .loginColumns{
          padding-left: 20px !important;
          max-width: 860px !important;
        } */
        @media only screen and (min-width: 1085px) {
        .style-box{
            padding: 80px !important;
        }
      }

      img{
        max-width: 60%;
        max-height: 60%;
      }
    </style>


    <!-- Mainly scripts -->
   <script src="<?php echo base_url(). "assets" ?>/js/jquery-3.1.1.min.js"></script>
   <script src="<?php echo base_url(). "assets" ?>/js/bootstrap.min.js"></script>
   <script src="<?php echo base_url(). "assets" ?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
   <script src="<?php echo base_url(). "assets" ?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

   <!-- Custom and plugin javascript -->
   <script src="<?php echo base_url(). "assets" ?>/js/inspinia.js"></script>
   <script src="<?php echo base_url(). "assets" ?>/js/plugins/pace/pace.min.js"></script>

   <!-- Steps -->
   <script src="<?php echo base_url(). "assets" ?>/js/plugins/steps/jquery.steps.min.js"></script>
   <!-- Jquery Validate -->
   <script src="<?php echo base_url(). "assets" ?>/js/plugins/validate/jquery.validate.min.js"></script>


</head>
