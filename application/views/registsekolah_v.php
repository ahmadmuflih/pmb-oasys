
  <link href="<?php echo base_url(). "assets" ?>/radio-style.css" rel="stylesheet">

  <body class="gray-bg">
    <div class="loginColumns animated fadeInDown">
      <div class="row">
          <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                            <h2>
                                Form Registrasi Sekolah
                            </h2>
                            <p>
                                Silahkan lakukan pendaftaran sekolah dengan mengisi form di bawah, pastikan data yang diisi telah sesuai.
                            </p>
                            <form id="form" class="wizard-big">
                                <!-- <h1>Pilih Jalur</h1>
                                <fieldset>
                                    <h2>Silahkan Pilih Jalur</h2>
                                    <div class="row">
                                      <div class="col-lg-6">
                                          <div class="radio">
                                              <label class="col-xs-12">
                                                  <input type="radio" name="periodedaftar" id="optionsRadios1" value="20171/1/1/1/1" checked="">
                                                  <div class="box-jalur">
                                                      <div class="row">
                                                          <div class="col-md-12">
                                                              <h4 class="jalur-header">
                                                                  MANDIRI<small class="text-header text-brand">GRATIS</small>
                                                                </h4>
                                                              <p class="jalur-sub-header">Pendaftaran periode 2017</p>
                                                          </div>
                                                          <div class="col-xs-6">
                                                              <div class="jalur-attr">Mulai Pendaftaran</div>
                                                              <div class="jalur-value">1 Mei 2017</div>
                                                          </div>
                                                          <div class="col-xs-6">
                                                              <div class="jalur-attr">Akhir Pendaftaran</div>
                                                              <div class="jalur-value">16 Mei 2018</div>
                                                          </div>
                                                          <div class="col-xs-6">
                                                              <div class="jalur-attr">Periode Pendaftaran</div>
                                                              <div class="jalur-value">2017/2018 Gasal</div>
                                                          </div>
                                                          <div class="col-xs-6">
                                                              <div class="jalur-attr">Gelombang</div>
                                                              <div class="jalur-value">Gelombang 1</div>
                                                          </div>
                                                          <div class="col-xs-6">
                                                              <span class="jalur-attr">Sistem Kuliah</span><br>
                                                              <span class="jalur-value">Reguler A</span>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </label>
                                              </div>
                                            </div>
                                      <div class="col-lg-6">
                                                <div class="radio">
                                                    <label class="col-xs-12">
                                                        <input type="radio" name="periodedaftar" id="optionsRadios1" value="20171/1/1/1/1" checked="">
                                                        <div class="box-jalur">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h4 class="jalur-header">
                                                                        MANDIRI<small class="text-header text-brand">GRATIS</small>
                                                                      </h4>
                                                                    <p class="jalur-sub-header">Pendaftaran periode 2017</p>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <div class="jalur-attr">Mulai Pendaftaran</div>
                                                                    <div class="jalur-value">1 Mei 2017</div>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <div class="jalur-attr">Akhir Pendaftaran</div>
                                                                    <div class="jalur-value">16 Mei 2018</div>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <div class="jalur-attr">Periode Pendaftaran</div>
                                                                    <div class="jalur-value">2017/2018 Gasal</div>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <div class="jalur-attr">Gelombang</div>
                                                                    <div class="jalur-value">Gelombang 1</div>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <span class="jalur-attr">Sistem Kuliah</span><br>
                                                                    <span class="jalur-value">Reguler A</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </label>
                                                    </div>
                                                  </div>
                                    </div>
                                </fieldset> -->
                                <h1>Profile</h1>
                                <fieldset>
                                    <h2>Informasi Biodata</h2>
                                    <div class="row">
                                      <div class="col-lg-6">
                                          <div class="form-group">
                                            <label>NPSN/NSS *</label>
                                            <input name="npsn" type="number" min="0" class="form-control required">
                                          </div>
                                          <div class="form-group">
                                            <label>Nama Sekolah *</label>
                                            <div class="input-group m-b">
                                            <div class="input-group-btn">
                                                <select class="dropdown btn btn-white dropdown-toggle">
                                                  <option value="1">SMA</option>
                                                  <option value="2">SMK</option>
                                                  <option value="3">MA</option>
                                                  <option value="4">MAK</option>
                                                </select>
                                            </div>
                                             <input type="text" class="form-control required"></div>
                                          </div>
                                          <div class="form-group">
                                              <label>Provinsi *</label>
                                              <select name="provinsi" type="text" class="form-control required">
                                                <option value="0" selected disabled>-Pilih Provinsi-</option>
                                                <option value="1">Jawa Barat</option>
                                              </select>
                                          </div>
                                          <div class="form-group">
                                              <label>Kab/Kota *</label>
                                              <select name="kab" type="text" class="form-control required">
                                                <option value="0" selected disabled>-Pilih Kabupaten/Kota-</option>
                                                <option value="1">Bandung (Kab)</option>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Alamat *</label>
                                            <input name="alamat" type="text" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Kontak *</label>
                                            <input name="kontak" type="number" min="0" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>Email *</label>
                                            <input name="email" type="text" class="form-control required email">
                                        </div>
                                      </div>
                                    </div>
                                </fieldset>
                                <h1>Akreditasi</h1>
                                <fieldset>
                                  <div class="row">
                                    <div class="col-lg-8">
                                      <div class="form-group">
                                          <label>Akreditasi *</label>
                                          <select class="form-control required" id="akreditasi">
                                            <option value="1" selected disabled>-Pilih Akreditasi-</option>
                                            <option value="2">A</option>
                                            <option value="3">B</option>
                                            <option value="4">C</option>
                                            <option value="5">Lainnya</option>
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label>Upload File AKreditasi *</label>
                                          <input name="kontak" type="file" accept="application/pdf" class="form-control required">
                                      </div>
                                    </div>
                                  </div>
                                </fieldset>
                                <h1>Finish</h1>
                                <fieldset>
                                    <h2>Syarat dan Ketentuan</h2>
                                    <p>Dengan ini data yang telah diisi telah sesuai dan sesuai dengan ketentuan yang berlaku</p>
                                    <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms">Saya setuju dengan syarat dan ketentuan.</label>
                                </fieldset>
                            </form>
                </div>
              </div>
            </div>
      </div>
    </div>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
  </body>
</html>
