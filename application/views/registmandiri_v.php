  <body class="gray-bg">
    <div class="loginColumns animated fadeInDown">
      <div class="row">
          <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                  <style media="screen">
                  hr {
                        display: block;
                        height: 1px;
                        border: 0;
                        border-top: 1px solid #ccc;
                        margin: 1em 0;
                        padding: 0;
                      }
                  </style>
                            <h2>
                                Form Registrasi Akun Ujian Masuk Mandiri
                            </h2>
                            <p>
                                Silahkan lakukan pendaftaran sekolah dengan mengisi form di bawah, pastikan data yang diisi telah sesuai.
                            </p>
                            <form id="form" class="wizard-big">
                                <h1>Biodata Siswa</h1>
                                <fieldset>
                                    <h2>Informasi Biodata</h2>
                                    <div class="row">
                                      <div class="col-lg-12">
                                          <div class="form-group">
                                              <label>Nama Lengkap *</label>
                                              <input name="nama" type="text" class="form-control required">
                                          </div>
                                          <div class="form-group">
                                              <label>No. KTP/ No. KK *</label>
                                              <input name="noktp" type="number" min="0" class="form-control required">
                                          </div>
                                          <div class="form-group">
                                              <label>Umur *</label>
                                              <input name="umur" type="number" min="0" class="form-control required">
                                          </div>
                                          <div class="form-group">
                                              <label>Jenis Kelamin *</label>
                                              <select name="jk" type="number" min="0" class="form-control required">
                                                <option value="0" disabled selected>-Pilih Jenis Kelamin</option>
                                                <option value="1">Laki-Laki</option>
                                                <option value="2">Perempuan</option>
                                              </select>
                                          </div><hr>
                                        <h4>Asal Siswa</h4>
                                        <div class="form-group">
                                            <label>Provinsi *</label>
                                            <select name="prov" type="number" min="0" class="form-control required">
                                              <option value="0" disabled selected>-Pilih Provinsi-</option>
                                              <option value="1">Kalimantan Selatan</option>
                                              <option value="2">Kalimantan Barat</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Kabupaten/Kota *</label>
                                            <select name="kb" type="number" min="0" class="form-control required">
                                              <option value="0" disabled selected>-Pilih Kabupaten-</option>
                                              <option value="1">Kab 1</option>
                                              <option value="2">Kab 2</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat *</label>
                                            <input name="alamat" type="text" class="form-control required">
                                        </div><hr>
                                        <div class="form-group">
                                            <label>Nomor Kontak *</label>
                                            <input name="kontak" type="number" min="0" class="form-control required">
                                        </div>
                                        <div class="form-group">
                                            <label>Email *</label>
                                            <input name="email" type="text" class="form-control required email">
                                        </div>
                                    </div>
                                </fieldset>
                                <h1>Biodata Sekolah</h1>
                                <fieldset>
                                    <h2>Informasi Sekolah</h2>
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <div class="form-group">
                                        <label>Nama Sekolah *</label>
                                        <div class="input-group m-b">
                                        <div class="input-group-btn">
                                            <select class="dropdown btn btn-white dropdown-toggle">
                                              <option value="0">-</option>
                                              <option value="1">SMA</option>
                                              <option value="2">SMK</option>
                                              <option value="3">MA</option>
                                              <option value="4">MAK</option>
                                            </select>
                                        </div>
                                        <input type="text" class="form-control required">
                                       </div>
                                      </div>
                                      <div class="form-group">
                                          <label>Provinsi Sekolah *</label>
                                          <select name="prov" type="number" min="0" class="form-control required">
                                            <option value="0" disabled selected>-Pilih Provinsi-</option>
                                            <option value="1">Kalimantan Selatan</option>
                                            <option value="2">Kalimantan Barat</option>
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label>Kabupaten/Kota Sekolah *</label>
                                          <select name="kb" type="number" min="0" class="form-control required">
                                            <option value="0" disabled selected>-Pilih Kabupaten-</option>
                                            <option value="1">Kab 1</option>
                                            <option value="2">Kab 2</option>
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label>Alamat Sekolah *</label>
                                          <input name="alamat" type="text" class="form-control required">
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group">
                                          <label>Jurusan SMA/SMK/MA *</label>
                                          <select name="jur" type="number" min="0" class="form-control required">
                                            <option value="0" disabled selected>-Pilih Jurusan Sekolah-</option>
                                            <option value="1">IPA</option>
                                            <option value="2">IPS</option>
                                            <option value="3">Bahasa</option>
                                            <option value="4">Lainnya</option>
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label>Tahun Lulus *</label>
                                          <select name="lulus" type="number" min="0" class="form-control required">
                                            <option value="0" disabled selected>-Pilih Tahun Lulus-</option>
                                            <option value="1">2018</option>
                                            <option value="2">2017</option>
                                            <option value="3">2016</option>
                                            <option value="4">2015</option>
                                          </select>
                                      </div>
                                      <div class="form-group">
                                          <label>Nomor Ijazah *</label>
                                          <input name="noijazah" type="text" class="form-control required">
                                      </div>
                                      <div class="form-group">
                                          <label>Nilai Rata2 UN *</label>
                                          <input name="nilaiun" type="number" min="0" max="999" class="form-control required">
                                      </div>
                                    </div>
                                    <div class="col-lg-12">
                                      <hr>
                                      <div class="form-group">
                                          <label>Piagam yang Dimiliki</label>
                                          <input name="piagam" type="file" accept="application/pdf,image/jpeg,image/x-png" class="form-control" multiple>
                                          <small style="color:green;">NB: Anda bisa mengunggah file lebih dari 1</small>
                                      </div>
                                    </div>
                                  </div>
                                </fieldset>
                                <h1>Biodata Orang Tua</h1>
                                <fieldset>
                                    <h2>Informasi Orang Tua/Wali</h2>
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <div class="form-group">
                                          <label>Nama Ayah *</label>
                                          <input name="nama1" type="text" class="form-control required">
                                      </div>
                                      <div class="form-group">
                                          <label>Pekerjaan Ayah *</label>
                                          <input name="pekerjaan1" type="text" class="form-control required">
                                      </div>
                                      <div class="form-group">
                                          <label>Alamat Ayah *</label>
                                          <input name="alamat1" type="text" class="form-control required">
                                      </div><hr>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-group">
                                          <label>Nama Ibu *</label>
                                          <input name="nama2" type="text" class="form-control required">
                                      </div>
                                      <div class="form-group">
                                          <label>Pekerjaan Ibu *</label>
                                          <input name="pekerjaan2" type="text" class="form-control required">
                                      </div>
                                      <div class="form-group">
                                          <label>Alamat Ibu *</label>
                                          <input name="alamat2" type="text" class="form-control required">
                                      </div><hr>
                                    </div>
                                    <div class="col-lg-12">
                                      <div class="form-group">
                                          <label>Telp Rumah/HP *</label>
                                          <input name="telp" type="number" min="0" class="form-control required">
                                      </div>
                                      <div class="form-group">
                                          <label>Rata2 Penghasilan Perbulan*</label>
                                          <select name="alamat1" type="text" class="form-control required">
                                            <option value="0" selected disabled>-Pilih Penghasilan Perbulan Orang Tua</option>
                                            <option value="1">>Rp 5.000.000</option>
                                            <option value="2">Rp 5.000.000 - Rp 2.500.000</option>
                                            <option value="2">Rp 2.500.000 <</option>
                                          </select>
                                      </div><br><hr>
                                      <div class="form-group">
                                          <label>Nama Wali *</label>
                                          <input name="nama3" type="text" class="form-control required">
                                      </div>
                                      <div class="form-group">
                                          <label>Pekerjaan Wali *</label>
                                          <input name="pekerjaan3" type="text" class="form-control required">
                                      </div>
                                      <div class="form-group">
                                          <label>Alamat Wali *</label>
                                          <input name="alamat3" type="text" class="form-control required">
                                      </div>
                                    </div>
                                  </div>
                                </fieldset>
                                <h1>Selesai</h1>
                                <fieldset>
                                    <h2>Syarat dan Ketentuan</h2>
                                    <p>Dengan ini data yang telah diisi telah sesuai dan sesuai dengan ketentuan yang berlaku</p>
                                    <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms">Saya setuju dengan syarat dan ketentuan.</label>
                                </fieldset>
                            </form>
                </div>
              </div>
            </div>
      </div>
    </div>

    <!-- <script type="text/javascript">
    jQuery.extend(jQuery.validator.messages, {
          required: "This field is required.",
          remote: "Please fix this field.",
          email: "Please enter a valid email address.",
          url: "Please enter a valid URL.",
          date: "Please enter a valid date.",
          dateISO: "Please enter a valid date (ISO).",
          number: "Please enter a valid number.",
          digits: "Please enter only digits.",
          creditcard: "Please enter a valid credit card number.",
          equalTo: "Please enter the same value again.",
          accept: "Please enter a value with a valid extension.",
          maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
          minlength: jQuery.validator.format("Please enter at least {0} characters."),
          rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
          range: jQuery.validator.format("Please enter a value between {0} and {1}."),
          max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
          min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
        });
    </script> -->
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
  </body>
</html>
