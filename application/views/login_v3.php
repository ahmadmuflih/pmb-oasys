
<body class="gray-bg top-navigation">
<div class="layer">

</div>
  <!-- <div class="row border-bottom white-bg">
  </div> -->
  <center>
  <nav class="navbar" role="navigation">
    <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed btn-whatever" type="button">
                    <i class="fa fa-reorder"></i>
                </button>
            </div>
    <br>
    <style>
    @media only screen and (max-width: 1023px) {
        .pmb-logo{
            display:none !important;
        }
    }
    @media only screen and (min-width: 1024px) {
        .pmb-logo2{
            display:none !important;
        }
        .disable-column{
            display:none !important;
        }
    }
    </style>
    <div class="col-md-offset-1 col-lg-11">
      <div class="col-md-5">
        <div class="col-md-3 disable-column">
            <img class="pmb-logo2" style="width:25%" src="<?php echo base_url()."assets" ?>/img/logounismuh.png" alt="logo_umpalu">
        </div>
        <div class="col-md-9">
        <h1 style="color:white;">
        <img class="pmb-logo" style="width:14%;margin-top:-10px;" src="<?php echo base_url()."assets" ?>/img/logounismuh.png" alt="logo_umpalu">
        PMB UNISMUH PALU</h1>
        </div>
      </div>
      <style media="screen">
        .aktif{
          background-color: transparent !important;
        }
        .top-navigation .nav > li > a {
            color: #FFF;
            background-color: transparent !important;
        }
        .top-navigation .nav > li.aktif > a,a:hover {
            color: #8064A2 !important;
            background-color: transparent !important;
        }
        .top-navigation .nav > li > a,a:visited {
            /* color: #676a6c !important; */
            background-color: transparent !important;
        }
        @media only screen and (min-width: 1030px) {
          .content-margin{
            margin-top: -20px !important;
          }
          .custom-layout{
            margin-top:100px !important;
        }
        }
        .layer {
          background-color: rgba(155, 193, 255, 0.4);
          position:fixed !important;
          width: 100%;
          height: 100%;
      }
      /* .white-a>li>a{
          color:#ffffff;
      } */
      
      </style>
      <div class="col-md-offset-1 col-md-6">
        <div class="navbar-collapse collapse" id="navbar">

          <ul class="nav navbar-nav navbar-custom">
          <li class="aktif">
              <a aria-expanded="false" role="button" href="layouts.html"> BERANDA</a>
          </li>
          <li>
              <a aria-expanded="false" role="button" href="http://unismuhpalu.ac.id/"> UNISMUH PALU</a>
          </li>
          <li>
              <a aria-expanded="false" role="button" data-toggle="modal" data-target="#contactModal"> KONTAK KAMI</a>
          </li>
          <li>
              <a aria-expanded="false" role="button" href="http://unismuhpalu.ac.id/penerimaan-calon-mahasiswa-i-baru/"> BANTUAN</a>
          </li>
        </ul>
        </div>
      </div>
    </div>
    <!-- <img src="img/s5_logo.png" alt="logo"> -->
  </nav>
</center>
    <div class="col-lg-12 style-box animated fadeInDown custom-layout">
        <div class="row content-margin">
            <div class="col-md-offset-1 col-md-6">
              <div class="carousel slide" id="carousel2">
                                <ol class="carousel-indicators">
                                    <li data-slide-to="0" data-target="#carousel2"  class="active"></li>
                                    <li data-slide-to="1" data-target="#carousel2"></li>
                                    <li data-slide-to="2" data-target="#carousel2" class=""></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img alt="image"  class="img-responsive" src="<?php echo base_url()."assets" ?>/img/slide1.png">
                                        <div class="carousel-caption">
                                            <p>PMB UNISMUH PALU</p>
                                        </div>
                                    </div>
                                    <div class="item ">
                                        <img alt="image"  class="img-responsive" src="<?php echo base_url()."assets" ?>/img/slide2.png">
                                        <div class="carousel-caption">
                                            <p>Kegiatan Wisuda Universitas Muhammadiyah Palu Tahun 2018</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <img alt="image"  class="img-responsive" src="<?php echo base_url()."assets" ?>/img/slide3.png">
                                        <div class="carousel-caption">
                                            <p>Kegiatan Wisuda Universitas Muhammadiyah Palu Tahun 2018</p>
                                        </div>
                                    </div>
                                </div>
                                <a data-slide="prev" href="#carousel2" class="left carousel-control">
                                      <span class="icon-prev"></span>
                                </a>
                                <a data-slide="next" href="#carousel2" class="right carousel-control">
                                    <span class="icon-next"></span>
                                </a>
                            </div>
            </div>
            <div class="col-md-4">
              <!-- <div class="ibox-title">
              </div> -->

                <div class="ibox-content">
                  <ul class="nav nav-tabs" role="tablist">
                  <li class="active full-tabs"><a class="nav-whatever" class="nav-whatever" data-toggle="tab" href="#home"><i class="fa fa-user-o"></i>LOGIN</a></li>
                  <li class="full-tabs"><a data-toggle="tab" href="#menu1" class="nav-whatever"><i class="fa fa-edit"></i> REGISTRASI</a></li>
                </ul>
                  <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                    <br>
                    <h4 class="card-title">Form Login Sistem</h4>
                    
                    <form class="m-t" id="form">
                        <div class="form-group">
                            <input type="text" name="username" id="username" class="form-control form-whatever" placeholder="Username" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control form-whatever" placeholder="Password" required="">
                        </div>
                        <button type="submit" name="masuk" class="btn btn-whatever block full-width m-b">Login</button>

                    </form>
                  </div>
                  <div id="menu1" class="tab-pane fade">
                    <br>
                    <h4 class="card-title">Form Registrasi</h4>
                    <form  id="formregister" class="m-t">
                        <div class="form-group">
                            <input type="text" name="nama" class="form-control form-whatever" placeholder="Nama Lengkap" required="">
                        </div>
                        <div class="form-group">
                            <input type="number" name="no_telp" min="0" class="form-control form-whatever" placeholder="Nomor Handphoone" required="">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control form-whatever" placeholder="Email" required="">
                        </div>
                        <div class="form-group">
                            <!-- <input type="email" name="email" class="form-control form-whatever" placeholder="Email" required=""> -->
                            <select name="id_batch" id="" class="form-control form-whatever">
                                <option value="0" disabled>-PILIH BATCH-</option>
                                <?php foreach($batch->result() as $row){
                                    echo "<option value='$row->id'>$row->periode - $row->nama</option>";
                                }?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-whatever block full-width m-b">Buat Akun</button>

                    </form>
                  </div>
                </div>

                    <p class="m-t">
                        <small>Sistem Informasi PMB UNISMUH PALU &copy; 2018</small>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
          <hr/>
            <div class="col-md-offset-1 col-md-6" style="color:white">
                Copyright UNIVERSITAS MUHAMMADIYAH PALU
            </div>
            <div class="col-md-4 text-right">
               <small style="color:white;">© 2019</small>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <!-- <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script> -->

    <div class="modal inmodal fade" id="contactModal" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title">Kontak Kami</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td><i class="fa fa-map-marker"></i></td>
                                                        <td><b>Jalan Jabal Nur, No.1 Kota Palu, Provinsi Sulawesi Tengah</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fa fa-phone"></i></td>
                                                        <td><b>0451-425627</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fa fa-envelope"></i></td>
                                                        <td><b>contact@unismuhpalu.ac.id</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-info" data-dismiss="modal">Mengerti</button>
                                        </div>
                                    </div>
                                </div>
                            </div>



</body>



</html>
<script>
var ip, browser;
$.getJSON("http://jsonip.com?callback=?", function (data) {
    ip = data.ip;
});
browser = navigator.userAgent;
$('#form').submit(function(){
    var formData = new FormData;
    var username = $('#username').val();
    var password = $('#password').val();

    formData.append('username',username);
    formData.append('password',password);
    formData.append('ip',ip);
    formData.append('browser',browser);
    $.ajax({
      url: '<?php echo base_url("Login/login");?>',
      data: formData,
      type: 'POST',
      // THIS MUST BE DONE FOR FILE UPLOADING
      contentType: false,
      processData: false,
      dataType: "JSON",
      success: function(data){
        console.log(data.message);
        if(data.status=='berhasil'){
          location.reload();
          //alert(data);
        }
      else if(data.status=='welcome')
        location.href = '<?php echo base_url().'welcome' ?>';
      else if(data.status=='reset')
          location.href = '<?php echo base_url().'reset' ?>';
      else
          swal("Gagal!", data.message, "error");
      },
    error: function(jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
    }
          });

    return false;
  });
  $('#formregister').submit(function(){

var form = $('#formregister')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
$.ajax({
  url: '<?php echo base_url("Login/register");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
      $('#formregister')[0].reset(); 
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})

return false;
});
</script>
