<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admission</title>
    <link rel="icon" href="<?php echo base_url(). "assets/bootstrap4/images/favicon.png" ?>" type="image/png" sizes="16x16">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Label Multipurpose Startup Business Template">
    <meta name="keywords" content="Label HTML Template, Label Startup Business Template, Startup Template">
    <link href="<?php echo base_url(). "assets/bootstrap4/css/bootstrap.min.css" ?>" rel="stylesheet" type="text/css" media="all" />
    <!-- <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"> -->
    <link type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600%7COpen+Sans:400%7CVarela+Round" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(). "assets/bootstrap4/css/animate.css" ?>"> <!-- Resource style -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(). "assets/bootstrap4/css/owl.carousel.css" ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(). "assets/bootstrap4/css/owl.theme.css" ?>">
    <link type="text/css" href="<?php echo base_url(). "assets/css/animate.css" ?>" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(). "assets/bootstrap4/css/jquery.accordion.css" ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(). "assets/bootstrap4/css/magnific-popup.css" ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(). "assets/bootstrap4/css/ionicons.min.css" ?>"> <!-- Resource style -->
    <link href="<?php echo base_url(). "assets/bootstrap4/css/style.css" ?>" rel="stylesheet" type="text/css" media="all" />
    <link type="text/css" href="<?php echo base_url()."assets" ?>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- <link rel="stylesheet" type="text/css" href="<//?php echo base_url()."assets" ?>/tooltipster/dist/css/tooltipster.bundle.min.css" /> -->
    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url()."assets" ?>/img/favoasys.png"/>

    <!-- tour -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(). "assets/bootstrap4/css/bootstrap-tour.min.css" ?>">

     <!-- Jquery and Js Plugins -->
     <script src="<?php echo base_url(). "assets/bootstrap4/js/jquery-2.1.1.js" ?>"></script>
    <script src="<?php echo base_url(). "assets/bootstrap4/js/popper.min.js" ?>"></script>
    <script src="<?php echo base_url(). "assets/bootstrap4/js/bootstrap.min.js" ?>"></script>
    <script src="<?php echo base_url(). "assets/bootstrap4/js/jquery.validate.min.js" ?>"></script>
    <script src="<?php echo base_url(). "assets/bootstrap4/js/plugins.js" ?>"></script>
    <script src="<?php echo base_url(). "assets/bootstrap4/js/jquery.accordion.js" ?>"></script>
    <script src="<?php echo base_url(). "assets/bootstrap4/js/validator.js" ?>"></script>
    <script src="<?php echo base_url(). "assets/bootstrap4/js/contact.js" ?>"></script>
    <script src="<?php echo base_url(). "assets/bootstrap4/js/custom.js" ?>"></script>
    <!-- sweealert -->
    <script src="<?php echo base_url()."assets" ?>/js/plugins/sweetalert/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>

    <!-- tour -->
    <script src="<?php echo base_url(). "assets/bootstrap4/js/bootstrap-tour.min.js" ?>"></script>
    <script>
    new WOW().init();
    </script>
  </head>