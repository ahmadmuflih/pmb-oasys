
  <!-- content -->
  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Konfigurasi Rekening</h2>
                </div>
                <div class="col-sm-8">
                </div>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="ibox-title">
                      <h4>Pengaturan Rekening</h4>
                    </div>
                        <div class="ibox-content col-md-12">
                        <form id="form" class="form">
                          <?php
                            foreach($config->result() as $row){
                              echo "<div class='form-group col-md-12'>";
                              echo "<label>$row->keterangan</label> <input type='text' placeholder='$row->placeholder' class='form-control' value='$row->value' name='$row->nama'>";
                              echo "</div>";
                            }
                          ?>
                        <center>
                            <button type="submit" class="btn btn-primary" title="SIMPAN KONFIGURASI"><span class="fa fa-save"></span> SIMPAN</button>
                          </center>
                        </div>
                        </form>
                  </div>
                </div>
          </div>
<script>
$('#form').submit(function(){
var form = $('#form')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
$.ajax({
  url: '<?php echo base_url("adminpmb/Konfigrekening/insert");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})
return false;
});
</script>