<div id="wrapper">

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="<?php echo base_url(). "assets/images/profile/admin-pf.png" ?>" width="70px" />
                         </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $_SESSION['data']['nama']; ?></strong>
                        </span> <span class="text-muted text-xs block">Admin PMB <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <!-- <li><a href="profile.html">Profile</a></li> -->
                        <!-- <li class="divider"></li> -->
                        <li><a href="<?php echo base_url()."logout" ?>">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    PMB
                </div>
            </li>
            <li <?php if(substr($page,0,1)=='1')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."adminpmb/home"?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
            </li>
            <li <?php if(substr($page,0,1)=='2')echo "class='active'"; ?>>
                <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Akun</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li <?php if(substr($page,0,2)=='21')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/ubahpassword"?>">Ubah Password</a>
                  </li>
                  <!-- <li <?php if(substr($page,0,2)=='22')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/riwayatlogin"?>">Riwayat Login</a>
                  </li> -->
                </ul>
            </li>
            <li <?php if(substr($page,0,1)=='3')echo "class='active'"; ?>>
                <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Master Data</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <!-- <li>
                    <a href="<?php echo base_url()."adminpmb/datasekolah"?>">Data Sekolah</a>
                  </li> -->
                  <li <?php if(substr($page,0,2)=='31')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/dataakun"?>">Data Akun</a>
                  </li>
                  <li <?php if(substr($page,0,2)=='32')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/dataregistrasi"?>">Data Registrasi</a>
                  </li>
                  <li <?php if(substr($page,0,2)=='33')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/dataujian"?>">Data Ujian</a>
                  </li>
                  <li <?php if(substr($page,0,2)=='34')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/databiaya"?>">Master Biaya Pendaftaran</a>
                  </li>
                  <li <?php if(substr($page,0,2)=='35')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/periodepmb"?>">Periode PMB</a>
                  </li>
                </ul>
            </li>
            <li <?php if(substr($page,0,1)=='4')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."adminpmb/dataseleksi"?>"><i class="fa fa-bullhorn"></i> <span class="nav-label">Seleksi Pendaftar</span></a>
            </li>
            <li <?php if(substr($page,0,1)=='5')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."adminpmb/heregistrasi"?>"><i class="fa fa-users"></i> <span class="nav-label">Heregistrasi</span></a>
            </li>
            <li <?php if(substr($page,0,1)=='6')echo "class='active'"; ?>>
                <a href="#"><i class="fa fa-gear"></i> <span class="nav-label">Konfigurasi</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li <?php if(substr($page,0,2)=='61')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/konfigrekening"?>">Konfigurasi Rekening</a>
                  </li>
                  <li <?php if(substr($page,0,2)=='62')echo "class='active'"; ?>>
                    <a href="<?php echo base_url()."adminpmb/konfigregistrasi"?>">Konfigurasi Registrasi</a>
                  </li>
                </ul>
            </li>
            <!-- <li <?php if(substr($page,0,1)=='6')echo "class='active'"; ?>>
                <a href="<?php echo base_url()."adminpmb/konfigurasi"?>"><i class="fa fa-gear"></i> <span class="nav-label">Konfigurasi</span></a>
            </li> -->
            <!-- <li>
                <a href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label">Bidik Misi</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="#">Prestasi Sekolah</a>
                    </li>
                    <li>
                        <a href="#">Hasil UN Sekolah</a>
                    </li>
                    <li>
                        <a href="#">Siswa Rekomendasi</a>
                    </li>
                </ul>
            </li> -->
            <!-- <li>
                <a href="#"><i class="fa fa-trophy"></i> <span class="nav-label">Pengaturan</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="#">Siswa Rekomendasi</a>
                    </li>
                </ul>
            </li> -->
        </ul>

    </div>
</nav>
