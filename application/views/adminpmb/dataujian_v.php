
  <!-- content -->
  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Data Ujian <b>Periode PMB <?php echo $_SESSION['batch']['periode']." - ".$_SESSION['batch']['nama'] ?></b></h2>
                </div>
                <div class="col-sm-6">
                    <div class="title-action">
                      <a href="#" data-toggle="modal" data-target="#importModal" class="btn btn-info"><i class="fa fa-download"></i> Import Peserta Ujian</a>
                      <button onclick="exportData()"  class="btn btn-warning"><i class="fa fa-upload"></i> Export Peserta Ujian</button>
                        <a href="#" data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
            <div class="ibox-title">

            </div>
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table id="mytable" class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Ujian</th>
                        <th>Tanggal Ujian</th>
                        <th>Ruangan Ujian</th>
                        <th>Jam</th>
                        <th>Peserta</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <!-- <tbody>
                      <tr>
                        <td>1</td>
                        <td>123456789</td>
                        <td>SENIN, 31/01/2019</td>
                        <td>Ruangan A012</td>
                        <td>09:00-10:00</td>
                        <td>
                          <center>
                          <a href="#" data-toggle="modal" data-target="#pesertaModal" class='btn btn-info btn-xs' title='Data Peserta Ujian'><span class='fa fa-users'></span></a>
                          <a href="#" data-toggle="modal" data-target="#editModal" class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></a>
                          <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                          </center>
                        </td>
                      </tr>
                    </tbody> -->
                  </table>
                </div>
              </div>
            </div>
          </div>

          <style>
            .popover.clockpicker-popover{
                  z-index: 9999 !important;
              }
          </style>

            <div class="modal inmodal fade" id="addModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Tambah Data Ujian</h4>
                        </div>
                        <div class="modal-body">
                        <form id="form" class="form">
                          <div class="form-group"><label>No. Ujian</label> <input name="no_ujian" type="text" placeholder="Masukkan No Ujian" class="form-control" required></div>
                          <div class="form-group"><label>Tanggal Ujian</label> <input name="tanggal_ujian" type="text" placeholder="Masukkan Tanggal Ujian" id="datepicker" class="form-control" required></div>
                          <div class="form-group"><label>Gedung Ujian</label>
                            <select class="form-control" id="gedung1">
                              <option value="0" selected disabled>-PILIH Gedung-</option>
                              <?php
                                foreach($gedung->result() as $row){
                                  echo "<option value='$row->id'>$row->kode - $row->nama</option>";
                                }
                              ?>
                            </select>
                          </div>
                          <div class="form-group"><label>Ruang Ujian</label>
                            <select class="form-control ruangan" name="id_ruangan" id="ruangan1">
                              <option value="0" selected disabled>-PILIH RUANGAN-</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Waktu Tes</label>
                            <div class="row">
                              <div class="col-sm-6">
                                <input type="text" placeholder="Masukkan Jam Mulai" class="form-control clockpicker" name="jam_mulai" readonly required>
                              </div>
                              <div class="col-sm-6">
                                <input type="text" placeholder="Masukkan Jam Selesai" class="form-control clockpicker" name="jam_selesai" readonly required>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="editModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit Data Ujian</h4>
                        </div>
                        <div class="modal-body">
                        <form id="formEdit" class="form">
                          <div class="form-group"><label>No. Ujian</label> <input name="no_ujian" type="text" placeholder="Masukkan No Ujian" class="form-control" id="no_ujian" required></div>
                          <div class="form-group"><label>Tanggal Ujian</label> <input name="tanggal_ujian" type="text" placeholder="Masukkan Tanggal Ujian" id="datepicker2" class="form-control" required></div>
                          <div class="form-group"><label>Gedung Ujian</label>
                            <select class="form-control gedung" id="gedung2">
                              <option value="0" selected disabled>-PILIH Gedung-</option>
                              <?php
                                foreach($gedung->result() as $row){
                                  echo "<option value='$row->id'>$row->kode - $row->nama</option>";
                                }
                              ?>
                            </select>
                          </div>
                          <div class="form-group"><label>Ruang Ujian</label>
                            <select class="form-control ruangan" name="id_ruangan" id="ruangan2">
                              <option value="0" selected disabled>-PILIH RUANGAN-</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Waktu Tes</label>
                            <div class="row">
                              <div class="col-sm-6">
                                <input type="text" placeholder="Masukkan Jam Mulai" class="form-control clockpicker" name="jam_mulai" id="jam_mulai" readonly required>
                              </div>
                              <div class="col-sm-6">
                                <input type="text" placeholder="Masukkan Jam Selesai" class="form-control clockpicker" name="jam_selesai" id="jam_selesai" readonly required>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="importModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Import Peserta Ujian</h4>
                        </div>
                        <div class="modal-body">
                        <form id="formImport" class="form">
                          <div class="form-group"><label>Import Data </label> <input type="file" name="import" id="import" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" placeholder="Import Data Peserta Ujian"class="form-control" required>
                          <small style="color:#fc5e55">*File harus dalam format xlxs/xls/csv</small>
                          </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button id="btnImport" type="submit" class="btn btn-primary">Import</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="pesertaModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Data Peserta Ujian</h4>
                        </div>
                        <div class="modal-body">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th width="5%">No</th>
                                <th>No. Reg.</th>
                                <th>Nama</th>
                                <th>Pilihan 1</th>
                                <th>Pilihan 2</th>
                              </tr>
                            </thead>
                            <tbody id="data_peserta">

                            </tbody>
                          </table>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <!-- <button type="button" class="btn btn-primary">Simpan</button> -->
                        </div>
                    </div>
                </div>
            </div>

            <script>
              var id_ujian,id_ruangan=0;
              $('.clockpicker').clockpicker({
                minutestep:5,
                autoclose:true
              });
              $(document).ready(function() {
              $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                        {
                            return {
                                "iStart": oSettings._iDisplayStart,
                                "iEnd": oSettings.fnDisplayEnd(),
                                "iLength": oSettings._iDisplayLength,
                                "iTotal": oSettings.fnRecordsTotal(),
                                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                            };
                        };

                        t = $("#mytable").DataTable({
                            initComplete: function() {
                                var api = this.api();
                                $('#mytable_filter input')
                                        .off('.DT')
                                        .on('keyup.DT', function(e) {
                                            if (e.keyCode == 13) {
                                                api.search(this.value).draw();
                                    }
                                });
                            },
                            oLanguage: {
                                sProcessing: "loading..."
                            },
                            processing: true,
                            serverSide: true,
                            select: true,
                            ajax: {"url": "<?php echo base_url("adminpmb/Dataujian/json");?>", "type": "POST"},
                            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
                ],
                            columns: [
                                {
                                    "data": "id",
                                    "orderable": false
                                },
                                {"data": "no_ujian"},
                                {"data": "tanggal"},
                                {"data": "ruangan"},
                                {"data": "jam"},
                                {"data": "peserta"},
                                {"data": "view"}
                            ],
                            order: [[1, 'asc']],
                            rowCallback: function(row, data, iDisplayIndex) {
                                var info = this.fnPagingInfo();
                                var page = info.iPage;
                                var length = info.iLength;
                                var index = page * length + (iDisplayIndex + 1);
                                $('td:eq(0)', row).html(index);
                            }
                        });


            });
            $('#gedung1').change(function(){
              var id_gedung = $(this).val();
              getRuangan(id_gedung,"ruangan1");
            });
            $('#gedung2').change(function(){
              var id_gedung = $(this).val();
              getRuangan(id_gedung,"ruangan2");
            });

            function getRuangan(id_gedung, ruangan){
              $.ajax({
                  url : "<?php echo site_url('adminpmb/Dataujian/getRuangan')?>",
                  type: "POST",
                  data: {"id_gedung":id_gedung},
                  dataType: "JSON",
                  success: function(data)
                  {
                    if (data.status=='berhasil') {
                      $('#'+ruangan).html(data.data);
                      if(id_ruangan!=0){
                        $('#'+ruangan).val(id_ruangan);
                        id_ruangan=0;
                      }

                    }
                  },
                      error: function (jqXHR, textStatus, errorThrown)
                      {
                        console.log(jqXHR);
                  console.log(textStatus);
                  console.log(errorThrown);
                      }
                });
            }


            $('#form').submit(function(){

            var form = $('#form')[0]; // You need to use standart javascript object here
            var formData = new FormData(form);
            formData.append('id_batch',<?php echo $_SESSION['batch']['id']?>);
            $.ajax({
              url: '<?php echo base_url("adminpmb/Dataujian/insert");?>',
              data: formData,
              type: 'POST',
              // THIS MUST BE DONE FOR FILE UPLOADING
              contentType: false,
              processData: false,
              dataType: "JSON",
              success: function(data){
                if (data.status=='berhasil') {
                  swal("Berhasil!", data.message, "success");
                  $('#form')[0].reset();
                $('#addModal').modal('hide');
                  reload_table();
                }else {
                  swal("Gagal!", data.message, "error");
                }
              },
                  error: function(jqXHR, textStatus, errorThrown)
                  {
              console.log(jqXHR);
              console.log(textStatus);
              console.log(errorThrown);
              alert('gagal');
            }
            })
            return false;
            });
            function reload_table()
            {
            $('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
            }
            
            $('#formEdit').submit(function(){

var form = $('#formEdit')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
formData.append('id_batch',<?php echo $_SESSION['batch']['id']?>);
formData.append('id_ujian',id_ujian);
$.ajax({
  url: '<?php echo base_url("adminpmb/Dataujian/update");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
      $('#formEdit')[0].reset();
    $('#editModal').modal('hide');
      reload_table();
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})
return false;
});
function peserta(id){
  id_ujian = id;
  $.ajax({
      url : "<?php echo site_url('adminpmb/Dataujian/getPeserta')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          $('#data_peserta').html(data.data);
          $('#pesertaModal').modal('show');
        }else {
          swal("Gagal!", data.message, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Gagal!", 'Terjadi kesalahan!', "error");
      }
  });
}
            function edit(id){
              id_ujian = id;
              $.ajax({
                  url : "<?php echo site_url('adminpmb/Dataujian/getUjian')?>",
                  type: "POST",
                  data: {'id':id},
                  dataType: "JSON",
                  success: function(data)
                  {
                    if (data.status=='berhasil') {
                      $('#no_ujian').val(data.data.no_ujian);
                      $('#datepicker2').val(data.data.tanggal_ujian);
                      $('#jam_mulai').val(data.data.jam_mulai);
                      $('#jam_selesai').val(data.data.jam_selesai);
                      id_ruangan = data.data.id_ruangan;
                      $('#gedung2').val(data.data.id_gedung);
                      getRuangan(data.data.id_gedung,"ruangan2");
                      $('#editModal').modal('show');
                    }else {
                      swal("Gagal!", data.message, "error");
                    }
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    swal("Gagal!", 'Terjadi kesalahan!', "error");
                  }
              });

            }

            function hapus(id){
              swal({
                  title: "Anda yakin?",
                  text: "Data ujian akan dihapus",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#1ab394",
                  confirmButtonText: "Ya, hapus!",
                  closeOnConfirm: false
              }, function () {
                $.ajax({
                    url : "<?php echo site_url('adminpmb/Dataujian/delete')?>",
                    type: "POST",
                    data: {'id':id},
                    dataType: "JSON",
                    success: function(data)
                    {
                      if (data.status=='berhasil') {
                        swal("Berhasil!", data.message, "success");
                        reload_table();
                      }else {
                        swal("Gagal!", data.message, "error");
                      }
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      swal("Gagal!", 'Terjadi kesalahan saat menghapus data, Ujian tersebut telah memiliki data!', "error");
                    }
                });

              });

              }
              function exportData(){
                var url = "<?php echo base_url().'adminpmb/Excel/exportUjian?batch='.$_SESSION['batch']['id']?>";
                var win = window.open(url, '_blank');
                win.focus();
              }
              $('#formImport').submit(function(){
            var file_data = $('#import').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('batch',<?php echo $_SESSION['batch']['id'] ?>);


              $('#btnImport').html("Importing..");
              $("#btnImport" ).prop( "disabled", false );
            $.ajax({
                url: '<?php echo base_url("adminpmb/Excel/importUjian/") ?>', // point to server-side PHP script
                dataType:'JSON',
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data){
                    //alert(php_script_response); // display response from the PHP script, if any
                    console.log(data);
                    $('#formImport')[0].reset();
                    swal({
                        title: "Berhasil",
                        text: data.success+"\n"+data.failed
                    });
                    reload_table();
                    $("#btnImport" ).prop( "disabled", false );
                    $('#btnImport').html("Import");
                }
            });

            return false;
    });
            </script>
