
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Pengumuman</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Terakhir Terbit 27/09/2018 09:34:57</a>
                        </li>
                        <!-- <li class="active">
                            <strong>Breadcrumb</strong>
                        </li> -->
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary"><i class="fa fa-send"></i> Terbitkan</a>
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
              <!-- <div class="col-lg-12"> -->
                <div class="ibox-title">
                  <div class="row">
                    <!-- <div class="col-lg-12"> -->
                      <!-- <div class="form-group">
                        <label class="control-label">Filter Tujuan:</label>
                        <div>
                          <select id="tujuan" class="form-control">
                            <option value="0" selected>SEMUA</option>
                          </select>
                        </div>
                      </div> -->
                      <!-- <div class="col-lg-12">
                        <div class="form-group">
                          <label class="control-label">Judul Pengumuman</label>
                          <div>
                            <input type="text" name="judul" placeholder="MASUKKAN JUDUL PENGUMUNAN" class="form-control" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <label class="control-label">Deskripsi</label>
                          <div>
                            <textarea type="text" name="deskripsi" placeholder="Masukkan Deskripsi Singkat" style="resize:none" class="form-control"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="control-label">Import File</label>+
                          <div>
                            <input type="file" name="file" placeholder="Import File Pengumuman" accept="application/msexcel" class="form-control" required>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group" style="margin-top:20px;">
                          <button type="button" class="btn btn-md btn-success" name="button"><i class="fa fa-upload"></i> IMPORT</button>
                        </div>
                      </div> -->
                    <!-- </div> -->
                  </div>
                </div>
                <div class="ibox-content">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover datatabeltagihan">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>NIM</th>
                          <th>Nama Mahasiswa</th>
                          <th>Fakultas</th>
                          <th>Jurusan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>123456789</td>
                          <td>Budi Santoso</td>
                          <td>EKONOMI</td>
                          <td>S1 EKONOMI</td>
                          <!-- <td>
                            <center>
                              <a href="<?php echo base_url()."adminpmb/dataregistrasi/edit"?>" class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></a>
                              <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                            </td> -->
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <center>
                      <button type="button" class="btn btn-md btn-primary" name="button"><i class="fa fa-save"></i> SIMPAN</button>
                        <button type="button" class="btn btn-md btn-warning" name="button" onclick="window.histroy.back()"><i class="fa fa-reply"></i> KEMBALI</button>
                    </center>
                  </div>
              <!-- </div> -->
            </div>
          </div><br><br>
