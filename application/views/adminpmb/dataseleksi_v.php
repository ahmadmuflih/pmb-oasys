
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Data Seleksi Pendaftar <b><?php echo $_SESSION['batch']['periode']." - ".$_SESSION['batch']['nama'] ?></b></h2>
                    <ol class="breadcrumb">
                        <li>
                            <!-- <a href="index.html">Terakhir Terbit 27/09/2018 09:34:57</a> -->
                        </li>
                        <!-- <li class="active">
                            <strong>Breadcrumb</strong>
                        </li> -->
                    </ol>
                </div>
                <div class="col-sm-6">
                    <div class="title-action">
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#importModal"><i class="fa fa-download"></i> Import</a>
                      <button onclick="exportData()" class="btn btn-warning"><i class="fa fa-upload"></i> Export</button>
                        <!-- <a href="<?php echo base_url()."adminpmb/dataregistrasi/add"?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a> -->
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
            <div class="ibox-title">
              <div class="row">
                <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label">Prodi:</label>
                        <div>
                        <select class="form-control" name="prodi" id="prodi">
                        <?php echo $jurusan_filter;?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                      <div class="form-group">
                        <label class="control-label">Status:</label>
                        <div>
                        <select class="form-control" name="status" id="status">
                        <option value='-1'>-PILIH STATUS-</option>
                        <option value='1'>LULUS</option>
                        <option value='0'>TIDAK LULUS</option>
                        </select>
                      </div>
                    </div>
                  </div>
                <div class="col-md-2 pull-right">
                    <div class="col-md-2">
                        <div class="form-group">
                          <label class="control-label"> </label>
                          <div>
                          <!-- <div class="btn-group">
                              <button data-toggle="dropdown" class="btn btn-success dropdown-toggle">STATUS LULUS <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                  <li><a href="#">LULUS</a></li>
                                  <li><a href="#">TIDAK LULUS</a></li>
                              </ul>
                          </div> -->
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
            </div>
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table id="mytable" class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No. Registrasi</th>
                        <th>Nama Lengkap</th>
                        <th>Pilihan 1</th>
                        <th>Pilihan 2</th>
                        <th>Lulus di</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- <tr>
                        <td>1</td>
                        <td align="center"><input type="checkbox"></td>
                        <td>123456789</td>
                        <td>Budi Santoso</td>
                        <td>(2) S1 Teknik Sipil</td>
                        <td>(6) S1 Ilmu-Ilmu Hukum</td>
                        <td>(2) S1 Teknik Sipil</td>
                        <td><span class="label label-primary">LULUS</span></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td align="center"><input type="checkbox"></td>
                        <td>123456789</td>
                        <td>Budi Santika</td>
                        <td>(11) S1 Ilmu Kesehatan Masyarakat</td>
                        <td>(1) S1 Manajemen</td>
                        <td>-</td>
                        <td><span class="label label-default">TIDAK LULUS</span></td>
                      </tr> -->
                    </tbody>
                  </table>
                </div>
                <!-- <center>
                  <button id="terbit" onclick="set(1)" class="btn btn-md btn-primary"><span class="fa fa-send"></span> TERBITKAN</button> <br>
                  <a href="" class="hidden" id="batal">BATALKAN TERBIT</a>
                </center> -->
              </div>
            </div>
          </div>

          <div class="modal inmodal fade" id="importModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Import Kelulusan</h4>
                        </div>
                        <div class="modal-body">
                        <form id="formImport" class="form">
                          <div class="form-group"><label>Import Data </label> <input type="file" name="import" id="import" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" placeholder="Import Data Peserta Ujian"class="form-control" required>
                          <small style="color:#fc5e55">*File harus dalam format xlxs/xls/csv</small>
                          </div>
                        
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button id="btnImport" type="submit" class="btn btn-primary">Import</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
              var id_prodi = 0, status = -1;
              $('#prodi').change(function() {
                id_prodi = $(this).val();
                loadData();
              });
              $('#status').change(function() {
                status = $(this).val();
                loadData();
              });
              loadData();
              function loadData(){
            $('#mytable').DataTable().destroy();
              $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                        {
                            return {
                                "iStart": oSettings._iDisplayStart,
                                "iEnd": oSettings.fnDisplayEnd(),
                                "iLength": oSettings._iDisplayLength,
                                "iTotal": oSettings.fnRecordsTotal(),
                                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                            };
                        };

                        t = $("#mytable").DataTable({
                            initComplete: function() {
                                var api = this.api();
                                $('#mytable_filter input')
                                        .off('.DT')
                                        .on('keyup.DT', function(e) {
                                            if (e.keyCode == 13) {
                                                api.search(this.value).draw();
                                    }
                                });
                            },
                            oLanguage: {
                                sProcessing: "loading..."
                            },
                            processing: true,
                            serverSide: true,
                            select: true,
                            ajax: {"url": "<?php echo base_url("adminpmb/Dataseleksi/json");?>", "type": "POST", "data":{
                              'id_prodi' : id_prodi,
                              'status' : status
                            }},
                            "columnDefs": [
                {
                    "targets": [ -1,1], //last column
                    "orderable": false, //set not orderable
                }, { "searchable": false, "targets": 1 },{
      "render": function ( data, type, row ) {
        // here you can convert data from base64 to hex and return it

        if(data == null){
          label = 'label-default';
          st = 'TIDAK LULUS';
        }else{
          label = 'label-primary';
          st = 'LULUS';
        }
        data = "<span class='label "+label+"'><span class='fa fa-pencil'></span> "+st+"</span><br>";

        return data
      },
      "targets": 6
   }

                ],
                            columns: [
                                {
                                    "data": "id",
                                    "orderable": false
                                },
                                {"data": "no_registrasi"},
                                {"data": "nama"},
                                {"data": "pilihan1"},
                                {"data": "pilihan2"},
                                {"data": "lulus"},
                                {"data": "pilihan_lulus"}
                            ],
                            order: [[1, 'asc']],
                            rowCallback: function(row, data, iDisplayIndex) {
                                var info = this.fnPagingInfo();
                                var page = info.iPage;
                                var length = info.iLength;
                                var index = page * length + (iDisplayIndex + 1);
                                $('td:eq(0)', row).html(index);
                            }
                        });


            };
              function exportData(){
                var url = "<?php echo base_url().'adminpmb/Excel/exportKelulusan?batch='.$_SESSION['batch']['id']?>&status="+status+"&prodi="+id_prodi;
                var win = window.open(url, '_blank');
                win.focus();
              }
              $('#formImport').submit(function(){
            var file_data = $('#import').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('batch',<?php echo $_SESSION['batch']['id'] ?>);

            
              $('#btnImport').html("Importing..");
              $("#btnImport" ).prop( "disabled", true );
            $.ajax({
                url: '<?php echo base_url("adminpmb/Excel/importKelulusan/") ?>', // point to server-side PHP script
                dataType:'JSON',
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(data){
                    //alert(php_script_response); // display response from the PHP script, if any
                    console.log(data);
                    $('#formImport')[0].reset();
                    swal({
                        title: "Berhasil",
                        text: data.success+"\n"+data.failed
                    });
                    reload_table();
                    $("#btnImport" ).prop( "disabled", false );
                    $('#btnImport').html("Import");
                }
            });
            
            return false;
    });
    function reload_table()
{
$('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
}
            </script>
