
  <!-- content -->
  <link href="<?php echo base_url()."assets" ?>/css/datatable-select.css" rel="stylesheet">
  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Master Data Periode PMB</h2>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                      <!-- <a href="#" class="btn btn-info"><i class="fa fa-download"></i> Export</a> -->
                        <!-- <a href="#" data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a> -->
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="col-lg-5">
                <div class="ibox-title">
                  <b>LIST PERIODE</b>
                  <a href="#" data-toggle="modal" data-target="#addModal" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Tambah Periode</a>
                </div>
                  <div class="ibox-content">
                    <div class="table-responsive">
                      <table id="mytable" class="table table-striped table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Periode</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody class="selector">
                          <!-- <tr class="selector">
                            <td>1</td>
                            <td>2017/2018</td>
                            <td>Aktif</td>
                            <td>
                              <center>
                              <a href="#" data-toggle="modal" data-target="#editModal" class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></a>
                              <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                              </center>
                            </td>
                          </tr>
                          <tr class="selector">
                            <td>2</td>
                            <td>2017/2018</td>
                            <td>Aktif</td>
                            <td>
                              <center>
                              <a href="#" data-toggle="modal" data-target="#editModal" class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></a>
                              <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                              </center>
                            </td>
                          </tr> -->
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
              <div class="col-lg-7">
                <div class="ibox-title">
                  <b>LIST BATCH</b> <small>Periode : <span class='periode_selected'></span></small>
                  <a href="#" data-toggle="modal" data-target="#addbatchModal" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus"></i> Tambah Batch</a>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                    <table id="mytable2" class="table table-striped table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Batch</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Status</th>
                            <th width="20%">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <!-- <tr>
                            <td>1</td>
                            <td>01/01/2019</td>
                            <td>15/02/2019</td>
                            <td>Aktif</td>
                            <td>
                              <center>
                              <a href="#" class='btn btn-success btn-xs' title='Aktifkan'><span class='glyphicon glyphicon-check'></span></a>
                              <a href="#" data-toggle="modal" data-target="#editbatchModal" class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-calendar'></span></a>
                              <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                              </center>
                            </td>
                          </tr> -->
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
          </div>

          <style>
            .popover.clockpicker-popover{
                  z-index: 9999 !important;
              }
          </style>

            <div class="modal inmodal fade" id="addModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Tambah Periode PMB</h4>
                        </div>
                        <div class="modal-body">
                        <form id="form" class="form">
                        <div class="form-group">
                            <label for="nip">Periode PMB: <span style="color:red;">*</span></label>
                            <div class="row">
                              <div class="col-lg-5">
                                <input type="text" id="yearpicker" name="tahun_awal" class="form-control" placeholder="Tahun" required>
                              </div>
                              <div class="col-lg-1">
                                <font size="5"><b>&sol;</b></font>
                              </div>
                              <div class="col-lg-5">
                                <input type="text" id="yearpicker2" name="tahun_akhir" class="form-control" placeholder="Tahun" required>
                              </div>
                            </div>
                          </div>
                          <!-- <div class="form-group"><label>Tanggal Mulai</label> <input type="text" placeholder="Masukkan Tanggal Mulai" id="datepicker" name="tanggal_mulai" class="form-control" required></div>
                          <div class="form-group"><label>Tanggal Selesai</label> <input type="text" placeholder="Masukkan Tanggal Selesai" id="datepicker2" name="tanggal_selesai" class="form-control" required></div> -->
                        
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="editModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit Data Periode</h4>
                        </div>
                        <div class="modal-body">
                        <form id="formEdit" class="form">
                        <div class="form-group">
                            <label for="nip">Periode PMB: <span style="color:red;">*</span></label>
                            <div class="row">
                              <div class="col-lg-5">
                                <input type="text" id="yearpickerEdit" name="yearpicker" class="form-control" placeholder="Tahun" disabled>
                              </div>
                              <div class="col-lg-1">
                                <font size="5"><b>&sol;</b></font>
                              </div>
                              <div class="col-lg-5">
                                <input type="text" id="yearpicker2Edit" name="yearpicker2" class="form-control" placeholder="Tahun" disabled>
                              </div>
                            </div>
                          </div>
                          <!-- <div class="form-group"><label>Tanggal Mulai</label> <input type="text" placeholder="Masukkan Tanggal Mulai" id="datepicker3" name="tanggal_mulai" class="form-control"></div>
                          <div class="form-group"><label>Tanggal Selesai</label> <input type="text" placeholder="Masukkan Tanggal Selesai" id="datepicker4" name="tanggal_selesai" class="form-control"></div> -->
                        
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="addbatchModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Tambah Batch Periode <span class='periode_selected'></span></h4>
                        </div>
                        <div class="modal-body">
                        <form id="formBatch" class="form">
                          <div class="form-group"><label>Nama Batch</label> <input type="text" placeholder="Masukkan Nama Batch" name="nama" class="form-control" required></div>
                          <div class="form-group"><label>Tanggal Mulai</label> <input type="text" placeholder="Masukkan Tanggal Mulai" id="datepicker5" name="tanggal_mulai" class="form-control" required></div>
                          <div class="form-group"><label>Tanggal Selesai</label> <input type="text" placeholder="Masukkan Tanggal Selesai" id="datepicker6" name="tanggal_selesai" class="form-control" required></div>
                        
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="editbatchModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit Batch <span class='batch_selected'></span> Periode <span class='periode_selected'></span></h4>
                        </div>
                        <div class="modal-body">
                        <form id="formBatchEdit" class="form">
                        <div class="form-group"><label>Nama Batch</label> <input type="text" placeholder="Masukkan Nama Batch" id="namaBatch" name="nama" class="form-control" required></div>
                          <div class="form-group"><label>Tanggal Mulai</label> <input type="text" placeholder="Masukkan Tanggal Mulai" id="datepicker7" name="tanggal_mulai" class="form-control" required></div>
                          <div class="form-group"><label>Tanggal Selesai</label> <input type="text" placeholder="Masukkan Tanggal Selesai" id="datepicker8" name="tanggal_selesai" class="form-control" required></div>
                        
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

          

            <script>
            
            var id_periode, id_batch;
              $('.clockpicker').clockpicker({
                minutestep:5,
                autoclose:true
              });
              $('#yearpicker').change(function() {
                var thn1 = parseInt($('#yearpicker').val());
                $('#yearpicker2').val((thn1+1));
              });
              $('#yearpicker2').change(function() {
                var thn2 = parseInt($('#yearpicker2').val());
                $('#yearpicker').val((thn2-1));
              });
              $(document).ready(function() {
              $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                        {
                            return {
                                "iStart": oSettings._iDisplayStart,
                                "iEnd": oSettings.fnDisplayEnd(),
                                "iLength": oSettings._iDisplayLength,
                                "iTotal": oSettings.fnRecordsTotal(),
                                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                            };
                        };

                        t = $("#mytable").DataTable({
                            initComplete: function() {
                                var api = this.api();
                                $('#mytable_filter input')
                                        .off('.DT')
                                        .on('keyup.DT', function(e) {
                                            if (e.keyCode == 13) {
                                                api.search(this.value).draw();
                                    }
                                });
                            },
                            oLanguage: {
                                sProcessing: "loading..."
                            },
                            processing: true,
                            serverSide: true,
                            select: true,
                            ajax: {"url": "<?php echo base_url("adminpmb/Periodepmb/json");?>", "type": "POST"},
                            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
                ],
                            columns: [
                                {
                                    "data": "id",
                                    "orderable": false
                                },
                                {"data": "periode"},
                                //{"data": "tanggal_mulai"},
                                //{"data": "tanggal_selesai"},
                                {"data": "status"},
                                {"data": "view"}
                            ],
                            order: [[1, 'asc']],
                            rowCallback: function(row, data, iDisplayIndex) {
                                var info = this.fnPagingInfo();
                                var page = info.iPage;
                                var length = info.iLength;
                                var index = page * length + (iDisplayIndex + 1);
                                $('td:eq(0)', row).html(index);
                            }
                        });
                        
                    $('#mytable tbody').on( 'click', 'tr', function () {
                            console.log(t.row( this ).data());
                        if ( $(this).hasClass('selected') ) {
                            //$(this).removeClass('selected');
                        }
                        else {
                            t.$('tr.selected').removeClass('selected');
                            $(this).addClass('selected');
                            id_periode = t.row(this).data().id;
                            $(".periode_selected").html(t.row(this).data().periode);
                            loadBatch();
                        }
                    } );
                
                    // $('#button').click( function () {
                    //     t.row('.selected').remove().draw( false );
                    // } );
                    $('.selector').css('cursor', 'pointer');
                  
                  
              var interval = setTimeout(function(){
                t.rows().every (function (rowIdx, tableLoop, rowLoop) {
                    
                    if (this.data().status === "Aktif") {
                      id_periode = this.data().id;
                      $(".periode_selected").html(this.data().periode);
                      console.log(this.data());
                      this.nodes().to$().addClass('selected');
                      loadBatch();
                    }
                  });
              },100);
              
            });

            function loadBatch(){
              $('#mytable2').DataTable().destroy();
              $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                        {
                            return {
                                "iStart": oSettings._iDisplayStart,
                                "iEnd": oSettings.fnDisplayEnd(),
                                "iLength": oSettings._iDisplayLength,
                                "iTotal": oSettings.fnRecordsTotal(),
                                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                            };
                        };

                        t2 = $("#mytable2").DataTable({
                            initComplete: function() {
                                var api = this.api();
                                $('#mytable_filter input')
                                        .off('.DT')
                                        .on('keyup.DT', function(e) {
                                            if (e.keyCode == 13) {
                                                api.search(this.value).draw();
                                    }
                                });
                            },
                            oLanguage: {
                                sProcessing: "loading..."
                            },
                            processing: true,
                            serverSide: true,
                            select: true,
                            ajax: {"url": "<?php echo base_url("adminpmb/Periodepmb/jsonBatch");?>", "type": "POST","data":{
                              "id_periode":id_periode
                            }},
                            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
                ],
                            columns: [
                                
                                {"data": "nama"},
                                {"data": "tanggal_mulai"},
                                {"data": "tanggal_selesai"},
                                {"data": "status"},
                                {"data": "view"}
                            ],
                            
                        });
            }

            $('#form').submit(function(){

            var form = $('#form')[0]; // You need to use standart javascript object here
            var formData = new FormData(form);
            $.ajax({
              url: '<?php echo base_url("adminpmb/Periodepmb/insert");?>',
              data: formData,
              type: 'POST',
              // THIS MUST BE DONE FOR FILE UPLOADING
              contentType: false,
              processData: false,
              dataType: "JSON",
              success: function(data){
                if (data.status=='berhasil') {
                  swal("Berhasil!", data.message, "success");
                  $('#form')[0].reset();
                $('#addModal').modal('hide');
                  reload_table();
                }else {
                  swal("Gagal!", data.message, "error");
                }
              },
                  error: function(jqXHR, textStatus, errorThrown)
                  {
              console.log(jqXHR);
              console.log(textStatus);
              console.log(errorThrown);
              alert('gagal');
            }
            })
            return false;
            });


            $('#formEdit').submit(function(){

var form = $('#formEdit')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
formData.append('id_periode',id_periode);
$.ajax({
  url: '<?php echo base_url("adminpmb/Periodepmb/update");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
      $('#formEdit')[0].reset();
    $('#editModal').modal('hide');
      reload_table();
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})
return false;
});
$('#formBatch').submit(function(){

var form = $('#formBatch')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
formData.append('id_tahun',id_periode);
$.ajax({
  url: '<?php echo base_url("adminpmb/Periodepmb/insertBatch");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
      $('#formBatch')[0].reset();
    $('#addbatchModal').modal('hide');
      loadBatch();
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})
return false;
});
function reload_table()
{
$('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
}
$('#formBatchEdit').submit(function(){

var form = $('#formBatchEdit')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
formData.append('id_batch',id_batch);
$.ajax({
  url: '<?php echo base_url("adminpmb/Periodepmb/updateBatch");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
      $('#formBatchEdit')[0].reset();
    $('#editModal').modal('hide');
      reload_tableBatch();
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})
return false;
});

function updateJadwal(id){
  id_periode = id;
  $.ajax({
      url : "<?php echo site_url('adminpmb/Periodepmb/getPeriode')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          $('#yearpickerEdit').val(data.data.tahun_awal);
          $('#yearpicker2Edit').val(data.data.tahun_akhir);
          $('#datepicker3').val(data.data.tanggal_mulai);
          $('#datepicker4').val(data.data.tanggal_selesai);
          $('#editModal').modal('show');
          reload_table();
        }else {
          swal("Gagal!", data.message, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Gagal!", 'Terjadi kesalahan!', "error");
      }
  });
  
}
function updateBatch(id){
  id_batch = id;
  $.ajax({
      url : "<?php echo site_url('adminpmb/Periodepmb/getBatch')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          $('#namaBatch').val(data.data.nama);
          $('#datepicker7').val(data.data.tanggal_mulai);
          $('#datepicker8').val(data.data.tanggal_selesai);
          $('#editbatchModal').modal('show');
        }else {
          swal("Gagal!", data.message, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Gagal!", 'Terjadi kesalahan', "error");
      }
  });
  
}
function hapus(id){
swal({
    title: "Anda yakin?",
    text: "Data periode akan dihapus",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#1ab394",
    confirmButtonText: "Ya, hapus!",
    closeOnConfirm: false
}, function () {
  $.ajax({
      url : "<?php echo site_url('adminpmb/Periodepmb/delete')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          swal("Berhasil!", data.message, "success");
          reload_table();
        }else {
          swal("Gagal!", data.message, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Gagal!", 'Terjadi kesalahan saat menghapus data, Tahun tersebut telah memiliki data!', "error");
      }
  });

});

}
function hapusBatch(id){
swal({
    title: "Anda yakin?",
    text: "Data Batch akan dihapus",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#1ab394",
    confirmButtonText: "Ya, hapus!",
    closeOnConfirm: false
}, function () {
  $.ajax({
      url : "<?php echo site_url('adminpmb/Periodepmb/deleteBatch')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          swal("Berhasil!", data.message, "success");
          reload_tableBatch();
        }else {
          swal("Gagal!", data.message, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Gagal!", 'Terjadi kesalahan saat menghapus data, Batch tersebut telah memiliki data!', "error");
      }
  });

});

}
    function setAktif(id){
    swal({
      title: "Anda yakin mengaktifkan periode ini?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#1ab394",
      confirmButtonText: "Ya, aktifkan!",
      closeOnConfirm: false
    }, function () {
    $.ajax({
      url : "<?php echo site_url('adminpmb/Periodepmb/setAktif')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          swal("Berhasil!", data.message, "success");
          reload_table();
        }else {
          swal("Gagal!", data.message, "error");
        }
      }
      });

    });
    
  }
  function setAktifBatch(id){
    swal({
      title: "Anda yakin mengaktifkan periode ini?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#1ab394",
      confirmButtonText: "Ya, aktifkan!",
      closeOnConfirm: false
    }, function () {
    $.ajax({
      url : "<?php echo site_url('adminpmb/Periodepmb/setAktifBatch')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          swal("Berhasil!", data.message, "success");
          reload_table();
          reload_tableBatch();
        }else {
          swal("Gagal!", data.message, "error");
        }
      }
      });

    });
    
  }
  function reload_table()
    {
        $('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
    }
    function reload_tableBatch()
    {
        $('#mytable2').DataTable().ajax.reload(null,false); //reload datatable ajax
    }
            </script>
