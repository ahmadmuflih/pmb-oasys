
  <!-- content -->
  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Master Data Biaya Pendaftaran</h2>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                      <!-- <a href="#" class="btn btn-info"><i class="fa fa-download"></i> Export</a> -->
                        <!-- <button type="button" onclick="tambah()" data-toggle="modal"  class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</button> -->
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
            <div class="ibox-title">
              <div class="row">
                            <br>
                            <form id="form">
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label class="control-label">Fakultas:</label>
                                  <div><select  id="fakultas" name="fakultas" type="text" class="form-control">
                                  <?php foreach ($fakultas->result() as $row){
                                    echo "<option value='$row->id'>$row->nama</option>";
                                  }
                                  ?>
                                  </select></div>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <label class="control-label">Prodi:</label>
                                  <div><select id="prodi" name="prodi" class="form-control">
                                    <option value='0'>SEMUA</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                  <label class="control-label">Pendaftar:</label>
                                  <div><select id="tipe" name="tipe" class="form-control">
                                    <option value='0'>SEMUA</option>
                                    <option value='1'>BARU</option>
                                    <option value='2'>PINDAHAN/LANJUTAN</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-2">
                              <div class="form-group">
                                <label class="control-label">Biaya Pendaftaran (Rp):</label>
                                <div><input name="biaya" id="biaya" type="number" min="0" class="form-control" required></div>
                              </div>
                            </div>
                            <!-- <div class="col-sm-2">
                              <div class="form-group">
                                <label class="control-label">Tgl. Selesai:</label>
                                <div><input name="tgl_selesai" type="text" class="form-control" id="datepicker2" required></div>
                              </div>
                            </div> -->
                            <div class="col-sm-2">
                              <div class="form-group">
                                <label class="control-label"> </label>
                                <div>
                                  <button type="submit" class="btn btn-md btn-success"><span class="fa fa-check"></span> Set Biaya</button>
                                </div>
                              </div>
                            </div>
                          </form>
                    </div>
            </div>
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Prodi</th>
                        <th>Biaya Pendaftaran (Baru)</th>
                        <th>Biaya Pendaftaran (Pindahan)</th>
                        <!-- <th>Status</th> -->
                      </tr>
                    </thead>
                    <tbody id="detail">
                        <!-- <tr>
                          <th style="background-color:#fff !important;" colspan="5">Fakultas KESEHATAN MASYARAKAT</th>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>S1 ILMU KESEHATAN MASYRAKAT</td>
                          <td>Rp 350000</td>
                          <td>Rp 400000</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>S1 HUKUM KELUARGA (AKHWAL SYAKHSHIYYAH) (Reguler)</td>
                          <td>Rp 350000</td>
                          <td>Rp 400000</td>
                        </tr>
                        <tr>
                          <th style="background-color:#fff !important;" colspan="5">Fakultas TEKNIK</th>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>S1 TEKNIK SIPIL</td>
                          <td>Rp 250000</td>
                          <td>Rp 300000</td>
                        </tr>
                        <tr>
                          <td>4</td>
                          <td>S1 TEKNIK KIMIA</td>
                          <td>Rp 250000</td>
                          <td>Rp 300000</td>
                        </tr> -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <style>
            .popover.clockpicker-popover{
                  z-index: 9999 !important;
              }
          </style>

            <div class="modal inmodal fade" id="addModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Tambah Data Ujian</h4>
                        </div>
                        <div class="modal-body">
                        <form class="form">
                          <div class="form-group"><label>No. Ujian</label> <input type="text" placeholder="Masukkan No Ujian" class="form-control"></div>
                          <div class="form-group"><label>Tanggal Ujian</label> <input type="text" placeholder="Masukkan Tanggal Ujian" id="datepicker" class="form-control"></div>
                          <div class="form-group"><label>Gedung Ujian</label>
                            <select class="form-control" name="ruangan" id="">
                              <option value="0" selected disabled>-PILIH Gedung-</option>
                              <option value="1">Gedung A</option>
                              <option value="2">Gedung B</option>
                            </select>
                          </div>
                          <div class="form-group"><label>Ruang Ujian</label>
                            <select class="form-control" name="ruangan" id="">
                              <option value="0" selected disabled>-PILIH RUANGAN-</option>
                              <option value="1">Ruangan A01</option>
                              <option value="2">Ruangan A02</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Waktu Tes</label> 
                            <div class="row">
                              <div class="col-md-6">
                                <input type="text" placeholder="Masukkan Jam Ujian" class="form-control clockpicker">
                              </div>
                              <div class="col-md-6">
                                <input type="text" placeholder="Masukkan Jam Selesai" class="form-control clockpicker">
                              </div>
                            </div>
                          </div>
                        </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button type="button" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal inmodal fade" id="editModal" role="dialog"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title">Edit Data Ujian</h4>
                        </div>
                        <div class="modal-body">
                        <form class="form">
                          <div class="form-group"><label>No. Ujian</label> <input type="text" placeholder="Masukkan No Ujian" value="123456789" class="form-control"></div>
                          <div class="form-group"><label>Tanggal Ujian</label> <input type="text" placeholder="Masukkan Tanggal Ujian" value="30/12/2018" id="datepicker" class="form-control"></div>
                          <div class="form-group"><label>Gedung Ujian</label>
                            <select class="form-control" name="ruangan" id="">
                              <option value="0" disabled>-PILIH Gedung-</option>
                              <option value="1" selected>Gedung A</option>
                              <option value="2">Gedung B</option>
                            </select>
                          </div>
                          <div class="form-group"><label>Ruang Ujian</label>
                            <select class="form-control" name="ruangan" id="">
                              <option value="0" disabled>-PILIH RUANGAN-</option>
                              <option value="1" selected>Ruangan A01</option>
                              <option value="2">Ruangan A02</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Waktu Tes</label> 
                            <div class="row">
                              <div class="col-md-6">
                                <input type="text" placeholder="Masukkan Jam Ujian" value="12:05" class="form-control clockpicker">
                              </div>
                              <div class="col-md-6">
                                <input type="text" placeholder="Masukkan Jam Selesai" value="14:05" class="form-control clockpicker">
                              </div>
                            </div>
                          </div>
                        </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                            <button type="button" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>

            <script>
            var fakultas = <?php echo $fakultas->row()->id ?>,prodi=0,tipe=0;
            getProdi();
            $('#fakultas').change(function(){
              fakultas=$(this).val();
              getProdi();
            });
            $('#prodi').change(function(){
              prodi=$(this).val();
            });
            function getProdi(){
            $.ajax({
                url : "<?php echo site_url('adminpmb/Databiaya/getProdi')?>",
                type: "POST",
                data: {"id_fakultas":fakultas},
                dataType: "JSON",
                success: function(data)
                {
                  if (data.status=='berhasil') {
                    $('#prodi').html(data.data);
                  }
                },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                    }
              });
          }
              $('.clockpicker').clockpicker({
                minutestep:5,
                autoclose:true
              });
              getData();
              function getData(){
                $.ajax({
                  url : "<?php echo site_url('adminpmb/Databiaya/getData')?>",
                  type: "POST",
                  dataType: "JSON",
                    success: function(data)
                    {
                      if (data.status=='berhasil') {
                        $('#detail').html(data.data);
                      }
                    },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                          console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                        }
                  });

              }
              $('#form').submit(function(){
                var form = $('#form')[0]; // You need to use standart javascript object here
                var formData = new FormData(form);
                $.ajax({
                  url: '<?php echo base_url("adminpmb/Databiaya/setBiaya");?>',
                  data: formData,
                  type: 'POST',
                  // THIS MUST BE DONE FOR FILE UPLOADING
                  contentType: false,
                  processData: false,
                  dataType: "JSON",
                  success: function(data){
                    console.log(data);
                    if (data.status=='berhasil') {
                      swal("Berhasil!", data.message, "success");
                      getData();
                    }else {
                      swal("Gagal!", data.message, "error");
                    }

                  },
                      error: function(jqXHR, textStatus, errorThrown)
                      {
                  console.log(jqXHR);
                  console.log(textStatus);
                  console.log(errorThrown);
                  alert('gagal');
                }
                })
                return false;
            });
            </script>
