
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-10">
                    <h2>Data Akun Pendaftar <b>Periode PMB <?php echo $_SESSION['batch']['periode']." - ".$_SESSION['batch']['nama'] ?></b></h2>
                </div>
                <div class="col-sm-2">
                    <div class="title-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
              <div class="row">
                <div class="col-lg-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1"> Data Akun</a></li>
                            <!-- <li class=""><a data-toggle="tab" href="#tab-2">Request Aktivasi <span class="label label-warning">1</span> </a></li> -->
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                  <div class="row">
                                    <!-- <div class="col-md-3">
                                      <div class="form-group">
                                        <label class="control-label">Filter Status</label>
                                        <select class="form-control" name="status">
                                          <option value="0">SEMUA</option>
                                          <option value="1">AKTIF</option>
                                          <option value="2">TIDAK AKTIF</option>
                                        </select>
                                      </div>
                                      </div> -->
                                  </div>
                                  <div class="table-responsive">
                                    <table id="mytable" class="table table-striped table-bordered table-hover">
                                      <thead>
                                        <tr>
                                          <th>No</th>
                                          <th>No Reg.</th>
                                          <th>Nama Lengkap</th>
                                          <th>No. Telp</th>
                                          <th>Email</th>
                                          <!-- <th>Status</th> -->
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <!-- <tr>
                                          <td>1</td>
                                          <td>123456789</td>
                                          <td>Budi Santoso</td>
                                          <td>08123456789</td>
                                          <td>budisan@gmail.com</td>
                                          <td><span class="label label-default">TIDAK AKTIF</span></td>
                                          <td>
                                            <center>
                                            <button type='button' class='btn btn-warning btn-xs' title='Edit Data' onclick=''><span class='glyphicon glyphicon-edit'></span></button>
                                            <button type='button' class='btn btn-danger btn-xs' title='Hapus Data' onclick=''><span class='glyphicon glyphicon-remove'></span></button>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>2</td>
                                          <td>123456789</td>
                                          <td>Adi Santoso</td>
                                          <td>08123456789</td>
                                          <td>adisan@gmail.com</td>
                                          <td><span class="label label-primary">AKTIF</span></td>
                                          <td>
                                            <center>
                                            <a class='btn btn-warning btn-xs' title='Edit Data' data-toggle="modal" data-target="#editModal"><span class='glyphicon glyphicon-edit'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                                          </td>
                                        </tr> -->
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                            </div>
                            


                    </div>
                </div>

                <div class="modal inmodal fade" id="editModal" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title">Edit Data Akun</h4>
                                    <!-- <small class="font-bold"></small> -->
                                </div>
                                <div class="modal-body">
                                    <form id="formEdit" role="form">
                                      <div class="form-group">
                                        <label>Nama Lengkap Pendaftar</label>
                                        <input type="text" placeholder="Masukkan Nama Pendaftar" value="" class="form-control" id="nama" name="nama" required>
                                      </div>
                                      <div class="form-group">
                                        <label>No. Telepon</label>
                                        <input type="number" min="0" placeholder="Masukkan No Telepon" value="" class="form-control" id="no_telp" name="no_telp" required>
                                      </div>
                                      <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" placeholder="Masukkan Email" value="" class="form-control" id="email" name="email">
                                      </div>
                                      <div class="form-group">
                                            <label>Batch</label>
                                            <select name="id_batch" class="form-control">
                                              <?php echo "<option value='".$_SESSION['batch']['id']."'>".$_SESSION['batch']['periode']." - ".$_SESSION['batch']['nama']."</option>";?>
                                            </select>
                                          </div>
                                      
                                      <div class="form-group">
                                        <label>Password</label><br>
                                        <button type="button" onclick='resetPassword()' class="btn btn-md btn-danger"><i class="fa fa-lock"></i> Reset Password</button>
                                      </div>
                                     
                                      
                                      <!-- <div class="form-group"><label>Password</label> <input type="password" placeholder="Password" class="form-control"></div> -->
                                  
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal inmodal fade" id="addModal" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title">Tambah Akun</h4>
                                        <!-- <small class="font-bold"></small> -->
                                    </div>
                                    <div class="modal-body">
                                        <form id="form" role="form">
                                          <div class="form-group">
                                            <label>Nama Lengkap Pendaftar</label>
                                            <input type="text" placeholder="Masukkan Nama Pendaftar" class="form-control" name="nama" required>
                                          </div>
                                          <div class="form-group">
                                            <label>No. Telepon</label>
                                            <input type="number" min="0" placeholder="Masukkan No Telepon" class="form-control" name="no_telp" required>
                                          </div>
                                          <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" placeholder="Masukkan Email" class="form-control" name="email" required>
                                          </div>
                                          <!-- <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" placeholder="Masukkan Username" class="form-control" required>
                                          </div>
                                          <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" placeholder="Masukkan Password" class="form-control" required>
                                          </div> -->
                                          <div class="form-group">
                                            <label>Batch</label>
                                            <select name="id_batch" class="form-control">
                                              <?php echo "<option value='".$_SESSION['batch']['id']."'>".$_SESSION['batch']['periode']." - ".$_SESSION['batch']['nama']."</option>";?>
                                            </select>
                                          </div>
                                          <!-- <div class="form-group"><label>Password</label> <input type="password" placeholder="Password" class="form-control"></div> -->
                                      
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

            <!-- <div class="row">
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover datatabeltagihan">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NIS</th>
                        <th>Nama Lengkap</th>
                        <th>Jurusan Sekolah</th>
                        <th>Nomor Kontak</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>123456789</td>
                        <td>Budi Santoso</td>
                        <td>IPA</td>
                        <td>budisan@gmail.com</td>
                        <td>08123456789</td>
                        <td>
                          <center>
                          <a class='btn btn-warning btn-xs' title='Edit Data' data-toggle="modal" data-target="#editModal"><span class='glyphicon glyphicon-edit'></span></a>
                          <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div> -->
          </div>
<script>
var id_camaba;
$(document).ready(function() {
              $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                        {
                            return {
                                "iStart": oSettings._iDisplayStart,
                                "iEnd": oSettings.fnDisplayEnd(),
                                "iLength": oSettings._iDisplayLength,
                                "iTotal": oSettings.fnRecordsTotal(),
                                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                            };
                        };

                        t = $("#mytable").DataTable({
                            initComplete: function() {
                                var api = this.api();
                                $('#mytable_filter input')
                                        .off('.DT')
                                        .on('keyup.DT', function(e) {
                                            if (e.keyCode == 13) {
                                                api.search(this.value).draw();
                                    }
                                });
                            },
                            oLanguage: {
                                sProcessing: "loading..."
                            },
                            processing: true,
                            serverSide: true,
                            select: true,
                            ajax: {"url": "<?php echo base_url("adminpmb/Dataakun/json");?>", "type": "POST"},
                            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
                ],
                            columns: [
                                {
                                    "data": "id",
                                    "orderable": false
                                },
                                {"data": "no_registrasi"},
                                {"data": "nama"},
                                {"data": "no_telp"},
                                {"data": "email"},
                                {"data": "view"}
                            ],
                            order: [[1, 'asc']],
                            rowCallback: function(row, data, iDisplayIndex) {
                                var info = this.fnPagingInfo();
                                var page = info.iPage;
                                var length = info.iLength;
                                var index = page * length + (iDisplayIndex + 1);
                                $('td:eq(0)', row).html(index);
                            }
                        });
                  
              
            });
            $('#form').submit(function(){

var form = $('#form')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
$.ajax({
  url: '<?php echo base_url("Login/register");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
      $('#form')[0].reset();
    $('#addModal').modal('hide');
      reload_table();
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})
return false;
});
function reload_table()
{
$('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
}
function edit(id){
  id_camaba = id;
  $.ajax({
      url : "<?php echo site_url('adminpmb/Dataakun/getCamaba')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          $('#nama').val(data.data.nama);
          $('#email').val(data.data.email);
          $('#no_telp').val(data.data.no_telp);
          $('#editModal').modal('show');
        }else {
          swal("Gagal!", data.message, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Gagal!", 'Terjadi kesalahan!', "error");
      }
  });
  
}
$('#formEdit').submit(function(){

var form = $('#formEdit')[0]; // You need to use standart javascript object here
var formData = new FormData(form);
formData.append("id",id_camaba);
$.ajax({
  url: '<?php echo base_url("adminpmb/Dataakun/update");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
      $('#formEdit')[0].reset();
    $('#editModal').modal('hide');
      reload_table();
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})
return false;
});
function resetPassword(){
swal({
    title: "Anda yakin?",
    text: "Password akan direset mengikuti no registrasi",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#1ab394",
    confirmButtonText: "Ya, reset!",
    closeOnConfirm: false
}, function () {
  $.ajax({
      url : "<?php echo site_url('adminpmb/Dataakun/resetPassword')?>",
      type: "POST",
      data: {'id':id_camaba},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          swal("Berhasil!", data.message, "success");
          reload_table();
        }else {
          swal("Gagal!", data.message, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Gagal!", 'Terjadi kesalahan!', "error");
      }
  });

});

}
function hapus(id){
swal({
    title: "Anda yakin?",
    text: "Data akun akan dihapus",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#1ab394",
    confirmButtonText: "Ya, hapus!",
    closeOnConfirm: false
}, function () {
  $.ajax({
      url : "<?php echo site_url('adminpmb/Dataakun/delete')?>",
      type: "POST",
      data: {'id':id},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status=='berhasil') {
          swal("Berhasil!", data.message, "success");
          reload_table();
        }else {
          swal("Gagal!", data.message, "error");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        swal("Gagal!", 'Terjadi kesalahan saat menghapus data, Akun tersebut telah memiliki data!', "error");
      }
  });

});

}
</script>