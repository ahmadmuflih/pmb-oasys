
  <!-- content -->
    <style media="screen">
      .icon-size{
        font-size: 30px !important;
        opacity: 0.6;
      }
    </style>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Dashboard</h2>
                </div>
            </div>

            <div class="wrapper wrapper-content">
              <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins animated fadeInDown">
                            <div class="ibox-title">
                                <h5> Akun</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $akun ?></h1>
                                <div class="pull-right"><i class="fa fa-user icon-size"></i></div>
                                <small>Total Akun</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins animated fadeInDown ">
                            <div class="ibox-title">
                                <h5>Pendaftar</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $pendaftar ?></h1>
                                <div class="pull-right"><i class="fa fa-group icon-size"></i></div>
                                <small>Total Pendaftar</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins animated fadeInDown">
                            <div class="ibox-title">
                                <h5>Kelulusan</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $lulus ?></h1>
                                <div class="pull-right"><i class="fa fa-building icon-size"></i></div>
                                <small>Total Pendaftar Lulus</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins animated fadeInDown">
                            <div class="ibox-title">
                                <h5>Jurusan</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?php echo $jurusan ?></h1>
                                <div class="pull-right"><i class="fa fa-tags icon-size"></i></div>
                                <small>Total Jurusan</small>
                            </div>
                        </div>
                      </div>
                </div>
              <div class="row">
                      <div class="col-lg-6">
                          <!-- <div class="ibox float-e-margins">
                              <div class="ibox-title">
                                  <h5>Aktivitas PMB</h5>
                              </div>
                              <div class="ibox-content">
                                  <div class="activity-stream">
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-pencil bg-primary"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Arian Nurrifqhi</span>
                                                        <span class="date">27/12/2018 Jam 09:00 </span>
                                                    </a>
                                                </div>
                                                Telah Mengisi Registrasi PMB
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-pencil bg-primary"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Hari Kuyoki</span>
                                                        <span class="date">27/12/2018 Jam 09:00 </span>
                                                    </a>
                                                </div>
                                                Telah Mengisi Registrasi PMB
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-pencil bg-primary"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Peter Klarkson</span>
                                                        <span class="date">27/12/2018 Jam 09:00 </span>
                                                    </a>
                                                </div>
                                                Telah Mengisi Registrasi PMB
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-pencil bg-primary"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Yan Weasley</span>
                                                        <span class="date">27/12/2018 Jam 09:00 </span>
                                                    </a>
                                                </div>
                                                Telah Mengisi Registrasi PMB
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-pencil bg-primary"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Harry Potter</span>
                                                        <span class="date">27/12/2018 Jam 09:00 </span>
                                                    </a>
                                                </div>
                                                Telah Mengisi Registrasi PMB
                                            </div>
                                        </div>
                                        <div class="stream">
                                            <div class="stream-badge">
                                                <i class="fa fa-pencil bg-primary"></i>
                                            </div>
                                            <div class="stream-panel">
                                                <div class="stream-info">
                                                    <a href="#">
                                                        <span>Daniel Strootman</span>
                                                        <span class="date">27/12/2018 Jam 09:00 </span>
                                                    </a>
                                                </div>
                                                Telah Mengisi Registrasi PMB
                                            </div>
                                        </div>
                                </div>
                              </div>
                          </div> -->
                          <div class="ibox float-e-margins">
                          <div class="ibox-title">
                              <h5>Periode PMB</h5>
                          </div>
                          <div class="ibox-content">
                            <ul id="navsmt" class="nav metismenu smtlist activated">
                                <!-- <li class="active"><a href=""><span class="nav-label">2018/2019 - Ganjil</span></a></li>
                                <li><a href=""><span class="nav-label">2017/2018 - Genap</span></a></li>
                                <li><a href=""><span class="nav-label">2017/2018 - Ganjil</span></a></li> -->
                                <?php
                                    foreach($batch->result() as $row){
                                        $active = "";
                                        if($row->id == $_SESSION['batch']['id']){
                                            $active = "active";
                                        }
                                        echo "<li class='$active' onclick='setBatch($row->id)'><a><span class='nav-label'>$row->periode - $row->nama</span></a></li>";
                                    }
                                ?>
                            </ul>
                          </div>
                        </div>
                          <!-- <div class="ibox float-e-margins">
                              <div class="ibox-title">
                                  <h5>Request Ticketing</h5>
                              </div>
                              <div class="ibox-content">
                                <div class="scroll_content">
                                  <table class="table table-hover no-margins">
                                      <thead>
                                      <tr>
                                          <th>Akun</th>
                                          <th>Date</th>
                                          <th>User</th>
                                          <th>Action</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td><small>Dosen</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 12:01am</td>
                                          <td>11223</td>
                                          <td>
                                                <a class='btn btn-primary btn-xs' title='Lihat Data' href='#'><span class='fa fa-eye'></span></a>
                                                <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-trash'></span></a>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td><small>Dosen</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 12:01am</td>
                                          <td>11223</td>
                                          <td>
                                            <a class='btn btn-primary btn-xs' title='Lihat Data' href='#'><span class='fa fa-eye'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-trash'></span></a>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td><small>Dosen</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 12:01am</td>
                                          <td>11223</td>
                                          <td>
                                            <a class='btn btn-primary btn-xs' title='Lihat Data' href='#'><span class='fa fa-eye'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-trash'></span></a>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td><small>Dosen</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 12:01am</td>
                                          <td>11223</td>
                                          <td>
                                            <a class='btn btn-primary btn-xs' title='Lihat Data' href='#'><span class='fa fa-eye'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-trash'></span></a>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td><small>Dosen</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 12:01am</td>
                                          <td>11223</td>
                                          <td>
                                            <a class='btn btn-primary btn-xs' title='Lihat Data' href='#'><span class='fa fa-eye'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-trash'></span></a>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td><small>Dosen</small> </td>
                                          <td><i class="fa fa-clock-o"></i> 12:01am</td>
                                          <td>11223</td>
                                          <td>
                                            <a class='btn btn-primary btn-xs' title='Lihat Data' href='#'><span class='fa fa-eye'></span></a>
                                            <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-trash'></span></a>
                                          </td>
                                      </tr>
                                      </tbody>
                                  </table>
                              </div>
                              </div>
                          </div> -->
                      </div>
                      <!-- <div class="col-lg-6">
                          <div class="ibox float-e-margins">
                              <div class="ibox-title">
                                  <h5>Kalender PMB </h5>
                              </div>
                              <div class="ibox-content">
                                  <div id="calendar"></div>
                              </div>
                          </div>
                      </div> -->
                </div>
            </div>

<script>
function setBatch(id){

var formData = new FormData();
formData.append('id',id);
$.ajax({
  url: '<?php echo base_url("adminpmb/Home/setBatch");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    //   alert(data.status);
    if (data.status=='berhasil') {
      location.reload();
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})

}
</script>
