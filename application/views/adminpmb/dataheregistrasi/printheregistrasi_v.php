<link href="<?php echo base_url()."assets" ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()."assets" ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url()."assets" ?>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url()."assets" ?>/js/plugins/sweetalert/sweetalert.min.js"></script>
<link href="<?php echo base_url()."assets" ?>/css/plugins/dataTables/datatables.min.css" rel="stylesheet">


<link href="<?php echo base_url()."assets" ?>/css/datatables.css" rel="stylesheet">

<link href="<?php echo base_url()."assets" ?>/css/plugins/select2/select2.min.css" rel="stylesheet">


<!-- akun dan smt list -->
<link href="<?php echo base_url()."assets" ?>/css/akun_smt.css" rel="stylesheet">

<link href="<?php echo base_url()."assets" ?>/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url()."assets" ?>/css/style.css" rel="stylesheet">


<!-- scripts -->

<!-- Mainly scripts -->
<script src="<?php echo base_url()."assets" ?>/js/jquery-3.1.1.min.js"></script>
<!-- <script>$.ajaxPrefilter(function( options, originalOptions, jqXHR ) { options.async = true; });</script> -->
<script src="<?php echo base_url()."assets" ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/fullcalendar/moment.min.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url()."assets" ?>/js/inspinia.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI  -->
<script src="<?php echo base_url()."assets" ?>/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="<?php echo base_url()."assets" ?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/dataTables/datatables.min.js"></script>

<!-- canvajs -->
<script src="<?php echo base_url()."assets" ?>/js/canvasjs-2.0.1/canvasjs.min.js"></script>



<!-- Typehead -->
<script src="<?php echo base_url()."assets" ?>/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
	<meta charset="utf-8">
	<title></title>
</head>

<body style="background-color:white !important;">

	<style>
		iframe {
			display: none !important;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		p,
		th,
		td,
		label {
			color: #232323 !important;
		}

		.margin-title {
			margin-top: -20px !important;
		}

		.choose {
			background-color: #1ab394 !important;
			color: white !important;
		}

		.crop-logo {
			width: 63px !important;
			height: 63px !important;
			position: absolute !important;
		}

		.tb-br>tr>td {
			border: 1px solid #000 !important;
			padding-left: 5px !important;
		}

		.tb-brh>tr>th {
			border: 1px solid #000 !important;
			padding-left: 5px !important;
		}

		body {
			-webkit-print-color-adjust: exact;
		}

		.line-bd {
			border-top: 1px solid #000 !important;
		}
		@page {
			size: auto;   /* auto is the initial value */
			margin-top: 0;  /* this affects the margin in the printer settings */
			margin-bottom: 0;  /* this affects the margin in the printer settings */
		}
	</style>

	<!-- content -->
	<br><br>
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-2">
			<img class="crop-logo" src="<?php echo base_url()."assets/img/oasys-logo.png" ?>" alt="logounismuh">
		</div>
		<div class="col-sm-12">
			<center>
				<h3><b>UNIVERSITAS OASYS</b></h3><br>
				<h3 class="margin-title"><b>REGISTRASI DAN HERREGISTRASI MAHASISWA</b></h3><br>
				<h3 class="margin-title"><b>TAHUN <?php echo $_SESSION['batch']['periode'] ?></b></h3>
				<!-- <h5>Jl. Ahmad Yani Km 4.5 Telp (0511) 3252623 Fax (0511) 3254344 Banjarmasin | http://www.uin-antasari.ac.id</h5> -->
			</center>
		</div>
		<style media="screen">
			.color3 {
				color: #8be0be;
			}

			.color4 {
				color: #ffa982;
			}

			/* .table-bordered>th>tr>td{

                      border: 2px solid #000 !important;
					} */
			/* tr.spaceUnder>td {
				padding-bottom: 10em;
			} */
		</style>
	</div>
	<div class="wrapper wrapper-content line-bd">
		<div class="row">
			<div class="row">
				<div class="col-md-4 pull-right" style="margin-right:10px">
						<h4><b>No. <?php echo $camaba->no_registrasi ?></b></h4><br>
						<!-- <h4 class="margin-title"><b id="semester">TAHUN AKADEMIK</b></h4> -->
				</div>
			</div>
			<!-- <div class="row"> -->
			<div class="row">
				<div style="margin-left:20px !important;" class="col-sm-12">
					<table style="width:100%;font-size:12;">
						<tbody>
							<tr>
								<td colspan="3"><b>I. IDENTITAS DIRI</b></td>
							</tr>
							<tr>
								<td width="25%">Nama</td>
								<td width="1%">:</td>
								<td width="50%"><?php echo $camaba->nama ?></td>
							</tr>
							<tr>
								<td>Tempat Tanggal Lahir</td>
								<td>:</td>
								<td><?php echo $camaba->tempat_lahir.", ".$camaba->tanggal_lahir ?></td>
							</tr>
							<tr>
								<td>Jenis Kelamin</td>
								<td>:</td>
								<td>
								<?php foreach($jk->result() as $row){
									if($row->id == $camaba->jenis_kelamin){
										echo $row->nama;
									}
								}?>
								</td>
							</tr>
							<tr>
								<td>Agama</td>
								<td>:</td>
								<td>
								<?php foreach($agama->result() as $row){
									if($row->id == $camaba->agama){
										echo $row->nama;
									}
								}?>
								</td>
							</tr>
							<tr>
								<td>Bakat</td>
								<td>:</td>
								<td><?php echo $camaba->bakat ?></td>
							</tr>
							<tr>
								<td>Prestasi</td>
								<td>:</td>
								<td><?php echo $camaba->prestasi ?></td>
							</tr>
							<tr>
								<td>Alamat Sekarang</td>
								<td>:</td>
								<td><?php echo $camaba->alamat ?></td>
							</tr>
							<tr class="spaceUnder">
								<td>Pekerjaan/Instansi</td>
								<td>:</td>
								<td><?php echo $camaba->pekerjaan ?></td>
							</tr>
							<tr>
								<td colspan="3"><b>&nbsp;</b></td>
							</tr>
							<tr>
								<td colspan="3"><b>II. ALAMAT</b></td>
							</tr>
							<tr>
								<td>No. KTP</td>
								<td>:</td>
								<td><?php echo $camaba->nik ?></td>
							</tr>
							<tr>
								<td>Jalan</td>
								<td>:</td>
								<td><?php echo $camaba->jalan ?></td>
							</tr>
							<tr>
								<td>Dusun/Desa</td>
								<td>:</td>
								<td><?php echo $camaba->dusun ?></td>
							</tr>
							<tr>
								<td>Kelurahan</td>
								<td>:</td>
								<td><?php echo $camaba->kelurahan ?></td>
							</tr>
							<tr>
								<td>Kecamatan</td>
								<td>:</td>
								<td><?php echo $camaba->kecamatan ?></td>
							</tr>
							<tr>
								<td>Kewarganegaraan</td>
								<td>:</td>
								<td><?php foreach($kewarganegaraan->result() as $row){
									if($row->id == $camaba->kewarganegaraan){
										echo $row->negara;
									}
								}?></td>
							</tr>
							<tr>
								<td>Jenis Tinggal</td>
								<td>:</td>
								<td><?php foreach($jenis_tinggal->result() as $row){
									if($row->id == $camaba->jenis_tinggal){
										echo $row->nama;
									}
								}?></td>
							</tr>
							<tr>
								<td>No Telp</td>
								<td>:</td>
								<td><?php echo $camaba->no_telp ?></td>
							</tr>
							<tr>
								<td>Email</td>
								<td>:</td>
								<td><?php echo $camaba->email ?></td>
							</tr>
							<tr>
								<td colspan="3"><b>&nbsp;</b></td>
							</tr>
							<tr>
								<td colspan="3"><b>III. STATUS PENDAFTARAN</b></td>
							</tr>
							<tr>
								<td>Terdaftar di Fakultas</td>
								<td>:</td>
								<td><?php echo $prodi->fakultas ?></td>
							</tr>
							<tr>
								<td>No. Stambuk</td>
								<td>:</td>
								<td><?php echo $camaba->no_stambuk ?></td>
							</tr>
							<tr>
								<td>Jurusan/Program Studi</td>
								<td>:</td>
								<td><?php echo $prodi->jenjang." ".$prodi->nama ?></td>
							</tr>
							<tr>
								<td>Status Kemahasiswaan</td>
								<td>:</td>
								<td><?php foreach($jenis_pendaftaran->result() as $row){
									if($row->id == $camaba->id_tipe_pendaftaran){
										echo $row->tipe;
									}
								}?></td>
							</tr>
							<?php if($camaba->id_tipe_pendaftaran == 2){?>
							<tr>
								<td colspan="3"><b>Mulai Dari</b></td>
							</tr>
							<tr>
								<td>&emsp;PTN/PTS</td>
								<td>:</td>
								<td><?php echo $camaba->pt_asal ?></td>
							</tr>
							<tr>
								<td>&emsp;Fakultas</td>
								<td>:</td>
								<td><?php echo $camaba->fakultas_asal ?></td>
							</tr>
							<tr>
								<td>&emsp;Jurusan/Program Studi</td>
								<td>:</td>
								<td><?php echo $camaba->prodi_asal ?></td>
							</tr>
							<tr>
								<td>Alasan Mutasi</td>
								<td>:</td>
								<td></td>
							</tr>
							<?php }?>
							<tr>
								<td colspan="3"><b>&nbsp;</b></td>
							</tr>
							<tr>
								<td colspan="3"><b>IV. ORANG TUA</b></td>
							</tr>
							<tr>
								<td><b>Nama Ayah</b></td>
								<td>:</td>
								<td><?php echo $camaba->nama_ayah ?></td>
							</tr>
							<tr>
								<td>Tempat tanggal lahir</td>
								<td>:</td>
								<td><?php echo $camaba->tempat_lahir_ayah.", ".$camaba->tgl_lahir_ayah ?></td>
							</tr>
							<tr>
								<td>Pendidikan Terakhir</td>
								<td>:</td>
								<td>
								<?php foreach($pendidikan->result() as $row){
									if($row->id == $camaba->pendidikan_ayah){
										echo $row->nama;
									}
								}?>
								</td>
							</tr>
							<tr>
								<td>Pekerjaan</td>
								<td>:</td>
								<td>
								<?php foreach($pekerjaan->result() as $row){
									if($row->id == $camaba->pekerjaan_ayah){
										echo $row->nama;
									}
								}?>
								</td>
							</tr>
							<tr>
								<td>Jumlah Penghasilan/bulan</td>
								<td>:</td>
								<td>
								<?php foreach($penghasilan->result() as $row){
									if($row->id == $camaba->penghasilan_ayah){
										echo $row->jumlah;
									}
								}?>
								</td>
							</tr>
							<tr>
								<td colspan="3"><b>&nbsp;</b></td>
							</tr>
							<tr>
								<td><b>Nama Ibu</b></td>
								<td>:</td>
								<td><?php echo $camaba->nama_ibu ?></td>
							</tr>
							<tr>
								<td>Tempat tanggal lahir</td>
								<td>:</td>
								<td><?php echo $camaba->tempat_lahir_ibu.", ".$camaba->tgl_lahir_ibu ?></td>
							</tr>
							<tr>
								<td>Pendidikan Terakhir</td>
								<td>:</td>
								<td><?php foreach($pendidikan->result() as $row){
									if($row->id == $camaba->pendidikan_ibu){
										echo $row->nama;
									}
								}?></td>
							</tr>
							<tr>
								<td>Pekerjaan</td>
								<td>:</td>
								<td><?php foreach($pekerjaan->result() as $row){
									if($row->id == $camaba->pekerjaan_ibu){
										echo $row->nama;
									}
								}?></td>
							</tr>
							<tr>
								<td>Jumlah Penghasilan/bulan</td>
								<td>:</td>
								<td>
								<?php foreach($penghasilan->result() as $row){
									if($row->id == $camaba->penghasilan_ibu){
										echo $row->jumlah;
									}
								}?>
								</td>
							</tr>
							<tr>
								<td colspan="3"><b>&nbsp;</b></td>
							</tr>
							<tr>
								<td><b>Nama Wali</b></td>
								<td>:</td>
								<td><?php echo $camaba->nama_wali ?></td>
							</tr>
							<tr>
								<td>Tempat tanggal lahir</td>
								<td>:</td>
								<td><?php echo $camaba->tempat_lahir_wali.", ".$camaba->tgl_lahir_wali ?></td>
							</tr>
							<tr>
								<td>Pendidikan Terakhir</td>
								<td>:</td>
								<td><?php foreach($pendidikan->result() as $row){
									if($row->id == $camaba->pendidikan_wali){
										echo $row->nama;
									}
								}?></td>
							</tr>
							<tr>
								<td>Pekerjaan</td>
								<td>:</td>
								<td><?php foreach($pekerjaan->result() as $row){
									if($row->id == $camaba->pekerjaan_wali){
										echo $row->nama;
									}
								}?></td>
							</tr>
							<tr>
								<td>Jumlah Penghasilan/bulan</td>
								<td>:</td>
								<td>
								<?php foreach($penghasilan->result() as $row){
									if($row->id == $camaba->penghasilan_wali){
										echo $row->jumlah;
									}
								}?>
								</td>
							</tr>
						</tbody>
					</table>
					&nbsp;
					<table width="100%" style="font-size:12;">
						<tbody>
							<tr>
								<td width="10%"><b><small>PERSYARATAN PENDAFTARAN ULANG :<small></b></td>
								<td>&nbsp;</td>
								<td width="5%" id="waktu">Sengkang, 20 Desember 2018</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>Mahasiswa Ybs</td>
							</tr>
							<tr>
								<td>1. Mengisi Formulir (Herregistrsai BAAK)</td>
								<td width="3%" align="left" rowspan="8" colspan="1"><img src="<?php echo base_url().$camaba->foto?>" style="width:100px;height:130px;"
									 alt="photo 2x3"> </td>
								<td rowspan="8" style="border-bottom:1px solid;">&nbsp;</td>
							</tr>
							<tr>
								<td>2. Fotocopy Ijazah Terakhir dilegalisir 3 lbr</td>
								<!-- <td rowspan="4" colspan="3">&nbsp;</td> -->
							</tr>
							<tr>
								<td>3. Fotocopy KTP, Kartu Keluarga, Akte Kelahiran (@ 3lbr)</td>
							</tr>
							<tr>
								<td>4. Pas Photo Ukuran:</td>
							</tr>
							<tr>
								<td>&emsp; a. 2 x 3 = 3 lbr</td>
							</tr>
							<tr>
								<td>&emsp; b. 3 x 4 = 3 lbr</td>
							</tr>
							<tr>
								<td>5. Surat Izin Belajar 3 lbr bagi PNS</td>
							</tr>
							<tr>
								<td>6. Surat Keterangan pindah dan Nilai Akademik Record Yang disahkan dari PT asal masing-masing 3 rangkap</td>
								<!-- <td><hr></td> -->
							</tr>
							<tr>
								<td>7. Fotocopy bukti bayar SPP 3 lbr dan memperlihatkan aslinya</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div> <br>

		<script>
		const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
  "Juli", "Agustus", "September", "Oktober", "November", "Desember"
];
var dt = new Date();
var time = "Sengkang, "+dt.getDate() + " " + monthNames[dt.getMonth()] + " " + dt.getFullYear();
$('#waktu').html(time);
print();
		</script>