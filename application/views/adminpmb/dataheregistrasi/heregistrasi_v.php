
  <!-- content -->
  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Data Heregistrasi</h2>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                      <button onclick="exportHeregistrasi()" class="btn btn-info"><i class="fa fa-upload"></i> Export</button>
                        <!-- <a href="<?php echo base_url()."adminpmb/dataherregistrasi/add"?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a> -->
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
            <div class="ibox-title">

            </div>
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table id="mytable" class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No. Stambuk</th>
                        <th>Nama Lengkap</th>
                        <th>Jenis Peserta</th>
                        <th>Nomor Kontak</th>
                        <th>Email</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- <tr>
                        <td>1</td>
                        <td>123456789</td>
                        <td>Budi Santoso</td>
                        <td>BARU</td>
                        <td>08123456789</td>
                        <td>budisan@gmail.com</td>
                        <td>
                          <center>
                          <a href="<?php echo base_url()."adminpmb/heregistrasi/edit"?>" class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></a>
                          <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                          </center>
                        </td>
                      </tr> -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
<script>

$(document).ready(function() {
              $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                        {
                            return {
                                "iStart": oSettings._iDisplayStart,
                                "iEnd": oSettings.fnDisplayEnd(),
                                "iLength": oSettings._iDisplayLength,
                                "iTotal": oSettings.fnRecordsTotal(),
                                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                            };
                        };

                        t = $("#mytable").DataTable({
                            initComplete: function() {
                                var api = this.api();
                                $('#mytable_filter input')
                                        .off('.DT')
                                        .on('keyup.DT', function(e) {
                                            if (e.keyCode == 13) {
                                                api.search(this.value).draw();
                                    }
                                });
                            },
                            oLanguage: {
                                sProcessing: "loading..."
                            },
                            processing: true,
                            serverSide: true,
                            select: true,
                            ajax: {"url": "<?php echo base_url("adminpmb/Heregistrasi/json");?>", "type": "POST"},
                            "columnDefs": [
                {
                    "targets": [ -1 ], //last column
                    "orderable": false, //set not orderable
                },
                ],
                            columns: [
                                {
                                    "data": "id",
                                    "orderable": false
                                },
                                {"data": "no_stambuk"},
                                {"data": "nama"},
                                {"data": "jenis"},
                                {"data": "no_telp"},
                                {"data": "email"},
                                {"data": "view"}
                            ],
                            order: [[1, 'asc']],
                            rowCallback: function(row, data, iDisplayIndex) {
                                var info = this.fnPagingInfo();
                                var page = info.iPage;
                                var length = info.iLength;
                                var index = page * length + (iDisplayIndex + 1);
                                $('td:eq(0)', row).html(index);
                            }
                        });


            });
            function reload_table()
{
$('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
}
function exportHeregistrasi(){
  var url = '<?php echo base_url("adminpmb/Excel/exportHeregistrasi") ?>';
  console.log(url);
  $(location).attr('href', url);
}
</script>
