
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Tambah Data Heregistrasi</h2>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="col-lg-3">
                <div class="ibox-content">
                  <center>
                    <img src="<?php echo base_url()."assets/images/profile/default.jpg"?>" style="width:200px" alt="foto_profil">
                  </center><br>
                  <center>
                    <label title="Upload image file" for="inputImage" class="btn btn-sm btn-primary" style="color:white">
                        <input type="file" accept="image/*" name="file" id="inputImage" class="hide"><i class="fa fa-camera"></i>
                        Ubah Foto
                    </label>
                  </center>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="tabs-container">
                       <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" href="#tab-1"> Identitas Diri</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-2">Data Alamat</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-3">Status Pendaftaran</a></li>
                           <li class=""><a data-toggle="tab" href="#tab-4">Data Orang Tua/Wali</a></li>
                       </ul>
                       <form class="form-horizontal">
                       <div class="tab-content">
                           <div id="tab-1" class="tab-pane active">
                               <div class="panel-body">
                                   <div class="row">
                                     <div class="col-lg-12">
                                       <div class="form-group"><label class="col-lg-2 control-label">Nama</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama" class="form-control" requied></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Tempat Lahir</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Tempat Lahir" class="form-control" requied></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Tanggal Lahir</label>
                                           <div class="col-lg-10"><input type="text" id="datepicker" placeholder="Masukkan Tanggal Lahir" class="form-control" requied></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Jenis Kelamin</label>
                                           <div class="col-lg-10">
                                             <select class="form-control" name="jk" required>
                                               <option value="0" selected disabled>-PILIH JENIS KELAMIN-</option>
                                               <option value="1">LAKI-LAKI</option>
                                               <option value="2">PEREMPUAN</option>
                                             </select>
                                           </div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Agama</label>
                                           <div class="col-lg-10">
                                             <select class="form-control" name="agama">
                                               <option value="0" selected disabled>-PILIH AGAMA-</option>
                                               <option value="1">BUDHA</option>
                                               <option value="2">HINDU</option>
                                               <option value="3">ISLAM</option>
                                               <option value="4">KATOLIK</option>
                                               <option value="5">KONGHUCU</option>
                                               <option value="6">PROTESTAN</option>
                                               <option value="7">LAINNYA</option>
                                             </select>
                                           </div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Hobi/Minat</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Hobi/Minat/Bakat/Prestasi" class="form-control" requied></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Alamat Sekarang</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Alamat Sekarang" class="form-control" requied></div>
                                       </div>
                                       <div class="form-group"><label class="col-lg-2 control-label">Pekerjaan/instansi</label>
                                           <div class="col-lg-10"><input type="text" placeholder="Masukkan Pekerjaan/Instansi" class="form-control" requied></div>
                                       </div>
                                     </div>
                                   </div>
                               </div>
                           </div>
                           <div id="tab-2" class="tab-pane">
                               <div class="panel-body">
                                  <div class="row">
                                    <div class="col-lg-12">
                                    <div class="form-group"><label class="col-lg-2 control-label">NIK</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan NIK" class="form-control" requied>
                                          <small>*Nomor KTP Tanpa Tanda Baca, Isikan Nomor Paspor Untuk Warga Negara Asing</small></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">NISN</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan NISN" class="form-control"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">NPWP</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan NPWP" class="form-control"></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Kewarganegaraan</label>
                                          <div class="col-lg-10">
                                            <select class="form-control" name="kwn" required>
                                              <option value="0" selected disabled>-PILIH KEWARGANEGARAAN-</option>
                                              <option value="1">WNI</option>
                                              <option value="2">WNA</option>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Provinsi</label>
                                          <div class="col-lg-10">
                                            <select name="kabupaten" id="" class="form-control">
                                              <option value="1" selected disabled>-PILIH Provinsi-</option>
                                              <option value="2">SULAWESI TENGAH</option>
                                              <option value="3">SULAWESI SELATAN</option>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Kota/Kabupaten</label>
                                          <div class="col-lg-10">
                                            <select name="kabupaten" id="" class="form-control">
                                              <option value="1" selected disabled>-PILIH KABUPATEN/KOTA-</option>
                                              <option value="2">KOTA PALU</option>
                                              <option value="3">KABUPATEN DONGGALA</option>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Kecamatan</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama Kecamatan" class="form-control" requied></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Kelurahan</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama Kelurahan" class="form-control" requied></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Jalan</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama Jalan" class="form-control" requied></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Dusun/Desa</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama Dusun/Desa" class="form-control" requied></div>
                                      </div>
                                        <div class="form-group">
                                        <label class="col-lg-2 control-label">RT</label>
                                            <div class="col-lg-5"><input type="text" placeholder="Masukkan RT" class="form-control" requied></div>
                                        </div>
                                        <div class="form-group">
                                        <label class="col-lg-2 control-label">RW</label>
                                            <div class="col-lg-5"><input type="text" placeholder="Masukkan RW" class="form-control" requied></div>
                                        </div>
                                        <div class="form-group">
                                        <label class="col-lg-2 control-label">Kode POS</label>
                                            <div class="col-lg-5"><input type="text" placeholder="Masukkan Kode POS" class="form-control" requied></div>
                                        </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Jenis Tinggal</label>
                                          <div class="col-lg-10">
                                            <select class="form-control" name="jenistinggal" required>
                                              <option value="0" selected disabled>-PILIH JENIS TINGGAL-</option>
                                              <option value="1">Orang Tua</option>
                                              <option value="2">Wali</option>
                                              <option value="3">Kost</option>
                                              <option value="4">Asrama</option>
                                              <option value="5">Lainnya</option>
                                            </select>
                                          </div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Transportasi</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Alat Transportasi" class="form-control" requied></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">No. Telepon</label>
                                          <div class="col-lg-10"><input type="number" min="0" placeholder="Masukkan No. HP" class="form-control" requied></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Email</label>
                                          <div class="col-lg-10"><input type="text" placeholder="Masukkan Email" class="form-control" requied></div>
                                      </div>
                                      <div class="form-group"><label class="col-lg-2 control-label">Penerima KPS?</label>
                                          <div class="col-lg-10">
                                            <select id="kps" class="form-control" name="jenistinggal" required>
                                              <option value="0" selected disabled>-PILIH-</option>
                                              <option value="1">IYA</option>
                                              <option value="2">TIDAK</option>
                                            </select>
                                          </div>
                                      </div>
                                      <div id="terimakps" class="form-group hidden"><label class="col-lg-2 control-label">No. KPS</label>
                                          <div class="col-lg-10"><input type="textr" placeholder="Masukkan Nomor KPS" class="form-control" requied></div>
                                      </div>
                                    </div>
                                  </div>
                               </div>
                           </div>
                           <div id="tab-3" class="tab-pane">
                             <div class="panel-body">
                               <div class="row">
                                 <div class="col-lg-12">
                                   <div class="form-group"><label class="col-lg-2 control-label">Terdaftar di Fakultas</label>
                                     <div class="col-lg-10">
                                       <select class="form-control" name="fakultas">
                                         <option value="0" selected disabled>-PILIH FAKULTAS</option>
                                         <option value="1">FAKULTAS EKONOMI</option>
                                         <option value="2">FAKULTAS TEKNIK</option>
                                       </select>
                                     </div>
                                   </div>
                                   <div class="form-group"><label class="col-lg-2 control-label">No. Stambuk</label>
                                       <div class="col-lg-10"><input type="text" placeholder="Masukkan No. Stambuk" value="1234567890" class="form-control" requied></div>
                                   </div>
                                   <div class="form-group"><label class="col-lg-2 control-label">Jurusan/Prodi</label>
                                       <div class="col-lg-10">
                                         <select class="form-control" name="jurusan">
                                           <option value="0" selected disabled>-PILIH JURUSAN-</option>
                                           <option value="1">S1 Ekonomi</option>
                                           <option value="2">S1 Akutansi</option>
                                         </select>
                                       </div>
                                   </div>
                                   <div class="form-group"><label class="col-lg-2 control-label">Status Kemahasiswaan</label>
                                       <div class="col-lg-10">
                                         <select class="form-control" name="statuskmhs" id="status">
                                         <option value="0" disabled>-PILIH STATUS KEMAHASISWAAN-</option>
                                           <option value="1" selected>Peserta Didik Baru</option>
                                           <option value="2">Pindahan</option>
                                           <option value="3">Alih Jenjang</option>
                                           <option value="4">Lintas Jalur</option>
                                           <option value="5">Rekognisi Pembelajaran lampau</option>
                                         </select>
                                       </div>
                                   </div>
                                   <div id="baru" class="hidden">
                                     <div class="form-group"><label class="col-lg-2 control-label">Asal Sekolah</label>
                                         <div class="col-lg-10"><input type="text" placeholder="Masukkan Asal Sekolah" class="form-control" requied></div>
                                     </div>
                                   </div>
                                   <div id="pindahan" class="hidden">
                                   <div class="form-group">
                                    <label class="col-lg-2 control-label">Mulai Dari:</label>
                                      <div class="col-lg-10">
                                        <label><small>PTN/PTS</small></label><input type="text" placeholder="Masukkan PTN/PTS" name="ptnpts" class="form-control" requied><br>
                                         <label><small>Fakultas</small></label><input type="text" placeholder="Masukkan Fakultas" name="fakultas" class="form-control" required><br>
                                         <label><small>Jurusan/Prodi</small></label><input type="text" placeholder="Masukkan Jurusan/Prodi" name="jurusan" class="form-control" required><br>
                                      </div>
                                    </div>
                                    <div class="form-group"><label class="col-lg-2 control-label">Alasan Mutasi</label>
                                        <div class="col-lg-10"><input type="text" placeholder="Masukkan Alasan Mutasi" class="form-control" requied></div>
                                    </div>
                                  </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                           <div id="tab-4" class="tab-pane">
                             <div class="panel-body">
                               <div class="row">
                                 <div class="col-lg-12">
                                   <div class="form-group">
                                    <label class="col-lg-2 control-label">Data Ayah:</label>
                                      <div class="col-lg-10">
                                        <label><small>Nama</small></label><input type="text" placeholder="Masukkan Nama Ayah" name="namaayah" class="form-control" requied><br>
                                        <label><small>Tempat Lahir</small></label><input type="text" placeholder="Masukkan Tempat Lahir" name="tempatlahir" class="form-control" required><br>
                                         <label><small>Tanggal Lahir</small></label><input type="text" placeholder="Masukkan Tanggal Lahir" id="datepicker2" name="tgllahirayah" class="form-control" required><br>
                                         <label><small>Pendidikan Terakhir</small></label>
                                            <select name="pendidikan" name="pendidikan" class="form-control">
                                              <option value="0" selected disable>-Pilih Pendidikan Terakhir-</option>
                                              <option value="2">Tidak Sekolah</option>
                                              <option value="3">SD / Sederajat</option>
                                              <option value="4">SMP / Sederajat</option>
                                              <option value="5">SMA/SMK/Sederajat</option>
                                              <option value="6">S1</option>
                                              <option value="7">S2</option>
                                              <option value="8">S3</option>
                                            </select><br>
                                         <label><small>Pekerjaan</small></label><input type="text" placeholder="Masukkan Pekerjaan" name="pekerjaan" class="form-control" required><br>
                                         <label><small>Penghasilan/Bulan</small></label><input value='0' type="number" min="0" placeholder="Masukkan Jumlah Penghasilan perBulan" name="penghasilan" class="form-control" required><br>
                                         <label><small>Alamat</small></label><input type="text" placeholder="Masukkan Alamat Ayah" name="alamatayah" class="form-control" required><br>
                                         <label><small>No. HP</small></label><input type="text" placeholder="Masukkan No. HP Ayah" name="nohpayah" class="form-control" required><br>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                     <label class="col-lg-2 control-label">Data Ibu:</label>
                                       <div class="col-lg-10">
                                         <label><small>Nama</small></label><input type="text" placeholder="Masukkan Nama Ibu" name="namaibu" class="form-control" requied><br>
                                         <label><small>Tempat Lahir</small></label><input type="text" placeholder="Masukkan Tempat Lahir" name="tempatlahir" class="form-control" required><br>
                                          <label><small>Tanggal Lahir</small></label><input type="text" placeholder="Masukkan Tanggal Lahir" id="datepicker3" name="tgllahirayah" class="form-control" required><br>
                                          <label><small>Pendidikan Terakhir</small></label>
                                            <select name="pendidikan" name="pendidikan" class="form-control">
                                              <option value="0" selected disable>-Pilih Pendidikan Terakhir-</option>
                                              <option value="2">Tidak Sekolah</option>
                                              <option value="3">SD / Sederajat</option>
                                              <option value="4">SMP / Sederajat</option>
                                              <option value="5">SMA/SMK/Sederajat</option>
                                              <option value="6">S1</option>
                                              <option value="7">S2</option>
                                              <option value="8">S3</option>
                                            </select><br>
                                          <label><small>Pekerjaan</small></label><input type="text" placeholder="Masukkan Pekerjaan" name="pekerjaan" class="form-control" required><br>
                                          <label><small>Penghasilan/Bulan</small></label><input value='0' type="number" min="0" placeholder="Masukkan Jumlah Penghasilan perbulan" name="penghasilan" class="form-control" required><br>
                                          <label><small>Alamat</small></label><input type="text" placeholder="Masukkan Alamat Ibu" name="alamatibu" class="form-control" required><br>
                                         <label><small>No. HP</small></label><input type="text" placeholder="Masukkan No. HP Ibu" name="nohpibu" class="form-control" required><br>
                                       </div>
                                     </div>
                                     <div class="form-group">
                                      <label class="col-lg-2 control-label">Data Wali:</label>
                                        <div class="col-lg-10">
                                          <label><small>Nama</small></label><input type="text" placeholder="Masukkan Nama Wali" name="namawali" class="form-control" requied><br>
                                          <label><small>Tempat Lahir</small></label><input type="text" placeholder="Masukkan Tempat Lahir" name="tempatlahir" class="form-control" required><br>
                                           <label><small>Tanggal Lahir</small></label><input type="text" placeholder="Masukkan Tanggal Lahir" id="datepicker4" name="tgllahirayah" class="form-control" required><br>
                                           <label><small>Pendidikan Terakhir</small></label>
                                            <select name="pendidikan" name="pendidikan" class="form-control">
                                              <option value="0" selected disable>-Pilih Pendidikan Terakhir-</option>
                                              <option value="2">Tidak Sekolah</option>
                                              <option value="3">SD / Sederajat</option>
                                              <option value="4">SMP / Sederajat</option>
                                              <option value="5">SMA/SMK/Sederajat</option>
                                              <option value="6">S1</option>
                                              <option value="7">S2</option>
                                              <option value="8">S3</option>
                                            </select><br>
                                           <label><small>Pekerjaan</small></label><input type="text" placeholder="Masukkan Pekerjaan" name="pekerjaan" class="form-control" required><br>
                                           <label><small>Penghasilan/Bulan</small></label><input value='0' type="number" min="0" placeholder="Masukkan Jumlah Penghasilan perbulan" name="penghasilan" class="form-control" required><br>
                                           <label><small>Alamat</small></label><input type="text" placeholder="Masukkan Alamat Wali" name="alamatwali" class="form-control" required><br>
                                         <label><small>No. HP</small></label><input type="text" placeholder="Masukkan No. HP Wali" name="nohpwali" class="form-control" required><br>
                                        </div>
                                      </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                       </div>
                       <div class="ibox-footer">
                         <center>
                           <button type="button" class="btn btn-md btn-primary" name="button"><i class="fa fa-send"></i> SUBMIT</button>
                           <button type="button" class="btn btn-md btn-warning" name="button" onclick="window.history.back()"><i class="fa fa-reply"></i> KEMBALI</button>
                         </center>
                       </div>
                     </form>
                   </div>
              </div>
            </div>
            <br><br>
          </div>

  <script type="text/javascript">
  $('#status').change(function(){
  status = $('#status').val();
  if(status=='1'){
    $('#baru').removeClass('hidden');
    $('#pindahan').addClass('hidden');
  }
  else{
    $('#pindahan').removeClass('hidden');
    $('#baru').addClass('hidden');
  }
});
  </script>

<script type="text/javascript">
  $('#kps').change(function(){
  kps = $('#kps').val();
  if(kps=='1'){
    $('#terimakps').removeClass('hidden');
  }
  else{
    $('#terimakps').addClass('hidden');
  }
});
  </script>
