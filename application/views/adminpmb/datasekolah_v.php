
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Data Sekolah</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
                <!-- <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div> -->
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="ibox-title">
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Filter Status</label> <select class="form-control" name="...">
                          <option value="1">Semua</option>
                          <option value="2">Telah diverifikasi</option>
                          <option value="3">Menunggu Verifikasi</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2 col-md-offset-7" style="margin-top:25px;">
                    <button class="btn btn-md btn-success" type="button" name="button"><small><i class="glyphicon glyphicon-ok"></i></small> Verifikasi Semua</button>
                  </div>
                </div>
              </div>
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover datatabeltagihan">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>NPSN/NSS</th>
                        <th>Nama Sekolah</th>
                        <th>Alamat</th>
                        <th>Nomor Kontak</th>
                        <th>Email</th>
                        <th>Akreditasi</th>
                        <th>Status</th>
                        <th width="120px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>123456789</td>
                        <td>SMA 1 Banjarmasin</td>
                        <td>Jl Kalimatan No.1, Kecamatan A, Kabupaten B, Povinsi C</td>
                        <td>0221030210</td>
                        <td>smansaban@gmail.com</td>
                        <td>A</td>
                        <td>Terverifikasi</td>
                        <td>
                          <center>
                            <a class='btn btn-success btn-xs' title='Verifikasi' href='#'><span class='glyphicon glyphicon-ok'></span></a>
                          <a class='btn btn-primary btn-xs' title='Lihat Data' href='<?php echo base_url()."adminpmb/datasekolah/detail"?>'><span class='glyphicon glyphicon-search'></span></a>
                          <a class='btn btn-warning btn-xs' title='Reset Password'><span class='glyphicon glyphicon-refresh'></span></a>
                          <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>123456780</td>
                        <td>SMA 2 Banjarmasin</td>
                        <td>Jl Sulawesi No.2, Kecamatan A, Kabupaten B, Povinsi C</td>
                        <td>0221030123</td>
                        <td>smandaban@gmail.com</td>
                        <td>B</td>
                        <td>Menunggu Verifikasi</td>
                        <td>
                          <center>
                            <a class='btn btn-success btn-xs' title='Verifikasi' href='#'><span class='glyphicon glyphicon-ok'></span></a>
                            <a class='btn btn-primary btn-xs' title='Lihat Data' href='#'><span class='glyphicon glyphicon-search'></span></a>
                          <a class='btn btn-warning btn-xs' title='Reset Password'><span class='glyphicon glyphicon-refresh'></span></a>
                          <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
