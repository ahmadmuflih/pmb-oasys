
  <!-- content -->
  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Data Ruangan Ujian</h2>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                      <a href="#" class="btn btn-info"><i class="fa fa-download"></i> Export</a>
                        <a href="<?php echo base_url()."adminpmb/dataregistrasi/add"?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
            <div class="ibox-title">
            
            </div>
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover datatabeltagihan">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No. Ruangan</th>
                        <th>Nama Ruangan</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>123</td>
                        <td>A102</td>
                        <td>
                          <center>
                          <a href="<?php echo base_url()."adminpmb/dataregistrasi/edit"?>" class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></a>
                          <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                          </center>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
