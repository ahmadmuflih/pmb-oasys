
  <!-- content -->
  <link href="<?php echo base_url()."assets" ?>/css/modal-img.css" rel="stylesheet">
  
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Tambah Data Registrasi</h2>
                </div>
            </div>

            <style media="screen">
            [class^='select2'] {
                border-radius: 0px !important;
                }
            </style>
            
            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="col-lg-3">
                <div class="ibox-content">
                  <center>
                    <img src="<?php echo base_url()."assets/images/profile/default.jpg"?>" style="width:200px" alt="foto_profil">
                  </center><br>
                  <center>
                    <label title="Upload image file" for="inputImage" class="btn btn-sm btn-primary" style="color:white">
                        <input type="file" accept="image/*" name="file" id="inputImage" class="hide"><i class="fa fa-camera"></i>
                        Ubah Foto
                    </label>
                  </center>
                  <hr>
                  <p>Silahkan Melakukan Pembayaran Pendaftaran Melalui Rekening </p> <br>
                  <table>
                    <tbody>
                      <tr>
                      <td>Nama Bank</td>
                      <td width="5%">:</td>
                      <td><b>BNI Syariah</b></td>
                      </tr>
                      <tr>
                      <td>No. Rekening</td>
                      <td>:</td>
                      <td><b>04192010192</b></td>
                      </tr>
                      <tr>
                      <td>Penerima</td>
                      <td>:</td>
                      <td><b>UNISMUH PALU</b></td>
                      </tr>
                      <tr>
                      <td>Jumlah Bayar</td>
                      <td>:</td>
                      <td><b>Rp 0</b></td>
                      </tr>
                    </tbody>
                  </table> <br>
                  <p> Lalu upload foto bukti pembayaran pendaftaran MABA <br> Silahkan <a onclick="scrollWin()">Klik disini</a></p>
                  <!-- <button class="btn btn-info btn-sm" onclick="scrollWin()">KLIK TO UPLOAD</button> -->

                  <script>
                    function scrollWin() {
                      $('html, body').animate({
                        scrollTop: '+=1000'
                      }, 2000);
                    }
                  </script>
                  <hr>
                  <center>
                    <p><b>STATUS PENGAJUAN</b></p>
                    <span class="label label-default">BELUM MENGAJUKAN</span>
                    <!-- <span class="label label-warning"><span class="fa fa-clock-o"></span> MENUNGGU ACC</span><br> -->
                    <!-- <span class="label label-primary"><span class="fa fa-check"></span> TELAH ACC</span> -->
                  </center>
                  <hr>
                  <center>
                    <p><b>SILAHKAN MELAKUKAN CETAK KARTU TEST</b></p>
                    <a href="<?php echo base_url()."camaba/registrasipmb/cetak" ?>" class="btn btn-sm btn-info"><i class="fa fa-print"></i> CETAK KARTU TEST</a>
                  </center>
                </div>
              </div>
              <div class="col-lg-9">
                <div class="ibox float-e-margins">
                  <div class="ibox-title">
                    <h5>No.Registrasi : A2013</h5>
                  </div>
                  <form class="form-horizontal">
                    <div class="ibox-content">
                      <div class="row">
                        <div class="col-lg-12">
                          <h3>Akun Registrasi</h3>
                          <div class="form-group"><label class="col-lg-2 control-label">Pilih Akun</label>
                            <div class="col-lg-10">
                            <select id="akun" type="text" name="akun" class="akun standart form-control">
                            </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <h3>A. Identitas Diri</h3>
                        </div>
                        <div class="col-lg-12">
                          <div class="form-group"><label class="col-lg-2 control-label">Nama</label>
                            <div class="col-lg-10"><input type="text" placeholder="Masukkan Nama" class="form-control" requied></div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Alamat</label>
                            <div class="col-lg-10"><input type="text" placeholder="Masukkan Alamar" class="form-control" requied></div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Tempat Lahir</label>
                            <div class="col-lg-10"><input type="text" placeholder="Masukkan Tempat Lahir" class="form-control" requied></div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Tanggal Lahir</label>
                            <div class="col-lg-10"><input type="text" id="datepicker" placeholder="Masukkan Tanggal Lahir" class="form-control"
                              requied></div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Kab/Kota/Provinsi</label>
                            <div class="col-lg-10"><input type="text" placeholder="Masukkan Tempat Lahir" class="form-control" requied></div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Jenis Kelamin</label>
                            <div class="col-lg-10">
                              <select class="form-control" name="agama">
                                <option value="0" selected disabled>-PILIH JENIS KELAMIN-</option>
                                <option value="1">LAKI-LAKI</option>
                                <option value="2">PEREMPUAN</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Agama</label>
                            <div class="col-lg-10">
                              <select class="form-control" name="agama">
                                <option value="0" selected disabled>-PILIH AGAMA-</option>
                                <option value="1">BUDHA</option>
                                <option value="2">HINDU</option>
                                <option value="3">ISLAM</option>
                                <option value="4">KATOLIK</option>
                                <option value="5">KONGHUCU</option>
                                <option value="6">PROTESTAN</option>
                                <option value="7">LAINNYA</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Jenis Pendaftar</label>
                            <div class="col-lg-10">
                              <select class="form-control" name="status" id="status">
                                <option value="0" selected disabled>-PILIH JENIS PENDAFTAR-</option>
                                <option value="1">BARU</option>
                                <option value="2">PINDAHAN/LANJUTAN</option>
                              </select>
                            </div>
                          </div>
                          <div class="hidden" id="baru">
                            <div class="form-group"><label class="col-lg-2 control-label">Asal SLTA/SMA</label>
                              <div class="col-lg-10"><input type="text" placeholder="Masukkan Asal SLTA/SMA dan Seerajat" class="form-control"
                                requied></div>
                            </div>
                          </div>
                          <div class="hidden" id="pindahan">
                            <div class="form-group"><label class="col-lg-2 control-label">Asal PTN/PTS</label>
                              <div class="col-lg-10"><input type="text" placeholder="Masukkan Asal PTN/PTS" class="form-control" requied></div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Fakultas</label>
                              <div class="col-lg-10"><input type="text" placeholder="Masukkan Asal Fakultas" class="form-control" requied></div>
                            </div>
                            <div class="form-group"><label class="col-lg-2 control-label">Jurusan/Prodi</label>
                              <div class="col-lg-10"><input type="text" placeholder="Masukkan Asal Jurusan / Program Studi" class="form-control" requied></div>
                            </div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Kewarganegaraan</label>
                            <div class="col-lg-10">
                              <select class="form-control" name="kwn">
                                <option value="0" selected disabled>-PILIH KEWARGANEGARAAN-</option>
                                <option value="1">WNI</option>
                                <option value="2">WNA</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Bakat/Minat</label>
                            <div class="col-lg-10"><input type="text" placeholder="Masukkan Minat/Bakat" class="form-control" requied></div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Prestasi</label>
                            <div class="col-lg-10"><input type="text" placeholder="Masukkan Prestasi yang pernah diraih" class="form-control"
                              requied></div>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <h3>B. Pilih Jurusan</h3>
                        </div>
                        <div class="col-lg-12">
                          <div class="form-group"><label class="col-lg-2 control-label">Pilihan 1</label>
                            <div class="col-lg-10">
                              <select class="form-control" name="kwn">
                                <option value="0" selected disabled>-PILIH JURUSAN-</option>
                                <optgroup label="I. FAKULTAS EKONOMI">
                                  <option value="1">1. S1 Manajemen</option>
                                </optgroup>
                                <optgroup label="II. FAKULTAS TEKNIK">
                                  <option value="2">2. S1 Teknik Sipil</option>
                                </optgroup>
                                <optgroup label="III. FKIP">
                                  <option value="3">3. S1 Pendidikan Bhs. Inggris</option>
                                  <option value="4">4. S1 Pendidikan Luar Sekolah</option>
                                  <option value="5">5. S1 PAUD</option>
                                </optgroup>
                                <optgroup label="IV. FAKULTAS HUKUM">
                                  <option value="6">6. S1 Ilmu-Ilmu Hukum</option>
                                </optgroup>
                                <optgroup label="V. FISIP">
                                  <option value="7">7. S1 Sosiologi</option>
                                  <option value="8">8. S1 Ilmu Administrasi Negara</option>
                                </optgroup>
                                <optgroup label="VI. FAKULTAS PERTANIAN">
                                  <option value="9">9. S1 Agribisnis</option>
                                  <option value="10">10. S1 Kehutanan</option>
                                </optgroup>
                                <optgroup label="VII. FAKULTAS KESEHATAN MASYARAKAT">
                                  <option value="11">11. S1 Ilmu Kesehatan Masyarakat</option>
                                </optgroup>
                                <optgroup label="VIII. FAKULTAS AGAMA ISLAM">
                                  <option value="12">12. S1 Dakwah/Penerangan Penyiaran Agama Islam</option>
                                  <option value="13">13. S1 Tarbiyah/Pendidikan Agama Islam</option>
                                  <option value="14">14. S1 Tarbiyah/Pendidikan Agama Islam</option>
                                  <option value="15">15. S1 PIAUD</option>
                                  <option value="16">16. S1 Hukum Ekonomi Syariah</option>
                                </optgroup>
                                <optgroup label="IX. PASCASARJANA">
                                  <option value="17">17. S2 Manajemen Pendidikan</option>
                                </optgroup>
                              </select>
                            </div>
                          </div>
                          <div class="form-group"><label class="col-lg-2 control-label">Pilihan 2</label>
                            <div class="col-lg-10">
                              <select class="form-control" name="kwn">
                                <option value="0" selected disabled>-PILIH JURUSAN-</option>
                                <optgroup label="I. FAKULTAS EKONOMI">
                                  <option value="1">1. S1 Manajemen</option>
                                </optgroup>
                                <optgroup label="II. FAKULTAS TEKNIK">
                                  <option value="2">2. S1 Teknik Sipil</option>
                                </optgroup>
                                <optgroup label="III. FKIP">
                                  <option value="3">3. S1 Pendidikan Bhs. Inggris</option>
                                  <option value="4">4. S1 Pendidikan Luar Sekolah</option>
                                  <option value="5">5. S1 PAUD</option>
                                </optgroup>
                                <optgroup label="IV. FAKULTAS HUKUM">
                                  <option value="6">6. S1 Ilmu-Ilmu Hukum</option>
                                </optgroup>
                                <optgroup label="V. FISIP">
                                  <option value="7">7. S1 Sosiologi</option>
                                  <option value="8">8. S1 Ilmu Administrasi Negara</option>
                                </optgroup>
                                <optgroup label="VI. FAKULTAS PERTANIAN">
                                  <option value="9">9. S1 Agribisnis</option>
                                  <option value="10">10. S1 Kehutanan</option>
                                </optgroup>
                                <optgroup label="VII. FAKULTAS KESEHATAN MASYARAKAT">
                                  <option value="11">11. S1 Ilmu Kesehatan Masyarakat</option>
                                </optgroup>
                                <optgroup label="VIII. FAKULTAS AGAMA ISLAM">
                                  <option value="12">12. S1 Dakwah/Penerangan Penyiaran Agama Islam</option>
                                  <option value="13">13. S1 Tarbiyah/Pendidikan Agama Islam</option>
                                  <option value="14">14. S1 Tarbiyah/Pendidikan Agama Islam</option>
                                  <option value="15">15. S1 PIAUD</option>
                                  <option value="16">16. S1 Hukum Ekonomi Syariah</option>
                                </optgroup>
                                <optgroup label="IX. PASCASARJANA">
                                  <option value="17">17. S2 Manajemen Pendidikan</option>
                                </optgroup>
                              </select>
                            </div>
                          </div>
                        </div>

                        <style>
                          tr.spaceUnder>td {
                            padding-bottom: 1em;
                          }
                        </style>

                        <div class="col-lg-6">
                          <h3>C. Kelengkapan Berkas</h3>
                        </div>
                        <div class="col-lg-12">
                        <table width="100%">
                            <tbody>
                            <tr class="spaceUnder">
                              <td align="center">
                              <span style="font-size:6em;" class="fa fa-file-pdf-o"></span><br><br>
                              <a href="" type="button" class="btn btn-primary"><span class="fa fa-file-pdf-o"></span> Lihat File</a>
                              </td>
                              <td>
                              <label for="">Upload Foto Ijazah (Asli)</label>
                              <input type="file" placeholder="Masukkan foto ijazah" accept="application/pdf" class="form-control"
                            requied></td>
                            </tr>
                            <tr class="spaceUnder">
                              <td align="center">
                              <span style="font-size:6em;" class="fa fa-file-pdf-o"></span><br><br>
                              <a href="" type="button" class="btn btn-primary"><span class="fa fa-file-pdf-o"></span> Lihat File</a>
                              </td>
                              <td>
                              <label for="">Upload Foto KTP / Keterangan Domisili (Asli)</label>
                              <input type="file" placeholder="Masukkan foto ktp" accept="application/pdf" class="form-control"
                            requied></td>
                            </tr>
                            <tr class="spaceUnder">
                              <td align="center">
                                <span style="font-size:6em;" class="fa fa-file-pdf-o"></span><br><br>
                                <a href="" type="button" class="btn btn-primary"><span class="fa fa-file-pdf-o"></span> Lihat File</a>
                              </td>
                              <td>
                              <label for="">Upload Foto Surat Kemigrasian (Opsional)</label>
                              <input type="file" placeholder="Masukkan foto surat migrasi" accept="application/pdf" class="form-control"
                            requied>
                              <small>*Khusus Pendaftar Asing</small>
                              </td>
                            </tr>
                            
                            <!-- additional -->
                            <tr class="spaceUnder">
                              <td align="center">
                                <span style="font-size:6em;" class="fa fa-file-pdf-o"></span><br><br>
                                <a href="" type="button" class="btn btn-primary"><span class="fa fa-file-pdf-o"></span> Lihat File</a>
                              </td>
                              <td>
                              <label for="">Upload Foto Transkrip NIlai PDF</label>
                              <input type="file" placeholder="Masukkan foto transkrip nilai" accept="application/pdf" class="form-control"
                            requied>
                              <small>*Khusus Jalur Pindahan/Alih Jenjang/Lintas Jalur</small>
                              </td>
                            </tr>
                            <!-- end additional -->

                              <tr class="spaceUnder">
                                <td align="center"><img src="<?php echo base_url()."assets/img/noimage.jpg"?>" class="myImg" style="width:100px" alt="Bukti Pembayaran PMB"></td>
                                <td>
                                <label for="">Upload Foto Bukti Pembayaran PMB (Asli)</label>
                                <input type="file" placeholder="Masukkan foto pembayaran" accept="image/*" class="form-control"
                              requied></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="col-lg-12"></div>
                      </div>
                    </div>
                    <div class="ibox-footer">
                      <center>
                        <button id="submit" onclick="set(-1)" type="button" class="btn btn-md btn-primary" name="button"><i class="fa fa-send"></i>
                          TAMBAHKAN</button>
                        <button type="button" class="btn btn-md btn-warning" name="button" onclick="window.history.back()"><i class="fa fa-reply"></i>
                          KEMBALI</button>
                      </center>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <br><br>
          </div>

          <!-- The Modal -->
          <div id="myModal" class="modal">
            <span class="close">&times;</span>
            <img class="modal-content" id="img01">
            <div id="caption"></div>
          </div>

          <script>
            // Get the modal
              var modal = document.getElementById('myModal');

              // Get the image and insert it inside the modal - use its "alt" text as a caption
              var img = $('.myImg');
              var modalImg = $("#img01");
              var captionText = document.getElementById("caption");
              $('.myImg').click(function(){
                  modal.style.display = "block";
                  var newSrc = this.src;
                  modalImg.attr('src', newSrc);
                  captionText.innerHTML = this.alt;
              });

              // Get the <span> element that closes the modal
              var span = document.getElementsByClassName("close")[0];

              // When the user clicks on <span> (x), close the modal
              span.onclick = function() {
                modal.style.display = "none";
              }
          </script>

  <script type="text/javascript">
  $(document).ready(function(){
    $(".akun").select2({
      placeholder: 'Pilih Akun',
      allowClear:true
    });
  });

  $('#status').change(function(){
  status = $('#status').val();
  if(status=='1'){
    $('#baru').removeClass('hidden');
    $('#pindahan').addClass('hidden');
  }
  else{
    $('#pindahan').removeClass('hidden');
    $('#baru').addClass('hidden');
  }
});
  </script>
