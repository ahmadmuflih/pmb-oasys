<link href="<?php echo base_url()."assets" ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()."assets" ?>/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url()."assets" ?>/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url()."assets" ?>/js/plugins/sweetalert/sweetalert.min.js"></script>
<link href="<?php echo base_url()."assets" ?>/css/plugins/dataTables/datatables.min.css" rel="stylesheet">


<link href="<?php echo base_url()."assets" ?>/css/datatables.css" rel="stylesheet">

<link href="<?php echo base_url()."assets" ?>/css/plugins/select2/select2.min.css" rel="stylesheet">


<!-- akun dan smt list -->
<link href="<?php echo base_url()."assets" ?>/css/akun_smt.css" rel="stylesheet">

<link href="<?php echo base_url()."assets" ?>/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url()."assets" ?>/css/style.css" rel="stylesheet">


<!-- scripts -->

<!-- Mainly scripts -->
<script src="<?php echo base_url()."assets" ?>/js/jquery-3.1.1.min.js"></script>
<!-- <script>$.ajaxPrefilter(function( options, originalOptions, jqXHR ) { options.async = true; });</script> -->
<script src="<?php echo base_url()."assets" ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/fullcalendar/moment.min.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url()."assets" ?>/js/inspinia.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI  -->
<script src="<?php echo base_url()."assets" ?>/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="<?php echo base_url()."assets" ?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()."assets" ?>/js/plugins/dataTables/datatables.min.js"></script>

<!-- canvajs -->
<script src="<?php echo base_url()."assets" ?>/js/canvasjs-2.0.1/canvasjs.min.js"></script>



<!-- Typehead -->
<script src="<?php echo base_url()."assets" ?>/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
	<meta charset="utf-8">
	<title></title>
</head>

<body style="background-color:white !important;">

	<style>
		iframe {
			display: none !important;
		}

		h1,
		h2,
		h3,
		h4,
		h5,
		p,
		th,
		td,
		label {
			color: #232323 !important;
		}

		.margin-title {
			margin-top: -20px !important;
		}

		.choose {
			background-color: #1ab394 !important;
			color: white !important;
		}

		.crop-logo {
			width: 93px !important;
			height: 93px !important;
			position: absolute !important;
		}

		.tb-br>tr>td {
			border: 1px solid #000 !important;
			padding-left: 5px !important;
		}

		.tb-brh>tr>th {
			border: 1px solid #000 !important;
			padding-left: 5px !important;
		}

		body {
			-webkit-print-color-adjust: exact;
			background-image: url("../../assets/img/grey-watermark.png");
		}

		.line-bd {
			border-top: 1px solid #000 !important;
		}
		@page {
			size: auto;   /* auto is the initial value */
			margin-top: 0;  /* this affects the margin in the printer settings */
			margin-bottom: 0;  /* this affects the margin in the printer settings */
		}
	</style>

	<!-- content -->
	<br><br>
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-sm-2">
			<img class="crop-logo" src="<?php echo base_url()."assets/img/oasys-logo.png" ?>" alt="logounismuh">
		</div>
		<div class="col-sm-12">
			<center>
				<h3><b>PANITIA PENERIMAAN MAHASISWA BARU</b></h3><br>
				<h3 class="margin-title"><b>UNIVERSITAS OASYS</b></h3><br>
				<h3 class="margin-title"><b>TAHUN AKADEMIK <?php echo $_SESSION['batch']['periode'] ?></b></h3>
				<!-- <h5>Jl. Ahmad Yani Km 4.5 Telp (0511) 3252623 Fax (0511) 3254344 Banjarmasin | http://www.uin-antasari.ac.id</h5> -->
			</center>
		</div>
		<style media="screen">
			.color3 {
				color: #8be0be;
			}

			.color4 {
				color: #ffa982;
			}

			/* .table-bordered>th>tr>td{

                      border: 2px solid #000 !important;
                    } */
		</style>
	</div>
	<div class="wrapper wrapper-content line-bd">
		<div class="row">
			<div class="row">
				<div class="col-sm-12">
					<center>
						<h4><b><u>KARTU TANDA PESERTA UJIAN</u></b></h4><br>
						<!-- <h4 class="margin-title"><b id="semester">TAHUN AKADEMIK</b></h4> -->
					</center>
				</div>
			</div>
			<!-- <div class="row"> -->
			<div class="row">
				<div style="margin-left:20px !important;" class="col-sm-12">

					<table style="width:100%">
						<tbody>
							<tr>
								<td width="16%">
									<h5>NO. UJIAN</h5>
								</td>
								<td width="1%">
									<h5>:</h5>
								</td>
								<td width="25%">
									<h5 class="noujian"><b><?php if(isset($ujian->no_ujian)) echo $ujian->no_ujian ?></b></h5>
								</td>
								<td width="20%"></td>
								<td width="10%">
									<h5>Ruang Ujian</h5>
								</td>
								<td width="1%">
									<h5>:</h5>
								</td>
								<td width="25%">
									<h5 class="ruangujian"><?php if(isset($ujian->gedung) && isset($ujian->ruangan)) echo $ujian->gedung." - ".$ujian->ruangan ?></h5>
								</td>
							</tr>
							<tr>
								<td>
									<h5>PILIHAN</h5>
								</td>
								<td>
									<h5>:</h5>
								</td>
								<td>
									<h5 class="nim"></h5>
								</td>
								<td></td>
								<td>
									<h5>Tanggal</h5>
								</td>
								<td>
									<h5>:</h5>
								</td>
								<td>
									<h5 class="tanggal"><?php if(isset($ujian->tanggal)) echo $ujian->tanggal ?></h5>
								</td>
							</tr>
                            <tr>
                                <td>
                                    <h5><b>&emsp;&emsp; 1. FAKULTAS/JURUSAN </b></h5>
                                </td>
                                <td>
                                    <h5>:</h5>
                                </td>
                                <td>
                                    <h5><b><?php if(isset($pilihan1)) echo $pilihan1 ?></b></h5>
                                </td>
                                <td></td>
								<td>
									<h5>Jam</h5>
								</td>
								<td>
									<h5>:</h5>
								</td>
								<td>
									<h5 class="jam"><?php if(isset($ujian->jam_mulai) && isset($ujian->jam_selesai)) echo $ujian->jam_mulai." - ".$ujian->jam_selesai ?></h5>
								</td>
                            </tr>
                            <tr>
                                <td>
                                    <h5><b>&emsp;&emsp; 2. FAKULTAS/JURUSAN </b></h5>
                                </td>
                                <td>
                                    <h5>:</h5>
                                </td>
                                <td>
                                    <h5><b><?php if(isset($pilihan2)) echo $pilihan2 ?></b></h5>
                                </td>
                            </tr>
						</tbody>
					</table>

                    <!-- <table>
                        <tbody>
                            <tr>
                                <td>
                                    <h5><b>&emsp;&emsp; 1. FAKULTAS/JURUSAN </b></h5>
                                </td>
                                <td>
                                    <h5>:</h5>
                                </td>
                                <td>
                                    <h5><b>KESEHATAN MASYARAKAT / Ilmu Kesehatan Masyarakat</b></h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h5><b>&emsp;&emsp; 2. FAKULTAS/JURUSAN </b></h5>
                                </td>
                                <td>
                                    <h5>:</h5>
                                </td>
                                <td>
                                    <h5><b>TEKNIK / TEKNIK SIPIL</b></h5>
                                </td>
                            </tr>
                        </tbody>
                    </table> -->
				</div>
			</div>
		</div> <br>
		<!-- <div class="row">
			<div class="ibox-content col-lg-12">
				<div class="table-responsive">
					<table class="table-bordered" width="100%" style="font-size:100% !important; font-color:#000 !important;">
						<thead class="tb-brh">
							<tr>
								<th>NO</th>
								<th>KODE</th>
								<th>MATA KULIAH</th>
								<th>K</th>
								<th>DOSEN</th>
								<th>KELAS</th>
								<th>RUANG</th>
								<th>HARI</th>
								<th>JAM</th>
							</tr>
						</thead>
						<tbody id="tablejadwal" class="tb-br">

						</tbody>
						<tfoot class="tb-br">
							<tr>
								<td colspan="3">
									<center><b>J U M L A H</b>
								</td>
								<td id="sks"></td>
								<td colspan="5"></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div> -->
		<div class="row">
                <center>
                    <h4><b>PANITIA PELAKSANA</b></h4>
                </center>
                <br><br>
			<div class="col-lg-12">
				<center>
                    <table style="margin-left:450px">
                        <tbody>
                            <tr>
                                <td>
                                    <h4><b id="waktu"></b></h4>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
					<table width="100%" style="margin-left:0px">
						<tbody>
							<tr>
								<td width="10%" align="center">
									<h5>Ketua</h5>
								</td>
								<td width="10%" align="center" rowspan="4"><img src="<?php echo base_url().$camaba->foto?>" style="width:100px;height:130px;"
									 alt="photo"> <br>
									 <div style="margin-top:20px;"> <h5> <?php echo $camaba->no_registrasi." - ".$camaba->nama ?></h5></div>
								</td>
								<td width="10%" align="center">
									<h5>Sekretaris</h5>
								</td>
                                <!-- <td width="10%">
                                    <h5 id="tanggal"></h5>
                                </td> -->
							</tr>
                            <tr>
                                <td align="center">
                                    <img src="<?php echo base_url()."assets/img/ttd1.png" ?>" style="width:150px;"
                                        alt="ttd1">
                                </td>
                                <td align="center">
                                    <img src="<?php echo base_url()."assets/img/ttd2.png" ?>" style="width:200px;"
                                        alt="ttd2">
                                </td>
                            </tr>
							<tr>
								<td align="center">
									<h5 class="ketua"><u>Nama Ketua Disini</u></h5>
								</td>
								<td align="center">
									<h5 class="sekretaris"><u>Nama Sekretaris Disini</u></h5>
								</td>
								<!-- <td>
									<h5 class="nama"></h5>
								</td> -->
							</tr>
                            <tr>
								<td align="center">
									<h5 class="nbmketua">NIP:1007 739</h5>
								</td>
								<td align="center">
									<h5 class="nbmsekretaris">NIP:1060 715</h5>
								</td>
								<!-- <td>
									<h5 class="nama"></h5>
								</td> -->
							</tr>
						</tbody>
					</table>
                    <br>
                    <hr style="border-top: dotted 3px;">
				</center>
			</div>
		</div>
	</div>
<script>
const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
  "Juli", "Agustus", "September", "Oktober", "November", "Desember"
];
var dt = new Date();
var time = "Sengkang, "+dt.getDate() + " " + monthNames[dt.getMonth()] + " " + dt.getFullYear();
$('#waktu').html(time);
print();
</script>