
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Data Registrasi <b>Periode PMB <?php echo $_SESSION['batch']['periode']." - ".$_SESSION['batch']['nama'] ?></b></h2>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                      <!-- <a href="#" class="btn btn-info"><i class="fa fa-upload"></i> Export</a> -->
                        <!-- <a href="<?php echo base_url()."adminpmb/dataregistrasi/add"?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a> -->
                    </div>
                </div>
                <div class="col-md-2 pull-right" style="margin-right:25px">
                    <div class="form-group">

                    <label class="control-label"> </label>
                          <button id="export" onclick="exportData()" class="btn btn-info pull-right hidden"><i class="fa fa-upload"></i> Export</button>
                    </div>
                  </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
            <div class="ibox-title">
              <div class="row">
                <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label">Filter Prodi:</label>
                        <div>
                        <select class="form-control" name="prodi" id="prodi">
                          <?php echo $jurusan_filter;?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                    <label class="control-label">Filter Pengajuan:</label>
                        <div>
                        <select class="form-control" name="tipe" id="tipe">
                          <option value="1">BELUM MENGAJUKAN</option>
                          <option value="2" selected>TELAH MENGAJUKAN</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group">
                    <label class="control-label">Filter Status:</label>
                        <div>
                        <select class="form-control" name="status" id="status">
                          <option value="-1">-PILIH STATUS-</option>
                          <option value="2">MENUNGGU ACC</option>
                          <option value="3">TELAH ACC</option>
                        </select>
                      </div>
                    </div>
                  </div>

                <div class="col-md-2 pull-right">
                    <div class="col-md-2">
                        <div class="form-group">
                          <label class="control-label"> </label>
                          <div class="btn-group" id="ubah_status">
                              <button data-toggle="dropdown" class="btn btn-success dropdown-toggle">UBAH STATUS <span class="caret"></span></button>
                              <ul class="dropdown-menu">
                                  <li><a onclick='ubahStatus(2)'>MENUNGGU ACC</a></li>
                                  <li><a onclick='ubahStatus(3)'>DI ACC</a></li>
                              </ul>

                          </div>

                        </div>

                    </div>

                  </div>
              </div>
            </div>
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table id="mytable" class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th></th>
                        <th>No. Registrasi</th>
                        <th>Nama Lengkap</th>
                        <th>Jenis</th>
                        <th>Pilihan 1</th>
                        <th>Pilihan 2</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- <tr>
                        <td>1</td>
                        <td align="center"><input type="checkbox"></td>
                        <td>123456789</td>
                        <td>Budi Santoso</td>
                        <td>S1 KESEHATAN MASYARAKAT</td>
                        <td>S1 ILMU HUKUM</td>
                        <td><span class="label label-default">MENUNGGU ACC</span></td>
                        <td>
                          <center>
                          <a href="<?php echo base_url()."adminpmb/dataregistrasi/edit"?>" class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></a>
                          <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                          </center>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td align="center"><input type="checkbox"></td>
                        <td>123456789</td>
                        <td>Budi Santika</td>
                        <td>S1 KESEHATAN MASYARAKAT</td>
                        <td>S1 MANAJEMEN</td>
                        <td><span class="label label-primary">TELAH ACC</span></td>
                        <td>
                          <center>
                          <a href='<?php echo base_url()."adminpmb/dataregistrasi/edit/$2"?>' class='btn btn-warning btn-xs' title='Edit Data'><span class='glyphicon glyphicon-edit'></span></a>
                          <button type='button' onclick='reset($1)' class='btn btn-danger btn-xs' title='Reset Data'><span class='glyphicon glyphicon-remove'></span></button>
                          </center>
                        </td>
                      </tr> -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
<script>
  var id_prodi = 0, status = -1, tipe = 2;
  $('#prodi').change(function() {
    id_prodi = $(this).val();
    loadData();
  });
  $('#tipe').change(function() {
    tipe = $(this).val();
    status = -1;
    if(tipe==1){
      $('#status').html("<option value='-1'>-PILIH STATUS-</option>"+
      "<option value='0'>BELUM MENGISI</option>"+
      "<option value='1'>BELUM UPLOAD BUKTI</option>");
      $('#ubah_status').addClass('hidden');
    }
    else{
      $('#status').html("<option value='-1'>-PILIH STATUS-</option>"+
      "<option value='2'>MENUNGGU ACC</option>"+
      "<option value='3'>TELAH ACC</option>");
      $('#ubah_status').removeClass('hidden');
    }
    loadData();
  });
  $('#status').change(function() {
    status = $(this).val();
    if(status == 3){
      $('#export').removeClass('hidden');
    }else{
      $('#export').addClass('hidden');
    }
    loadData();
  });
  loadData();
function loadData(){
            $('#mytable').DataTable().destroy();
              $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                        {
                            return {
                                "iStart": oSettings._iDisplayStart,
                                "iEnd": oSettings.fnDisplayEnd(),
                                "iLength": oSettings._iDisplayLength,
                                "iTotal": oSettings.fnRecordsTotal(),
                                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                            };
                        };

                        t = $("#mytable").DataTable({
                            initComplete: function() {
                                var api = this.api();
                                $('#mytable_filter input')
                                        .off('.DT')
                                        .on('keyup.DT', function(e) {
                                            if (e.keyCode == 13) {
                                                api.search(this.value).draw();
                                    }
                                });
                            },
                            oLanguage: {
                                sProcessing: "loading..."
                            },
                            processing: true,
                            serverSide: true,
                            select: true,
                            ajax: {"url": "<?php echo base_url("adminpmb/Dataregistrasi/json");?>", "type": "POST", "data":{
                              'id_prodi' : id_prodi,
                              'status' : status,
                              'tipe' : tipe
                            }},
                            "columnDefs": [
                {
                    "targets": [ -1,1], //last column
                    "orderable": false, //set not orderable
                }, { "searchable": false, "targets": 1 },{
      "render": function ( data, type, row ) {
        // here you can convert data from base64 to hex and return it

        if(data == 0){
          label = 'label-default';
          st = 'BELUM MENGISI';
        }
        else if(data== 1){
           label = 'label-default';
           st = 'BELUM UPLOAD BUKTI';
         }
         else if(data== 2){
           label = 'label-warning';
           st = 'MENUNGGU ACC';
         }
         else{
           label = 'label-primary';
           st = 'TELAH ACC';
         }
               data = "<span class='label "+label+"'><span class='fa fa-pencil'></span> "+st+"</span><br>";

        return data
      },
      "targets": 7
   }

                ],
                            columns: [
                                {
                                    "data": "id",
                                    "orderable": false
                                },
                                {"data": "check"},
                                {"data": "no_registrasi"},
                                {"data": "nama"},
                                {"data": "jenis"},
                                {"data": "pilihan1"},
                                {"data": "pilihan2"},
                                {"data": "status"},
                                {"data": "view"}
                            ],
                            order: [[2, 'asc']],
                            rowCallback: function(row, data, iDisplayIndex) {
                                var info = this.fnPagingInfo();
                                var page = info.iPage;
                                var length = info.iLength;
                                var index = page * length + (iDisplayIndex + 1);
                                $('td:eq(0)', row).html(index);
                            }
                        });


            };
            function ubahStatus(st){
      console.log("acc");
      arr = [];
      $("input:checkbox[name=checkstatus]:checked").each(function(){
        arr.push($(this).val());
    });
    if(arr.length==0){
      swal("", "Centang camaba terlebih dahulu");
    }
    else{
      var formData = new FormData();
      for(var i =0; i < arr.length; i++)
        formData.append('id_camaba[]',arr[i]);
      formData.append('status',st);
      $.ajax({
        url: '<?php echo base_url("adminpmb/Dataregistrasi/ubahStatus");?>',
        data: formData,
        type: 'POST',
        // THIS MUST BE DONE FOR FILE UPLOADING
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
          if (data.status=='berhasil') {
            swal("Berhasil!", data.message, "success");
            loadData();
          }else {
            swal("Gagal!", data.message, "error");
          }
        },
            error: function(jqXHR, textStatus, errorThrown)
            {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        alert('gagal');
      }
      })
      return false;
      }
  }
  function reset(id){
    swal({
    title: "Anda yakin?",
    text: "Status registrasi camaba akan menjadi 'BELUM MENGISI'",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#1ab394",
    confirmButtonText: "Ya, reset!",
    closeOnConfirm: false
}, function () {
    var formData = new FormData();

        formData.append('id_camaba[]',id);
      formData.append('status',0);
      $.ajax({
        url: '<?php echo base_url("adminpmb/Dataregistrasi/ubahStatus");?>',
        data: formData,
        type: 'POST',
        // THIS MUST BE DONE FOR FILE UPLOADING
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
          if (data.status=='berhasil') {
            swal("Berhasil!", data.message, "success");
            loadData();
          }else {
            swal("Gagal!", data.message, "error");
          }
        },
            error: function(jqXHR, textStatus, errorThrown)
            {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        alert('gagal');
      }
      })
    });
  }
  function reload_table()
{
$('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
}
function exportData(){
  var url = "<?php echo base_url().'adminpmb/Excel/exportRegistrasi?batch='.$_SESSION['batch']['id']?>&prodi="+id_prodi;
  var win = window.open(url, '_blank');
  win.focus();
}
  </script>
