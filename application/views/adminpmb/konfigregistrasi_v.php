
  <!-- content -->
  <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-8">
                    <h2>Konfigurasi Registrasi Periode <b><?php echo $_SESSION['batch']['periode']." - ".$_SESSION['batch']['nama']?></b></h2>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
            <div class="wrapper wrapper-content">
              <form id="form">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="ibox-title">
                      <div class="row">
                        <div class="col-md-3">
                          <h3>Pengaturan Registrasi</h3>
                        </div>
                      </div>
                    </div>
                        <div class="ibox-content col-md-12">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <?php
                                $cek = "";
                                if($batch->aktif_registrasi == 1){
                                  $cek = "checked='checked'";
                                }
                                echo "<label> <input type='checkbox' $cek name='aktif_registrasi' class='checkbox-primary'> Aktifkan Registrasi Akun PMB </label><br><br>";
                                $cek = "";
                                if($batch->aktif_cetak_ujian == 1){
                                  $cek = "checked='checked'";
                                }
                                echo "<label> <input type='checkbox' $cek name='aktif_cetak_ujian' class='checkbox-primary'> Aktifkan Cetak Kartu Test/Ujian </label><br><br>";
                                $cek = "";
                                if($batch->aktif_pengumuman_lulus == 1){
                                  $cek = "checked='checked'";
                                }
                                echo "<label> <input type='checkbox' $cek name='aktif_pengumuman_lulus' class='checkbox-primary'> Aktifkan Pengumuman Kelulusan </label><br><br>";
                                $cek = "";
                                if($batch->aktif_cetak_herregistrasi == 1){
                                  $cek = "checked='checked'";
                                }
                                echo "<label> <input type='checkbox' $cek name='aktif_cetak_herregistrasi' class='checkbox-primary'> Aktifkan Cetak Kartu Her Registrasi </label><br>";
                                
                                ?>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div style="margin-top:40px" class="col-lg-12">
                          <center>
                            <button type="submit" class="btn btn-primary" title="SIMPAN KONFIGURASI"><span class="fa fa-save"></span> SIMPAN</button>
                          </center>
                    </div>
                </div>
              </form>
          </div>
<script>
$('#form').submit(function(){
var form = $('#form')[0]; // You need to use standart javascript object here
var formData = new FormData();
formData.append('id',<?php echo $batch->id?>);
var checkbox = $("#form").find("input[type=checkbox]");
$.each(checkbox, function(key, val){
  console.log(this);
  if(this.checked){
   formData.append($(val).attr('name'), 1);
}else{
   formData.append($(val).attr('name'), 0);
}
});
$.ajax({
  url: '<?php echo base_url("adminpmb/Konfigregistrasi/insert");?>',
  data: formData,
  type: 'POST',
  // THIS MUST BE DONE FOR FILE UPLOADING
  contentType: false,
  processData: false,
  dataType: "JSON",
  success: function(data){
    if (data.status=='berhasil') {
      swal("Berhasil!", data.message, "success");
    }else {
      swal("Gagal!", data.message, "error");
    }
  },
      error: function(jqXHR, textStatus, errorThrown)
      {
  console.log(jqXHR);
  console.log(textStatus);
  console.log(errorThrown);
  alert('gagal');
}
})
return false;
});
</script>