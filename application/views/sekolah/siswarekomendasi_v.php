
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Data Siswa Rekomendasi Bidik Misi</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
                <!-- <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div> -->
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="ibox float-e-margins">
                <div class="ibox-title">
                  <form>
                  <div class="row">
                    <div class="col-md-6 pull-right">
                      <div class="input-group"><input type="text" class="typeahead_1 form-control" placeholder="Cari siswa yang akan direkomendasikan di sini..."> <span class="input-group-btn"> <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</button> </span></div>
                    </div>
                  </div><br>
                </form>
                </div>
                <div class="ibox-content col-lg-12">

                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover datatabeltagihan">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>NIS</th>
                          <th>Nama Lengkap</th>
                          <th>Jurusan Sekolah</th>
                          <th>Nomor Kontak</th>
                          <th>Email</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>123456789</td>
                          <td>Budi Santoso</td>
                          <td>IPA</td>
                          <td>budisan@gmail.com</td>
                          <td>08123456789</td>
                          <td>
                            <center>
                            <!-- <a class='btn btn-warning btn-xs' title='Edit Data' data-toggle="modal" data-target="#editModal"><span class='glyphicon glyphicon-edit'></span></a> -->
                            <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

<script>
      $(document).ready(function(){

          $('.typeahead_1').typeahead({
              source: ["item 1","item 2","item 3"]
          });

          $.get('js/api/typehead_collection.json', function(data){
              $(".typeahead_2").typeahead({ source:data.countries });
          },'json');

          $('.typeahead_3').typeahead({
              source: [
                  {"name": "Afghanistan", "code": "AF", "ccn0": "040"},
                  {"name": "Land Islands", "code": "AX", "ccn0": "050"},
                  {"name": "Albania", "code": "AL","ccn0": "060"},
                  {"name": "Algeria", "code": "DZ","ccn0": "070"}
              ]
          });


      });
  </script>
