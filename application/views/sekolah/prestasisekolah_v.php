
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Data Prestasi Sekolah</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div>
            </div>
            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="ibox-content col-lg-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover datatabeltagihan">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Kegiatan</th>
                        <th>Kelas</th>
                        <th>Tingkat</th>
                        <th>Pencapaian/Hasil</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Debat Bahasa Inggris</td>
                        <td>11 IPA A</td>
                        <td>Kabupaten/Kota</td>
                        <td>Juara 2</td>
                        <td>
                          <center>
                          <a class='btn btn-warning btn-xs' title='Edit Data' data-toggle="modal" data-target="#editModal"><span class='glyphicon glyphicon-edit'></span></a>
                          <a class='btn btn-danger btn-xs' title='Hapus Data' href='#'><span class='glyphicon glyphicon-remove'></span></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="modal inmodal" id="addModal" tabindex="-1" role="dialog"  aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content animated fadeInDown">
                      <div class="modal-header">
                          <h4 class="modal-title">Tambah Data Prestasi Sekolah Baru</h4>
                          <small>Pastikan data yang diisi telah sesuai</small>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal">
                        <div class="form-group">
                          <label for="nama">Nama Kegiatan: <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" placeholder="Masukkan Nama Kegiatan" required>
                        </div>
                        <div class="form-group">
                          <label for="nama">Kelas: <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" placeholder="Masukkan Kelas" required>
                        </div>
                        <div class="form-group">
                          <label for="nama">Tingkat: <span style="color:red;">*</span></label>
                          <select class="form-control" required>
                            <option value="1" selected disabled>-Pilih Tingkat Prestasi-</option>
                            <option value="2">Kecamatan</option>
                            <option value="3">Kabupaten/Kota</option>
                            <option value="4">Provinsi</option>
                            <option value="5">Nasional</option>
                            <option value="6">Internasional</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="nama">Pencapaian/Hasil: <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" placeholder="Masukkan Pencapaian Prestasi Sekolah" required>
                        </div>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                  </div>
              </div>
          </div>

          <div class="modal inmodal" id="editModal" tabindex="-1" role="dialog"  aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content animated fadeInDown">
                      <div class="modal-header">
                          <h4 class="modal-title">Edit Data Prestasi Sekolah</h4>
                          <small>Pastikan data yang diisi telah sesuai</small>
                      </div>
                      <div class="modal-body">
                        <form class="form-horizontal">
                        <div class="form-group">
                          <label for="nama">Nama Kegiatan: <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" value="Debat Bahasa Inggris" required>
                        </div>
                        <div class="form-group">
                          <label for="nama">Kelas: <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" value="11 IPA A" required>
                        </div>
                        <div class="form-group">
                          <label for="nama">Tingkat: <span style="color:red;">*</span></label>
                          <select class="form-control" required>
                            <option value="1"disabled>-Pilih Tingkat Prestasi-</option>
                            <option value="2">Kecamatan</option>
                            <option value="3" selected>Kabupaten/Kota</option>
                            <option value="4">Provinsi</option>
                            <option value="5">Nasional</option>
                            <option value="6">Internasional</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="nama">Pencapaian/Hasil: <span style="color:red;">*</span></label>
                          <input type="text" class="form-control" value="Juara 2" required>
                        </div>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                  </div>
              </div>
          </div>
