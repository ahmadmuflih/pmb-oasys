
  <!-- content -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Edit Profil</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">This is</a>
                        </li>
                        <li class="active">
                            <strong>Breadcrumb</strong>
                        </li>
                    </ol>
                </div>
                <!-- <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#myModal4"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>
                </div> -->
            </div>

            <div class="wrapper wrapper-content">
            <div class="row">
              <div class="ibox-content col-lg-12">
                <form class="form-horizontal">
                          <h2>Identitas Sekolah</h2>
                          <div class="hr-line-dashed"></div><br >
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Nama Sekolah:</label>
                              <div class="col-lg-6"><input type="text" class="form-control"></div>
                            </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">NPSN/NSS:</label>
                              <div class="col-lg-6"><input type="number" min="0" class="form-control"></div>
                            </div>
                            <div class="form-group">
                              <label class="col-lg-2 control-label">Provinsi:</label>
                                <div class="col-lg-6"><select class="form-control">
                                  <option value="1" selected disabled>-Pilih Provinsi-</option>
                                  <option value="2">Jawa Barat</option>
                                  <option value="3">Jawa Tengah</option>
                                </select></div>
                              </div>
                            <div class="form-group">
                              <label class="col-lg-2 control-label">Kab/Kota:</label>
                                <div class="col-lg-6"><select class="form-control">
                                  <option value="1" selected disabled>-Pilih Kab/Kota-</option>
                                  <option value="2">Kabupaten Bandung</option>
                                  <option value="3">Kota Bandung</option>
                                </select></div>
                              </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Alamat Sekolah:</label>
                              <div class="col-lg-6"><input type="text" class="form-control"></div>
                            </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Nomor Kontak:</label>
                              <div class="col-lg-6"><input type="number" min="0" class="form-control"></div>
                            </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Pemilik:</label>
                              <div class="col-lg-6"><select class="form-control">
                                <option value="1" selected disabled>-Pilih Pemilik-</option>
                                <option value="2">Pemerintah</option>
                                <option value="3">Masyarakat</option>
                              </select></div>
                            </div>
                          <div class="form-group">
                            <label class="col-lg-2 control-label">Jenis Sekolah:</label>
                              <div class="col-lg-6"><select class="form-control">
                                <option value="1" selected disabled>-Pilih Jenis Sekolah-</option>
                                <option value="2">SMA</option>
                                <option value="3">MA</option>
                                <option value="4">SMK</option>
                                <option value="5">MAK</option>
                              </select></div>
                            </div>
                            <div class="form-group">
                              <label class="col-lg-2 control-label">Status Akreditasi:</label>
                                <div class="col-lg-6"><select class="form-control" id="akreditasi">
                                  <option value="1" selected disabled>-Pilih Akreditasi-</option>
                                  <option value="2">A</option>
                                  <option value="3">B</option>
                                  <option value="4">C</option>
                                  <option value="5">Lainnya</option>
                                </select></div>
                              </div>
                            <div id="nilai_ak" class="form-group hidden">
                              <label class="col-lg-2 control-label">Nilai Akreditasi:</label>
                                <div class="col-lg-6"><input type="number" min="0" class="form-control"></div>
                              </div>
                            <div class="form-group">
                              <label class="col-lg-2 control-label">Upload File Akreditasi:</label>
                                <div class="col-lg-6"><input type="file" class="form-control" accept="application/pdf,application/msword"></div>
                              </div>
                <br >
                <center>
                  <button type="submit" class="btn btn-w-m btn-primary" name="button"><i class="fa fa-save"></i> Simpan</button>
                </center>
              </form>
              </div>
            </div> <br >
          </div>

    <script type="text/javascript">
      $('#akreditasi').change(function(){
          akreditasi = $('#akreditasi').val();
          if(akreditasi=='5'){
            $('#nilai_ak').removeClass('hidden');
          }
          else{
            $('#nilai_ak').addClass('hidden');
          }
        });
    </script>
