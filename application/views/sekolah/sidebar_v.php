<div id="wrapper">

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="<?php echo base_url()."assets" ?>/img/profile_small.jpg" />
                         </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">David Williams</strong>
                        </span> <span class="text-muted text-xs block">Sekolah <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="active">
                <a href="<?php echo base_url()."sekolah/home"?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Akun</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li>
                    <a href="<?php echo base_url()."sekolah/ubahpassword"?>">Ubah Password</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()."sekolah/riwayatlogin"?>">Riwayat Login</a>
                  </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Master Data</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li>
                    <a href="<?php echo base_url()."sekolah/editprofile"?>">Profil Sekolah</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url()."sekolah/datasiswa"?>">Data Siswa</a>
                  </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label">Bidik Misi</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="<?php echo base_url()."sekolah/prestasisekolah"?>">Prestasi Sekolah</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url()."sekolah/hasilun"?>">Hasil UN Sekolah</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url()."sekolah/siswarekomendasi"?>">Siswa Rekomendasi</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-trophy"></i> <span class="nav-label">PSB</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="#">Siswa Rekomendasi</a>
                    </li>
                </ul>
            </li>
        </ul>

    </div>
</nav>
