
<body class="gray-bg">

<style media="screen">
  .color-menu{
    color: #1ab394;
  }
  a:hover > .fa-building {
    color: white !important;
  }
  a:hover > .fa-user {
  color: white !important;
  }
  a:hover > .fa-file-text {
  color: white !important;
  }
  a:hover > .fa-graduation-cap {
  color: white !important;
  }
  /* a:visited {color:#1ab394;} */
</style>

    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-lg-6">
              <h2>Pilih Menu di Bawah</h2><br>
              <div class="row">
                    <div class="main-header-left">
        					<div class="main-header-left-mobile-bg"></div>
        					<ul class="header-menu">
        						<li class="header-menu-item">
        							<a href="<?php echo base_url()."registsekolah" ?>"class="header-menu-link">
        								<i class="fa fa-building" style="font-size:40px;"></i>
        								<div class="header-menu-label">
        									Registrasi Sekolah
        								</div>
        							</a>
        						</li>
        						<li class="header-menu-item">
        							<a href="<?php echo base_url()."registmandiri" ?>" class="header-menu-link">
        								<i class="fa fa-user" style="font-size:40px;"></i>
        								<div class="header-menu-label">
        									Registrasi Mandiri
        								</div>
        							</a>
        						</li>
        						<li class="header-menu-item">
        							<a href="#" class="header-menu-link">
        								<i class="fa fa-file-text" style="font-size:40px;"></i>
        								<div class="header-menu-label">
        									Panduan Registrasi
        								</div>
        							</a>
        						</li>
        						<li class="header-menu-item">
        							<a href="#" class="header-menu-link">
        								<i class="fa fa-graduation-cap" style="font-size:40px;"></i>
        								<div class="header-menu-label">
        									Program Studi
        								</div>
        							</a>
        						</li>
        					</ul>
										<a class="scrolldown inline-icon icon-arrow-down icon-30" href="#"></a>
									</div>
                </div>
            </div>
            <style media="screen">
            .crop {
              width: 79px;
              height: auto;
              overflow: hidden;
            }
            </style>

            <div class="col-md-6">
              <center>
              <!-- <img src="<?php echo base_url()."assets" ?>/img/logo-uin.png" class="crop" alt="logo-uin"> -->
              <h1>LOGIN PMB</h1>
            </center><br>
                <div class="ibox-content">
                  <?php
                    if (isset($_POST['masuk'])){
                      $u = $this->input->post('usr');
                      $p = $this->input->post('pwd');
                      $this->M_login->getLoginData($u,$p);
                    }
                  ?>
                    <form class="m-t" action="" method="post">
                        <div class="form-group">
                            <input type="text" name="usr" class="form-control" placeholder="Username" required="">
                        </div>
                        <div class="form-group">
                            <input type="password" name="pwd" class="form-control" placeholder="Password" required="">
                        </div>
                        <button type="submit" name="masuk" class="btn btn-primary block full-width m-b">Login</button>

                        <a href="#">
                            <small>Forgot password?</small>
                        </a>
                    </form>
                </div>
            </div>
            <div class="col-md-12 text-right">
              <small>© 2018</small>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright @4nesia
            </div>
        </div>
    </div>

</body>


</html>
