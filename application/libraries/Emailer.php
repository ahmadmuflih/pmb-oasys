<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailer {
    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $receipent;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $subject;

    /**
     * PROPDESCRIPTION
     * 
     * @access public
     * @var PROPTYPE
     */
    public $message;

    protected $CI;

        // We'll use a constructor, as you can't directly call a function
        // from a property definition.
    public function __construct()
    {
            // Assign the CodeIgniter super-object
            $this->CI =& get_instance();
    }
/**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getReceipent() {
        return $this->receipent;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $receipent ARGDESCRIPTION
     */
    public function setReceipent($receipent) {
        $this->receipent = $receipent;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $subject ARGDESCRIPTION
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @return RETURNTYPE RETURNDESCRIPTION
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * METHODDESCRIPTION
     * 
     * @access public
     * @param ARGTYPE $message ARGDESCRIPTION
     */
    public function setMessage($message) {
        $this->message = $message;
    }
    public function send(){
		$this->CI->load->library('email');

	//SMTP & mail configuration
	$config = array(
	    'protocol'  => 'smtp',
	    'smtp_host' => 'ssl://smtp.googlemail.com',
	    'smtp_port' => 465,
	    'smtp_user' => 'bot4nesia@gmail.com',
	    'smtp_pass' => 'completingtogether123',
	    'mailtype'  => 'html',
	    'charset'   => 'utf-8',
	    'newline' 	=> '\r\n'
	);
	$this->CI->email->initialize($config);
	$this->CI->email->set_mailtype("html");
	$this->CI->email->set_newline("\r\n");

	//Email content
	

	$this->CI->email->to($this->receipent);
	$this->CI->email->from('bot4nesia@gmail.com','Info Registrasi OASYS');
	$this->CI->email->subject($this->subject);
	$this->CI->email->message($this->message);

	//Send email
	$this->CI->email->send();
	}
}

