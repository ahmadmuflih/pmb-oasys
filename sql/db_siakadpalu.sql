-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 28, 2019 at 02:13 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_siakadpalu`
--

-- --------------------------------------------------------

--
-- Table structure for table `bris_uin`
--

CREATE TABLE `bris_uin` (
  `nim` char(16) NOT NULL,
  `nama_mhs` varchar(200) NOT NULL,
  `nama_thn_ak` varchar(200) NOT NULL,
  `jk` varchar(2) DEFAULT NULL,
  `nama_fakultas` varchar(150) NOT NULL,
  `nama_prodi` varchar(150) NOT NULL,
  `jenis_pembayaran` varchar(100) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `ket` varchar(200) DEFAULT NULL,
  `status_aktif` varchar(25) NOT NULL,
  `bebas_masalah` int(2) NOT NULL,
  `status_bayar` int(2) NOT NULL,
  `no_ref_bank` varchar(250) NOT NULL,
  `channel` varchar(250) NOT NULL,
  `tanggal_bayar` datetime NOT NULL,
  `virtual_account` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_jabatan_dosen`
--

CREATE TABLE `m_jabatan_dosen` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `minimal_jam` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jabatan_dosen`
--

INSERT INTO `m_jabatan_dosen` (`id`, `nama`, `minimal_jam`) VALUES
(1, 'Asisten Ahli', 21),
(2, 'Lektor', 17),
(3, 'Lektor Kepala', 13),
(4, 'Guru Besar', 9);

-- --------------------------------------------------------

--
-- Table structure for table `m_status_kegiatan_lkd`
--

CREATE TABLE `m_status_kegiatan_lkd` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_status_kegiatan_lkd`
--

INSERT INTO `m_status_kegiatan_lkd` (`id`, `nama`) VALUES
(0, 'Not Fixed'),
(1, 'Fixed');

-- --------------------------------------------------------

--
-- Table structure for table `m_unit_kerja`
--

CREATE TABLE `m_unit_kerja` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_unit_kerja`
--

INSERT INTO `m_unit_kerja` (`id`, `nama`) VALUES
(1, 'Bagian Akademik'),
(2, 'Sistem Informasi');

-- --------------------------------------------------------

--
-- Table structure for table `t_absensi`
--

CREATE TABLE `t_absensi` (
  `id` int(11) NOT NULL,
  `id_pilih_matkul` int(11) NOT NULL,
  `id_pertemuan` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tap` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_admin`
--

CREATE TABLE `t_admin` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_admin`
--

INSERT INTO `t_admin` (`id`, `id_pegawai`, `id_status`) VALUES
(7, 397, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_admin_fakultas`
--

CREATE TABLE `t_admin_fakultas` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_fakultas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_admin_jurusan`
--

CREATE TABLE `t_admin_jurusan` (
  `id` int(11) NOT NULL,
  `id_admin` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_agama`
--

CREATE TABLE `t_agama` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_agama`
--

INSERT INTO `t_agama` (`id`, `nama`) VALUES
(1, 'Islam'),
(2, 'Kristen'),
(3, 'Katolik'),
(4, 'Hindu'),
(5, 'Buddha'),
(6, 'Konghucu'),
(99, 'Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `t_akreditasi_sekolah`
--

CREATE TABLE `t_akreditasi_sekolah` (
  `id` int(11) NOT NULL,
  `nama` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_akreditasi_sekolah`
--

INSERT INTO `t_akreditasi_sekolah` (`id`, `nama`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `t_akun`
--

CREATE TABLE `t_akun` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1',
  `baru` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_akun`
--

INSERT INTO `t_akun` (`id`, `username`, `password`, `id_status`, `baru`) VALUES
(1011, 'adminku', '$2y$10$nXrXkQzaG9GpWWlhBzICXelmcv.9t89yWrcNrY5xzLmsNMcX8hzTi', 1, 1),
(1012, '1511071001', '$2y$10$R9Wb73kq9rMU4q6KJaC8sO6ClTt1r2SiYX8lOZLcv9LWbgCJDrotS', 1, 0),
(1013, '1511071002', '$2y$10$5vg7LVqoa65T0gbFBuIEm.ismTfu/adDBnmZHnICRz6ajOqHvRg7a', 1, 0),
(1014, '1511071003', '$2y$10$.jYbqtQuNfzxTlkf09j1S.8aqkGez78Kkg5vhMgOuIfl71VnPQ.Wu', 1, 0),
(1015, '1511071004', '$2y$10$eYeNe6xJmDmBjEsGEiIpbup9edUxakL7ZUuGBLbdtjzvfJDqxUmMS', 1, 0),
(1016, '1511071005', '$2y$10$wG96DQ6Bs7ttts.9.oJPneL5FJV6bjVkub0vvp8BfWfXYGq8N2vo.', 1, 0),
(1017, '1511071006', '$2y$10$5qPtm4EFsAnKot2.PXXBNufJoH1p9xJ4mjquBYlMcR42pc5K.O0Ma', 1, 0),
(1018, '1511071007', '$2y$10$vvJKnlCmH/UoZpZim7NDw.8D2u.ewJkAplJ9o/eKbseyxJoWyLBPu', 1, 0),
(1019, '1511071008', '$2y$10$JSdtGRI86jDJqe679kOeDOXj8yYkmCYuTuLQEEBa15cv/5ANOt3ee', 1, 0),
(1020, '1511071009', '$2y$10$Ou0OpQkCOzT/FlQx.dpQauZIcGsBaw0kV6Bpcr0mnKQDRzd/aNGky', 1, 0),
(1021, '1511071010', '$2y$10$olhJp.gyGZfeBKaTHEDH6uHYwDzE6fjJlolx5mZH3xVT4VxRKIYO.', 1, 0),
(1022, '1511071011', '$2y$10$ztV8raNFgs1LKmGxpt62L.Jaqn691bspCZlYiJkSAYw5EoEEonZ2C', 1, 0),
(1023, '1511071012', '$2y$10$32IMM26/j2MjPwAMpjQPweuutSlIEa6a8Htg7Pg.3EmuPTtaNILf2', 1, 0),
(1024, '1511071013', '$2y$10$5DcIDWuqdteZ8ei/IMmXYueBbmRTx7PINgc/MT2GmOhuikMaNvHae', 1, 0),
(1025, '1511071014', '$2y$10$aSkm7RvQhrC.SYjpr14icOzkk8RdZ6HkQo3jmpx4w6xQrNntpXsRe', 1, 0),
(1026, '1511071015', '$2y$10$lJQvqE4TVKxerUvo7Cq9PukLXi1JRb6H3X7crq3t7rLskjVT2XAjO', 1, 0),
(1027, '1511071016', '$2y$10$6Rht5stLvl3W0FZXEwT.pOlwXbCJU5V5KVurUb9W5W0WhenAKHspy', 1, 0),
(1028, '1511071017', '$2y$10$/U11mM8Mgg3AgYzxG/HqHOTd8CHwN8xatQIkPIfQAs8J1sMhw8xaG', 1, 0),
(1029, '1511071018', '$2y$10$X5Nypu8aLEHNf3somkxeI.eMCCUSs8MeeYrOcD.9gRATKjSuJN2vC', 1, 0),
(1030, '1511071019', '$2y$10$cWGuBbkIbJBbq.OTVHR6te/mhjKk8e1Q8y6f9zMXy4HR7pxOjGI0u', 1, 0),
(1031, '1511071020', '$2y$10$bkKHugWUyy7wmypJr3bDgu6SlwU1R2TXBgREBMb/NPAQXM.d6Ta/G', 1, 0),
(1032, '1511071021', '$2y$10$MPgKH3tQg5nIjKuxiwmYj.gcTWibcheI9NBLw8s54B8kwfJKKSyFu', 1, 0),
(1033, '1511071022', '$2y$10$gUBY9zaJzsfRNMNggVE6w.9sXtbCf/v.7js2NbukND6AdadJnYRpm', 1, 0),
(1034, '1511071023', '$2y$10$j5Ve0LJZ9q9OPbKRns7ygeON.Tb7HKQ.795PGiGWv0i7fV2p3DHiW', 1, 0),
(1035, '1511071024', '$2y$10$H2c8vJF.Xe/Zrcg67SkCRep.jFIed1pGR4tgAFyPrv2OhpwImOT1O', 1, 0),
(1036, '1511071025', '$2y$10$sg0QeEVURf6ULukz5IqFn.sM19Tsd8hw0Rjb8CpmTorOwnERvPYRq', 1, 0),
(1037, '1511071026', '$2y$10$YFTjoD3xLsp.oPNN5raKiOx2GHmVqUYzwxRfgH5CBxwwoOWUFtxqG', 1, 0),
(1038, '1511071027', '$2y$10$PeILd/QouiFTmpeQjjsbyOrcKuYjy1XrouDHS.wv8NQ23XR9k2Teq', 1, 0),
(1039, '1511071028', '$2y$10$knomac7QslpIokKHH9e7IensKVc0IkEqi3RJ574p.alKiUuMHnOha', 1, 0),
(1040, '1511071029', '$2y$10$b0Ld7pC2sR5M90nGt9m7XOAEqg9h5qQ3kTAGzpQdNKClnY3wht5vG', 1, 0),
(1041, '1511071030', '$2y$10$4N7VjmJsJ0yfHDWUKoEys.ZYu9UY2V5FSEyOhzNjfXrAzMayfFaKK', 1, 0),
(1042, '1511071031', '$2y$10$ITRHL85NLL6W83x4zr8Y6uw6Kej8fDI0if4a565FruClVTvceehD6', 1, 0),
(1043, '1511071032', '$2y$10$w0l5jf5r/MtuiLDcUvcYNOhH/WvQi3eTB5SDVn0I5hnBK5jv3fYPi', 1, 0),
(1044, '1511071033', '$2y$10$bK20VJk2Y9OesV3WcJlO7ui50.1ijG4vApHleWniIeXpcdmBXCITG', 1, 0),
(1045, '1511071034', '$2y$10$KyenKHSxn8FJ319sQwSyRO5vksNdj9aHIjCmuDt80JSej/s7mif1e', 1, 0),
(1046, '1511071035', '$2y$10$DR0X4Wv0jUyZihDq2JJPr.6NHbUmZXrbffDsSj348CqSYsFm84Q8y', 1, 0),
(1047, '1511071036', '$2y$10$nfs9avxlJ4kck8YzJwtxWOA2iJoyaYN0ycJW/1bJe8nmhEM0TvMkK', 1, 0),
(1048, '1511071037', '$2y$10$2SLrkvap0/FSP/tiyl2PF.s3Fnb.KusDeORKmadmf7bhLW3..bdTm', 1, 0),
(1049, '1511071038', '$2y$10$3cvLJEe.vGZ1W9ycVizzNO7GB8JKQ.tw3ymlV6bmILhngH1gGRROi', 1, 0),
(1050, '1511071039', '$2y$10$6MkBhtY0/hZ7W.nRpjNQZulnBGJNm/IzhaRUgN85YIHfBwuYaCbF2', 1, 0),
(1051, '1511071040', '$2y$10$sy0z09qmGdrCD/.6gVpk.ubnYUXzZb5ztmV5SNkxq8zIc.9K.zcoS', 1, 0),
(1052, '1511071041', '$2y$10$MeLqGiEfhwhrePJ.NMOKi.ojXQs8YqMuFEtPDivQ6TkDgW.jqNHDe', 1, 0),
(1053, '1511071042', '$2y$10$0QgkGYFe6TPY/HPcG3vYBOw86DoR5QoLu4kBUzf.7WLQyTDvHInci', 1, 0),
(1054, '1511071043', '$2y$10$8amWUhtayC.QBvLnAGBUle/Qi5ij77WSCDVosyz1JqonXboc0Z0kC', 1, 0),
(1055, '1511071044', '$2y$10$.dwsUn1WMlaJzwB2jQ6Xj.9yfK4A4i/AtHTIykP1SqSqJgBErn0.C', 1, 0),
(1056, '1511071045', '$2y$10$lMuhSUkf95DX0BKeqZr.eegAYqIlNY1Ens5alCylMbHyuBpE5er9C', 1, 0),
(1057, '1511071046', '$2y$10$aqHx5tDanOc1GF4k4W1vH.KJsEn8gfvFJRV5FLewEg7dXuETPObHW', 1, 0),
(1058, '1511071047', '$2y$10$i2SoVsTBtolT1Cz8pJSmQONsd2UDluav8dzyk3GgsD/f1yITGz2y.', 1, 0),
(1059, '1511071048', '$2y$10$ntR6xJy4O4B9DXUeQTaSRONdVWo8cGEPvgT.L3zNGi6JPCMRZOmx.', 1, 0),
(1060, '1511071049', '$2y$10$yk11wHFPGma0g3o/bRmbqO7BGKYD8VwXt0eHWyijUrctMMXOYmpE.', 1, 0),
(1061, '1511071050', '$2y$10$f4r5ULOAc69yAID1yD0v5ej8PhRUrbzsrflK9AQnnroiVVm1nnc3W', 1, 0),
(1062, '1511071051', '$2y$10$YQzUfuPu5rb7gQt3gcPPYuFi05Z2OregUNZcnC3iXeczmcE6y9uRu', 1, 0),
(1063, '1511071052', '$2y$10$v/nRxfUevdlGVLEKfuKfsu61JxRgtC1ysDZNHLUA12HXvf1AZk2xC', 1, 0),
(1064, '1511071053', '$2y$10$MXjpiahbbUDB8/cRUt8c7.UhdLAzg8AOH9sQuxUvxqVz9z92HLtaC', 1, 0),
(1065, '1511071054', '$2y$10$ZpAQ.dj4nTHeLEYgQ9Fie.8.k/B5sWnZOKKaat7gmYb.oc1Mqz.hW', 1, 0),
(1066, '1511071055', '$2y$10$3xGy9Dq1dnf9HRPfG8/BP.gOvrKafd7c5TbW/HjzpzSe87ROeE9Wu', 1, 0),
(1067, '1511071056', '$2y$10$0Zshg55u9Uex9kpsFdGleeps7x3c6jJCQIdgK2dbJn/S2Akjbh4MS', 1, 0),
(1068, '1511071057', '$2y$10$0YxSdb76FIvG/cPE6.hCt.d7Y4dhiGHrXEM8YsL/1Iz92XYuvKsC2', 1, 0),
(1069, '1511071058', '$2y$10$/SLgbb/P/HC4yhuyOtzEau3mE.sIndir9DDVOJUztc0zcjG6FHgxG', 1, 0),
(1070, '1511071059', '$2y$10$iObG2r.cVtdp1vUZYy1THuUSaAsvk3iaiscy1UyoZGvdFRh3lyfXi', 1, 0),
(1071, '1511071060', '$2y$10$cCuYpzcv3JpiVvWCto0HiuYQHe0.gYQTcAtkuT5z4GbcI43eeqoje', 1, 0),
(1072, '1511071061', '$2y$10$d6tHUCh.oOFFK.K9LXrVx.HVVoeX8WieLzZ8fcpKE92Fb7NtVVSPK', 1, 0),
(1073, '1511071062', '$2y$10$Ys6zIF1Dbq018DdbLO5dG.O5VUvs9odlzIixXsUPbwb0tmcN7ocCi', 1, 0),
(1074, '1511071063', '$2y$10$Kd6OHKN5pkHrSgIj4.KpZ.3sXg7er0M1WNxXPkafywCTIZuV3wi4G', 1, 0),
(1075, '1511071064', '$2y$10$Sgv2kd/YNIlujZsVEvwCD.CEbaZBPdwS4peqKtYEEsp0umYNmj7qi', 1, 0),
(1076, '1511071065', '$2y$10$u1DCx9LvVIY60TcKpao/Re7hMmHHvgHmXsPEcpkl5wgWcdowWoKZq', 1, 0),
(1077, '1511071066', '$2y$10$omiXij5OTlP9OpqGoB39oeUWsM2XbSnblSv0Mkxe7..osP1wu3.0K', 1, 0),
(1078, '1511071067', '$2y$10$VEIaSLyJrj2gce5gjUab0eWPl8J7k5PSF/Nx/40er2DjI.kpqYf9O', 1, 0),
(1079, '1511071068', '$2y$10$3rrXEEgxfocLQRWU1mQAxO4XIy3g8dyn9sMkAKixe06k7GGYdyH/u', 1, 0),
(1080, '1511071069', '$2y$10$J/SIae8he1BDcjKCf06Ozu3R3CmovdzTK0L1djT0odAXD7.s9DIVG', 1, 0),
(1081, '1511071070', '$2y$10$fXuBvHQ9HDBYSlJW8gaERevSMUQtXd/yMv6NsIkRS.LBUp1oBq3QC', 1, 0),
(1082, '1511071071', '$2y$10$BUbKxNnt9q2MECt7SI5GS.apG5QscCb16WMMsHlf9zC35Q6Q3Ex/e', 1, 0),
(1083, '1511071072', '$2y$10$STAGYQBzjAyVfC9L0pWlrOB6WL.b3Ld80tK49EDj03eFLS24CPGgS', 1, 0),
(1084, '1511071073', '$2y$10$NYwncF7WjvVLQv20M2sRE.uWo2WMPEhYWnd1mjBVjHVgYZeum44mC', 1, 0),
(1085, '1511071074', '$2y$10$LVWHRVwK7TeBrj31M68vt.K04E/JxirXKwEjIuVytdMr5eYgvhvn6', 1, 0),
(1086, '1511071075', '$2y$10$8hvHa5nCenMtcs1Q7LDOweRhJ4/MQtRr4FRDAH57x4XyGW34RUPVq', 1, 0),
(1087, '1511071076', '$2y$10$VazbE55gxrGd.ezoLfW8ae.swwqCAV4ZJ0GWK17PH6fK/7AMIYtzW', 1, 0),
(1088, '1511071077', '$2y$10$ZPtKwX1mvh6KK3ysBCT3/.ghf9QyohMnxW5UqLvCFgpHHg0sVza0y', 1, 0),
(1089, '1511071078', '$2y$10$DwUZA9U1A0q4WDgiaIlMie2jpph/ZtyJVh9Ae9FWvlWV9kAtfA1/S', 1, 0),
(1090, '1511071079', '$2y$10$.S0ecVuXEM9/f1MbHVevNe0ydq3Ee1vZ6qmAGs6o0XMOTKZEEgPeS', 1, 0),
(1091, '1511071080', '$2y$10$BFCIqQsKc0hkIMD6SCuS1eG42Vv11yAPvzGBUo9FuATtYGoj3nEV.', 1, 0),
(1092, '1511071081', '$2y$10$bRw4hD4fjinYtpTwZiPbrejstZWGsqhiHFPKMQis7ojagEMQnS5yO', 1, 0),
(1093, '1511071082', '$2y$10$mu.EqumcOmlP.B4wheUy6O1bLLs9/MQrhXYelTtX.VrLxCSW8jNZi', 1, 0),
(1094, '1511071083', '$2y$10$Pr5Xqi63gjn36OP2MfNFK.o5/M5SffRK7BJz2AwfKg8w8Bh109QIa', 1, 0),
(1095, '1511071084', '$2y$10$Xqh2SUE56rLRCHflTPSrJePeBMSB86GAothh3kKejHGYeU7MyuMay', 1, 0),
(1096, '1511071085', '$2y$10$KeeIa.xWiPHG6uERrmmz9uCGCYHdkX6kCXfeU4xOxDhsPoSrAs/xa', 1, 0),
(1097, '1511071086', '$2y$10$HTldvXEogAJ5rPcorzZx3.SenzrhL1SZfCxEmBAm7gTrqV10UfK8y', 1, 0),
(1098, '1511071087', '$2y$10$u23uMkwmtY367tkErTV04uQ5Kv1BaPKLeWAB5kdL7.cwRe9Hd2hGy', 1, 0),
(1099, '1511071088', '$2y$10$JuuhuRdi1rz6aEDmehT01uZl8BEdAWC8GzRu8DK3RBELVnA2YJzGi', 1, 0),
(1100, '1511071089', '$2y$10$Di6Y2gWyUy7uK8vbWrrGVew5O/n0jFXz9TSn8dqq9.OiwyM6Hg.1K', 1, 0),
(1101, '1511071090', '$2y$10$vTl/dRs4AzSMnoTtPU5mVeOX5kApI7O/9bkTaxPCXR2mSFn3F1rzW', 1, 0),
(1102, '1511071091', '$2y$10$j2MGdJzNzc5/Q.dP45.vp.5/bBbjWdlGZMkFXy3s3qk0KotJwaiP.', 1, 0),
(1103, '1511071092', '$2y$10$nM08bZNcD6BpksUY9.Tll.XLqlD6/q4SRtGaTOKrAR/4Ewl2FZZiS', 1, 0),
(1104, '1511071093', '$2y$10$Mrx0Xm58gyuyFgkIRRb9veemFi/njzWvqYiBpWhDwAjKIJQtHWyrK', 1, 0),
(1105, '1511071094', '$2y$10$JuXY6FGaN2Omq7BUhxO9HO6hnzKEDSwJv1HMNNx9UJTkum2N6Ry3u', 1, 0),
(1106, '1511071095', '$2y$10$rITDl9Kw4/M1.Ju0lEYCe.xISyVRwbjHs5HYFqsr7jMQawgXdgEtS', 1, 0),
(1107, '1511071096', '$2y$10$AYrNyMwx0OJPumOAOBQZTerFolx1caNKh3EtmkWLWCAvL2seHX/xS', 1, 0),
(1108, '1511071097', '$2y$10$XOTXJWobQD/cZUyRSrnp1.7dGxtykHco8P942BziVUw0ArqbhL4VK', 1, 0),
(1109, '1511071098', '$2y$10$nGuQdnR67ne6Wj8JK.G7fuVCBwqTZzhrCyLJhkynKzfJrKbeF.Pna', 1, 0),
(1110, '1511071099', '$2y$10$C1q.UP871PqDa0rKj3ceHeH2NIXbrlWLgwV7LJzoDExgE88HPiUTa', 1, 0),
(1111, '1511071100', '$2y$10$rSA32fGBYAzdWQ.wdXuEr.UIXUO4SCXUyVfTtccPyPs6PwcSEIgFG', 1, 0),
(1112, '1511071101', '$2y$10$DbjzA880cXpMQrmB5w50j.10CWREIimnTOd9v2rMT9E/NVeGKpV8y', 1, 0),
(1113, '1511071102', '$2y$10$yH75P21qxHH3CddzHhrc.uzIwNmg0geZQAsvlmdRZ8e3jK5gaxr.W', 1, 0),
(1114, '1511071103', '$2y$10$p3QaopyJSBcrdQncunJnb.2AieQrxNwO4mSOH1MepGGt1a3kYZ5SG', 1, 0),
(1115, '1511071104', '$2y$10$97DXIl/n/PP2EHDzJfsFVO2iRsCeDDGJy7shPBJAOB2kmYFmT.8YG', 1, 0),
(1116, '1511071105', '$2y$10$sb.qkqEHGWQbYfRvV6cEG.jOZN0lyHgZC3gzTrErjNcsGS8lwSN2C', 1, 0),
(1117, '1511071106', '$2y$10$rao8zs8W4nElL2Pxg82HAOMnBlXmK7dgWbf4DmxfHaNk/5xVjDHiu', 1, 0),
(1118, '1511071107', '$2y$10$31hP05cd3/oCKtSKa2rGCeA/BcE0i1aEjDebPYjjnjvxbhZOOtLyC', 1, 0),
(1119, '1511071108', '$2y$10$lTgIBDmY2tHzgs7OzMsR1u4U6K4hQVciCHfSlbxSkwsFtRF7uDAPC', 1, 0),
(1120, '1511071109', '$2y$10$n3p3T9wuOlUI.wYwRXcbFeoxNArJgQIU2ZIQNMKRlBbBDe1fg/h0i', 1, 0),
(1121, '1511071110', '$2y$10$LxzcnuWwFhy310Ed/Ig3UujHcVYu6/boLAhjknbSihl/ahcn3j1xW', 1, 0),
(1122, '1511071111', '$2y$10$dIjVHavJHMzPEjQuqOxdY.3XR9/Hwue8lp9F1qGDkETy5TaKVOhSi', 1, 0),
(1123, '1511071112', '$2y$10$WO13k09o4beaqCpWibmat.Kj5DuPFdZFbjlj1xrDSVZKHI619Hldm', 1, 0),
(1124, '1511071113', '$2y$10$yXPGCRTA1JgYG34hfkNS5Oeli6TVp8/XP.pE2skLANCH7lt4OZ1qi', 1, 0),
(1125, '1511071114', '$2y$10$Hq6qO4FfWqCWDXjqKr/tGuQPHe0DJM6eJGXmW54yWUllRFxjoM/o6', 1, 0),
(1126, '1511071115', '$2y$10$8/RueyHtiPIFUe3Rds20Zu9yZocXBy11UjgiWd0RrcegW6evHQb1S', 1, 0),
(1127, '1511071116', '$2y$10$z9bds0.0cG.p6/89rqXh4ulVAPFqG55.HzxjaCAvsmWuSDa7Jzcgi', 1, 0),
(1128, '1511071117', '$2y$10$Zm97gMZ4SyuMOeIr4casYu5GEE7YA0.nzbbJHJ60Mr.7GW.lpzH8m', 1, 0),
(1129, '1511071118', '$2y$10$YrvUHLRJbE5lIeWpN0uuTuBTxFAThHrmIv2OO1RMlq96WaI0IKqqm', 1, 0),
(1130, '1511071119', '$2y$10$5WTzrS2nUtwXSM3gXr.7XuxadPsQ01s3hcwGYQbjah.FpEb/zdb9u', 1, 0),
(1131, '1511071120', '$2y$10$/39ClDc/GTOg.AT4f2Des.pqo3Uw9mZAlePcY86vT8/AH5vxOIiAu', 1, 0),
(1132, '1511071121', '$2y$10$9VZeFMd6E00gP2IWoLTXqOfo4ob3njJJlFZ4rurJSLCzEfw1dSitS', 1, 0),
(1133, '1511071122', '$2y$10$JbBVBqCtCtZw6NT6TWdjV.9secD7kxVXeTSeeNpfi.Db1L6lHZBhK', 1, 0),
(1134, '1511071123', '$2y$10$AKpVzQM9o0NRaEz4FX76vuVVeT6wTaYidtI0aPFoLoLhSBJgYqeBC', 1, 0),
(1135, '1511071124', '$2y$10$Hqnwp8SDmszbhnZhJWQ.wOOvtg.Xr7P8/zRBHhAM3X91.JX8r/Ija', 1, 0),
(1136, '1511071125', '$2y$10$HYdcT8kEn4EAbWsT2bVP5urYA2WfiGaPsNVNtLaw58xzQlxatBTbK', 1, 0),
(1137, '1511071126', '$2y$10$nPBwR8rKge0wDLtxm5yloe6fY7Gf0jphwxExpnx1EHrPD8Z1PXet6', 1, 0),
(1138, '1511071127', '$2y$10$onWI2iMUbw.d6L2mO9rGMOoVnCPAC9pycg9.Yz7jv51E3Ct5dDdIC', 1, 0),
(1139, '1511071128', '$2y$10$S3Rm5lchON/x4/4nvwGmvOx9BT3FythQF/lLlHe0aEQmECG/jbIZ.', 1, 0),
(1140, '1511071129', '$2y$10$ObEpBSQzmcDcPL/Kjs6rKOOwNRCQdetXcZ80DfebOwmmXOSLw2KhS', 1, 0),
(1141, '1511071130', '$2y$10$hHX9UvmHmOrm1.6Q3Q55bOhDC/AGKAD.W779apxVMPe6PDHqT/eW6', 1, 0),
(1142, '1511071131', '$2y$10$XgJP0K/6LyLGrIu84MapB.gG4Wme/gVMWyT5xL5h8zPyefvZ66gqy', 1, 0),
(1143, '1511071132', '$2y$10$eiVeWGdSm/XXeXkTwNSrXO7G.TO/tqyS1IVDxgOAG8QxYRQv9WSP.', 1, 0),
(1144, '1511071133', '$2y$10$AvMT7e.qJBhKAXNtpCbF2.xKLwR/evfSMjXzA0MbWJRve3WmnNr5u', 1, 0),
(1145, '1511071134', '$2y$10$u4q22i5qDSbnBIZNuC6irOTA7hAKs.IeMytzgIpqETm52PgMjga4.', 1, 0),
(1146, '1511071135', '$2y$10$ARjCol/4Frbcbnpvi93jaelQSexbFeVJyunpbIL47wOomewW5XHXi', 1, 0),
(1147, '1511071136', '$2y$10$52ALDNuGkmRed/eMzjQQcO2EgiSTqgxeYL1lJhfAr1wGGUr9ydVHO', 1, 0),
(1148, '1511071137', '$2y$10$qDiB6lq/6Wwt5npy3e/9jOHPBv8PEksq8LiDeq0ZkF3nx66pK3ZG2', 1, 0),
(1149, '1511071138', '$2y$10$HhcBs5K2OxHh2AzFtW/y5OW.YDAmIHWQzVv1bAcNaDbU93HPMj5kC', 1, 0),
(1150, '1511071139', '$2y$10$Z4ReLdc8fuT9blb6p67cguRuRrF1quiNj.6jcKQeb8un6W1uo53Yq', 1, 0),
(1151, '1511071140', '$2y$10$oFEfqJcgpX1GgxxNQmrUxO/ZUxI.JfQDEjHMt.5UCEVZGohF4k6Vu', 1, 0),
(1152, '1511071141', '$2y$10$2qlEa5lZzoE4bhO1EpGHveQ1WloC/XmLOzoNkjRgtCE4PoJoOMjeW', 1, 0),
(1153, '1511071142', '$2y$10$9ust.3z4Hv0nVYBtwHWGSefhHIIGfK9Mz9Qm7Ts1klISZz8EPZsFy', 1, 0),
(1154, '1511071143', '$2y$10$iPeckQMaWy0hUEyf17iLue5rNxkf0eKuX7a9hd5cxGrh2ihZ6LV3m', 1, 0),
(1155, '1511071144', '$2y$10$NKB8Vj3clM3HeJQ6hKdmDO78RIP2vJwEh56ymNVr/WOTGqDp9kDA6', 1, 0),
(1156, '1511071145', '$2y$10$rAKA75lwss2fYZJ41aIOz.rl./x7lkREUdht6p2G4nJhuoRCEG.1y', 1, 0),
(1157, '1511071146', '$2y$10$.ysK.exP21SlYi4Tw6ak5ucTfrTunA7NAfzmzKgQxy624WnKxZB.6', 1, 0),
(1158, '1511071147', '$2y$10$XlrNyxHJkHzrMbT1RYSCIOuR7WFSaLTwd..2CRHbzqpU0EWUywKd.', 1, 0),
(1159, '1521071148', '$2y$10$ca9Bun39Grbpv2y42WXxqen723DBzxkq.yzmaS/PY/TEXXwQna.De', 1, 0),
(1160, '1521071149', '$2y$10$H0cYr3GKGxZ5P99Ih2dHzO.dOzahphFBLyOcNEdZxGX0WeUpi7/YG', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_akun_pmb`
--

CREATE TABLE `t_akun_pmb` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_status` int(1) NOT NULL DEFAULT '1',
  `baru` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_akun_pmb`
--

INSERT INTO `t_akun_pmb` (`id`, `username`, `password`, `id_status`, `baru`) VALUES
(2, '77483296', '$2y$10$VcKJHLuc67fdp547SJ7Ddu4OwK0WB2LdJGvSxkNhFcabz/RAjiMj6', 1, 0),
(3, '07072221', '$2y$10$u4jYdG1Z5KBTeBvUqMMmEeHSh4I1x.ziAQK54bQf4gTzR0bHbxiXm', 1, 0),
(4, '88570939', '$2y$10$WsluLj3C7jXlAMfmVW0qTeSe2r3R.mOxMMKe8iv243DnRdOLEcUuO', 1, 0),
(5, '52444360', '$2y$10$POPDD0zO9ohQ8ixQzIsHrOJ/h./qOn6q8GeJdSkbcgw3i5RENWX/G', 1, 0),
(6, '42027849', '$2y$10$Gi6QiAqrzPA22QFRSG7KleIsv30OrFnFKubgnu9chEmWBqi1bccjK', 1, 0),
(7, '77066990', '$2y$10$bj/XuI4ot/YBJUrw4OEhCegeyimWCNkOULV9w0m6mdQAnarW59rSO', 1, 0),
(8, '42403338', '$2y$10$zeHWbl4NRY5OZh8QSyXN8ux6ZFKTojXqhfpItUEaoJdAhhidGE9U2', 1, 0),
(10, '14904907', '$2y$10$P7mUBk56oGih1vSamb9gCOh13Kcb.kA7ixr0kEOGOq2/UeHK1rbOa', 1, 0),
(12, '20137042', '$2y$10$pu43ebVal224WWUMRAXUAelXmXTL7wZRiTHekwcjJ0ig/CVFi0IRO', 1, 0),
(13, '17775833', '$2y$10$KYFmwpF/f3xN4aq7Mgd1ke9fLmiIKSJu8z5pzCIEl9Cad02IOBh7m', 1, 0),
(14, '30510939', '$2y$10$I.JIPD8w7C0WfhzZ.E4eI.tBoQ7MtR5Zo7Ng1pTJe.KRHFCqeHyFi', 1, 0),
(15, '90966240', '$2y$10$NBxSb/YXstJjzZYhrlpCqO8YsURzkZCzGGD8TRyHGOZsQZtwS5Nxu', 1, 0),
(38, '23875713', '$2y$10$4mKAerNfx30.9B40qOailOZX5J/GH/wdy8XEvzXWaunZYc1iBq5TS', 1, 0),
(39, '85403867', '$2y$10$MQ7O7cNGMZ4CGTu/pBU8MenoGqa8w/8e8gjfN6x/hSu6yDrykry5W', 1, 0),
(40, '70750470', '$2y$10$nhCTLyhzZP82DSxtTjOJXOH1Dm5.RjYbUaH30uYV6oO5fS.teYpl.', 1, 0),
(41, '60451931', '$2y$10$/l8UG4CAE14F0WBayiKNFOvgISIoF6VXMvhmK6wIHD2mpWXkZNAL2', 1, 0),
(42, '72671535', '$2y$10$NK1HMCDlFgJk0sl0oTEJc.reogueL/LYPDVIxEPPBCHDrGptvBv4i', 1, 0),
(43, '68260934', '$2y$10$9kFToNoTIjshRw/ftd7WhOaI4JoPd70yp12pjBi7eam0f.BU1NtVe', 1, 0),
(44, '08604545', '$2y$10$L3C46Scs0lvG88TMS9uFAub/xslcFKk5d.bkgLyVg1oRQgO7HcbOC', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_alat_transportasi`
--

CREATE TABLE `t_alat_transportasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_alat_transportasi`
--

INSERT INTO `t_alat_transportasi` (`id`, `nama`) VALUES
(1, 'Jalan kaki'),
(2, 'Kendaraan pribadi'),
(3, 'Angkutan umum/bus/pete-pete'),
(4, 'Mobil/bus antar jemput'),
(5, 'Kereta api'),
(6, 'Ojek'),
(7, 'Andong/bendi/sado/dokar/delman/becak'),
(8, 'Perahu penyeberangan/rakit/getek'),
(11, 'Kuda'),
(12, 'Sepeda'),
(13, 'Sepeda motor'),
(14, 'Mobil pribadi'),
(99, 'Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `t_batch_pmb`
--

CREATE TABLE `t_batch_pmb` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `status` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `aktif_registrasi` int(11) NOT NULL,
  `aktif_cetak_ujian` int(11) NOT NULL,
  `aktif_pengumuman_lulus` int(11) NOT NULL DEFAULT '0',
  `aktif_cetak_herregistrasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_batch_pmb`
--

INSERT INTO `t_batch_pmb` (`id`, `nama`, `tanggal_mulai`, `tanggal_selesai`, `status`, `id_tahun`, `aktif_registrasi`, `aktif_cetak_ujian`, `aktif_pengumuman_lulus`, `aktif_cetak_herregistrasi`) VALUES
(7, 'Batch 2', '2018-12-12', '2018-12-30', 0, 1, 0, 0, 0, 0),
(8, 'Batch 1', '2018-12-19', '2018-12-22', 1, 1, 1, 1, 0, 1),
(9, 'Batch 1', '2018-12-13', '2018-12-20', 0, 3, 0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_biaya_pendaftaran`
--

CREATE TABLE `t_biaya_pendaftaran` (
  `id` int(11) NOT NULL,
  `biaya` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `id_tipe_pendaftaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_biaya_pendaftaran`
--

INSERT INTO `t_biaya_pendaftaran` (`id`, `biaya`, `id_jurusan`, `id_tipe_pendaftaran`) VALUES
(1, 300000, 40, 1),
(2, 300000, 40, 2),
(3, 300000, 41, 1),
(4, 300000, 41, 2),
(5, 300000, 42, 1),
(6, 300000, 42, 2),
(7, 300000, 43, 1),
(8, 300000, 43, 2),
(9, 300000, 44, 1),
(10, 300000, 44, 2),
(11, 300000, 45, 1),
(12, 300000, 45, 2),
(13, 300000, 48, 1),
(14, 300000, 48, 2),
(15, 300000, 49, 1),
(16, 300000, 49, 2),
(17, 350000, 50, 1),
(18, 350000, 50, 2),
(19, 300000, 51, 1),
(20, 300000, 51, 2),
(21, 300000, 52, 1),
(22, 300000, 52, 2),
(23, 300000, 53, 1),
(24, 300000, 53, 2),
(25, 300000, 54, 1),
(26, 300000, 54, 2),
(27, 300000, 55, 1),
(28, 300000, 55, 2),
(29, 340000, 56, 1),
(30, 340000, 56, 2),
(31, 300000, 46, 1),
(32, 300000, 46, 2),
(33, 300000, 47, 1),
(34, 300000, 47, 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_camaba`
--

CREATE TABLE `t_camaba` (
  `id` int(11) NOT NULL,
  `no_registrasi` varchar(15) NOT NULL,
  `no_stambuk` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `id_kabupaten` char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `agama` int(11) DEFAULT NULL,
  `npwp` varchar(20) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nisn` varchar(25) NOT NULL,
  `id_tipe_pendaftaran` int(11) DEFAULT NULL,
  `sma_asal` varchar(50) NOT NULL,
  `pt_asal` varchar(50) NOT NULL,
  `fakultas_asal` varchar(50) NOT NULL,
  `prodi_asal` varchar(50) NOT NULL,
  `kewarganegaraan` varchar(3) DEFAULT NULL,
  `bakat` varchar(100) NOT NULL,
  `prestasi` varchar(100) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `ijazah` varchar(100) NOT NULL,
  `ktp` varchar(100) NOT NULL,
  `surat_imigrasi` varchar(100) NOT NULL,
  `transkrip` varchar(100) NOT NULL,
  `bukti_pembayaran` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `foto` varchar(100) NOT NULL DEFAULT 'assets/images/profile/default.jpg',
  `email` varchar(30) NOT NULL,
  `jalan` varchar(200) NOT NULL,
  `rt` varchar(3) NOT NULL,
  `rw` varchar(3) NOT NULL,
  `dusun` varchar(30) NOT NULL,
  `kelurahan` varchar(40) NOT NULL,
  `kecamatan` varchar(20) NOT NULL,
  `kode_pos` varchar(15) NOT NULL,
  `jenis_tinggal` int(11) DEFAULT NULL,
  `alat_transportasi` int(11) DEFAULT NULL,
  `terima_kps` int(11) NOT NULL,
  `no_kps` varchar(20) NOT NULL,
  `nik_ayah` varchar(20) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `tgl_lahir_ayah` date NOT NULL,
  `pendidikan_ayah` int(11) DEFAULT NULL,
  `pekerjaan_ayah` int(11) DEFAULT NULL,
  `penghasilan_ayah` int(11) DEFAULT NULL,
  `alamat_ayah` varchar(100) NOT NULL,
  `no_telp_ayah` varchar(15) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `tgl_lahir_ibu` date NOT NULL,
  `pendidikan_ibu` int(11) DEFAULT NULL,
  `pekerjaan_ibu` int(11) DEFAULT NULL,
  `penghasilan_ibu` int(11) DEFAULT NULL,
  `alamat_ibu` varchar(100) NOT NULL,
  `no_telp_ibu` varchar(15) NOT NULL,
  `nama_wali` int(11) NOT NULL,
  `tgl_lahir_wali` date NOT NULL,
  `pendidikan_wali` int(11) DEFAULT NULL,
  `pekerjaan_wali` int(11) DEFAULT NULL,
  `penghasilan_wali` int(11) DEFAULT NULL,
  `alamat_wali` varchar(100) NOT NULL,
  `no_telp_wali` varchar(15) NOT NULL,
  `tempat_lahir_ayah` varchar(30) NOT NULL,
  `tempat_lahir_ibu` varchar(30) NOT NULL,
  `tempat_lahir_wali` varchar(30) NOT NULL,
  `pilihan_lulus` int(11) DEFAULT NULL,
  `id_akun` int(11) NOT NULL,
  `id_batch` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT '0',
  `id_ujian` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_camaba`
--

INSERT INTO `t_camaba` (`id`, `no_registrasi`, `no_stambuk`, `nama`, `alamat`, `tempat_lahir`, `tanggal_lahir`, `id_kabupaten`, `jenis_kelamin`, `agama`, `npwp`, `nik`, `nisn`, `id_tipe_pendaftaran`, `sma_asal`, `pt_asal`, `fakultas_asal`, `prodi_asal`, `kewarganegaraan`, `bakat`, `prestasi`, `pekerjaan`, `ijazah`, `ktp`, `surat_imigrasi`, `transkrip`, `bukti_pembayaran`, `no_telp`, `foto`, `email`, `jalan`, `rt`, `rw`, `dusun`, `kelurahan`, `kecamatan`, `kode_pos`, `jenis_tinggal`, `alat_transportasi`, `terima_kps`, `no_kps`, `nik_ayah`, `nama_ayah`, `tgl_lahir_ayah`, `pendidikan_ayah`, `pekerjaan_ayah`, `penghasilan_ayah`, `alamat_ayah`, `no_telp_ayah`, `nama_ibu`, `tgl_lahir_ibu`, `pendidikan_ibu`, `pekerjaan_ibu`, `penghasilan_ibu`, `alamat_ibu`, `no_telp_ibu`, `nama_wali`, `tgl_lahir_wali`, `pendidikan_wali`, `pekerjaan_wali`, `penghasilan_wali`, `alamat_wali`, `no_telp_wali`, `tempat_lahir_ayah`, `tempat_lahir_ibu`, `tempat_lahir_wali`, `pilihan_lulus`, `id_akun`, `id_batch`, `id_status`, `id_ujian`) VALUES
(2, 'A0001', '', 'baso', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', 'ID', '', '', '', '', '', '', '', '', '1211', 'assets/images/profile/default.jpg', 'baso@4nesia.com', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', 0, '0000-00-00', 0, 0, 0, '', '', '', '', '', NULL, 2, 9, 0, NULL),
(3, 'A0002', '12345', 'Baso Ahmad Muflih', 'a', '2', '1901-12-26', '1202', 'L', 1, '', '', '', 1, '1', '', '', '', 'ID', '1', '2', '', 'assets/files/berkas/1301178453_Registrasi___Telkom_University1.pdf', 'assets/files/berkas/akta.pdf', '', 'assets/files/berkas/Screenshot_20181201-090635.jpg', '', '2', 'assets/images/profile/camaba/ronaldo.jpg', 'ahmadbaso977@gmail.com', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', 0, '0000-00-00', 0, 0, 0, '', '', '', '', '', 40, 3, 9, 3, 1),
(4, 'A0003', '', 'baso', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', 'ID', '', '', '', '', '', '', '', '', '2', 'assets/images/profile/default.jpg', 'ahmadbaso98@gmail.com', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', 0, '0000-00-00', 0, 0, 0, '', '', '', '', '', NULL, 4, 9, 0, NULL),
(5, 'B0001', '', 'sa', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', 'ID', '', '', '', '', '', '', '', '', '211', 'assets/images/profile/default.jpg', 'bas2o@4nesia.com', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', 0, '0000-00-00', 0, 0, 0, '', '', '', '', '', NULL, 5, 9, 0, NULL),
(6, 'B0002', '', 'Muhammad Ridwan', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', 'ID', '', '', '', '', '', '', '', '', '08281281833', 'assets/images/profile/default.jpg', 'ridwan@4nesia.com', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', 0, '0000-00-00', 0, 0, 0, '', '', '', '', '', NULL, 6, 9, 0, NULL),
(7, 'C0002', '12344', 'baso', 'a', '2', '0000-00-00', '1202', 'L', 1, '', '', '', 1, '1', '', '', '', 'ID', '1', '2', '', 'assets/files/berkas/310114011.PDF', 'assets/files/berkas/1301178453_Registrasi___Telkom_University.pdf', '9', 'assets/files/berkas/Screen_Shot_2018-12-01_at_11_50_48-min.png', '', '2', 'assets/images/profile/camaba/a.png', 'ahmadbaso977@gmail.com', '', '', '', '', '', '', '', 1, 2, 0, '', '', '', '1901-12-28', 3, 2, 13, '', '', '', '1901-12-14', 1, 3, 12, '', '', 0, '1901-12-27', 21, 3, 13, '', '', '', '', '', 40, 7, 9, 3, 4),
(10, 'A0004', '', '1', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '2', 'assets/images/profile/default.jpg', 'ahmadbaso99@gmail.com', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', 0, '0000-00-00', 0, 0, 0, '', '', '', '', '', NULL, 10, 9, 0, NULL),
(12, 'B0004', '', 'a', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '3', 'assets/images/profile/default.jpg', 'a@c.com', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', 0, '0000-00-00', 0, 0, 0, '', '', '', '', '', NULL, 12, NULL, 0, NULL),
(15, 'B0005', '', 'Arian', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '2', 'assets/images/profile/default.jpg', 'arian@4nesia.com', '', '', '', '', '', '', '', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', '', '0000-00-00', 0, 0, 0, '', '', 0, '0000-00-00', 0, 0, 0, '', '', '', '', '', NULL, 15, 9, NULL, 4),
(38, '-', '', '', '', '', '1970-01-01', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '085242525485', 'assets/images/profile/camaba/FB_IMG_1553081037030.jpg', 'eklesiapatade11@gmail.com', '', '', '', '', '', '', '', NULL, NULL, 0, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', 0, '0000-00-00', NULL, NULL, NULL, '', '', '', '', '', NULL, 38, 8, 0, NULL),
(39, '-', '', 'Tiara wahyuniyanti', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '082296785754', 'assets/images/profile/default.jpg', 'wtiara933@gmail.com', '', '', '', '', '', '', '', NULL, NULL, 0, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', 0, '0000-00-00', NULL, NULL, NULL, '', '', '', '', '', NULL, 39, 8, 0, NULL),
(40, '-', '', 'MASLAN M', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '085395679075', 'assets/images/profile/default.jpg', 'marailalan@gmail.com', '', '', '', '', '', '', '', NULL, NULL, 0, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', 0, '0000-00-00', NULL, NULL, NULL, '', '', '', '', '', NULL, 40, 8, 0, NULL),
(41, '-', '', 'NI KETUT SUSANA ARMIATI', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '082349237017', 'assets/images/profile/default.jpg', 'nanaarmianti@gmail.com', '', '', '', '', '', '', '', NULL, NULL, 0, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', 0, '0000-00-00', NULL, NULL, NULL, '', '', '', '', '', NULL, 41, 8, 0, NULL),
(42, '-', '', 'Ayu sri artini', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '081237952213', 'assets/images/profile/default.jpg', 'ayuartiniyudhanta707@gmail.com', '', '', '', '', '', '', '', NULL, NULL, 0, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', 0, '0000-00-00', NULL, NULL, NULL, '', '', '', '', '', NULL, 42, 8, 0, NULL),
(43, '-', '', 'Ayu sri artini', 'Kasimbar', 'Laemanta', '1998-07-30', '7271', 'P', 4, '', '', '', 1, 'SMA', '', '', '', 'WNA', 'Ekonomi manajemen', 'Tidak ada', '', '', '', '', '', '', '081237952213', 'assets/images/profile/camaba/IMG_86331.JPG', 'ayuartini123colacola@gmail.com', '', '', '', '', '', '', '', NULL, NULL, 0, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', 0, '0000-00-00', NULL, NULL, NULL, '', '', '', '', '', NULL, 43, 8, 0, NULL),
(44, '-', '', 'Debby ramadani', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', NULL, '', '', '', '', NULL, '', '', '', '', '', '', '', '', '085217660230', 'assets/images/profile/default.jpg', 'debbyramadani2@gmail.com', '', '', '', '', '', '', '', NULL, NULL, 0, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', '', '0000-00-00', NULL, NULL, NULL, '', '', 0, '0000-00-00', NULL, NULL, NULL, '', '', '', '', '', NULL, 44, 8, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_config_kampus`
--

CREATE TABLE `t_config_kampus` (
  `id` int(11) NOT NULL,
  `option_code` varchar(20) NOT NULL,
  `option_type` int(11) NOT NULL,
  `option_category` int(11) NOT NULL,
  `option_name` varchar(50) NOT NULL,
  `option_data` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_config_kampus`
--

INSERT INTO `t_config_kampus` (`id`, `option_code`, `option_type`, `option_category`, `option_name`, `option_data`) VALUES
(1, 'kode_badan_hukum', 2, 1, 'Kode Badan Hukum', '0'),
(2, 'kode_perguruan', 2, 1, 'Kode Perguruan Tinggi', '1'),
(3, 'nama_perguruan', 2, 1, 'Nama Perguruan Tinggi', '2'),
(4, 'singkatan', 2, 1, 'Nama Singkatan', '3'),
(5, 'tanggal_berdiri', 2, 1, 'Tanggal Berdiri', '4'),
(6, 'alamat', 2, 2, 'Alamat', '5'),
(7, 'kota', 2, 2, 'Kota', '6'),
(8, 'pos', 1, 2, 'POS', '7'),
(9, 'no_telp', 1, 2, 'No. Telepon', '8'),
(10, 'fax', 1, 2, 'Fax', '9'),
(11, 'email', 3, 2, 'Email', '10'),
(12, 'website', 2, 2, 'Website', '11'),
(13, 'no_akta', 2, 3, 'Nomor', '1'),
(14, 'tanggal_akta', 4, 3, 'Tanggal Akta', '2'),
(15, 'no_pn', 2, 3, 'Nomor P.N', '3'),
(16, 'tanggal_berdiri_akta', 4, 3, 'Tanggal Berdiri', '4'),
(17, 'no_pengesahan', 2, 4, 'No Pengesahan', '1'),
(18, 'tanggal_pengesahan', 2, 4, 'Tanggal Pengesahan', '2'),
(19, 'akreditasi', 2, 4, 'Akreditasi', '3');

-- --------------------------------------------------------

--
-- Table structure for table `t_config_lkd`
--

CREATE TABLE `t_config_lkd` (
  `id` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `jam` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_config_lkd`
--

INSERT INTO `t_config_lkd` (`id`, `id_jabatan`, `id_kategori`, `jam`) VALUES
(1, 1, 1, 7.5),
(2, 1, 2, 7.5),
(4, 1, 4, 15.5),
(5, 1, 5, 1),
(6, 2, 1, 7.5),
(7, 2, 2, 5.5),
(9, 2, 4, 19.5),
(10, 2, 5, 1),
(11, 3, 1, 7.5),
(12, 3, 2, 2.5),
(14, 3, 4, 23.5),
(15, 3, 5, 1),
(16, 4, 1, 7.5),
(17, 4, 2, 1),
(19, 4, 4, 27.5),
(20, 4, 5, 1),
(25, 1, 7, 0),
(26, 2, 7, 0),
(27, 3, 7, 0),
(28, 4, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_config_nilai`
--

CREATE TABLE `t_config_nilai` (
  `id` int(11) NOT NULL,
  `huruf` varchar(3) NOT NULL,
  `bobot` float NOT NULL,
  `batas_bawah` float NOT NULL,
  `batas_atas` float NOT NULL,
  `id_lulus` int(11) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_config_option`
--

CREATE TABLE `t_config_option` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_config_option`
--

INSERT INTO `t_config_option` (`id`, `nama`) VALUES
(1, 'number'),
(2, 'text'),
(3, 'email'),
(4, 'date');

-- --------------------------------------------------------

--
-- Table structure for table `t_config_rekening_pmb`
--

CREATE TABLE `t_config_rekening_pmb` (
  `id` int(11) NOT NULL,
  `nama` varchar(10) NOT NULL,
  `keterangan` varchar(20) NOT NULL,
  `placeholder` varchar(30) NOT NULL,
  `value` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_config_rekening_pmb`
--

INSERT INTO `t_config_rekening_pmb` (`id`, `nama`, `keterangan`, `placeholder`, `value`) VALUES
(1, 'nama', 'Nama Bank', 'Masukkan Nama Bank', 'BANK BNI SYARIAH'),
(2, 'norek', 'No Rekening', 'Masukkan No Rekening', '04192010192'),
(3, 'penerima', 'Nama Penerima', 'Masukkan Nama Penerima', 'UNISMUH PALU');

-- --------------------------------------------------------

--
-- Table structure for table `t_dekan`
--

CREATE TABLE `t_dekan` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_dekan`
--

INSERT INTO `t_dekan` (`id`, `id_pegawai`, `id_status`) VALUES
(1, 397, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_detail_lkd`
--

CREATE TABLE `t_detail_lkd` (
  `id` int(11) NOT NULL,
  `jam_awal` varchar(10) NOT NULL,
  `jam_akhir` varchar(10) NOT NULL,
  `id_kegiatan` int(11) DEFAULT NULL,
  `id_lkd_harian` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_detail_lkd`
--

INSERT INTO `t_detail_lkd` (`id`, `jam_awal`, `jam_akhir`, `id_kegiatan`, `id_lkd_harian`, `created_at`, `updated_at`) VALUES
(1, '08:00', '10:00', 1, 3, '2018-09-22 13:57:43', '2018-09-22 13:57:43'),
(2, '10:00', '12:00', 2, 4, '2018-09-22 13:59:19', '2018-09-22 13:59:19'),
(3, '11:00', '13:00', 1, 5, '2018-09-22 13:59:49', '2018-09-22 13:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `t_detail_nilai`
--

CREATE TABLE `t_detail_nilai` (
  `id` int(11) NOT NULL,
  `id_penilaian` int(11) NOT NULL,
  `id_komponen` int(11) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_dosen`
--

CREATE TABLE `t_dosen` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `kode` varchar(4) NOT NULL,
  `nidn` varchar(20) DEFAULT NULL,
  `id_fakultas` int(11) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `id_pangkat` int(11) DEFAULT NULL,
  `tmt_pangkat` date DEFAULT NULL,
  `tmt_golongan` date DEFAULT NULL,
  `tmt_jabatan_awal` date DEFAULT NULL,
  `tmt_jabatan_akhir` date DEFAULT NULL,
  `id_matkul` int(11) DEFAULT NULL,
  `serdos` int(11) DEFAULT NULL,
  `diklat` varchar(100) DEFAULT NULL,
  `dpk` varchar(10) DEFAULT NULL,
  `no_sk_pns` varchar(30) NOT NULL,
  `tgl_sk_pns` date DEFAULT NULL,
  `no_sk_non_pns` varchar(30) NOT NULL,
  `tgl_sk_non_pns` date DEFAULT NULL,
  `status_aktif` int(11) DEFAULT NULL,
  `akta_mengajar` int(11) NOT NULL,
  `izin_mengajar` int(11) NOT NULL,
  `id_unit_kerja` int(11) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_fakultas`
--

CREATE TABLE `t_fakultas` (
  `id` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_fakultas`
--

INSERT INTO `t_fakultas` (`id`, `kode`, `nama`) VALUES
(7, '0001', 'EKONOMI'),
(8, '0002', 'TEKNIK'),
(9, '0003', 'KEGURUAN DAN ILMU PENDIDIKAN'),
(10, '0004', 'HUKUM'),
(11, '0005', 'ILMU SOSIAL DAN ILMU POLITIK'),
(12, '0006', 'PERTANIAN'),
(13, '0007', 'KESEHATAN MASYARAKAT'),
(14, '0008', 'AGAMA ISLAM'),
(15, '0009', 'PASCASARJANA');

-- --------------------------------------------------------

--
-- Table structure for table `t_gedung`
--

CREATE TABLE `t_gedung` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_gedung`
--

INSERT INTO `t_gedung` (`id`, `kode`, `nama`, `id_status`) VALUES
(1, 'GKU3', 'Tokong Nanas', 1),
(2, 'GKU1', 'Gedung A', 1),
(3, 'GKU2', 'Gedung B', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_hari`
--

CREATE TABLE `t_hari` (
  `id` int(11) NOT NULL,
  `nama` varchar(10) NOT NULL,
  `nama_english` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_hari`
--

INSERT INTO `t_hari` (`id`, `nama`, `nama_english`) VALUES
(1, 'Senin', 'Monday'),
(2, 'Selasa', 'Tuesday'),
(3, 'Rabu', 'Wednesday'),
(4, 'Kamis', 'Thursday'),
(5, 'Jumat', 'Friday'),
(6, 'Sabtu', 'Saturday'),
(7, 'Minggu', 'Sunday');

-- --------------------------------------------------------

--
-- Table structure for table `t_history_pembayaran`
--

CREATE TABLE `t_history_pembayaran` (
  `nim` char(16) NOT NULL,
  `nama_mhs` varchar(200) NOT NULL,
  `nama_thn_ak` int(200) NOT NULL,
  `jk` varchar(2) NOT NULL,
  `nama_fakultas` varchar(150) NOT NULL,
  `nama_prodi` varchar(150) NOT NULL,
  `jenis_pembayaran` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `ket` varchar(200) NOT NULL,
  `status_aktif` int(25) NOT NULL,
  `bebas_masalah` int(2) NOT NULL,
  `status_bayar` int(2) NOT NULL,
  `no_ref_bank` varchar(250) NOT NULL,
  `channel` varchar(250) NOT NULL,
  `tanggal_bayar` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `virtual_account` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_izin_krs`
--

CREATE TABLE `t_izin_krs` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_izin_krs`
--

INSERT INTO `t_izin_krs` (`id`, `nama`) VALUES
(0, 'TIDAK BOLEH'),
(1, 'BOLEH');

-- --------------------------------------------------------

--
-- Table structure for table `t_jadwal_kelas`
--

CREATE TABLE `t_jadwal_kelas` (
  `id` int(11) NOT NULL,
  `id_kelas_matkul` int(11) DEFAULT NULL,
  `id_ruangan` int(11) DEFAULT NULL,
  `id_hari` int(11) NOT NULL,
  `jam_mulai` varchar(6) NOT NULL,
  `jam_selesai` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_jadwal_krs`
--

CREATE TABLE `t_jadwal_krs` (
  `id` int(11) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_jalur_masuk`
--

CREATE TABLE `t_jalur_masuk` (
  `id` int(11) NOT NULL,
  `nama` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jalur_masuk`
--

INSERT INTO `t_jalur_masuk` (`id`, `nama`) VALUES
(1, 'SBMPTN'),
(2, 'SNMPTN'),
(3, 'PMDK Penelusuran minat dan kemampuan (akademik)'),
(4, 'Prestasi Penelusuran minat dan kemampuan (prestasi non-akademik)'),
(5, 'Seleksi Mandiri PTN'),
(6, 'Seleksi Mandiri PTS'),
(7, 'Ujian Masuk Bersama PTN (UMB-PT)'),
(8, 'Ujian Masuk Bersama PTS (UMB-PTS)'),
(9, 'Program Internasional'),
(11, 'Program Kerjasama Perusahaan/Institusi/Pemerintah');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_kelamin`
--

CREATE TABLE `t_jenis_kelamin` (
  `id` varchar(1) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis_kelamin`
--

INSERT INTO `t_jenis_kelamin` (`id`, `nama`) VALUES
('L', 'Laki-laki'),
('P', 'Perempuan');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_kelas`
--

CREATE TABLE `t_jenis_kelas` (
  `id` int(11) NOT NULL,
  `nama` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis_kelas`
--

INSERT INTO `t_jenis_kelas` (`id`, `nama`) VALUES
(1, 'Fisik'),
(2, 'Virtual');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_matkul`
--

CREATE TABLE `t_jenis_matkul` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis_matkul`
--

INSERT INTO `t_jenis_matkul` (`id`, `nama`) VALUES
(1, 'UNIVERSITAS'),
(2, 'FAKULTAS'),
(3, 'PRODI');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_pendaftaran`
--

CREATE TABLE `t_jenis_pendaftaran` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis_pendaftaran`
--

INSERT INTO `t_jenis_pendaftaran` (`id`, `nama`) VALUES
(1, 'Peserta didik baru'),
(2, 'Pindahan'),
(3, 'Naik kelas'),
(4, 'Akselerasi'),
(5, 'Mengulang'),
(6, 'Lanjutan semester'),
(8, 'Pindah Alih Bentuk'),
(11, 'Alih Jenjang'),
(12, 'Lintas Jalur');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_pertemuan`
--

CREATE TABLE `t_jenis_pertemuan` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis_pertemuan`
--

INSERT INTO `t_jenis_pertemuan` (`id`, `nama`) VALUES
(1, 'PERKULIAHAN'),
(2, 'UTS'),
(3, 'UAS');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_tinggal`
--

CREATE TABLE `t_jenis_tinggal` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis_tinggal`
--

INSERT INTO `t_jenis_tinggal` (`id`, `nama`) VALUES
(1, 'Bersama orang tua'),
(2, 'Wali'),
(3, 'Kost'),
(4, 'Asrama'),
(5, 'Panti Asuhan'),
(99, 'Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenjang_jurusan`
--

CREATE TABLE `t_jenjang_jurusan` (
  `id` int(11) NOT NULL,
  `nama` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenjang_jurusan`
--

INSERT INTO `t_jenjang_jurusan` (`id`, `nama`) VALUES
(1, 'S3'),
(2, 'S2'),
(3, 'S1'),
(4, 'D4'),
(5, 'D3');

-- --------------------------------------------------------

--
-- Table structure for table `t_jurusan`
--

CREATE TABLE `t_jurusan` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `uuid` varchar(10) NOT NULL,
  `nama` varchar(80) NOT NULL,
  `tanggal_berdiri` date NOT NULL,
  `nama_gelar` varchar(10) NOT NULL,
  `total_sks` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `no_sk_dikti` varchar(40) NOT NULL,
  `tanggal_sk_dikti` date NOT NULL,
  `tanggal_berakhir_sk_dikti` date NOT NULL,
  `file_sk_dikti` varchar(200) NOT NULL,
  `no_sk_ban` varchar(40) NOT NULL,
  `tanggal_sk_ban` date NOT NULL,
  `tanggal_berakhir_sk_ban` date NOT NULL,
  `file_sk_ban` varchar(200) NOT NULL,
  `id_fakultas` int(11) NOT NULL,
  `id_jenjang` int(11) NOT NULL,
  `id_semester_mulai` int(11) DEFAULT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jurusan`
--

INSERT INTO `t_jurusan` (`id`, `kode`, `uuid`, `nama`, `tanggal_berdiri`, `nama_gelar`, `total_sks`, `email`, `no_telp`, `no_sk_dikti`, `tanggal_sk_dikti`, `tanggal_berakhir_sk_dikti`, `file_sk_dikti`, `no_sk_ban`, `tanggal_sk_ban`, `tanggal_berakhir_sk_ban`, `file_sk_ban`, `id_fakultas`, `id_jenjang`, `id_semester_mulai`, `id_status`) VALUES
(40, '61201', '0', 'MANAJEMEN', '2010-02-02', 'S.E.', 150, '', '', '0', '2010-03-03', '2019-02-27', '', '', '1970-01-01', '1970-01-01', '', 7, 3, 14, 1),
(41, '22201', '0', 'TEKNIK SIPIL', '2004-02-18', 'S.T.', 140, '', '', '0', '2013-05-20', '2019-12-18', '', '', '1970-01-01', '1970-01-01', '', 8, 3, 14, 1),
(42, '86205', '0', 'PENDIDIKAN LUAR SEKOLAH', '2009-05-11', 'S.Pd', 132, '', '', '0', '2014-06-11', '2018-03-01', '', '', '1970-01-01', '1970-01-01', '', 9, 3, 14, 1),
(43, '86207', '0', 'PENDIDIKAN GURU PENDIDIKAN ANAK USIA DINI', '2014-06-10', 'S.Pd', 121, '', '0', '0', '2017-05-24', '2019-02-08', '', '', '1970-01-01', '1970-01-01', '', 9, 3, 14, 1),
(44, '88203', '0', 'PENDIDIKAN BAHASA INGGRIS', '2013-06-19', 'S.Pd', 142, '', '', '0', '2017-06-13', '2019-01-10', '', '', '1970-01-01', '1970-01-01', '', 9, 3, 2, 1),
(45, '74201', '0', 'ILMU HUKUM', '2014-05-19', 'S.H', 131, '', '', '0', '2016-07-21', '2019-01-31', '', '', '1970-01-01', '1970-01-01', '', 10, 3, 1, 1),
(46, '69201', '0', 'SOSIOLOGI', '2014-06-10', 'S.Si', 147, '', '', '0', '2017-10-24', '2019-11-28', '', '', '1970-01-01', '1970-01-01', '', 11, 3, 1, 1),
(47, '63201', '0', 'ILMU ADMINISTRASI NEGARA', '2009-06-23', 'S.Si', 140, '', '', '0', '2014-07-31', '2019-11-28', '', '', '1970-01-01', '1970-01-01', '', 11, 3, 14, 1),
(48, '54201', '0', 'AGRIBISNIS', '2015-07-15', 'S.Tp', 112, '', '', '0', '2016-07-14', '2019-01-26', '', '', '1970-01-01', '1970-01-01', '', 12, 3, 1, 1),
(49, '54251', '0', 'ILMU KEHUTANAN', '2015-05-25', 'S.Tp', 134, '', '', '0', '2016-07-21', '2019-02-14', '', '', '1970-01-01', '1970-01-01', '', 12, 3, 1, 1),
(50, '13201', '13201', 'ILMU KESEHATAN MASYARAKAT', '2010-06-22', 'S.Km', 145, '', '', '0', '2011-03-23', '2019-01-04', '', '', '1970-01-01', '1970-01-01', '', 13, 3, 12, 1),
(51, '86208', '0', 'PENDIDIKAN AGAMA ISLAM', '2005-12-07', 'S.Ag', 143, '', '', '0', '2009-06-09', '2018-12-31', '', '', '1970-01-01', '1970-01-01', '', 14, 3, 12, 1),
(52, '86233', '0', 'PENDIDIKAN ISLAM ANAK USIA DINI', '2014-06-10', 'S.Pd', 127, '', '', '0', '2015-07-16', '2019-01-04', '', '', '1970-01-01', '1970-01-01', '', 14, 3, 1, 1),
(53, '60202', '0', 'HUKUM EKONOMI SYARI\'AH', '2014-06-25', 'S.Ag', 124, '', '', '0', '2017-10-23', '2019-02-08', '', '', '1970-01-01', '1970-01-01', '', 14, 3, 1, 1),
(54, '74230', '0', 'HUKUM KELUARGA/AHWAL SYAKHSIYAH', '2012-02-29', 'S.Ag', 133, '', '0', '0', '2017-06-20', '2019-01-04', '', '', '1970-01-01', '1970-01-01', '', 14, 3, 14, 1),
(55, '70233', '0', 'KOMUNIKASI PENYIARAN ISLAM', '2013-05-20', 'S.Ag', 144, '', '', '0', '2017-09-24', '2019-01-18', '', '', '1970-01-01', '1970-01-01', '', 14, 3, 14, 1),
(56, '70134', '0', 'MANAJEMEN PENDIDIKAN', '2014-06-25', 'M.E.', 121, '', '', '0', '2016-10-25', '2019-01-18', '', '', '1970-01-01', '1970-01-01', '', 15, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_jurusan_sekolah`
--

CREATE TABLE `t_jurusan_sekolah` (
  `id` int(11) NOT NULL,
  `nama` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jurusan_sekolah`
--

INSERT INTO `t_jurusan_sekolah` (`id`, `nama`) VALUES
(1, 'IPA'),
(2, 'IPS'),
(3, 'BAHASA'),
(4, 'KEAGAMAAN'),
(5, 'LAINNYA');

-- --------------------------------------------------------

--
-- Table structure for table `t_kabupaten`
--

CREATE TABLE `t_kabupaten` (
  `id` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `id_provinsi` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_kabupaten`
--

INSERT INTO `t_kabupaten` (`id`, `id_provinsi`, `nama`) VALUES
('1101', '11', 'KABUPATEN SIMEULUE'),
('1102', '11', 'KABUPATEN ACEH SINGKIL'),
('1103', '11', 'KABUPATEN ACEH SELATAN'),
('1104', '11', 'KABUPATEN ACEH TENGGARA'),
('1105', '11', 'KABUPATEN ACEH TIMUR'),
('1106', '11', 'KABUPATEN ACEH TENGAH'),
('1107', '11', 'KABUPATEN ACEH BARAT'),
('1108', '11', 'KABUPATEN ACEH BESAR'),
('1109', '11', 'KABUPATEN PIDIE'),
('1110', '11', 'KABUPATEN BIREUEN'),
('1111', '11', 'KABUPATEN ACEH UTARA'),
('1112', '11', 'KABUPATEN ACEH BARAT DAYA'),
('1113', '11', 'KABUPATEN GAYO LUES'),
('1114', '11', 'KABUPATEN ACEH TAMIANG'),
('1115', '11', 'KABUPATEN NAGAN RAYA'),
('1116', '11', 'KABUPATEN ACEH JAYA'),
('1117', '11', 'KABUPATEN BENER MERIAH'),
('1118', '11', 'KABUPATEN PIDIE JAYA'),
('1171', '11', 'KOTA BANDA ACEH'),
('1172', '11', 'KOTA SABANG'),
('1173', '11', 'KOTA LANGSA'),
('1174', '11', 'KOTA LHOKSEUMAWE'),
('1175', '11', 'KOTA SUBULUSSALAM'),
('1201', '12', 'KABUPATEN NIAS'),
('1202', '12', 'KABUPATEN MANDAILING NATAL'),
('1203', '12', 'KABUPATEN TAPANULI SELATAN'),
('1204', '12', 'KABUPATEN TAPANULI TENGAH'),
('1205', '12', 'KABUPATEN TAPANULI UTARA'),
('1206', '12', 'KABUPATEN TOBA SAMOSIR'),
('1207', '12', 'KABUPATEN LABUHAN BATU'),
('1208', '12', 'KABUPATEN ASAHAN'),
('1209', '12', 'KABUPATEN SIMALUNGUN'),
('1210', '12', 'KABUPATEN DAIRI'),
('1211', '12', 'KABUPATEN KARO'),
('1212', '12', 'KABUPATEN DELI SERDANG'),
('1213', '12', 'KABUPATEN LANGKAT'),
('1214', '12', 'KABUPATEN NIAS SELATAN'),
('1215', '12', 'KABUPATEN HUMBANG HASUNDUTAN'),
('1216', '12', 'KABUPATEN PAKPAK BHARAT'),
('1217', '12', 'KABUPATEN SAMOSIR'),
('1218', '12', 'KABUPATEN SERDANG BEDAGAI'),
('1219', '12', 'KABUPATEN BATU BARA'),
('1220', '12', 'KABUPATEN PADANG LAWAS UTARA'),
('1221', '12', 'KABUPATEN PADANG LAWAS'),
('1222', '12', 'KABUPATEN LABUHAN BATU SELATAN'),
('1223', '12', 'KABUPATEN LABUHAN BATU UTARA'),
('1224', '12', 'KABUPATEN NIAS UTARA'),
('1225', '12', 'KABUPATEN NIAS BARAT'),
('1271', '12', 'KOTA SIBOLGA'),
('1272', '12', 'KOTA TANJUNG BALAI'),
('1273', '12', 'KOTA PEMATANG SIANTAR'),
('1274', '12', 'KOTA TEBING TINGGI'),
('1275', '12', 'KOTA MEDAN'),
('1276', '12', 'KOTA BINJAI'),
('1277', '12', 'KOTA PADANGSIDIMPUAN'),
('1278', '12', 'KOTA GUNUNGSITOLI'),
('1301', '13', 'KABUPATEN KEPULAUAN MENTAWAI'),
('1302', '13', 'KABUPATEN PESISIR SELATAN'),
('1303', '13', 'KABUPATEN SOLOK'),
('1304', '13', 'KABUPATEN SIJUNJUNG'),
('1305', '13', 'KABUPATEN TANAH DATAR'),
('1306', '13', 'KABUPATEN PADANG PARIAMAN'),
('1307', '13', 'KABUPATEN AGAM'),
('1308', '13', 'KABUPATEN LIMA PULUH KOTA'),
('1309', '13', 'KABUPATEN PASAMAN'),
('1310', '13', 'KABUPATEN SOLOK SELATAN'),
('1311', '13', 'KABUPATEN DHARMASRAYA'),
('1312', '13', 'KABUPATEN PASAMAN BARAT'),
('1371', '13', 'KOTA PADANG'),
('1372', '13', 'KOTA SOLOK'),
('1373', '13', 'KOTA SAWAH LUNTO'),
('1374', '13', 'KOTA PADANG PANJANG'),
('1375', '13', 'KOTA BUKITTINGGI'),
('1376', '13', 'KOTA PAYAKUMBUH'),
('1377', '13', 'KOTA PARIAMAN'),
('1401', '14', 'KABUPATEN KUANTAN SINGINGI'),
('1402', '14', 'KABUPATEN INDRAGIRI HULU'),
('1403', '14', 'KABUPATEN INDRAGIRI HILIR'),
('1404', '14', 'KABUPATEN PELALAWAN'),
('1405', '14', 'KABUPATEN S I A K'),
('1406', '14', 'KABUPATEN KAMPAR'),
('1407', '14', 'KABUPATEN ROKAN HULU'),
('1408', '14', 'KABUPATEN BENGKALIS'),
('1409', '14', 'KABUPATEN ROKAN HILIR'),
('1410', '14', 'KABUPATEN KEPULAUAN MERANTI'),
('1471', '14', 'KOTA PEKANBARU'),
('1473', '14', 'KOTA D U M A I'),
('1501', '15', 'KABUPATEN KERINCI'),
('1502', '15', 'KABUPATEN MERANGIN'),
('1503', '15', 'KABUPATEN SAROLANGUN'),
('1504', '15', 'KABUPATEN BATANG HARI'),
('1505', '15', 'KABUPATEN MUARO JAMBI'),
('1506', '15', 'KABUPATEN TANJUNG JABUNG TIMUR'),
('1507', '15', 'KABUPATEN TANJUNG JABUNG BARAT'),
('1508', '15', 'KABUPATEN TEBO'),
('1509', '15', 'KABUPATEN BUNGO'),
('1571', '15', 'KOTA JAMBI'),
('1572', '15', 'KOTA SUNGAI PENUH'),
('1601', '16', 'KABUPATEN OGAN KOMERING ULU'),
('1602', '16', 'KABUPATEN OGAN KOMERING ILIR'),
('1603', '16', 'KABUPATEN MUARA ENIM'),
('1604', '16', 'KABUPATEN LAHAT'),
('1605', '16', 'KABUPATEN MUSI RAWAS'),
('1606', '16', 'KABUPATEN MUSI BANYUASIN'),
('1607', '16', 'KABUPATEN BANYU ASIN'),
('1608', '16', 'KABUPATEN OGAN KOMERING ULU SELATAN'),
('1609', '16', 'KABUPATEN OGAN KOMERING ULU TIMUR'),
('1610', '16', 'KABUPATEN OGAN ILIR'),
('1611', '16', 'KABUPATEN EMPAT LAWANG'),
('1612', '16', 'KABUPATEN PENUKAL ABAB LEMATANG ILIR'),
('1613', '16', 'KABUPATEN MUSI RAWAS UTARA'),
('1671', '16', 'KOTA PALEMBANG'),
('1672', '16', 'KOTA PRABUMULIH'),
('1673', '16', 'KOTA PAGAR ALAM'),
('1674', '16', 'KOTA LUBUKLINGGAU'),
('1701', '17', 'KABUPATEN BENGKULU SELATAN'),
('1702', '17', 'KABUPATEN REJANG LEBONG'),
('1703', '17', 'KABUPATEN BENGKULU UTARA'),
('1704', '17', 'KABUPATEN KAUR'),
('1705', '17', 'KABUPATEN SELUMA'),
('1706', '17', 'KABUPATEN MUKOMUKO'),
('1707', '17', 'KABUPATEN LEBONG'),
('1708', '17', 'KABUPATEN KEPAHIANG'),
('1709', '17', 'KABUPATEN BENGKULU TENGAH'),
('1771', '17', 'KOTA BENGKULU'),
('1801', '18', 'KABUPATEN LAMPUNG BARAT'),
('1802', '18', 'KABUPATEN TANGGAMUS'),
('1803', '18', 'KABUPATEN LAMPUNG SELATAN'),
('1804', '18', 'KABUPATEN LAMPUNG TIMUR'),
('1805', '18', 'KABUPATEN LAMPUNG TENGAH'),
('1806', '18', 'KABUPATEN LAMPUNG UTARA'),
('1807', '18', 'KABUPATEN WAY KANAN'),
('1808', '18', 'KABUPATEN TULANGBAWANG'),
('1809', '18', 'KABUPATEN PESAWARAN'),
('1810', '18', 'KABUPATEN PRINGSEWU'),
('1811', '18', 'KABUPATEN MESUJI'),
('1812', '18', 'KABUPATEN TULANG BAWANG BARAT'),
('1813', '18', 'KABUPATEN PESISIR BARAT'),
('1871', '18', 'KOTA BANDAR LAMPUNG'),
('1872', '18', 'KOTA METRO'),
('1901', '19', 'KABUPATEN BANGKA'),
('1902', '19', 'KABUPATEN BELITUNG'),
('1903', '19', 'KABUPATEN BANGKA BARAT'),
('1904', '19', 'KABUPATEN BANGKA TENGAH'),
('1905', '19', 'KABUPATEN BANGKA SELATAN'),
('1906', '19', 'KABUPATEN BELITUNG TIMUR'),
('1971', '19', 'KOTA PANGKAL PINANG'),
('2101', '21', 'KABUPATEN KARIMUN'),
('2102', '21', 'KABUPATEN BINTAN'),
('2103', '21', 'KABUPATEN NATUNA'),
('2104', '21', 'KABUPATEN LINGGA'),
('2105', '21', 'KABUPATEN KEPULAUAN ANAMBAS'),
('2171', '21', 'KOTA B A T A M'),
('2172', '21', 'KOTA TANJUNG PINANG'),
('3101', '31', 'KABUPATEN KEPULAUAN SERIBU'),
('3171', '31', 'KOTA JAKARTA SELATAN'),
('3172', '31', 'KOTA JAKARTA TIMUR'),
('3173', '31', 'KOTA JAKARTA PUSAT'),
('3174', '31', 'KOTA JAKARTA BARAT'),
('3175', '31', 'KOTA JAKARTA UTARA'),
('3201', '32', 'KABUPATEN BOGOR'),
('3202', '32', 'KABUPATEN SUKABUMI'),
('3203', '32', 'KABUPATEN CIANJUR'),
('3204', '32', 'KABUPATEN BANDUNG'),
('3205', '32', 'KABUPATEN GARUT'),
('3206', '32', 'KABUPATEN TASIKMALAYA'),
('3207', '32', 'KABUPATEN CIAMIS'),
('3208', '32', 'KABUPATEN KUNINGAN'),
('3209', '32', 'KABUPATEN CIREBON'),
('3210', '32', 'KABUPATEN MAJALENGKA'),
('3211', '32', 'KABUPATEN SUMEDANG'),
('3212', '32', 'KABUPATEN INDRAMAYU'),
('3213', '32', 'KABUPATEN SUBANG'),
('3214', '32', 'KABUPATEN PURWAKARTA'),
('3215', '32', 'KABUPATEN KARAWANG'),
('3216', '32', 'KABUPATEN BEKASI'),
('3217', '32', 'KABUPATEN BANDUNG BARAT'),
('3218', '32', 'KABUPATEN PANGANDARAN'),
('3271', '32', 'KOTA BOGOR'),
('3272', '32', 'KOTA SUKABUMI'),
('3273', '32', 'KOTA BANDUNG'),
('3274', '32', 'KOTA CIREBON'),
('3275', '32', 'KOTA BEKASI'),
('3276', '32', 'KOTA DEPOK'),
('3277', '32', 'KOTA CIMAHI'),
('3278', '32', 'KOTA TASIKMALAYA'),
('3279', '32', 'KOTA BANJAR'),
('3301', '33', 'KABUPATEN CILACAP'),
('3302', '33', 'KABUPATEN BANYUMAS'),
('3303', '33', 'KABUPATEN PURBALINGGA'),
('3304', '33', 'KABUPATEN BANJARNEGARA'),
('3305', '33', 'KABUPATEN KEBUMEN'),
('3306', '33', 'KABUPATEN PURWOREJO'),
('3307', '33', 'KABUPATEN WONOSOBO'),
('3308', '33', 'KABUPATEN MAGELANG'),
('3309', '33', 'KABUPATEN BOYOLALI'),
('3310', '33', 'KABUPATEN KLATEN'),
('3311', '33', 'KABUPATEN SUKOHARJO'),
('3312', '33', 'KABUPATEN WONOGIRI'),
('3313', '33', 'KABUPATEN KARANGANYAR'),
('3314', '33', 'KABUPATEN SRAGEN'),
('3315', '33', 'KABUPATEN GROBOGAN'),
('3316', '33', 'KABUPATEN BLORA'),
('3317', '33', 'KABUPATEN REMBANG'),
('3318', '33', 'KABUPATEN PATI'),
('3319', '33', 'KABUPATEN KUDUS'),
('3320', '33', 'KABUPATEN JEPARA'),
('3321', '33', 'KABUPATEN DEMAK'),
('3322', '33', 'KABUPATEN SEMARANG'),
('3323', '33', 'KABUPATEN TEMANGGUNG'),
('3324', '33', 'KABUPATEN KENDAL'),
('3325', '33', 'KABUPATEN BATANG'),
('3326', '33', 'KABUPATEN PEKALONGAN'),
('3327', '33', 'KABUPATEN PEMALANG'),
('3328', '33', 'KABUPATEN TEGAL'),
('3329', '33', 'KABUPATEN BREBES'),
('3371', '33', 'KOTA MAGELANG'),
('3372', '33', 'KOTA SURAKARTA'),
('3373', '33', 'KOTA SALATIGA'),
('3374', '33', 'KOTA SEMARANG'),
('3375', '33', 'KOTA PEKALONGAN'),
('3376', '33', 'KOTA TEGAL'),
('3401', '34', 'KABUPATEN KULON PROGO'),
('3402', '34', 'KABUPATEN BANTUL'),
('3403', '34', 'KABUPATEN GUNUNG KIDUL'),
('3404', '34', 'KABUPATEN SLEMAN'),
('3471', '34', 'KOTA YOGYAKARTA'),
('3501', '35', 'KABUPATEN PACITAN'),
('3502', '35', 'KABUPATEN PONOROGO'),
('3503', '35', 'KABUPATEN TRENGGALEK'),
('3504', '35', 'KABUPATEN TULUNGAGUNG'),
('3505', '35', 'KABUPATEN BLITAR'),
('3506', '35', 'KABUPATEN KEDIRI'),
('3507', '35', 'KABUPATEN MALANG'),
('3508', '35', 'KABUPATEN LUMAJANG'),
('3509', '35', 'KABUPATEN JEMBER'),
('3510', '35', 'KABUPATEN BANYUWANGI'),
('3511', '35', 'KABUPATEN BONDOWOSO'),
('3512', '35', 'KABUPATEN SITUBONDO'),
('3513', '35', 'KABUPATEN PROBOLINGGO'),
('3514', '35', 'KABUPATEN PASURUAN'),
('3515', '35', 'KABUPATEN SIDOARJO'),
('3516', '35', 'KABUPATEN MOJOKERTO'),
('3517', '35', 'KABUPATEN JOMBANG'),
('3518', '35', 'KABUPATEN NGANJUK'),
('3519', '35', 'KABUPATEN MADIUN'),
('3520', '35', 'KABUPATEN MAGETAN'),
('3521', '35', 'KABUPATEN NGAWI'),
('3522', '35', 'KABUPATEN BOJONEGORO'),
('3523', '35', 'KABUPATEN TUBAN'),
('3524', '35', 'KABUPATEN LAMONGAN'),
('3525', '35', 'KABUPATEN GRESIK'),
('3526', '35', 'KABUPATEN BANGKALAN'),
('3527', '35', 'KABUPATEN SAMPANG'),
('3528', '35', 'KABUPATEN PAMEKASAN'),
('3529', '35', 'KABUPATEN SUMENEP'),
('3571', '35', 'KOTA KEDIRI'),
('3572', '35', 'KOTA BLITAR'),
('3573', '35', 'KOTA MALANG'),
('3574', '35', 'KOTA PROBOLINGGO'),
('3575', '35', 'KOTA PASURUAN'),
('3576', '35', 'KOTA MOJOKERTO'),
('3577', '35', 'KOTA MADIUN'),
('3578', '35', 'KOTA SURABAYA'),
('3579', '35', 'KOTA BATU'),
('3601', '36', 'KABUPATEN PANDEGLANG'),
('3602', '36', 'KABUPATEN LEBAK'),
('3603', '36', 'KABUPATEN TANGERANG'),
('3604', '36', 'KABUPATEN SERANG'),
('3671', '36', 'KOTA TANGERANG'),
('3672', '36', 'KOTA CILEGON'),
('3673', '36', 'KOTA SERANG'),
('3674', '36', 'KOTA TANGERANG SELATAN'),
('5101', '51', 'KABUPATEN JEMBRANA'),
('5102', '51', 'KABUPATEN TABANAN'),
('5103', '51', 'KABUPATEN BADUNG'),
('5104', '51', 'KABUPATEN GIANYAR'),
('5105', '51', 'KABUPATEN KLUNGKUNG'),
('5106', '51', 'KABUPATEN BANGLI'),
('5107', '51', 'KABUPATEN KARANG ASEM'),
('5108', '51', 'KABUPATEN BULELENG'),
('5171', '51', 'KOTA DENPASAR'),
('5201', '52', 'KABUPATEN LOMBOK BARAT'),
('5202', '52', 'KABUPATEN LOMBOK TENGAH'),
('5203', '52', 'KABUPATEN LOMBOK TIMUR'),
('5204', '52', 'KABUPATEN SUMBAWA'),
('5205', '52', 'KABUPATEN DOMPU'),
('5206', '52', 'KABUPATEN BIMA'),
('5207', '52', 'KABUPATEN SUMBAWA BARAT'),
('5208', '52', 'KABUPATEN LOMBOK UTARA'),
('5271', '52', 'KOTA MATARAM'),
('5272', '52', 'KOTA BIMA'),
('5301', '53', 'KABUPATEN SUMBA BARAT'),
('5302', '53', 'KABUPATEN SUMBA TIMUR'),
('5303', '53', 'KABUPATEN KUPANG'),
('5304', '53', 'KABUPATEN TIMOR TENGAH SELATAN'),
('5305', '53', 'KABUPATEN TIMOR TENGAH UTARA'),
('5306', '53', 'KABUPATEN BELU'),
('5307', '53', 'KABUPATEN ALOR'),
('5308', '53', 'KABUPATEN LEMBATA'),
('5309', '53', 'KABUPATEN FLORES TIMUR'),
('5310', '53', 'KABUPATEN SIKKA'),
('5311', '53', 'KABUPATEN ENDE'),
('5312', '53', 'KABUPATEN NGADA'),
('5313', '53', 'KABUPATEN MANGGARAI'),
('5314', '53', 'KABUPATEN ROTE NDAO'),
('5315', '53', 'KABUPATEN MANGGARAI BARAT'),
('5316', '53', 'KABUPATEN SUMBA TENGAH'),
('5317', '53', 'KABUPATEN SUMBA BARAT DAYA'),
('5318', '53', 'KABUPATEN NAGEKEO'),
('5319', '53', 'KABUPATEN MANGGARAI TIMUR'),
('5320', '53', 'KABUPATEN SABU RAIJUA'),
('5321', '53', 'KABUPATEN MALAKA'),
('5371', '53', 'KOTA KUPANG'),
('6101', '61', 'KABUPATEN SAMBAS'),
('6102', '61', 'KABUPATEN BENGKAYANG'),
('6103', '61', 'KABUPATEN LANDAK'),
('6104', '61', 'KABUPATEN MEMPAWAH'),
('6105', '61', 'KABUPATEN SANGGAU'),
('6106', '61', 'KABUPATEN KETAPANG'),
('6107', '61', 'KABUPATEN SINTANG'),
('6108', '61', 'KABUPATEN KAPUAS HULU'),
('6109', '61', 'KABUPATEN SEKADAU'),
('6110', '61', 'KABUPATEN MELAWI'),
('6111', '61', 'KABUPATEN KAYONG UTARA'),
('6112', '61', 'KABUPATEN KUBU RAYA'),
('6171', '61', 'KOTA PONTIANAK'),
('6172', '61', 'KOTA SINGKAWANG'),
('6201', '62', 'KABUPATEN KOTAWARINGIN BARAT'),
('6202', '62', 'KABUPATEN KOTAWARINGIN TIMUR'),
('6203', '62', 'KABUPATEN KAPUAS'),
('6204', '62', 'KABUPATEN BARITO SELATAN'),
('6205', '62', 'KABUPATEN BARITO UTARA'),
('6206', '62', 'KABUPATEN SUKAMARA'),
('6207', '62', 'KABUPATEN LAMANDAU'),
('6208', '62', 'KABUPATEN SERUYAN'),
('6209', '62', 'KABUPATEN KATINGAN'),
('6210', '62', 'KABUPATEN PULANG PISAU'),
('6211', '62', 'KABUPATEN GUNUNG MAS'),
('6212', '62', 'KABUPATEN BARITO TIMUR'),
('6213', '62', 'KABUPATEN MURUNG RAYA'),
('6271', '62', 'KOTA PALANGKA RAYA'),
('6301', '63', 'KABUPATEN TANAH LAUT'),
('6302', '63', 'KABUPATEN KOTA BARU'),
('6303', '63', 'KABUPATEN BANJAR'),
('6304', '63', 'KABUPATEN BARITO KUALA'),
('6305', '63', 'KABUPATEN TAPIN'),
('6306', '63', 'KABUPATEN HULU SUNGAI SELATAN'),
('6307', '63', 'KABUPATEN HULU SUNGAI TENGAH'),
('6308', '63', 'KABUPATEN HULU SUNGAI UTARA'),
('6309', '63', 'KABUPATEN TABALONG'),
('6310', '63', 'KABUPATEN TANAH BUMBU'),
('6311', '63', 'KABUPATEN BALANGAN'),
('6371', '63', 'KOTA BANJARMASIN'),
('6372', '63', 'KOTA BANJAR BARU'),
('6401', '64', 'KABUPATEN PASER'),
('6402', '64', 'KABUPATEN KUTAI BARAT'),
('6403', '64', 'KABUPATEN KUTAI KARTANEGARA'),
('6404', '64', 'KABUPATEN KUTAI TIMUR'),
('6405', '64', 'KABUPATEN BERAU'),
('6409', '64', 'KABUPATEN PENAJAM PASER UTARA'),
('6411', '64', 'KABUPATEN MAHAKAM HULU'),
('6471', '64', 'KOTA BALIKPAPAN'),
('6472', '64', 'KOTA SAMARINDA'),
('6474', '64', 'KOTA BONTANG'),
('6501', '65', 'KABUPATEN MALINAU'),
('6502', '65', 'KABUPATEN BULUNGAN'),
('6503', '65', 'KABUPATEN TANA TIDUNG'),
('6504', '65', 'KABUPATEN NUNUKAN'),
('6571', '65', 'KOTA TARAKAN'),
('7101', '71', 'KABUPATEN BOLAANG MONGONDOW'),
('7102', '71', 'KABUPATEN MINAHASA'),
('7103', '71', 'KABUPATEN KEPULAUAN SANGIHE'),
('7104', '71', 'KABUPATEN KEPULAUAN TALAUD'),
('7105', '71', 'KABUPATEN MINAHASA SELATAN'),
('7106', '71', 'KABUPATEN MINAHASA UTARA'),
('7107', '71', 'KABUPATEN BOLAANG MONGONDOW UTARA'),
('7108', '71', 'KABUPATEN SIAU TAGULANDANG BIARO'),
('7109', '71', 'KABUPATEN MINAHASA TENGGARA'),
('7110', '71', 'KABUPATEN BOLAANG MONGONDOW SELATAN'),
('7111', '71', 'KABUPATEN BOLAANG MONGONDOW TIMUR'),
('7171', '71', 'KOTA MANADO'),
('7172', '71', 'KOTA BITUNG'),
('7173', '71', 'KOTA TOMOHON'),
('7174', '71', 'KOTA KOTAMOBAGU'),
('7201', '72', 'KABUPATEN BANGGAI KEPULAUAN'),
('7202', '72', 'KABUPATEN BANGGAI'),
('7203', '72', 'KABUPATEN MOROWALI'),
('7204', '72', 'KABUPATEN POSO'),
('7205', '72', 'KABUPATEN DONGGALA'),
('7206', '72', 'KABUPATEN TOLI-TOLI'),
('7207', '72', 'KABUPATEN BUOL'),
('7208', '72', 'KABUPATEN PARIGI MOUTONG'),
('7209', '72', 'KABUPATEN TOJO UNA-UNA'),
('7210', '72', 'KABUPATEN SIGI'),
('7211', '72', 'KABUPATEN BANGGAI LAUT'),
('7212', '72', 'KABUPATEN MOROWALI UTARA'),
('7271', '72', 'KOTA PALU'),
('7301', '73', 'KABUPATEN KEPULAUAN SELAYAR'),
('7302', '73', 'KABUPATEN BULUKUMBA'),
('7303', '73', 'KABUPATEN BANTAENG'),
('7304', '73', 'KABUPATEN JENEPONTO'),
('7305', '73', 'KABUPATEN TAKALAR'),
('7306', '73', 'KABUPATEN GOWA'),
('7307', '73', 'KABUPATEN SINJAI'),
('7308', '73', 'KABUPATEN MAROS'),
('7309', '73', 'KABUPATEN PANGKAJENE DAN KEPULAUAN'),
('7310', '73', 'KABUPATEN BARRU'),
('7311', '73', 'KABUPATEN BONE'),
('7312', '73', 'KABUPATEN SOPPENG'),
('7313', '73', 'KABUPATEN WAJO'),
('7314', '73', 'KABUPATEN SIDENRENG RAPPANG'),
('7315', '73', 'KABUPATEN PINRANG'),
('7316', '73', 'KABUPATEN ENREKANG'),
('7317', '73', 'KABUPATEN LUWU'),
('7318', '73', 'KABUPATEN TANA TORAJA'),
('7322', '73', 'KABUPATEN LUWU UTARA'),
('7325', '73', 'KABUPATEN LUWU TIMUR'),
('7326', '73', 'KABUPATEN TORAJA UTARA'),
('7371', '73', 'KOTA MAKASSAR'),
('7372', '73', 'KOTA PAREPARE'),
('7373', '73', 'KOTA PALOPO'),
('7401', '74', 'KABUPATEN BUTON'),
('7402', '74', 'KABUPATEN MUNA'),
('7403', '74', 'KABUPATEN KONAWE'),
('7404', '74', 'KABUPATEN KOLAKA'),
('7405', '74', 'KABUPATEN KONAWE SELATAN'),
('7406', '74', 'KABUPATEN BOMBANA'),
('7407', '74', 'KABUPATEN WAKATOBI'),
('7408', '74', 'KABUPATEN KOLAKA UTARA'),
('7409', '74', 'KABUPATEN BUTON UTARA'),
('7410', '74', 'KABUPATEN KONAWE UTARA'),
('7411', '74', 'KABUPATEN KOLAKA TIMUR'),
('7412', '74', 'KABUPATEN KONAWE KEPULAUAN'),
('7413', '74', 'KABUPATEN MUNA BARAT'),
('7414', '74', 'KABUPATEN BUTON TENGAH'),
('7415', '74', 'KABUPATEN BUTON SELATAN'),
('7471', '74', 'KOTA KENDARI'),
('7472', '74', 'KOTA BAUBAU'),
('7501', '75', 'KABUPATEN BOALEMO'),
('7502', '75', 'KABUPATEN GORONTALO'),
('7503', '75', 'KABUPATEN POHUWATO'),
('7504', '75', 'KABUPATEN BONE BOLANGO'),
('7505', '75', 'KABUPATEN GORONTALO UTARA'),
('7571', '75', 'KOTA GORONTALO'),
('7601', '76', 'KABUPATEN MAJENE'),
('7602', '76', 'KABUPATEN POLEWALI MANDAR'),
('7603', '76', 'KABUPATEN MAMASA'),
('7604', '76', 'KABUPATEN MAMUJU'),
('7605', '76', 'KABUPATEN MAMUJU UTARA'),
('7606', '76', 'KABUPATEN MAMUJU TENGAH'),
('8101', '81', 'KABUPATEN MALUKU TENGGARA BARAT'),
('8102', '81', 'KABUPATEN MALUKU TENGGARA'),
('8103', '81', 'KABUPATEN MALUKU TENGAH'),
('8104', '81', 'KABUPATEN BURU'),
('8105', '81', 'KABUPATEN KEPULAUAN ARU'),
('8106', '81', 'KABUPATEN SERAM BAGIAN BARAT'),
('8107', '81', 'KABUPATEN SERAM BAGIAN TIMUR'),
('8108', '81', 'KABUPATEN MALUKU BARAT DAYA'),
('8109', '81', 'KABUPATEN BURU SELATAN'),
('8171', '81', 'KOTA AMBON'),
('8172', '81', 'KOTA TUAL'),
('8201', '82', 'KABUPATEN HALMAHERA BARAT'),
('8202', '82', 'KABUPATEN HALMAHERA TENGAH'),
('8203', '82', 'KABUPATEN KEPULAUAN SULA'),
('8204', '82', 'KABUPATEN HALMAHERA SELATAN'),
('8205', '82', 'KABUPATEN HALMAHERA UTARA'),
('8206', '82', 'KABUPATEN HALMAHERA TIMUR'),
('8207', '82', 'KABUPATEN PULAU MOROTAI'),
('8208', '82', 'KABUPATEN PULAU TALIABU'),
('8271', '82', 'KOTA TERNATE'),
('8272', '82', 'KOTA TIDORE KEPULAUAN'),
('9101', '91', 'KABUPATEN FAKFAK'),
('9102', '91', 'KABUPATEN KAIMANA'),
('9103', '91', 'KABUPATEN TELUK WONDAMA'),
('9104', '91', 'KABUPATEN TELUK BINTUNI'),
('9105', '91', 'KABUPATEN MANOKWARI'),
('9106', '91', 'KABUPATEN SORONG SELATAN'),
('9107', '91', 'KABUPATEN SORONG'),
('9108', '91', 'KABUPATEN RAJA AMPAT'),
('9109', '91', 'KABUPATEN TAMBRAUW'),
('9110', '91', 'KABUPATEN MAYBRAT'),
('9111', '91', 'KABUPATEN MANOKWARI SELATAN'),
('9112', '91', 'KABUPATEN PEGUNUNGAN ARFAK'),
('9171', '91', 'KOTA SORONG'),
('9401', '94', 'KABUPATEN MERAUKE'),
('9402', '94', 'KABUPATEN JAYAWIJAYA'),
('9403', '94', 'KABUPATEN JAYAPURA'),
('9404', '94', 'KABUPATEN NABIRE'),
('9408', '94', 'KABUPATEN KEPULAUAN YAPEN'),
('9409', '94', 'KABUPATEN BIAK NUMFOR'),
('9410', '94', 'KABUPATEN PANIAI'),
('9411', '94', 'KABUPATEN PUNCAK JAYA'),
('9412', '94', 'KABUPATEN MIMIKA'),
('9413', '94', 'KABUPATEN BOVEN DIGOEL'),
('9414', '94', 'KABUPATEN MAPPI'),
('9415', '94', 'KABUPATEN ASMAT'),
('9416', '94', 'KABUPATEN YAHUKIMO'),
('9417', '94', 'KABUPATEN PEGUNUNGAN BINTANG'),
('9418', '94', 'KABUPATEN TOLIKARA'),
('9419', '94', 'KABUPATEN SARMI'),
('9420', '94', 'KABUPATEN KEEROM'),
('9426', '94', 'KABUPATEN WAROPEN'),
('9427', '94', 'KABUPATEN SUPIORI'),
('9428', '94', 'KABUPATEN MAMBERAMO RAYA'),
('9429', '94', 'KABUPATEN NDUGA'),
('9430', '94', 'KABUPATEN LANNY JAYA'),
('9431', '94', 'KABUPATEN MAMBERAMO TENGAH'),
('9432', '94', 'KABUPATEN YALIMO'),
('9433', '94', 'KABUPATEN PUNCAK'),
('9434', '94', 'KABUPATEN DOGIYAI'),
('9435', '94', 'KABUPATEN INTAN JAYA'),
('9436', '94', 'KABUPATEN DEIYAI'),
('9471', '94', 'KOTA JAYAPURA');

-- --------------------------------------------------------

--
-- Table structure for table `t_kandidat_dosen`
--

CREATE TABLE `t_kandidat_dosen` (
  `id` int(11) NOT NULL,
  `id_matkul` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_kaprodi`
--

CREATE TABLE `t_kaprodi` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kaprodi`
--

INSERT INTO `t_kaprodi` (`id`, `id_pegawai`, `id_status`) VALUES
(1, 397, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori_kegiatan_lkd`
--

CREATE TABLE `t_kategori_kegiatan_lkd` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alias` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori_kegiatan_lkd`
--

INSERT INTO `t_kategori_kegiatan_lkd` (`id`, `nama`, `alias`) VALUES
(1, 'Pengajaran', 'ajar'),
(2, 'Pembimbingan', 'bimbing'),
(4, 'Penelitian dan Pengabdian', 'litab'),
(5, 'Kegiatan Penunjang', 'tunjang'),
(7, 'Pengujian', 'uji');

-- --------------------------------------------------------

--
-- Table structure for table `t_kecamatan`
--

CREATE TABLE `t_kecamatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_kegiatan_lkd`
--

CREATE TABLE `t_kegiatan_lkd` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kegiatan_lkd`
--

INSERT INTO `t_kegiatan_lkd` (`id`, `nama`, `id_kategori`, `id_status`) VALUES
(1, 'Menulis Jurnal', 4, 0),
(2, 'Mengajar', 1, 1),
(3, 'Membimbing', 2, 0),
(4, 'Menguji', 7, 0),
(5, 'Menerjemahkan Buku', 4, 0),
(6, 'Tunjang', 5, 0),
(7, 'Pengabdian', 4, 0),
(8, 'Penelitian', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_kelas`
--

CREATE TABLE `t_kelas` (
  `id` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `kuota` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `id_jenis_kelas` int(11) NOT NULL,
  `id_doswal` int(11) DEFAULT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_kelas_jurusan`
--

CREATE TABLE `t_kelas_jurusan` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kelas_jurusan`
--

INSERT INTO `t_kelas_jurusan` (`id`, `kode`, `nama`) VALUES
(1, 'R', 'Reguler'),
(2, 'Ex', 'Ekstensi');

-- --------------------------------------------------------

--
-- Table structure for table `t_kelas_matkul`
--

CREATE TABLE `t_kelas_matkul` (
  `id` int(11) NOT NULL,
  `kapasitas` int(11) NOT NULL DEFAULT '0',
  `id_kelas` int(11) DEFAULT NULL,
  `id_plot` int(11) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `published` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_kewarganegaraan`
--

CREATE TABLE `t_kewarganegaraan` (
  `id` varchar(3) NOT NULL,
  `negara` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kewarganegaraan`
--

INSERT INTO `t_kewarganegaraan` (`id`, `negara`) VALUES
('ID', 'Indonesia'),
('WNA', 'Warga Negara Asing');

-- --------------------------------------------------------

--
-- Table structure for table `t_komponen_nilai`
--

CREATE TABLE `t_komponen_nilai` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `bobot` int(11) NOT NULL,
  `id_kelas_matkul` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_krs_mahasiswa`
--

CREATE TABLE `t_krs_mahasiswa` (
  `id` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_pembayaran` varchar(25) DEFAULT NULL,
  `id_history_pembayaran` varchar(25) DEFAULT NULL,
  `id_status` int(11) NOT NULL DEFAULT '-1',
  `id_izin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_lkd_harian`
--

CREATE TABLE `t_lkd_harian` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `id_pengajuan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_lkd_harian`
--

INSERT INTO `t_lkd_harian` (`id`, `tanggal`, `id_pengajuan`, `created_at`, `updated_at`) VALUES
(1, '2018-08-30', 1, '2018-09-22 13:56:47', '2018-09-22 13:56:47'),
(2, '2018-09-05', 2, '2018-09-22 13:57:06', '2018-09-22 13:57:06'),
(3, '2018-09-22', 3, '2018-09-22 13:57:42', '2018-09-22 13:57:42'),
(4, '2018-09-07', 2, '2018-09-22 13:59:19', '2018-09-22 13:59:19'),
(5, '2018-09-01', 1, '2018-09-22 13:59:49', '2018-09-22 13:59:49');

-- --------------------------------------------------------

--
-- Table structure for table `t_mahasiswa`
--

CREATE TABLE `t_mahasiswa` (
  `id` int(11) NOT NULL,
  `nim` varchar(15) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `tempat_lahir` varchar(60) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `nik` varchar(20) NOT NULL,
  `id_ktm` varchar(20) NOT NULL,
  `agama` int(11) DEFAULT NULL,
  `nisn` varchar(20) DEFAULT NULL,
  `id_jalur_masuk` int(11) DEFAULT NULL,
  `npwp` varchar(20) DEFAULT NULL,
  `kewarganegaraan` varchar(2) DEFAULT NULL,
  `no_pendaftaran` varchar(15) NOT NULL,
  `jenis_pendaftaran` int(11) DEFAULT NULL,
  `tgl_masuk_sp` date DEFAULT NULL,
  `tgl_lulus` date DEFAULT NULL,
  `mulai_semester` int(11) NOT NULL,
  `jalan` varchar(200) DEFAULT NULL,
  `rt` varchar(3) DEFAULT NULL,
  `rw` varchar(3) DEFAULT NULL,
  `dusun` varchar(30) DEFAULT NULL,
  `kelurahan` varchar(40) DEFAULT NULL,
  `kecamatan` varchar(20) DEFAULT NULL,
  `kabupaten` char(4) DEFAULT NULL,
  `kode_pos` varchar(15) DEFAULT NULL,
  `jenis_tinggal` int(11) DEFAULT NULL,
  `alat_transportasi` int(11) DEFAULT NULL,
  `telp_rumah` varchar(20) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `terima_kps` int(11) DEFAULT '0',
  `no_kps` varchar(20) DEFAULT NULL,
  `nik_ayah` varchar(20) DEFAULT NULL,
  `nama_ayah` varchar(50) DEFAULT NULL,
  `tgl_lahir_ayah` date DEFAULT NULL,
  `pendidikan_ayah` int(11) DEFAULT NULL,
  `pekerjaan_ayah` int(11) DEFAULT NULL,
  `penghasilan_ayah` int(11) DEFAULT NULL,
  `alamat_ayah` varchar(100) NOT NULL,
  `no_telp_ayah` varchar(15) NOT NULL,
  `nik_ibu` varchar(20) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `tgl_lahir_ibu` date DEFAULT NULL,
  `pendidikan_ibu` int(11) DEFAULT NULL,
  `pekerjaan_ibu` int(11) DEFAULT NULL,
  `penghasilan_ibu` int(11) DEFAULT NULL,
  `alamat_ibu` varchar(100) NOT NULL,
  `no_telp_ibu` varchar(15) NOT NULL,
  `nama_wali` varchar(50) DEFAULT NULL,
  `tgl_lahir_wali` date DEFAULT NULL,
  `pendidikan_wali` int(11) DEFAULT NULL,
  `pekerjaan_wali` int(11) DEFAULT NULL,
  `penghasilan_wali` int(11) DEFAULT NULL,
  `alamat_wali` varchar(100) NOT NULL,
  `no_telp_wali` varchar(15) NOT NULL,
  `foto` varchar(150) NOT NULL DEFAULT 'assets\\images\\profiles\\default.jpg',
  `s_kelas` varchar(30) NOT NULL,
  `s_jurusan` int(11) DEFAULT NULL,
  `s_sekolah` int(11) DEFAULT NULL,
  `s_nilai_unas` float NOT NULL,
  `s_nama` varchar(50) NOT NULL,
  `s_kabupaten` char(4) DEFAULT NULL,
  `s_kecamatan` varchar(30) NOT NULL,
  `s_jalan` varchar(100) NOT NULL,
  `s_kode_pos` varchar(10) NOT NULL,
  `nim_asal` varchar(15) NOT NULL,
  `kode_pt_asal` varchar(10) NOT NULL,
  `pindahan_asing` int(11) DEFAULT NULL,
  `id_prodi` int(11) NOT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_dosenpa` int(11) DEFAULT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1',
  `id_akun` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_mahasiswa`
--

INSERT INTO `t_mahasiswa` (`id`, `nim`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `nik`, `id_ktm`, `agama`, `nisn`, `id_jalur_masuk`, `npwp`, `kewarganegaraan`, `no_pendaftaran`, `jenis_pendaftaran`, `tgl_masuk_sp`, `tgl_lulus`, `mulai_semester`, `jalan`, `rt`, `rw`, `dusun`, `kelurahan`, `kecamatan`, `kabupaten`, `kode_pos`, `jenis_tinggal`, `alat_transportasi`, `telp_rumah`, `no_hp`, `email`, `terima_kps`, `no_kps`, `nik_ayah`, `nama_ayah`, `tgl_lahir_ayah`, `pendidikan_ayah`, `pekerjaan_ayah`, `penghasilan_ayah`, `alamat_ayah`, `no_telp_ayah`, `nik_ibu`, `nama_ibu`, `tgl_lahir_ibu`, `pendidikan_ibu`, `pekerjaan_ibu`, `penghasilan_ibu`, `alamat_ibu`, `no_telp_ibu`, `nama_wali`, `tgl_lahir_wali`, `pendidikan_wali`, `pekerjaan_wali`, `penghasilan_wali`, `alamat_wali`, `no_telp_wali`, `foto`, `s_kelas`, `s_jurusan`, `s_sekolah`, `s_nilai_unas`, `s_nama`, `s_kabupaten`, `s_kecamatan`, `s_jalan`, `s_kode_pos`, `nim_asal`, `kode_pt_asal`, `pindahan_asing`, `id_prodi`, `id_kelas`, `id_dosenpa`, `id_status`, `id_akun`) VALUES
(1, '1511071001', 'AHMAD SOLIKHIN', 'Paleran', '1997-06-24', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.yos sudarso', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Suyati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1012),
(2, '1511071002', 'GREVI MILA KAMELIA', 'Palu', '1997-09-15', 'P', '0', '', 2, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.angkasa', NULL, NULL, NULL, 'Birobuli Utara', '186002', NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Suriani', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1013),
(3, '1511071003', 'FITRI DESI RONDONUWU', 'Tomohon', '1995-12-16', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'PPK Tinggede', NULL, NULL, NULL, 'Lasoani', '186003', NULL, NULL, 99, NULL, NULL, NULL, 'fitrideysi@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Sarini Sartje L.Loho', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1014),
(4, '1511071004', 'MUH IHSAN REZA', 'Palu', '1993-06-22', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-08', NULL, 1, 'JL. Layana Indah', NULL, NULL, NULL, 'Tondo', '186003', NULL, NULL, 1, NULL, NULL, NULL, 'muhammadihsan.reza@gmail.com', 0, NULL, NULL, 'Udin Tubo', NULL, NULL, NULL, NULL, '', '', NULL, 'Nurmin', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1015),
(5, '1511071005', 'FAHRUL', 'Labuan', '1993-12-21', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl. Lantiggau No.38', NULL, NULL, NULL, 'Labuan', '180209', NULL, NULL, 1, NULL, NULL, NULL, 'fahruyado@yahoo.com', 0, NULL, NULL, 'Asriado', NULL, NULL, NULL, NULL, '', '', NULL, 'Siti Ramna', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1016),
(6, '1511071006', 'HASRIA', 'Palu', '1983-12-19', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'BTN Citra Banua Nagaya Blok D No.17', NULL, NULL, NULL, 'Kabonena', '186001', NULL, NULL, 99, NULL, NULL, NULL, 'hasriaiyha@gmail.com', 0, NULL, NULL, 'moh. Thalib', NULL, NULL, NULL, NULL, '', '', NULL, 'Hasia', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1017),
(7, '1511071007', 'NOVIANTI KASIM', 'Luwuk', '1997-11-11', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.hangtuah 1', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'darwin kasim', NULL, NULL, NULL, NULL, '', '', NULL, 'Rusni Polo', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1018),
(8, '1511071008', 'ANGGRAENI', 'Usongi', '1998-06-14', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.pendidikan', NULL, NULL, NULL, 'Tondo', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Amirudin', NULL, NULL, NULL, NULL, '', '', NULL, 'Hasna. H', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1019),
(9, '1511071009', 'RAHMAWATI A  MAPARESSA', 'Pagimana', '1992-03-28', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1020),
(10, '1511071010', 'NUR ISMY SADIA', 'Palu', '1997-09-11', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.munif rahman', NULL, NULL, NULL, 'Kabonena', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'sukman', NULL, NULL, NULL, NULL, '', '', NULL, 'Mariam', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1021),
(11, '1511071011', 'ROBIATUL WAHIDAH', 'Momunu', '1997-12-14', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Mantikulore', '186003', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Fatmawati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1022),
(12, '1511071012', 'NUR AIDAH', 'Sigenti', '1993-11-29', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1023),
(13, '1511071013', 'RINA MONIKA GINOGA', 'PALU', '1995-11-05', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1024),
(14, '1511071014', 'AKRI AGUSPANDRI LUTANI', 'Palu', '1992-08-17', 'L', '0', '', 2, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'donggala kodi', '186001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'asjod lutani', NULL, NULL, NULL, NULL, '', '', NULL, 'Naomi', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1025),
(15, '1511071015', 'MIRNAWATI', 'Pare-Pare', '1994-07-13', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.darussalam palu', NULL, NULL, NULL, 'tatura utara', '186002', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'amiruddin', NULL, NULL, NULL, NULL, '', '', NULL, 'siti rahma', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1026),
(16, '1511071016', 'NURWANA SARI', 'Palu', '1996-05-11', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'Jl.Kacang Panjang lrg.1', NULL, NULL, NULL, 'Kamunji', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Sumasniati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1027),
(17, '1511071017', 'AHMAD A LANDONG', 'Laulalang', '1997-12-17', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl. Labu', NULL, NULL, NULL, 'Balaroa', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Amir Landong', NULL, NULL, NULL, NULL, '', '', NULL, 'Rida Haris', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1028),
(18, '1511071018', 'DESSI LUSMIANTI TOKII', 'Bewa', '1989-12-04', 'P', '0', '', 2, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'bewa', NULL, NULL, NULL, 'bewa', '180302', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Yakobus tokii', NULL, NULL, NULL, NULL, '', '', NULL, 'ratna sari saselah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1029),
(19, '1511071019', 'I KETUT WARJA', 'Keliki', '1996-08-11', 'L', '0', '', 4, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.bouraq perum. Gria garuda indah blok A1/B', NULL, NULL, NULL, 'Lasoani', '186001', NULL, NULL, 2, NULL, NULL, NULL, 'ketut_warja@yahoo.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'ni nyoman rayu', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1030),
(20, '1511071020', 'NOVIANA', 'Palu', '1991-11-16', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.laluna', NULL, NULL, NULL, 'Tondo', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Puspawati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1031),
(21, '1511071021', 'NI MADE DWI ASTUTI', 'Tanalanto', '1974-01-12', 'P', '0', '', 2, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl. Kesadaran 1 No. 9', NULL, NULL, NULL, 'tatura utara', '186002', NULL, NULL, 1, NULL, NULL, NULL, 'madedewiastuti86@gmail.com', 0, NULL, NULL, 'i Nyoman Suastika', NULL, NULL, NULL, NULL, '', '', NULL, 'Ni Ketut Namiadi', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1032),
(22, '1511071022', 'SITI RAHMI', 'Uetoli', '1994-05-01', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'BTN bumi indah tinggede', NULL, NULL, NULL, 'Marawola', '181010', NULL, NULL, 2, NULL, NULL, NULL, 'sitirahmi0105@gmail.com', 0, NULL, NULL, 'bahrun C. Bakali', NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Rusni A. Mohune', NULL, NULL, NULL, NULL, '', '', 'Saleha SP.Ag', NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1033),
(23, '1511071023', 'WIRANTI A. LAMAKA', 'Busak', '1998-06-02', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.una-una no 44A', NULL, NULL, NULL, 'lolu utara', '186002', NULL, NULL, 2, NULL, NULL, NULL, 'whywhy.djow@yahoo.com', 0, NULL, NULL, 'ardin R lamaka', NULL, NULL, NULL, NULL, '', '', NULL, 'Nurhayati Hi Abdullah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1034),
(24, '1511071024', 'VINNY RAHMAYANTI', 'Tondo', '1992-01-30', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'Jl.Una-una 44A', NULL, NULL, NULL, 'Lalu Utara', '186003', NULL, NULL, 99, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Nurhayati Hi Abdullah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1035),
(25, '1511071025', 'OKI WATI', 'Marowo', '1994-11-14', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'Jl.Maleo', NULL, NULL, NULL, 'Palu Selatan', '186002', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Sapiha H.Sony', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1036),
(26, '1511071026', 'LIVIA PRATIWI', 'Palu', '1997-11-11', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2012-10-20', NULL, 1, 'jl.matikulore no 04', NULL, NULL, NULL, 'Lasoani', '186003', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'mohamad zulfin', NULL, NULL, NULL, NULL, '', '', NULL, 'ulfah muslimah watung', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1037),
(27, '1511071027', 'SARIPA SALMA', 'Watumeeto', '1995-11-12', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.baiya raya no.70', NULL, NULL, NULL, 'Baiya', '186003', NULL, NULL, 99, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'hj.harung', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1038),
(28, '1511071028', 'EDWIN SETIAWAN', 'Ampana', '1997-08-05', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.zebra V no 33', NULL, NULL, NULL, 'Birobuli', '186002', NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'harold edward', NULL, NULL, NULL, NULL, '', '', NULL, 'aprina lingke', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1039),
(29, '1511071029', 'CITRA  T', 'Palu', '1995-11-22', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.otista', NULL, NULL, NULL, 'Palu Timur', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Nias', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1040),
(30, '1511071030', 'WIDYAWATI', 'Tawaili', '1995-06-06', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.dayo dara cp4 blok B no 4', NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 99, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'astriani', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1041),
(31, '1511071031', 'BETHARIA WULANDARI TARUANGI', 'Baturube', '1987-06-03', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1042),
(32, '1511071032', 'RUMILA', 'Lemo', '1996-05-23', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.pipa air', NULL, NULL, NULL, 'Palu Barat', '186001', NULL, NULL, 99, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Gusna', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1043),
(33, '1511071033', 'MUHAMMAD ARMID MUSTAFA', 'Palu', '1993-07-18', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-08', NULL, 1, 'BTN Palupi Blok 13 no.32', NULL, NULL, NULL, 'Palupi', '186002', NULL, NULL, 1, NULL, NULL, NULL, 'armitpradita@gmail.com', 0, NULL, NULL, 'Usrin Mustafa', NULL, NULL, NULL, NULL, '', '', NULL, 'Syarifah Modjo', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1044),
(34, '1511071034', 'ANGGRAENI', 'Sidoan', '1997-01-24', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'Jl.Sisingamangaraja', NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Nurliansi', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1045),
(35, '1511071035', 'SRI WIDYAWATI', 'Ulavan', '1997-05-24', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.hangtuah 1', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'sapia', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1046),
(36, '1511071036', 'MOH  MI\'RAJ NOOR', 'Palu', '1997-06-02', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'Jl.Kihajar dewantara sigi biromaru', NULL, NULL, NULL, 'Lolu', '181013', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Zaitun S.Sugi', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1047),
(37, '1511071037', 'MUH  SYAWAL RIZALDI', 'Palu', '1997-03-01', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'Jl. Kelapa dua', NULL, NULL, NULL, 'Lere', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Najemawati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1048),
(38, '1511071038', 'I NYOMAN TRI WARDIKA', 'Martajaya', '1998-06-26', 'L', '0', '', 4, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl. Zebra 3 no 26', NULL, NULL, NULL, 'Birobuli', '186002', NULL, NULL, 99, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Ni ketut sukerni', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1049),
(39, '1511071039', 'HIJRIANTO', 'Toli-Toli', '1978-07-21', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'BTN Pesona Nokilalaki Blok B4 No.7', NULL, NULL, NULL, 'Tondo', '186003', NULL, NULL, 99, NULL, NULL, NULL, 'hijrianto78@gmail', 0, NULL, NULL, 'Hj. Patta Djalling', NULL, NULL, NULL, NULL, '', '', NULL, 'Siti Maulana', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1050),
(40, '1511071040', 'DEASY MANGUMI', 'Ponggerang', '1986-12-08', 'P', '0', '', 2, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl. Anoa 1 lrg.Pemuda Pancasila', NULL, NULL, NULL, 'tatura utara', '186002', NULL, NULL, 99, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Marten Todingan', NULL, NULL, NULL, NULL, '', '', NULL, 'Naomi Lele', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1051),
(41, '1511071041', 'MOHAMAD BAYU', 'watunonju', '1990-12-10', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1052),
(42, '1511071042', 'SUDHARMIN', 'Tomini', '1979-02-07', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'BTN II Blok C2 No. 17', NULL, NULL, NULL, 'Mamboro', '186004', NULL, NULL, 1, NULL, NULL, NULL, 'darmin.andina07@gmail.com', 0, NULL, NULL, 'Moh. Arwi', NULL, NULL, NULL, NULL, '', '', NULL, 'Masnia', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1053),
(43, '1511071043', 'ULFA NURWAHYUNINGRUM', 'Palu', '1997-05-10', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.Yos sudarso Komp.TNI AL', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 1, NULL, NULL, NULL, 'ulfahnurwahyuningrum@yahoo.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Sodina', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1054),
(44, '1511071044', 'SAGITA', 'Leli', '1998-03-27', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.bantilan', NULL, NULL, NULL, 'Lere', '186001', NULL, NULL, 3, NULL, NULL, NULL, 'gietashagita@gamil.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Aena', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1055),
(45, '1511071045', 'SULTONI', 'Malang', '1996-10-01', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, 'jl.trans palu napu', NULL, NULL, NULL, 'Desa Bobo', '181005', NULL, NULL, 1, NULL, NULL, NULL, 'sulthoni.n.libra@facebook.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'sadia', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1056),
(46, '1511071046', 'MOH ANDIKA CANDRA', 'Palu', '1997-12-07', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'Perumnas Balaroa No.10 Palu', NULL, NULL, NULL, 'Balaroa', '186001', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Andi Zaitun', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1057),
(47, '1511071047', 'DINI OKTAVIANI', 'Toli-Toli', '1998-10-01', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.emmysaelan lrg.manunggal no.1', NULL, NULL, NULL, 'Lolu', '186002', NULL, NULL, 99, NULL, NULL, NULL, 'ninnonk01@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'nuryanti', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1058),
(48, '1511071048', 'MUTHMAINNAH', 'Palu', '1997-06-15', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'padanjakaya', NULL, NULL, NULL, 'Pengawu', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'irmatriani', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1059),
(49, '1511071049', 'MUZDALIFAH', 'Palu', '1997-07-28', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.manggis no 7A palu', NULL, NULL, NULL, 'Balaroa', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Resfina Finarti welahi', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1060),
(50, '1511071050', 'AYU SETYA NINGSIH', 'Margapura', '1996-10-10', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 1, NULL, NULL, NULL, 'ayusetyaningsih110@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Mutini', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1061),
(51, '1511071051', 'MOH REZA VAHLEVI', 'kalangkangan', '1997-07-01', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'palu selatan', '186002', NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Evinovia', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1062),
(52, '1511071052', 'AKRAM MUHSININ', 'palu', '1998-06-04', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.garuda 2 no 23', NULL, NULL, NULL, 'birobuli utara', '186002', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'arifin halid', NULL, NULL, NULL, NULL, '', '', NULL, 'Meutia Rita', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1063),
(53, '1511071053', 'YULIANA', 'lambara', '1992-05-06', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Lambara', '186004', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hadimah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1064),
(54, '1511071054', 'ASI', 'patukuki', '1996-09-10', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'towua', '186002', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'sitima', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1065),
(55, '1511071055', 'WILDANI DWI DINIYAH', 'luwuk', '1997-01-02', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-11-18', NULL, 1, NULL, NULL, NULL, NULL, 'birobuli utara', '186002', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'sulastri lamansa', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1066),
(56, '1511071056', 'FADIL', 'palu', '1997-08-03', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'taweli', '186003', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Fadlia', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1067),
(57, '1511071057', 'KARLINA S KUNTUAMAS', 'timbulon', '1996-11-10', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Satria M. Daad', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1068),
(58, '1511071058', 'NURHAYANTI', 'toli-toli', '1993-07-06', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'birobuli utara', '186002', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'NgaisyahNgaisyah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1069),
(59, '1511071059', 'MEGA SILVANA', 'samarinda', '1994-05-01', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'yojo kodi', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'asdiana', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1070),
(60, '1511071060', 'SUCERNI', 'binangga', '1979-06-07', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'kawatuna', '186003', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hakia', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1071),
(61, '1511071061', 'NADYA ANGGITASARI', 'jakarta', '1997-11-13', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'purwatiningsih', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1072),
(62, '1511071062', 'ADE MELINDA', 'palopo', '1997-05-23', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'lasoani', '186003', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Suharni Sandana', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1073),
(63, '1511071063', 'WARZIKA FADLIA', 'palu', '1994-02-03', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1074),
(64, '1511071064', 'FITRA DEVI DAMAYANTI', 'palu', '1997-02-05', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'tatura utara', '186002', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'iriani', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1075),
(65, '1511071065', 'RENNY FEBRINA', 'palu', '1984-02-24', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'donggala kodi', '186001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Rosmini', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1076),
(66, '1511071066', 'RIFALDHI', 'mamboro', '1996-11-08', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'mamboro', '186004', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Rukmini', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1077),
(67, '1511071067', 'MILA KARMILA KAMASE', 'tambarana', '1998-08-09', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'besusu timur', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Hatidja Arifin', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1078),
(68, '1511071068', 'MOHAMAD FIKRI', 'Taweli', '1996-04-15', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1079),
(69, '1511071069', 'NADIRA', 'palu', '1997-07-17', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'mamboro', '186004', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Djaenab Lasinong', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1080),
(70, '1511071070', 'ANDRIANI APRISYE NURSALFATARI', 'palu', '1998-04-14', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'sigi', '181013', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'hadassa', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1081),
(71, '1511071071', 'BELA SAFITRI LATOWALE', 'Toli-Toli', '1998-02-02', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'btn teluk palu permai blok J/2', NULL, NULL, NULL, 'Tondo', '186003', NULL, NULL, 2, NULL, NULL, NULL, 'bellasafitri145@yahoo.co.id', 0, NULL, NULL, 'moh.fathun latowale', NULL, NULL, NULL, NULL, '', '', NULL, 'anita faddlia dg.parebba', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1082),
(72, '1511071072', 'UNGGUL WIDYARTO B', 'Palu', '1994-04-07', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.pipa air', NULL, NULL, NULL, 'donggala kodi', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Bambang Sugiarto', NULL, NULL, NULL, NULL, '', '', NULL, 'Chandra Nurlaila', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1083),
(73, '1511071073', 'MOH REZAL', 'Tilung', '1994-04-23', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.manggis', NULL, NULL, NULL, 'Balaroa', '186001', NULL, NULL, 3, NULL, NULL, NULL, 'echa_cacinong@yahoo.com', 0, NULL, NULL, 'zulkifli', NULL, NULL, NULL, NULL, '', '', NULL, 'nursiam', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1084),
(74, '1511071074', 'YAN SETIAWAN', 'Palu', '1995-03-10', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl.Dayodara No. 1', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 1, NULL, NULL, NULL, 'yansetiawan28@gmail.com', 0, NULL, NULL, 'Syafrudin', NULL, NULL, NULL, NULL, '', '', NULL, 'Joharni', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1085),
(75, '1511071075', 'ASYRANI NURDIN', 'Watampone', '1993-11-24', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl.karana BTN 2 Blok c/33', NULL, NULL, NULL, 'Mamboro', '186004', NULL, NULL, 2, NULL, NULL, NULL, 'run.nee2412@gmail.com', 0, NULL, NULL, 'Nurdin HB', NULL, NULL, NULL, NULL, '', '', NULL, 'Nadirah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1086),
(76, '1511071076', 'BERLIAN WULANSARI', 'Moutong', '1994-09-03', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'Desa Lero', NULL, NULL, NULL, 'Desa Lero', '180210', NULL, NULL, 1, NULL, NULL, NULL, 'berwul39@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Nurfaida', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1087),
(77, '1511071077', 'KADE ARIANTO', 'Bali', '1992-04-07', 'L', '0', '', 4, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl. Lagarutu no.15', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 99, NULL, NULL, NULL, 'ariantokade@gmail.com', 0, NULL, NULL, 'ketut cutet', NULL, NULL, NULL, NULL, '', '', NULL, 'Wayan Srimpen', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1088),
(78, '1511071078', 'HASRIANTY', 'Palu', '1997-04-17', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'Desa Potoya', NULL, NULL, NULL, 'Desa Potoya', '181007', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'Hasan', NULL, NULL, NULL, NULL, '', '', NULL, 'Maryam', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1089),
(79, '1511071079', 'WAHYU RAMADHAN', 'Lembasada', '1998-01-16', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.manggis', NULL, NULL, NULL, 'Balaroa', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'fabotin tayib', NULL, NULL, NULL, NULL, '', '', NULL, 'hamna', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1090),
(80, '1511071080', 'AGUSSALIM B', 'rappang', '1977-08-15', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'kamonji', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Sauwaleng', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1091),
(81, '1511071081', 'FERDIANSYAH', 'donggala', '1990-09-11', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'maleni', '180233', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Munifah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1092),
(82, '1511071082', 'SITI ZAHRA', 'palasa', '1991-07-07', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'kayu malue ngapa', '186004', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Muniati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1093),
(83, '1511071083', 'BELA DWIJAYANTI', 'kayumalue', '1994-08-30', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'birobuli', '186002', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Siti Nurfah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1094),
(84, '1511071084', 'IKRAM MAULANA', 'poso', '1993-09-04', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1095),
(85, '1511071085', 'KAMRAN', 'Siboang', '1985-08-30', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl.s. Parman no.69', NULL, NULL, NULL, 'Besusu TIMUR', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Rabiah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1096),
(86, '1511071086', 'INDRA', 'LEBITI', '1996-04-02', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.hangtuah', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Esni', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1097),
(87, '1511071087', 'SUKMAWATI LATOWA', 'TOIBA', '1993-05-21', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.btn lasoani no 16 blok F7', NULL, NULL, NULL, 'Lasoani', '186002', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Isma Lasewa', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1098),
(88, '1511071088', 'ABDUL CHALID', 'MAMBORO', '1997-08-13', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl. Tandame', NULL, NULL, NULL, 'Mamboro', '186004', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Kasmawati Abd. Majid', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1099),
(89, '1511071089', 'NUR AFNI', 'PALU', '1979-06-08', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1100),
(90, '1511071090', 'ABDUL VARID', 'PALU', '1990-06-08', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl. Bougenvil', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 1, NULL, NULL, NULL, 'abdulvarid0890@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Fitriani', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1101),
(91, '1511071091', 'IVHAL VEVIVHAR', 'BONE', '1995-01-22', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl. Teluk raya np.3', NULL, NULL, NULL, 'TONDO', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Asmiati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1102),
(92, '1511071092', 'MUHAMMAD RIZAL', 'PALU', '1984-04-16', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'dusun II Desa Sibedi', NULL, NULL, NULL, 'Marawola', '181010', NULL, NULL, 2, NULL, NULL, NULL, 'ijal.paloemara@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Nuri Burahima', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1103),
(93, '1511071093', 'YOYON KUSWANTO', 'biromaru', '1989-10-10', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1104),
(94, '1511071094', 'MUH FADEL CAESAR', 'PALU', '1997-09-11', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl. Cemara 1 No. 36', NULL, NULL, NULL, 'donggala kodi', '186001', NULL, NULL, 1, NULL, NULL, NULL, 'caesar.fadel@yahoo.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Patri', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1105),
(95, '1511071095', 'ANA PURNAMA', 'palu', '1997-03-01', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise Valangguni', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1106),
(96, '1511071096', 'NUR AFNI IMBRAN', 'BOSANGON', '1996-12-25', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Balaroa', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Nurtia', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1107),
(97, '1511071097', 'MARNIWATI', 'KUMALIGON', '1995-04-07', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl. Watumapida', NULL, NULL, NULL, 'lolu utara', '186003', NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hadija M Nauk', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1108),
(98, '1511071098', 'SANUDIN BORA', 'MARISA', '1992-03-20', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl.seruni raya', NULL, NULL, NULL, 'Balaroa', '186001', NULL, NULL, 99, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'naomi', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1109);
INSERT INTO `t_mahasiswa` (`id`, `nim`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `nik`, `id_ktm`, `agama`, `nisn`, `id_jalur_masuk`, `npwp`, `kewarganegaraan`, `no_pendaftaran`, `jenis_pendaftaran`, `tgl_masuk_sp`, `tgl_lulus`, `mulai_semester`, `jalan`, `rt`, `rw`, `dusun`, `kelurahan`, `kecamatan`, `kabupaten`, `kode_pos`, `jenis_tinggal`, `alat_transportasi`, `telp_rumah`, `no_hp`, `email`, `terima_kps`, `no_kps`, `nik_ayah`, `nama_ayah`, `tgl_lahir_ayah`, `pendidikan_ayah`, `pekerjaan_ayah`, `penghasilan_ayah`, `alamat_ayah`, `no_telp_ayah`, `nik_ibu`, `nama_ibu`, `tgl_lahir_ibu`, `pendidikan_ibu`, `pekerjaan_ibu`, `penghasilan_ibu`, `alamat_ibu`, `no_telp_ibu`, `nama_wali`, `tgl_lahir_wali`, `pendidikan_wali`, `pekerjaan_wali`, `penghasilan_wali`, `alamat_wali`, `no_telp_wali`, `foto`, `s_kelas`, `s_jurusan`, `s_sekolah`, `s_nilai_unas`, `s_nama`, `s_kabupaten`, `s_kecamatan`, `s_jalan`, `s_kode_pos`, `nim_asal`, `kode_pt_asal`, `pindahan_asing`, `id_prodi`, `id_kelas`, `id_dosenpa`, `id_status`, `id_akun`) VALUES
(99, '1511071099', 'ALAN JABIR', 'PALU', '1996-10-18', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.kelor no 21', NULL, NULL, NULL, 'DUYU', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Rabiah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1110),
(100, '1511071100', 'MOH. SYAIR', 'donggala', '1986-11-10', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'tanjung batu', '180208', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Rabiah D', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1111),
(101, '1511071101', 'JEANE APRILIA SHYANET', 'PALU', '1996-04-20', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl. Hangtuah', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 4, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Indriyati HI. Beike', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1112),
(102, '1511071102', 'ABDUL LATIF Z  DG MATOJO', 'POSO', '1997-09-08', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.kihajar dewantara no 72 A palu', NULL, NULL, NULL, 'TALISE', '186003', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'faizah nasir', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1113),
(103, '1511071103', 'INDAH SRI ASTUTI', 'SIPI', '1997-04-04', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.hangtuah', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Dra.Mosa', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1114),
(104, '1511071104', 'GULAM PRAYUDHA', 'pakuli', '1992-03-13', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-08', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1115),
(105, '1511071105', 'ASRI  REZKIANA RIDWAN', 'OGOAMAS', '1993-02-17', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl.miranti 2 No. 1 Perumnas Tinggede', NULL, NULL, NULL, 'Tinggede', '181010', NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Sarnawiah Labada', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1116),
(106, '1511071106', 'SATRIA BAGAS FEBRIANTO', 'SURABAYA', '1995-07-04', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl. Purnawirawan', NULL, NULL, NULL, 'tatura utara', '186001', NULL, NULL, 2, NULL, NULL, NULL, 'satriabagas5452@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Sumiati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1117),
(107, '1511071107', 'JUSMAN', 'SAILONG', '1978-08-02', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl.munif Rahman', NULL, NULL, NULL, 'SILAE', '186001', NULL, NULL, 99, NULL, NULL, NULL, 'jusmanlab00@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Fatimah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1118),
(108, '1511071108', 'RUSMILA', 'TANAHMEA', '1987-12-25', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Banawa Selatan', '180224', NULL, NULL, 1, NULL, NULL, NULL, 'rusmilamilaaja@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Isabenne', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1119),
(109, '1511071109', 'MUHAMMAD RUSLI', 'PARE-PARE', '1982-09-13', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, 'jl.trans sulawesi letawa', NULL, NULL, NULL, 'LETAWA', '186003', NULL, NULL, 99, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hajrah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1120),
(110, '1511071110', 'PUJI ASTUTI', 'SESE', '1991-12-01', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj.berlin', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1121),
(111, '1511071111', 'WARNI', 'buol', '1997-08-08', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1122),
(112, '1511071112', 'SUCI INDAH SARI', 'Toli-Toli', '1997-12-02', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, 'jl. Samratulangi no 48', NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'ariati', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1123),
(113, '1511071113', 'SRI WAHYUNI TABA', 'palu', '1997-07-20', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'rostianti paido', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1124),
(114, '1511071114', 'FIRDAYANTI', 'toli-toli', '1997-11-23', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'munawarah', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1125),
(115, '1511071115', 'UBAI DILLAH', 'toli-toli', '1998-01-28', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'upik', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1126),
(116, '1511071116', 'RATNA SARI', 'lantapan', '1991-10-10', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'duyu', '186001', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj.subaeda', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1127),
(117, '1511071117', 'FITRIANA', 'toli-toli', '1997-02-15', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'pengawu', '186002', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hasna', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1128),
(118, '1511071118', 'FACHARI MAULANA', 'toli-tolo', '1995-08-08', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'pengawu', '186002', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'andriani', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1129),
(119, '1511071119', 'MOH FARID HIDAYAT', 'toli-toli', '1997-10-25', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'samsiar', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1130),
(120, '1511071120', 'SANDI. S', 'pinrang', '1996-12-08', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'jumiarti', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1131),
(121, '1511071121', 'SYAMSIAR AWALUDIN', 'toli-toli', '1996-06-08', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'salma', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1132),
(122, '1511071122', 'NURYANA PALLOGE', 'toli-toli', '1996-07-12', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, 'yanapalloge@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'meryam katim', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1133),
(123, '1511071123', 'SEPTIANI', 'Sandana', '1995-09-24', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, NULL, NULL, NULL, NULL, 'septiani.siman@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj.husnaeni', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1134),
(124, '1511071124', 'TIENCY PRESTHINA ACHMAD', 'Leok', '1997-07-03', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, NULL, NULL, NULL, NULL, 'tiencyachmad@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hatija Madas', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1135),
(125, '1511071125', 'SITTI MUNAWAROH', 'Kokobuka', '1993-08-29', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-08', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Yayuk', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1136),
(126, '1511071126', 'RUKMINI', 'pomayagon', '1996-09-25', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1137),
(127, '1511071127', 'RANTI IWAN', 'Kali', '1998-03-02', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'sumiati l larekeng', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1138),
(128, '1511071128', 'SRI LAILU K. NAOTA', 'Tayadun', '1997-10-03', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'rohani is si.o', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1139),
(129, '1511071129', 'SULISTIANI S. LARATU', 'bongo', '1997-04-27', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1140),
(130, '1511071130', 'DIASASTIZA', 'Kali', '1997-10-04', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Jamalia B Sese', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1141),
(131, '1511071131', 'DEVIRAWATI', 'Buol', '1996-12-25', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'delvliantri us gente', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1142),
(132, '1511071132', 'MARWANSA R. PAERA', 'timbulon', '1996-01-20', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'sulatri sj k banti', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1143),
(133, '1511071133', 'NURUL FITRAH', 'LEOK', '1997-01-30', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1144),
(134, '1511071134', 'MOHAMAD RIFAI', 'Busak 2', '1996-06-11', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'kasmiar m paweli', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1145),
(135, '1511071135', 'LISDYAWATI', 'palu', '1983-04-24', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, 'lisdyawati83@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Nurjani', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1146),
(136, '1511071136', 'RAHMAWATI R. SAPENI', 'Poso', '1985-03-25', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, 'hirawanadirianto@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'suriaty binangkari', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1147),
(137, '1511071137', 'MARYAM Hi. ARSYAD', 'Poso', '1986-02-14', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, 'arsyad_maryam@yahoo.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'farida m.libe', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1148),
(138, '1511071138', 'UPRIANI', 'manisa', '1983-11-28', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'taliese', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1149),
(139, '1511071139', 'DEASY JUNIARTI', 'Poso', '1987-06-12', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'salma djmaludin', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1150),
(140, '1511071140', 'JUSMIATI', 'Sidondo', '1986-04-14', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, 'jusmiati.1404@gmail.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Hj. Nurkaya', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1151),
(141, '1511071141', 'LISA OKTAVIN GINTU', 'Tentena', '1983-10-19', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Lindmandang', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1152),
(142, '1511071142', 'ANITA TRIANI KARNO', 'Bone Sompe', '1987-06-24', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'ratna yusuf', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1153),
(143, '1511071143', 'MOH RIZKY ISLAMI', 'toli-toli', '1998-07-20', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'talise', '186003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1154),
(144, '1511071144', 'DIGNA RIZKI ADINDA', 'Parigi', '1997-11-20', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 1, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'birobuli', '186002', NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Misviany Rotinsulu', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1155),
(145, '1511071145', 'RELISTA SAMUDIN', 'Gorontalo', '1993-02-09', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Fatria Ismail', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1156),
(146, '1511071146', 'OLVIANA', 'Nupabomba', '1992-10-06', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'tanantovea', '180225', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, 'Ihnar', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1157),
(147, '1511071147', 'MOH TAUFIK KOROMPOT', 'Toli-Toli', '1995-05-30', 'L', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2015-08-02', NULL, 1, NULL, NULL, NULL, NULL, 'Talise', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '-', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1158),
(148, '1521071148', 'RISMAWATI', 'Labuan', '1993-07-28', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2016-01-04', NULL, 8, 'JALAN NURI', NULL, NULL, NULL, 'Palu Selatan', '186003', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'RIJAL H LAMAIDDI', NULL, NULL, NULL, NULL, '', '', NULL, 'DARSIMA ABD RAHMAN', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1159),
(149, '1521071149', 'SITI HARTINA PUTRI', 'KAYU AGUNG', '1997-01-13', 'P', '0', '', 1, NULL, NULL, NULL, 'ID', '', 2, '2016-01-04', NULL, 8, 'DESA PONDING-PONDING', NULL, NULL, NULL, 'DESA PONDING-PONDING', '180117', NULL, NULL, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, 'HASRUN PANTANEMO', NULL, NULL, NULL, NULL, '', '', NULL, 'UMI KHASANAH', NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', NULL, NULL, 0, '', NULL, '', '', '', '', '', NULL, 50, NULL, NULL, 1, 1160);

-- --------------------------------------------------------

--
-- Table structure for table `t_matkul`
--

CREATE TABLE `t_matkul` (
  `id` int(11) NOT NULL,
  `kode` varchar(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `sks_tatap_muka` int(11) DEFAULT '0',
  `sks_praktek` int(11) DEFAULT '0',
  `sks_lapangan` int(11) DEFAULT '0',
  `sks_simulasi` int(11) DEFAULT '0',
  `semester` int(11) NOT NULL,
  `status_sap` int(11) DEFAULT '1',
  `status_silabus` int(11) DEFAULT '1',
  `status_bahan_ajar` int(11) DEFAULT '1',
  `status_acara_praktek` int(11) DEFAULT '0',
  `status_diktat` int(11) DEFAULT '1',
  `id_status` int(11) DEFAULT '1',
  `id_jenis` int(11) DEFAULT NULL,
  `jenis_mk` char(1) DEFAULT NULL,
  `kelompok_mk` varchar(30) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_matkul`
--

INSERT INTO `t_matkul` (`id`, `kode`, `nama`, `sks_tatap_muka`, `sks_praktek`, `sks_lapangan`, `sks_simulasi`, `semester`, `status_sap`, `status_silabus`, `status_bahan_ajar`, `status_acara_praktek`, `status_diktat`, `id_status`, `id_jenis`, `jenis_mk`, `kelompok_mk`, `tanggal_mulai`, `tanggal_selesai`, `id_prodi`) VALUES
(1, 'S101MPK2', 'Pendidikan Agama', 2, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(2, 'S102MPK2', 'Bahasa Indonesia', 2, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(3, 'S103MPK2', 'Pancasila', 2, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(4, 'S104MPK2', 'Pendidikan Kewarganegaraan', 2, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(5, 'S105MPK2', 'Bahasa Inggris', 2, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(6, 'S106MKK2', 'Biomedik I (Biologi, Kimia & Biokimia)', 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(7, 'S107MPK3', 'Sosio-Antropologi Kesehatan', 2, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(8, 'S108PHD2', 'Dasar Ilmu Kesehatan Masyarakat', 2, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(9, 'S109MKK2', 'Psikologi Kesehatan', 2, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(10, 'S110MKK2', 'Alislam dan Kemuhammadiyahan I', 2, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(11, 'S201MKK2', 'Biomedik  II (Anatomi, Fisologi & Patologi)', 1, 1, 0, 0, 2, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(12, 'S202MKK2', 'Biomedik  III (Parasitologi & Mikrobiologi)', 1, 1, 0, 0, 2, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(13, 'S203PHD2', 'Pendidikan Kesehatan Masyarakat', 2, 0, 0, 0, 2, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(14, 'S204PHE2', 'Dasar Epidemiologi', 2, 0, 0, 0, 2, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(15, 'S205PHE2', 'Pengantar Vektor dan Reservoir Penyakit', 1, 1, 0, 0, 2, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(16, 'S206PHB2', 'Dasar Kependudukan', 2, 0, 0, 0, 2, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(17, 'S207PHK2', 'Dasar Kesehatan Lingkungan', 2, 0, 0, 0, 2, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(18, 'S208PHK2', 'Dasar Keselamatan Dan Kesehatan Kerja ', 2, 0, 0, 0, 2, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(19, 'S209PHP2', 'Komunikasi Kesehatan', 2, 0, 0, 0, 2, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(20, 'S210MKK2', 'Alislam dan Kemuhammadiyahan II', 2, 0, 0, 0, 2, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(21, 'S301PHP2', 'Promosi Kesehatan', 2, 0, 0, 0, 3, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(22, 'S302PHB2', 'Biostatistik Deskriptif', 2, 0, 0, 0, 3, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(23, 'S303PHA2', 'Organisasi Manajemen Kesehatan', 2, 0, 0, 0, 3, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(24, 'S304PHA2', 'Farmakologi', 1, 1, 0, 0, 3, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(25, 'S305PHE2', 'Surveilans Kesehatan Masyarakat', 2, 0, 0, 0, 3, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(26, 'S306PHG3', 'Dasar Ilmu Gizi Kesehatan Masyarakat', 2, 1, 0, 0, 3, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(27, 'S307PHB2', 'Dasar Kespro Dan KIA', 2, 0, 0, 0, 3, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(28, 'S308MKK2', 'Aplikasi Komputer', 1, 1, 0, 0, 3, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(29, 'S309MKA2', 'Perencanaan Dan Evaluasi Kesehatan', 2, 0, 0, 0, 3, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(30, 'S310MKK2', 'Alislam dan Kemuhammadiyahan III', 2, 0, 0, 0, 3, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(31, 'S401PHA2', 'Administrasi & Kebijakan Kesehatan', 2, 0, 0, 0, 4, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(32, 'S402PHB3', 'Biostatistik Inferensial ', 2, 1, 0, 0, 4, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(33, 'S403PHA2', 'Manajemen SDM Kesehatan', 2, 0, 0, 0, 4, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(34, 'S404PHE2', 'Epidemiologi Peny Menular', 2, 0, 0, 0, 4, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(35, 'S405PHD2', 'Etika Dan Hukum Kesehatan', 2, 0, 0, 0, 4, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(36, 'S406PHD2', 'PBL Kesehatan Masyarakat I', 2, 0, 0, 0, 4, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(37, 'S407PHA3', 'Ekonomi Kesehatan ', 2, 1, 0, 0, 4, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(38, 'S408PHK2', 'Analisis Kualitas Lingkungan', 2, 0, 0, 0, 4, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(39, 'S601PHA2', 'Manajemen Mutu Kesehatan', 2, 0, 0, 0, 4, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(40, 'S501PHA2', 'Pembiayaan dan Penganggaran Kesehatan', 1, 1, 0, 0, 5, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(41, 'S502PHA2', 'Kepemimpinan  dan Berpikir Sistem Kesehatan Masyar', 2, 0, 0, 0, 5, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(42, 'S503PHE2', 'Manajemen Bencana', 2, 0, 0, 0, 5, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(43, 'S504PHD2', 'PBL Kesehatan Masyarakat II', 0, 0, 2, 0, 5, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(44, 'S505MKK2', 'Kewirausahaan', 1, 1, 0, 0, 5, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(45, 'S506PHB2', 'Manajemen Data', 1, 1, 0, 0, 5, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(46, 'S507PHP2', 'Dinamika Kelompok', 2, 0, 0, 0, 5, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(47, 'S508MKK2', 'Tafsir Ilmiah Ayat-Ayat Kesehatan', 2, 0, 0, 0, 5, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(48, 'S509PHE2', 'Epidemiologi Penyakit Tidak Menular', 2, 0, 0, 0, 5, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(49, 'S510PHA2', 'Teknologi Informasi Kesehatan', 1, 1, 0, 0, 5, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(50, 'S409MKK2', 'Alislam dan Kemuhammadiyahan IV', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(51, 'S602PHA2', 'Pengembangan dan Pengorganisasian Masyarakat', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(52, 'S603PHD2', 'Penulisan Ilmiah', 1, 1, 0, 0, 6, 0, 0, 1, 1, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(53, 'S604PHD2', 'PBL Kesehatan Masyarakat III', 0, 0, 2, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(54, 'S701PHD4', 'Metodologi Penelitian  Kualitatif dan Kuantitatif', 4, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(55, 'S801PHS4', 'Skripsi', 4, 0, 0, 0, 8, 0, 0, 1, 0, 0, 1, NULL, 'S', NULL, NULL, NULL, 50),
(56, 'S802PHN4', 'Kuliah Kerja Nyata ', 4, 0, 0, 0, 8, 0, 0, 1, 0, 0, 1, NULL, 'A', NULL, NULL, NULL, 50),
(57, 'S605AK12', 'Politik Kesehatan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(58, 'S606AK12', 'Analisis Kebijakan Kesehatan', 4, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(59, 'S607AK12', 'Asuransi Kesehatan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(60, 'S608AK12', 'Manajemen Pemasaran Yankes', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(61, 'S609AK12', 'Manajemen Starategi Kesehatan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(62, 'S702AK12', 'Administrasi Pembangunan Kesehatan', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(63, 'S703AK12', 'Perancanaan Dan Evaluasi Kesehatan', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(64, 'S704AK12', 'Seminar Administrasi Kebijakan Kesehatan', 1, 1, 0, 0, 7, 0, 0, 1, 1, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(65, 'S705AK14', 'Magang AKK', 0, 0, 4, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(66, 'S605BK22', 'Statistik Non Parametrik', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(67, 'S606BK22', 'Analisis Dan Evaluasi Data KB', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(68, 'S607BK22', 'Analisis Kependudukan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(69, 'S608BK22', 'Kesehatan Reproduksi Remaja', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(70, 'S609BK22', 'Metode Kontrasepsi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(71, 'S702BK22', 'Program KKB', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(72, 'S703BK22', 'Rapid Survei', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(73, 'S704BK22', 'Kesehatan Reproduksi Usia Lanjut', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(74, 'S705BK22', 'Magang Biostatistik / KKB', 0, 0, 4, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(75, 'S605EP32', 'Epidemiologi Kesehatan Reproduksi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(76, 'S606EP32', 'Epidemiologi Kesehatan Lingkungan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(77, 'S607EP32', 'Program P2M', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(78, 'S608EP32', 'Epidemiologi Perilaku', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(79, 'S609EP32', 'Epidemiologi Kesehatan Kerja', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(80, 'S702EP32', 'Current Issues Epidemiologi', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(81, 'S703EP32', 'Epidemiologi Perencanaan Kesehatan', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(82, 'S704EP32', 'Praktik Surveilans', 1, 1, 0, 0, 7, 0, 0, 1, 1, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(83, 'S705EP34', 'Magang Epidemiologi', 0, 0, 4, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(84, 'S605GZ42', 'Biokimia Gizi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(85, 'S606GZ42', 'Immunologi Dan Zat Gizi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(86, 'S607GZ42', 'Penentuan Status Gizi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(87, 'S608GZ42', 'Praktikum Gizi Masyarakat', 1, 1, 0, 0, 6, 0, 0, 1, 1, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(88, 'S609GZ42', 'Aspek Sosial Pangan Dan Gizi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(89, 'S702GZ42', 'Kapita Selekta Gizi', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(90, 'S703GZ42', 'Analisis Kebijakan Pangan dan Gizi', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(91, 'S704GZ42', 'Ekologi Pangan Dan Gizi', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(92, 'S705GZ44', 'Magang Gizi', 0, 0, 4, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(93, 'S605KK54', 'Amdal Kesmas', 2, 2, 0, 0, 6, 0, 0, 1, 1, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(94, 'S606KK52', 'Pencemaran Lingkungan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(95, 'S607KK52', 'Kimia Lingkungan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(96, 'S608KK52', 'Pengendalian Vektor', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(97, 'S702KK52', 'Pengelolaan Limbah Padat, Cair Dan Gas', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(98, 'S703KK52', 'Manajemen Penyehatan Makanan dan Minuman', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(99, 'S704KK52', 'Praktikum KL-KK', 1, 1, 0, 0, 7, 0, 0, 1, 1, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(100, 'S705KK54', 'Magang KL-KK', 0, 0, 4, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(101, 'S605PK62', 'Pendidikan dan Latihan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(102, 'S606PK62', 'Advokasi Dan Negosiasi Kesehatan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(103, 'S607PK62', 'Susbud Kes dan Ilmu Perilaku', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(104, 'S608PK62', 'Strategi Promosi Kesehatan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(105, 'S609PK62', 'Teknologi Pengembangan Media', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(106, 'S702PK62', 'Current Issues Promkes', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(107, 'S703PK62', 'KIE Kesehatan', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(108, 'S704PK62', 'Perencanaan Dan Evaluasi Program Promkes Di Instit', 2, 0, 0, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(109, 'S705PK64', 'Magang PKIP', 0, 0, 4, 0, 7, 0, 0, 1, 0, 0, 1, NULL, 'C', NULL, NULL, NULL, 50),
(110, 'SP01EP32', 'Epidemiologi Perilaku', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(111, 'SP02EP32', 'Psikologi Industri', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(112, 'SP03EP32', 'Penulisan Kreatif', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(113, 'SP01AK12', 'Ekonomi Makro dan Mikro', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(114, 'SP02AK12', 'Perilaku Konseling Yankes', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(115, 'SP03AK12', 'Metode Penelitian AKK', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(116, 'SP04AK12', 'STLO', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(117, 'SP01GZ42', 'Penilaian pertumbuhan dan perkembangan Anak ', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(118, 'SP02GZ42', 'Pemasaran Sosioal Pengan Dan Gizi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(119, 'SP03GZ42', 'Gizi Ibu Dan Anak', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(120, 'SP04GZ42', 'Sistem Informasi Pangan dan Gizi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(121, 'SP01KK52', 'Taksikologi Industri', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(122, 'SP02KK52', 'Ergonomi dan Faal Kerja', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(123, 'SP03KK52', 'Kesling Kawasan Pantai dan Pesisir', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(124, 'SP04KK52', 'Epidemiologi Kesehatan Lingkungan', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(125, 'SP01BK22', 'Metodologi', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(126, 'SP02BK22', 'Rekam Medik', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(127, 'SP03BK22', 'Rancangan Sampel', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50),
(128, 'SP04BK22', 'Sistem Informasi Manajemen', 2, 0, 0, 0, 6, 0, 0, 1, 0, 0, 1, NULL, 'B', NULL, NULL, NULL, 50);

-- --------------------------------------------------------

--
-- Table structure for table `t_matkul_jurusan`
--

CREATE TABLE `t_matkul_jurusan` (
  `id` int(11) NOT NULL,
  `id_matkul` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pangkat_dosen`
--

CREATE TABLE `t_pangkat_dosen` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_pns` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pangkat_dosen`
--

INSERT INTO `t_pangkat_dosen` (`id`, `kode`, `nama`, `id_pns`) VALUES
(1, 'IV/d', 'Pembina Utama Madya', 1),
(2, 'IV/c', 'Pembina Utama Muda', 1),
(3, 'IV/b', 'Pembina Tk. I', 1),
(4, 'IV/a', 'Pembina', 1),
(5, 'III/d', 'Penata Tk.I', 1),
(6, 'III/c', 'Penata', 1),
(7, 'III/b', 'Penata Muda Tk.I', 1),
(8, 'III/a', 'Penata Muda', 1),
(9, '', 'Dosen Tetap Bukan PNS', 0),
(10, 'II/d', 'Pengatur Tk.I', 1),
(11, 'II/c', 'Pengatur', 1),
(12, 'II/b', 'Pengatur Muda Tk.I', 1),
(13, 'II/a', 'Pengatur Muda', 1),
(14, 'I/d', 'Juru Tk.I', 1),
(15, 'I/c', 'Juru', 1),
(16, 'I/b', 'Juru Muda Tk.I', 1),
(17, 'I/a', 'Juru Muda', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_pegawai`
--

CREATE TABLE `t_pegawai` (
  `id` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `gelar` varchar(20) NOT NULL,
  `no_ktp` varchar(20) NOT NULL,
  `npwp` varchar(20) NOT NULL,
  `agama` int(11) DEFAULT NULL,
  `jalan` varchar(100) NOT NULL,
  `rt` varchar(5) NOT NULL,
  `rw` varchar(5) NOT NULL,
  `kabupaten` int(11) DEFAULT NULL,
  `kecamatan` varchar(40) NOT NULL,
  `kode_pos` varchar(10) NOT NULL,
  `kewarganegaraan` varchar(3) DEFAULT NULL,
  `no_hp` varchar(15) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `status_perkawinan` int(11) DEFAULT NULL,
  `nama_pasangan` varchar(50) NOT NULL,
  `pekerjaan_pasangan` int(11) DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `pendidikan` varchar(3) DEFAULT NULL,
  `kampus` varchar(40) DEFAULT NULL,
  `tahun_lulus` varchar(5) DEFAULT NULL,
  `tempat_lahir` varchar(20) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `foto` varchar(100) NOT NULL DEFAULT 'assets\\images\\profiles\\default.jpg',
  `ttd` varchar(150) NOT NULL,
  `id_akun` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pegawai`
--

INSERT INTO `t_pegawai` (`id`, `nip`, `nama`, `gelar`, `no_ktp`, `npwp`, `agama`, `jalan`, `rt`, `rw`, `kabupaten`, `kecamatan`, `kode_pos`, `kewarganegaraan`, `no_hp`, `nama_ibu`, `status_perkawinan`, `nama_pasangan`, `pekerjaan_pasangan`, `jenis_kelamin`, `pendidikan`, `kampus`, `tahun_lulus`, `tempat_lahir`, `tanggal_lahir`, `email`, `no_telp`, `foto`, `ttd`, `id_akun`, `id_status`) VALUES
(397, '000000000', 'Adminku', '', '', '', NULL, '', '', '', NULL, '', '', NULL, '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'assets\\images\\profiles\\default.jpg', '', 1011, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_pekerjaan`
--

CREATE TABLE `t_pekerjaan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pekerjaan`
--

INSERT INTO `t_pekerjaan` (`id`, `nama`) VALUES
(1, 'Tidak bekerja'),
(2, 'Nelayan'),
(3, 'Petani'),
(4, 'Peternak'),
(5, 'PNS/TNI/Polri'),
(6, 'Karyawan Swasta'),
(7, 'Pedagang Kecil'),
(8, 'Pedagang Besar'),
(9, 'Wiraswasta'),
(10, 'Wirausaha'),
(11, 'Buruh'),
(12, 'Pensiunan'),
(98, 'Sudah Meninggal'),
(99, 'Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `t_pembayaran`
--

CREATE TABLE `t_pembayaran` (
  `nim` char(16) NOT NULL,
  `nama_mhs` varchar(200) NOT NULL,
  `nama_thn_ak` int(200) NOT NULL,
  `jk` varchar(2) NOT NULL,
  `nama_fakultas` varchar(150) NOT NULL,
  `nama_prodi` varchar(150) NOT NULL,
  `jenis_pembayaran` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `ket` varchar(200) NOT NULL,
  `status_aktif` int(25) NOT NULL,
  `bebas_masalah` int(2) NOT NULL,
  `status_bayar` int(2) NOT NULL,
  `no_ref_bank` varchar(250) NOT NULL,
  `channel` varchar(250) NOT NULL,
  `tanggal_bayar` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `virtual_account` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pendidikan`
--

CREATE TABLE `t_pendidikan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pendidikan`
--

INSERT INTO `t_pendidikan` (`id`, `nama`) VALUES
(1, 'Tidak Sekolah'),
(2, 'Paud'),
(3, 'TK / Sederajat'),
(4, 'Putus SD'),
(5, 'SD / Sederajat'),
(6, 'SMP / Sederajat'),
(7, 'SMA / Sederajat'),
(8, 'Paket A'),
(9, 'Paket B'),
(10, 'Paket C'),
(20, 'D1'),
(21, 'D2'),
(22, 'D3'),
(23, 'D4'),
(25, 'Profesi'),
(30, 'S1'),
(32, 'Sp-1'),
(35, 'S2'),
(37, 'Sp-2'),
(40, 'S3'),
(90, 'Non formal'),
(91, 'Informal'),
(99, 'Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `t_pendidikan_pegawai`
--

CREATE TABLE `t_pendidikan_pegawai` (
  `id` varchar(10) NOT NULL,
  `no` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pendidikan_pegawai`
--

INSERT INTO `t_pendidikan_pegawai` (`id`, `no`, `nama`) VALUES
('AKADEMIK', 11, ''),
('D1', 9, ''),
('D2', 8, ''),
('D3', 7, ''),
('D4', 6, ''),
('PROFESI', 10, ''),
('S1', 3, ''),
('S2', 2, ''),
('S3', 1, ''),
('SP-1', 4, ''),
('SP-2', 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_pengajuan_bulanan_lkd`
--

CREATE TABLE `t_pengajuan_bulanan_lkd` (
  `id` int(11) NOT NULL,
  `kode_bulan` varchar(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `status_pengajuan` int(11) DEFAULT '-1',
  `waktu_pengajuan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pengajuan_lkd`
--

CREATE TABLE `t_pengajuan_lkd` (
  `id` int(11) NOT NULL,
  `waktu_pengajuan` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_periode` int(11) DEFAULT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `status_pengajuan` int(11) DEFAULT '-1',
  `total` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pengajuan_lkd`
--

INSERT INTO `t_pengajuan_lkd` (`id`, `waktu_pengajuan`, `id_periode`, `id_dosen`, `status_pengajuan`, `total`, `created_at`, `updated_at`) VALUES
(1, '0000-00-00 00:00:00', 1, NULL, -1, 0, '2018-09-22 13:56:47', '2018-09-22 13:56:47'),
(2, '0000-00-00 00:00:00', 2, NULL, -1, 0, '2018-09-22 13:57:06', '2018-09-22 13:57:06'),
(3, '0000-00-00 00:00:00', 3, NULL, -1, 0, '2018-09-22 13:57:42', '2018-09-22 13:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `t_penghasilan`
--

CREATE TABLE `t_penghasilan` (
  `id` int(11) NOT NULL,
  `jumlah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_penghasilan`
--

INSERT INTO `t_penghasilan` (`id`, `jumlah`) VALUES
(11, 'Kurang dari Rp. 500,000 '),
(12, 'Rp. 500,000 - Rp. 999,999 '),
(13, 'Rp. 1,000,000 - Rp. 1,999,999 '),
(14, 'Rp. 2,000,000 - Rp. 4,999,999 '),
(15, 'Rp. 5,000,000 - Rp. 20,000,000 '),
(16, 'Lebih dari Rp. 20,000,000');

-- --------------------------------------------------------

--
-- Table structure for table `t_pengumuman`
--

CREATE TABLE `t_pengumuman` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` varchar(1000) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_tujuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_penilaian`
--

CREATE TABLE `t_penilaian` (
  `id` int(11) NOT NULL,
  `nilai_akhir` float NOT NULL,
  `huruf_indeks` varchar(3) NOT NULL,
  `nilai_indeks` float NOT NULL,
  `kode_matkul` varchar(15) NOT NULL,
  `nama_matkul` varchar(40) NOT NULL,
  `nim_mhs` varchar(15) NOT NULL,
  `kelas` varchar(15) NOT NULL,
  `id_pilih_matkul` int(11) DEFAULT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  `kode_semester` varchar(11) NOT NULL,
  `kode_prodi` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_periode_lkd`
--

CREATE TABLE `t_periode_lkd` (
  `id` int(11) NOT NULL,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_periode_lkd`
--

INSERT INTO `t_periode_lkd` (`id`, `tanggal_awal`, `tanggal_akhir`) VALUES
(1, '2018-08-27', '2018-09-02'),
(2, '2018-09-03', '2018-09-09'),
(3, '2018-09-17', '2018-09-23');

-- --------------------------------------------------------

--
-- Table structure for table `t_pertemuan`
--

CREATE TABLE `t_pertemuan` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jam_mulai` varchar(6) NOT NULL,
  `jam_selesai` varchar(6) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `keterangan` varchar(500) NOT NULL,
  `date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `id_status` int(11) NOT NULL DEFAULT '0',
  `id_jenis` int(11) NOT NULL DEFAULT '1',
  `id_ruangan` int(11) DEFAULT NULL,
  `id_kelas_matkul` int(11) NOT NULL,
  `id_jadwal_kelas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pilihan_prodi`
--

CREATE TABLE `t_pilihan_prodi` (
  `id` int(11) NOT NULL,
  `id_camaba` int(11) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  `no_pilihan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_pilih_matkul_mhs`
--

CREATE TABLE `t_pilih_matkul_mhs` (
  `id` int(11) NOT NULL,
  `id_kelas_matkul` int(11) NOT NULL,
  `id_krs_mhs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_plot_matkul`
--

CREATE TABLE `t_plot_matkul` (
  `id` int(11) NOT NULL,
  `id_jurusan` int(11) DEFAULT NULL,
  `id_tingkat` int(11) DEFAULT NULL,
  `id_matkul` int(11) DEFAULT NULL,
  `kode` varchar(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `sks` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_semester` int(11) DEFAULT NULL,
  `id_dosen_koor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_prestasi`
--

CREATE TABLE `t_prestasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(90) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `id_tingkat_prestasi` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_provinsi`
--

CREATE TABLE `t_provinsi` (
  `id` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_provinsi`
--

INSERT INTO `t_provinsi` (`id`, `nama`) VALUES
('11', 'ACEH'),
('12', 'SUMATERA UTARA'),
('13', 'SUMATERA BARAT'),
('14', 'RIAU'),
('15', 'JAMBI'),
('16', 'SUMATERA SELATAN'),
('17', 'BENGKULU'),
('18', 'LAMPUNG'),
('19', 'KEPULAUAN BANGKA BELITUNG'),
('21', 'KEPULAUAN RIAU'),
('31', 'DKI JAKARTA'),
('32', 'JAWA BARAT'),
('33', 'JAWA TENGAH'),
('34', 'DI YOGYAKARTA'),
('35', 'JAWA TIMUR'),
('36', 'BANTEN'),
('51', 'BALI'),
('52', 'NUSA TENGGARA BARAT'),
('53', 'NUSA TENGGARA TIMUR'),
('61', 'KALIMANTAN BARAT'),
('62', 'KALIMANTAN TENGAH'),
('63', 'KALIMANTAN SELATAN'),
('64', 'KALIMANTAN TIMUR'),
('65', 'KALIMANTAN UTARA'),
('71', 'SULAWESI UTARA'),
('72', 'SULAWESI TENGAH'),
('73', 'SULAWESI SELATAN'),
('74', 'SULAWESI TENGGARA'),
('75', 'GORONTALO'),
('76', 'SULAWESI BARAT'),
('81', 'MALUKU'),
('82', 'MALUKU UTARA'),
('91', 'PAPUA BARAT'),
('94', 'PAPUA');

-- --------------------------------------------------------

--
-- Table structure for table `t_rektor`
--

CREATE TABLE `t_rektor` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_rektor`
--

INSERT INTO `t_rektor` (`id`, `id_pegawai`, `id_status`) VALUES
(1, 397, 1),
(2, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_riwayat_login`
--

CREATE TABLE `t_riwayat_login` (
  `id` int(11) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(20) NOT NULL,
  `browser` varchar(200) NOT NULL,
  `id_status` int(11) DEFAULT NULL,
  `id_akun` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_riwayat_login`
--

INSERT INTO `t_riwayat_login` (`id`, `waktu`, `ip_address`, `browser`, `id_status`, `id_akun`) VALUES
(1, '2018-08-13 05:07:45', '36.79.195.167', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 0, 1011),
(2, '2018-08-13 05:07:47', '36.79.195.167', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(3, '2018-08-13 05:08:08', '36.79.195.167', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(4, '2018-08-16 09:57:34', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(5, '2018-08-16 10:08:22', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(6, '2018-08-16 10:08:41', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(7, '2018-08-16 10:09:15', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(8, '2018-08-19 02:46:49', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(9, '2018-08-19 02:46:50', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(10, '2018-08-19 02:47:00', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(11, '2018-08-19 02:47:00', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(12, '2018-08-19 03:02:02', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(13, '2018-08-21 02:09:56', '36.71.232.109', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(14, '2018-08-25 12:14:47', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(15, '2018-08-25 12:14:57', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(16, '2018-08-25 12:26:29', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(17, '2018-08-25 12:26:41', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(18, '2018-08-25 13:12:58', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(19, '2018-08-25 14:02:53', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(20, '2018-08-26 00:41:32', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(21, '2018-08-26 00:58:45', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(22, '2018-08-26 15:39:51', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(23, '2018-08-26 16:37:40', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(24, '2018-08-26 16:37:55', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(25, '2018-08-26 16:38:06', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(26, '2018-08-26 16:38:32', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(27, '2018-08-26 16:38:49', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(28, '2018-08-26 16:44:00', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(29, '2018-08-26 16:52:40', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(30, '2018-08-26 16:53:22', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(31, '2018-08-26 17:10:07', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(32, '2018-08-26 17:10:13', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(33, '2018-08-26 17:15:33', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(34, '2018-08-26 17:16:04', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(35, '2018-08-26 17:18:40', '125.161.82.236', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(36, '2018-08-27 13:47:05', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(37, '2018-08-28 03:11:40', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(38, '2018-08-28 03:58:14', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(39, '2018-08-28 03:58:44', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(40, '2018-08-28 04:09:03', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(41, '2018-08-28 04:21:12', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(42, '2018-08-28 04:21:25', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 0, NULL),
(43, '2018-08-28 04:21:28', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 0, NULL),
(44, '2018-08-28 04:21:30', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(45, '2018-08-28 04:22:27', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(46, '2018-08-28 04:22:58', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(47, '2018-08-28 04:23:28', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(48, '2018-08-28 04:29:18', '36.79.174.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(49, '2018-09-01 04:28:42', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(50, '2018-09-01 04:29:23', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(51, '2018-09-01 16:01:14', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(52, '2018-09-01 16:01:47', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(53, '2018-09-01 16:02:57', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(54, '2018-09-02 03:51:01', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(55, '2018-09-02 03:56:00', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(56, '2018-09-02 05:45:48', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(57, '2018-09-02 05:47:50', '36.71.234.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(58, '2018-09-03 12:32:44', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(59, '2018-09-03 12:32:51', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(60, '2018-09-03 12:33:44', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(61, '2018-09-03 14:24:49', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(62, '2018-09-03 15:04:05', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(63, '2018-09-03 16:09:21', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(64, '2018-09-03 16:15:00', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(65, '2018-09-03 16:16:05', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(66, '2018-09-04 02:21:11', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(67, '2018-09-04 02:22:26', '118.96.243.127', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(68, '2018-09-05 14:26:10', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(69, '2018-09-08 03:15:07', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(70, '2018-09-08 03:15:18', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(71, '2018-09-08 03:37:58', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(72, '2018-09-08 07:35:53', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(73, '2018-09-08 07:35:55', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(74, '2018-09-08 07:35:55', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(75, '2018-09-08 07:35:55', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(76, '2018-09-08 07:35:56', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(77, '2018-09-08 13:06:56', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(78, '2018-09-08 13:55:09', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(79, '2018-09-08 17:37:11', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(80, '2018-09-08 23:56:06', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(81, '2018-09-09 04:01:46', '118.96.211.63', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(82, '2018-09-09 10:47:07', '36.71.232.82', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(83, '2018-09-09 13:33:22', '36.71.232.82', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(84, '2018-09-09 22:59:56', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(85, '2018-09-15 01:52:35', '36.71.234.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(86, '2018-09-16 07:29:02', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 0, NULL),
(87, '2018-09-16 07:29:05', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(88, '2018-09-16 09:15:22', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(89, '2018-09-16 10:03:03', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(90, '2018-09-16 10:09:17', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(91, '2018-09-16 10:09:18', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(92, '2018-09-16 10:17:16', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(93, '2018-09-16 10:25:47', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(94, '2018-09-16 10:28:14', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(95, '2018-09-16 10:29:54', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(96, '2018-09-16 10:33:58', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(97, '2018-09-16 10:36:51', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(98, '2018-09-16 10:37:56', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(99, '2018-09-16 10:41:57', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 0, 1011),
(100, '2018-09-16 10:42:00', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(101, '2018-09-16 10:42:35', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(102, '2018-09-16 10:42:55', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, 1011),
(103, '2018-09-16 10:44:07', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(104, '2018-09-16 23:13:14', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(105, '2018-09-16 23:13:14', '36.71.231.169', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', 1, NULL),
(106, '2018-09-22 10:18:13', '36.72.203.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(107, '2018-09-22 10:18:14', '36.72.203.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(108, '2018-09-22 10:18:15', '36.72.203.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(109, '2018-09-22 10:18:25', '36.72.203.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(110, '2018-09-22 13:55:41', '36.72.203.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(111, '2018-09-23 01:30:25', '36.72.203.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(112, '2018-09-23 01:34:09', '36.72.203.129', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(113, '2018-09-23 13:56:01', '36.79.199.68', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(114, '2018-09-23 13:56:02', '36.79.199.68', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(115, '2018-09-24 00:10:03', '36.79.199.68', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(116, '2018-09-24 10:43:50', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(117, '2018-09-24 10:47:44', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 0, NULL),
(118, '2018-09-24 10:47:46', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(119, '2018-09-29 11:23:49', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(120, '2018-09-29 11:27:18', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(121, '2018-09-29 11:29:12', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(122, '2018-09-29 11:29:13', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(123, '2018-09-29 11:29:14', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(124, '2018-09-29 11:52:19', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(125, '2018-09-29 12:36:08', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(126, '2018-09-29 12:36:49', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(127, '2018-09-29 12:36:49', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(128, '2018-09-29 14:15:53', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(129, '2018-09-29 14:17:45', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(130, '2018-09-30 01:18:11', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(131, '2018-09-30 10:09:05', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(132, '2018-09-30 10:09:09', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(133, '2018-09-30 13:35:47', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(134, '2018-09-30 13:36:04', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(135, '2018-09-30 13:37:14', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(136, '2018-09-30 13:37:38', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(137, '2018-09-30 15:17:01', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(138, '2018-09-30 15:21:32', '36.72.14.71', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(139, '2018-10-26 16:19:35', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(140, '2018-10-26 16:35:09', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(141, '2018-10-26 16:35:29', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(142, '2018-10-26 16:36:11', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(143, '2018-10-26 18:45:54', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(144, '2018-10-27 05:12:33', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(145, '2018-10-27 07:00:38', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(146, '2018-10-27 09:54:01', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(147, '2018-10-27 14:16:12', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(148, '2018-10-29 13:47:10', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(149, '2018-10-29 13:47:20', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(150, '2018-10-29 13:47:31', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(151, '2018-10-29 13:48:04', '180.245.191.12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, 1011),
(152, '2018-10-29 13:53:19', 'undefined', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1, NULL),
(153, '2018-11-30 16:27:56', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 0, 1011),
(154, '2018-11-30 16:28:02', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 0, 1011),
(155, '2018-11-30 16:28:07', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 0, 1011),
(156, '2018-11-30 16:28:26', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(157, '2018-11-30 16:30:36', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 0, 1011),
(158, '2018-11-30 16:30:42', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(159, '2018-11-30 16:31:03', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(160, '2018-11-30 16:31:09', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(161, '2018-11-30 16:31:14', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 0, 1011),
(162, '2018-11-30 16:32:02', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 0, 1011),
(163, '2018-11-30 16:32:08', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(164, '2018-11-30 16:33:35', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(165, '2018-11-30 16:33:46', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(166, '2018-11-30 16:34:46', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(167, '2018-11-30 16:35:21', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(168, '2018-11-30 16:35:28', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(169, '2018-11-30 16:35:36', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, NULL),
(170, '2018-11-30 17:31:58', '36.79.251.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(171, '2018-12-01 14:05:29', '36.69.193.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(172, '2018-12-01 14:20:16', '36.69.193.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, NULL),
(173, '2018-12-03 14:06:54', 'undefined', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(174, '2018-12-03 14:24:35', '36.69.193.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(175, '2018-12-03 14:26:22', '36.69.193.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(176, '2018-12-03 14:30:14', '36.69.193.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(177, '2018-12-03 15:52:41', '36.69.193.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(178, '2018-12-04 08:31:05', '36.69.193.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(179, '2018-12-05 11:03:10', '36.65.249.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(180, '2018-12-05 11:46:44', '36.65.249.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(181, '2018-12-05 11:48:44', '36.65.249.183', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(182, '2018-12-08 14:08:06', '125.163.19.25', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(183, '2018-12-15 15:32:05', '125.163.6.167', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36', 1, 1011),
(184, '2018-12-19 01:38:30', '36.85.232.173', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(185, '2018-12-19 09:22:25', '36.79.249.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(186, '2018-12-19 09:23:25', '36.79.249.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(187, '2018-12-19 09:27:42', '36.79.249.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(188, '2018-12-19 14:14:47', '36.79.249.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(189, '2018-12-19 16:12:35', '36.79.249.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(190, '2018-12-19 16:16:08', '36.79.249.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(191, '2018-12-20 06:19:18', 'undefined', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(192, '2018-12-20 09:01:04', '36.79.249.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(193, '2018-12-20 15:37:06', '36.79.249.164', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(194, '2018-12-21 05:40:37', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(195, '2018-12-24 12:47:56', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(196, '2018-12-26 06:21:19', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(197, '2018-12-26 12:06:12', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(198, '2018-12-26 12:18:57', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(199, '2018-12-26 12:22:25', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(200, '2018-12-26 13:04:05', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(201, '2018-12-26 13:25:17', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(202, '2018-12-26 13:26:41', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(203, '2018-12-26 14:40:33', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(204, '2018-12-26 15:11:14', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(205, '2018-12-27 03:56:06', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(206, '2018-12-27 05:12:42', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(207, '2018-12-27 05:13:14', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(208, '2018-12-27 05:13:41', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(209, '2018-12-27 05:14:07', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(210, '2018-12-27 05:18:49', '36.79.250.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(211, '2018-12-29 10:58:08', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(212, '2018-12-29 15:48:04', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(213, '2018-12-30 12:59:18', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(214, '2018-12-30 14:12:38', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(215, '2018-12-30 14:28:19', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(216, '2018-12-30 14:34:00', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(217, '2018-12-30 14:43:17', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(218, '2018-12-30 15:00:54', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(219, '2018-12-30 15:02:52', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(220, '2018-12-30 15:04:43', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(221, '2018-12-30 15:20:15', '36.80.98.203', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(222, '2019-01-01 12:30:15', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(223, '2019-01-01 15:47:02', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(224, '2019-01-01 15:49:13', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(225, '2019-01-03 14:48:18', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(226, '2019-01-03 17:25:38', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(227, '2019-01-03 17:29:31', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(228, '2019-01-03 17:34:10', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(229, '2019-01-03 17:36:20', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(230, '2019-01-03 17:52:14', '36.72.195.61', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(231, '2019-01-10 10:39:15', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(232, '2019-01-11 10:00:06', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(233, '2019-01-11 12:23:49', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(234, '2019-01-11 12:27:12', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(235, '2019-01-11 12:27:35', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(236, '2019-01-11 12:49:22', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(237, '2019-01-11 12:50:08', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(238, '2019-01-11 12:50:17', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(239, '2019-01-11 12:51:32', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(240, '2019-01-11 12:52:59', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(241, '2019-01-11 12:56:35', '180.245.176.6', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(242, '2019-01-20 16:04:18', '36.79.168.205', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(243, '2019-01-20 16:05:44', '36.79.168.205', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(244, '2019-01-21 05:59:52', '36.79.168.205', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(245, '2019-01-21 06:03:19', '36.79.168.205', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(246, '2019-01-21 06:44:51', '36.79.168.205', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 1, 1011),
(247, '2019-03-21 11:39:00', '36.69.195.144', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1, 1011),
(248, '2019-03-21 11:56:22', '36.69.195.144', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1, 1011),
(249, '2019-03-21 12:28:07', '36.69.195.144', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1, 1011),
(250, '2019-03-21 13:10:44', '36.69.195.144', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1, 1011),
(251, '2019-03-21 13:24:32', '36.69.195.144', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1, 1011),
(252, '2019-03-21 13:28:38', '36.69.195.144', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 1, 1011),
(253, '2019-03-27 07:58:27', '36.79.163.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, 1011),
(254, '2019-03-27 08:00:47', '36.79.163.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, 1011),
(255, '2019-03-27 08:01:04', '36.79.163.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, 1011),
(256, '2019-03-27 08:02:41', '36.79.163.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, 1011),
(257, '2019-03-27 08:11:08', '36.79.163.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, 1011),
(258, '2019-03-28 08:46:28', '36.79.163.22', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, 1011),
(259, '2019-03-28 10:46:55', '36.72.11.151', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 1, 1011);

-- --------------------------------------------------------

--
-- Table structure for table `t_role_admin`
--

CREATE TABLE `t_role_admin` (
  `id_admin` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_role_admin`
--

INSERT INTO `t_role_admin` (`id_admin`, `id_role`, `id_status`) VALUES
(7, 6, 0),
(7, 1, 1),
(7, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_role_dekan`
--

CREATE TABLE `t_role_dekan` (
  `id` int(11) NOT NULL,
  `id_dekan` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_fakultas` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_role_dekan`
--

INSERT INTO `t_role_dekan` (`id`, `id_dekan`, `id_role`, `id_fakultas`, `id_status`) VALUES
(1, 1, 1, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_role_jurusan`
--

CREATE TABLE `t_role_jurusan` (
  `id` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_role_kaprodi`
--

CREATE TABLE `t_role_kaprodi` (
  `id` int(11) NOT NULL,
  `id_kajur` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_role_kaprodi`
--

INSERT INTO `t_role_kaprodi` (`id`, `id_kajur`, `id_role`, `id_jurusan`, `id_status`) VALUES
(1, 1, 1, 50, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_role_rektor`
--

CREATE TABLE `t_role_rektor` (
  `id` int(11) NOT NULL,
  `id_rektor` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_role_rektor`
--

INSERT INTO `t_role_rektor` (`id`, `id_rektor`, `id_role`, `id_status`) VALUES
(1, 1, 1, 1),
(10, 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_ruangan`
--

CREATE TABLE `t_ruangan` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `id_ruangan` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `id_gedung` int(11) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ruangan`
--

INSERT INTO `t_ruangan` (`id`, `kode`, `id_ruangan`, `nama`, `kapasitas`, `id_gedung`, `id_status`) VALUES
(1, 'GKU3.01.01', 'RG222', 'ruangan 1 lt 1', 30, 1, 1),
(2, 'GKU3.01.02', 'RG232', 'ruangan 2 lt 1', 30, 1, 1),
(3, 'GKU2.01.01', 'RG211', 'ruangan 1 lt 1', 39, 3, 1),
(4, 'GKU1.01.02', 'RG123', 'ruangan 2 lt 1', 30, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_sekolah`
--

CREATE TABLE `t_sekolah` (
  `id` int(11) NOT NULL,
  `npsn` varchar(15) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `kode_pos` varchar(10) NOT NULL,
  `kontak` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `file_akreditasi` varchar(100) NOT NULL,
  `link_perbaikan` varchar(100) DEFAULT NULL,
  `id_akun` int(11) DEFAULT NULL,
  `id_tipe` int(11) NOT NULL,
  `id_akreditasi` int(11) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_sekolah`
--

INSERT INTO `t_sekolah` (`id`, `npsn`, `nama`, `alamat`, `kode_pos`, `kontak`, `email`, `file_akreditasi`, `link_perbaikan`, `id_akun`, `id_tipe`, `id_akreditasi`, `id_kabupaten`, `id_status`, `created_at`, `updated_at`) VALUES
(1, '123456', '3 Banjarmasin', 'Jalan Pahlawan no 3, Kel. ABC, Kec. DEF', '22121', '21212112', 'ahmadbaso97@gmail.com', '0', NULL, NULL, 1, 1, 6371, 0, '2018-03-26 13:56:22', '2018-03-26 13:56:22'),
(9, '43434343', '2 Banjarmasin', 'jalan a', '12', '22', 'baso@4nesia.com', '', NULL, NULL, 3, 1, 5271, 0, '2018-03-26 14:03:45', '2018-03-26 14:03:45'),
(10, '03932223', '1 Banjarmasin', 'abcdef', '212112', '212112', 'ahmadbaso97@gmail.com', '', NULL, NULL, 1, 1, 6371, 0, '2018-03-26 14:09:57', '2018-03-26 14:09:57'),
(12, '333233', '1 Bandung', 'abc', '12', '2', 'dev@4nesia.com', '', NULL, NULL, 1, 1, 3273, 0, '2018-03-26 14:11:21', '2018-03-26 14:11:21'),
(13, '1234', '3 Sengkang', 'jalan cendana', '1234', '12322', 'ahmadbaso97@gmail.com', 'assets/pdf/schools/PMB_UJIAN_MASUK_MANDIRI.pdf', NULL, NULL, 1, 2, 7313, 0, '2018-03-26 23:56:27', '2018-03-26 23:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `t_semester`
--

CREATE TABLE `t_semester` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_semester`
--

INSERT INTO `t_semester` (`id`, `nama`) VALUES
(1, 'GANJIL'),
(2, 'GENAP'),
(3, 'PENDEK');

-- --------------------------------------------------------

--
-- Table structure for table `t_semester_ajaran`
--

CREATE TABLE `t_semester_ajaran` (
  `id` int(11) NOT NULL,
  `kode` varchar(6) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `id_semester` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `id_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_semester_ajaran`
--

INSERT INTO `t_semester_ajaran` (`id`, `kode`, `tanggal_mulai`, `tanggal_selesai`, `id_semester`, `id_tahun`, `id_status`) VALUES
(1, '20151', '0000-00-00', '0000-00-00', 1, 1, 0),
(2, '20161', '0000-00-00', '0000-00-00', 1, 2, 0),
(7, '20171', '0000-00-00', '0000-00-00', 1, 3, 0),
(8, '20152', '0000-00-00', '0000-00-00', 2, 1, 0),
(9, '20181', '2018-09-03', '2018-12-21', 1, 4, 1),
(10, '20182', '1970-01-01', '2018-08-25', 2, 4, 0),
(11, '20172', '0000-00-00', '0000-00-00', 2, 3, 0),
(12, '20111', '0000-00-00', '0000-00-00', 1, 5, 0),
(13, '20112', '0000-00-00', '0000-00-00', 2, 5, 0),
(14, '20121', '2018-08-01', '2018-12-31', 1, 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_status_absensi`
--

CREATE TABLE `t_status_absensi` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_absensi`
--

INSERT INTO `t_status_absensi` (`id`, `nama`) VALUES
(1, 'Hadir'),
(2, 'Alpha'),
(3, 'Sakit'),
(4, 'Izin'),
(5, 'Dispensasi');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_camaba`
--

CREATE TABLE `t_status_camaba` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_camaba`
--

INSERT INTO `t_status_camaba` (`id`, `nama`) VALUES
(0, 'BELUM MENGAJUKAN'),
(1, 'MENUNGGU PEMBAYARAN'),
(2, 'MENUNGGU VERIFIKASI'),
(3, 'TELAH DIVERIFIKASI');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_dosen`
--

CREATE TABLE `t_status_dosen` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_dosen`
--

INSERT INTO `t_status_dosen` (`id`, `nama`) VALUES
(1, 'AKTIF MENGAJAR'),
(2, 'CUTI'),
(3, 'KELUAR'),
(4, 'ALMARHUM'),
(5, 'PENSIUN'),
(6, 'STUDI LANJUT'),
(7, 'TUGAS DI INSTANSI LAIN');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_jurusan`
--

CREATE TABLE `t_status_jurusan` (
  `id` int(11) NOT NULL,
  `kode` varchar(2) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_jurusan`
--

INSERT INTO `t_status_jurusan` (`id`, `kode`, `nama`) VALUES
(-1, 'N', 'NON-AKTIF'),
(1, 'A', 'AKTIF'),
(2, 'B', 'ALIH BENTUK'),
(3, 'H', 'HAPUS'),
(4, 'K', 'ALIH KELOLA'),
(5, 'M', 'MERGER');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_krs`
--

CREATE TABLE `t_status_krs` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_krs`
--

INSERT INTO `t_status_krs` (`id`, `nama`) VALUES
(-1, 'Belum Mengajukan'),
(0, 'Menunggu ACC'),
(1, 'Telah di-ACC');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_lkd`
--

CREATE TABLE `t_status_lkd` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_lkd`
--

INSERT INTO `t_status_lkd` (`id`, `nama`) VALUES
(-1, 'Belum Mengajukan ACC'),
(0, 'Menunggu ACC'),
(1, 'Telah di-ACC');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_login`
--

CREATE TABLE `t_status_login` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_login`
--

INSERT INTO `t_status_login` (`id`, `nama`) VALUES
(0, 'FAILED'),
(1, 'SUCCESS');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_lulus`
--

CREATE TABLE `t_status_lulus` (
  `id` int(11) NOT NULL,
  `nama` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_lulus`
--

INSERT INTO `t_status_lulus` (`id`, `nama`) VALUES
(0, 'TIDAK LULUS'),
(1, 'LULUS');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_mahasiswa`
--

CREATE TABLE `t_status_mahasiswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_mahasiswa`
--

INSERT INTO `t_status_mahasiswa` (`id`, `nama`) VALUES
(1, 'AKTIF'),
(2, 'CUTI/TERMINAL'),
(3, 'DROP OUT/PUTUS STUDI'),
(4, 'KELUAR'),
(5, 'LULUS'),
(6, 'NON-AKTIF'),
(7, 'PINDAH JURUSAN');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_pengajuan_sekolah`
--

CREATE TABLE `t_status_pengajuan_sekolah` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_pengajuan_sekolah`
--

INSERT INTO `t_status_pengajuan_sekolah` (`id`, `nama`) VALUES
(-1, 'Menunggu Perbaikan'),
(0, 'Menunggu Verifikasi'),
(1, 'Terverifikasi');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_pernikahan`
--

CREATE TABLE `t_status_pernikahan` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_pernikahan`
--

INSERT INTO `t_status_pernikahan` (`id`, `nama`) VALUES
(1, 'Menikah'),
(2, 'Belum Menikah'),
(3, 'Janda/Duda');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_pertemuan`
--

CREATE TABLE `t_status_pertemuan` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_pertemuan`
--

INSERT INTO `t_status_pertemuan` (`id`, `nama`) VALUES
(0, 'BELUM DI-SUBMIT'),
(1, 'TELAH DI-SUBMIT');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_pns`
--

CREATE TABLE `t_status_pns` (
  `id` int(11) NOT NULL,
  `nama` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_pns`
--

INSERT INTO `t_status_pns` (`id`, `nama`) VALUES
(0, 'NON PNS'),
(1, 'PNS');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_serdos`
--

CREATE TABLE `t_status_serdos` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_serdos`
--

INSERT INTO `t_status_serdos` (`id`, `nama`) VALUES
(0, 'belum serdos'),
(1, 'sudah serdos');

-- --------------------------------------------------------

--
-- Table structure for table `t_status_user`
--

CREATE TABLE `t_status_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_status_user`
--

INSERT INTO `t_status_user` (`id`, `nama`) VALUES
(0, 'Non Aktif'),
(1, 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `t_tahun`
--

CREATE TABLE `t_tahun` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `tahun_awal` varchar(10) NOT NULL,
  `tahun_akhir` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tahun`
--

INSERT INTO `t_tahun` (`id`, `kode`, `tahun_awal`, `tahun_akhir`) VALUES
(1, '2015', '2015', '2016'),
(2, '2016', '2016', '2017'),
(3, '2017', '2017', '2018'),
(4, '2018', '2018', '2019'),
(5, '2011', '2011', '2012'),
(6, '2012', '2012', '2013');

-- --------------------------------------------------------

--
-- Table structure for table `t_tahun_pmb`
--

CREATE TABLE `t_tahun_pmb` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `tahun_awal` varchar(10) NOT NULL,
  `tahun_akhir` varchar(10) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tahun_pmb`
--

INSERT INTO `t_tahun_pmb` (`id`, `kode`, `tahun_awal`, `tahun_akhir`, `tanggal_mulai`, `tanggal_selesai`, `status`) VALUES
(1, ' 2018', ' 2018', '2019', '2018-12-21', '2018-12-28', 1),
(3, ' 2019', ' 2019', '2020', '0000-00-00', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_tingkat`
--

CREATE TABLE `t_tingkat` (
  `id` int(11) NOT NULL,
  `nama` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tingkat`
--

INSERT INTO `t_tingkat` (`id`, `nama`) VALUES
(1, 'Tingkat I'),
(2, 'Tingkat II'),
(3, 'Tingkat III'),
(4, 'Tingkat IV');

-- --------------------------------------------------------

--
-- Table structure for table `t_tingkat_jurusan`
--

CREATE TABLE `t_tingkat_jurusan` (
  `id` int(11) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `id_tingkat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_tingkat_prestasi`
--

CREATE TABLE `t_tingkat_prestasi` (
  `id` int(11) NOT NULL,
  `nama` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tingkat_prestasi`
--

INSERT INTO `t_tingkat_prestasi` (`id`, `nama`) VALUES
(1, 'INTERNASIONAL'),
(2, 'NASIONAL'),
(3, 'REGIONAL');

-- --------------------------------------------------------

--
-- Table structure for table `t_tipe_admin`
--

CREATE TABLE `t_tipe_admin` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tipe_admin`
--

INSERT INTO `t_tipe_admin` (`id`, `kode`, `nama`) VALUES
(1, 'super', 'Super Admin'),
(2, 'sisfo', 'Admin Sisfo'),
(3, 'smb', 'Admin SMB'),
(4, 'keuangan', 'Admin Keuangan'),
(5, 'fakultas', 'Admin Fakultas'),
(6, 'jurusan', 'Admin Jurusan');

-- --------------------------------------------------------

--
-- Table structure for table `t_tipe_dekan`
--

CREATE TABLE `t_tipe_dekan` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tipe_dekan`
--

INSERT INTO `t_tipe_dekan` (`id`, `kode`, `nama`) VALUES
(1, 'dekan', 'Dekan'),
(2, 'wadek1', 'Wakil Dekan Akademik'),
(3, 'wadek2', 'Wakil Dekan Keuangan'),
(4, 'wadek3', 'Wakil Dekan Kemahasiswaan');

-- --------------------------------------------------------

--
-- Table structure for table `t_tipe_kaprodi`
--

CREATE TABLE `t_tipe_kaprodi` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tipe_kaprodi`
--

INSERT INTO `t_tipe_kaprodi` (`id`, `kode`, `nama`) VALUES
(1, 'kaprodi', 'KETUA JURUSAN'),
(2, 'sekprodi', 'SEKRETARIS JURUSAN');

-- --------------------------------------------------------

--
-- Table structure for table `t_tipe_pendaftaran`
--

CREATE TABLE `t_tipe_pendaftaran` (
  `id` int(11) NOT NULL,
  `tipe` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tipe_pendaftaran`
--

INSERT INTO `t_tipe_pendaftaran` (`id`, `tipe`) VALUES
(1, 'BARU'),
(2, 'PINDAHAN/LANJUTAN');

-- --------------------------------------------------------

--
-- Table structure for table `t_tipe_rektor`
--

CREATE TABLE `t_tipe_rektor` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tipe_rektor`
--

INSERT INTO `t_tipe_rektor` (`id`, `kode`, `nama`) VALUES
(1, 'rektor', 'Rektor'),
(2, 'warek1', 'Wakil Rektor Bid. Akademik dan Kelembagaan'),
(3, 'warek2', 'Wakil Rektor Bid. Adm. Umum dan Keuangan'),
(4, 'warek3', 'Wakil Rektor Bid. Kemahasiswaan dan Kerja Sama');

-- --------------------------------------------------------

--
-- Table structure for table `t_tipe_sekolah`
--

CREATE TABLE `t_tipe_sekolah` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tipe_sekolah`
--

INSERT INTO `t_tipe_sekolah` (`id`, `nama`) VALUES
(1, 'SMA'),
(2, 'SMK'),
(3, 'MA'),
(4, 'MAK');

-- --------------------------------------------------------

--
-- Table structure for table `t_tujuan_pengumuman`
--

CREATE TABLE `t_tujuan_pengumuman` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tujuan_pengumuman`
--

INSERT INTO `t_tujuan_pengumuman` (`id`, `nama`) VALUES
(1, 'DOSEN DAN MAHASISWA'),
(2, 'DOSEN'),
(3, 'MAHASISWA');

-- --------------------------------------------------------

--
-- Table structure for table `t_ujian_pmb`
--

CREATE TABLE `t_ujian_pmb` (
  `id` int(11) NOT NULL,
  `no_ujian` varchar(10) NOT NULL,
  `tanggal_ujian` date NOT NULL,
  `jam_mulai` varchar(10) NOT NULL,
  `jam_selesai` varchar(10) NOT NULL,
  `id_ruangan` int(11) DEFAULT NULL,
  `id_batch` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ujian_pmb`
--

INSERT INTO `t_ujian_pmb` (`id`, `no_ujian`, `tanggal_ujian`, `jam_mulai`, `jam_selesai`, `id_ruangan`, `id_batch`) VALUES
(1, '123', '2019-01-08', '08:00', '09:00', 3, 9),
(4, '21', '2019-01-01', '13:00', '15:00', 4, 9);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `status`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
('dosen', 'ce28eed1511f631af6b2a7bb0a85d636', 'dosen'),
('mhs', '0357a7592c4734a8b1e12bc48e29e9e9', 'mhs');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bris_uin`
--
ALTER TABLE `bris_uin`
  ADD PRIMARY KEY (`virtual_account`);

--
-- Indexes for table `m_jabatan_dosen`
--
ALTER TABLE `m_jabatan_dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_status_kegiatan_lkd`
--
ALTER TABLE `m_status_kegiatan_lkd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_unit_kerja`
--
ALTER TABLE `m_unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_absensi`
--
ALTER TABLE `t_absensi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_pilih_matkul` (`id_pilih_matkul`,`id_pertemuan`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_pertemuan` (`id_pertemuan`);

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_admin_ibfk_1` (`id_pegawai`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_admin_fakultas`
--
ALTER TABLE `t_admin_fakultas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_admin` (`id_admin`,`id_fakultas`),
  ADD KEY `id_fakultas` (`id_fakultas`);

--
-- Indexes for table `t_admin_jurusan`
--
ALTER TABLE `t_admin_jurusan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_admin` (`id_admin`,`id_jurusan`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- Indexes for table `t_agama`
--
ALTER TABLE `t_agama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_akreditasi_sekolah`
--
ALTER TABLE `t_akreditasi_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_akun`
--
ALTER TABLE `t_akun`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_akun_pmb`
--
ALTER TABLE `t_akun_pmb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_alat_transportasi`
--
ALTER TABLE `t_alat_transportasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_batch_pmb`
--
ALTER TABLE `t_batch_pmb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `id_tahun` (`id_tahun`);

--
-- Indexes for table `t_biaya_pendaftaran`
--
ALTER TABLE `t_biaya_pendaftaran`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_jurusan` (`id_jurusan`,`id_tipe_pendaftaran`),
  ADD KEY `id_tipe_pendaftaran` (`id_tipe_pendaftaran`);

--
-- Indexes for table `t_camaba`
--
ALTER TABLE `t_camaba`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_akun` (`id_akun`),
  ADD KEY `id_kabupaten` (`id_kabupaten`),
  ADD KEY `jenis_kelamin` (`jenis_kelamin`),
  ADD KEY `agama` (`agama`),
  ADD KEY `id_tipe_pendaftaran` (`id_tipe_pendaftaran`),
  ADD KEY `kewarganegaraan` (`kewarganegaraan`),
  ADD KEY `id_kabupaten_2` (`id_kabupaten`),
  ADD KEY `id_kabupaten_3` (`id_kabupaten`),
  ADD KEY `id_batch` (`id_batch`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_ujian` (`id_ujian`),
  ADD KEY `pilihan_lulus` (`pilihan_lulus`),
  ADD KEY `jenis_tinggal` (`jenis_tinggal`),
  ADD KEY `alat_transportasi` (`alat_transportasi`),
  ADD KEY `pendidikan_ayah` (`pendidikan_ayah`),
  ADD KEY `pekerjaan_ayah` (`pekerjaan_ayah`),
  ADD KEY `penghasilan_ayah` (`penghasilan_ayah`),
  ADD KEY `pedidikan_ibu` (`pendidikan_ibu`),
  ADD KEY `pekerjaan_ibu` (`pekerjaan_ibu`),
  ADD KEY `penghasilan_ibu` (`penghasilan_ibu`),
  ADD KEY `pendidikan_wali` (`pendidikan_wali`),
  ADD KEY `pekerjaan_wali` (`pekerjaan_wali`),
  ADD KEY `penghasilan_wali` (`penghasilan_wali`);

--
-- Indexes for table `t_config_kampus`
--
ALTER TABLE `t_config_kampus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_type` (`option_type`),
  ADD KEY `option_category` (`option_category`);

--
-- Indexes for table `t_config_lkd`
--
ALTER TABLE `t_config_lkd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jabatan` (`id_jabatan`,`id_kategori`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `t_config_nilai`
--
ALTER TABLE `t_config_nilai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_prodi` (`id_prodi`),
  ADD KEY `id_lulus` (`id_lulus`),
  ADD KEY `id_semester` (`id_semester`);

--
-- Indexes for table `t_config_option`
--
ALTER TABLE `t_config_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_config_rekening_pmb`
--
ALTER TABLE `t_config_rekening_pmb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_dekan`
--
ALTER TABLE `t_dekan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_detail_lkd`
--
ALTER TABLE `t_detail_lkd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kegiatan` (`id_kegiatan`),
  ADD KEY `id_lkd_harian` (`id_lkd_harian`);

--
-- Indexes for table `t_detail_nilai`
--
ALTER TABLE `t_detail_nilai`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_penilaian_2` (`id_penilaian`,`id_komponen`),
  ADD KEY `id_penilaian` (`id_penilaian`),
  ADD KEY `id_komponen` (`id_komponen`);

--
-- Indexes for table `t_dosen`
--
ALTER TABLE `t_dosen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tipe` (`id_pangkat`),
  ADD KEY `id_unit_kerja` (`id_unit_kerja`),
  ADD KEY `id_jabatan` (`id_jabatan`),
  ADD KEY `id_tipe_2` (`id_pangkat`),
  ADD KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_fakultas` (`id_fakultas`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_matkul` (`id_matkul`),
  ADD KEY `serdos` (`serdos`),
  ADD KEY `status_aktif` (`status_aktif`),
  ADD KEY `akta_mengajar` (`akta_mengajar`),
  ADD KEY `izin_mengajar` (`izin_mengajar`),
  ADD KEY `id_prodi` (`id_prodi`);

--
-- Indexes for table `t_fakultas`
--
ALTER TABLE `t_fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_gedung`
--
ALTER TABLE `t_gedung`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_hari`
--
ALTER TABLE `t_hari`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_history_pembayaran`
--
ALTER TABLE `t_history_pembayaran`
  ADD PRIMARY KEY (`virtual_account`,`nama_thn_ak`);

--
-- Indexes for table `t_izin_krs`
--
ALTER TABLE `t_izin_krs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jadwal_kelas`
--
ALTER TABLE `t_jadwal_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kelas_matkul` (`id_kelas_matkul`),
  ADD KEY `id_ruangan` (`id_ruangan`),
  ADD KEY `id_hari` (`id_hari`);

--
-- Indexes for table `t_jadwal_krs`
--
ALTER TABLE `t_jadwal_krs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_semester` (`id_semester`,`id_jurusan`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- Indexes for table `t_jalur_masuk`
--
ALTER TABLE `t_jalur_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenis_kelamin`
--
ALTER TABLE `t_jenis_kelamin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenis_kelas`
--
ALTER TABLE `t_jenis_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenis_matkul`
--
ALTER TABLE `t_jenis_matkul`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenis_pendaftaran`
--
ALTER TABLE `t_jenis_pendaftaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenis_pertemuan`
--
ALTER TABLE `t_jenis_pertemuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenis_tinggal`
--
ALTER TABLE `t_jenis_tinggal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenjang_jurusan`
--
ALTER TABLE `t_jenjang_jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jurusan`
--
ALTER TABLE `t_jurusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_fakultas` (`id_fakultas`),
  ADD KEY `id_jenjang` (`id_jenjang`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_semester_mulai` (`id_semester_mulai`);

--
-- Indexes for table `t_jurusan_sekolah`
--
ALTER TABLE `t_jurusan_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kabupaten`
--
ALTER TABLE `t_kabupaten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `regencies_province_id_index` (`id_provinsi`);

--
-- Indexes for table `t_kandidat_dosen`
--
ALTER TABLE `t_kandidat_dosen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_matkul` (`id_matkul`),
  ADD KEY `id_dosen` (`id_dosen`);

--
-- Indexes for table `t_kaprodi`
--
ALTER TABLE `t_kaprodi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_kategori_kegiatan_lkd`
--
ALTER TABLE `t_kategori_kegiatan_lkd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kecamatan`
--
ALTER TABLE `t_kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kegiatan_lkd`
--
ALTER TABLE `t_kegiatan_lkd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_kelas`
--
ALTER TABLE `t_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jurusan` (`id_jurusan`),
  ADD KEY `id_tahun` (`id_tahun`),
  ADD KEY `id_jenis_kelas` (`id_jenis_kelas`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_doswal` (`id_doswal`);

--
-- Indexes for table `t_kelas_jurusan`
--
ALTER TABLE `t_kelas_jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kelas_matkul`
--
ALTER TABLE `t_kelas_matkul`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_kelas_2` (`id_kelas`,`id_plot`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_plot` (`id_plot`),
  ADD KEY `id_dosen` (`id_dosen`);

--
-- Indexes for table `t_kewarganegaraan`
--
ALTER TABLE `t_kewarganegaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_komponen_nilai`
--
ALTER TABLE `t_komponen_nilai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kelas_matkul` (`id_kelas_matkul`);

--
-- Indexes for table `t_krs_mahasiswa`
--
ALTER TABLE `t_krs_mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pembayaran` (`id_pembayaran`),
  ADD KEY `id_history_pembayaran` (`id_history_pembayaran`),
  ADD KEY `id_pembayaran_2` (`id_pembayaran`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_mahasiswa` (`id_mahasiswa`),
  ADD KEY `id_semester` (`id_semester`),
  ADD KEY `id_izin` (`id_izin`);

--
-- Indexes for table `t_lkd_harian`
--
ALTER TABLE `t_lkd_harian`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tanggal` (`tanggal`,`id_pengajuan`),
  ADD KEY `id_pengajuan` (`id_pengajuan`);

--
-- Indexes for table `t_mahasiswa`
--
ALTER TABLE `t_mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nim` (`nim`),
  ADD KEY `id_akun` (`id_akun`),
  ADD KEY `jenis_kelamin` (`jenis_kelamin`),
  ADD KEY `jenis_kelamin_2` (`jenis_kelamin`),
  ADD KEY `agama` (`agama`),
  ADD KEY `id_jalur_masuk` (`id_jalur_masuk`),
  ADD KEY `jenis_pendaftaran` (`jenis_pendaftaran`),
  ADD KEY `mulai_semester` (`mulai_semester`),
  ADD KEY `kecamatan` (`kecamatan`),
  ADD KEY `jenis_tinggal` (`jenis_tinggal`),
  ADD KEY `alat_transportasi` (`alat_transportasi`),
  ADD KEY `pekerjaan_ayah` (`pekerjaan_ayah`),
  ADD KEY `pendidikan_ayah` (`pendidikan_ayah`),
  ADD KEY `penghasilan_ayah` (`penghasilan_ayah`),
  ADD KEY `pendidikan_ibu` (`pendidikan_ibu`),
  ADD KEY `pekerjaan_ibu` (`pekerjaan_ibu`),
  ADD KEY `penghasilan_ibu` (`penghasilan_ibu`),
  ADD KEY `pendidikan_wali` (`pendidikan_wali`),
  ADD KEY `pekerjaan_wali` (`pekerjaan_wali`),
  ADD KEY `penghasilan_wali` (`penghasilan_wali`),
  ADD KEY `kewarganegaraan` (`kewarganegaraan`),
  ADD KEY `id_prodi` (`id_prodi`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `s_jurusan` (`s_jurusan`),
  ADD KEY `s_sekolah` (`s_sekolah`),
  ADD KEY `s_kabupaten` (`s_kabupaten`),
  ADD KEY `s_kecamatan` (`s_kecamatan`),
  ADD KEY `kabupaten` (`kabupaten`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_dosenpa` (`id_dosenpa`);

--
-- Indexes for table `t_matkul`
--
ALTER TABLE `t_matkul`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_prodi` (`id_prodi`),
  ADD KEY `status_sap` (`status_sap`);

--
-- Indexes for table `t_matkul_jurusan`
--
ALTER TABLE `t_matkul_jurusan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_matkul_2` (`id_matkul`,`id_jurusan`),
  ADD KEY `id_matkul` (`id_matkul`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- Indexes for table `t_pangkat_dosen`
--
ALTER TABLE `t_pangkat_dosen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pns` (`id_pns`);

--
-- Indexes for table `t_pegawai`
--
ALTER TABLE `t_pegawai`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_user` (`id_akun`),
  ADD KEY `pendidikan` (`pendidikan`),
  ADD KEY `jenis_kelamin` (`jenis_kelamin`),
  ADD KEY `kabupaten` (`kabupaten`),
  ADD KEY `kewarganegaraan` (`kewarganegaraan`),
  ADD KEY `status_perkawinan` (`status_perkawinan`),
  ADD KEY `pekerjaan_pasangan` (`pekerjaan_pasangan`),
  ADD KEY `agama` (`agama`);

--
-- Indexes for table `t_pekerjaan`
--
ALTER TABLE `t_pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pembayaran`
--
ALTER TABLE `t_pembayaran`
  ADD PRIMARY KEY (`virtual_account`);

--
-- Indexes for table `t_pendidikan`
--
ALTER TABLE `t_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pendidikan_pegawai`
--
ALTER TABLE `t_pendidikan_pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pengajuan_bulanan_lkd`
--
ALTER TABLE `t_pengajuan_bulanan_lkd`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode_bulan` (`kode_bulan`,`id_dosen`),
  ADD KEY `status_pengajuan` (`status_pengajuan`),
  ADD KEY `t_pengajuan_bulanan_lkd_ibfk_1` (`id_dosen`);

--
-- Indexes for table `t_pengajuan_lkd`
--
ALTER TABLE `t_pengajuan_lkd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dosen` (`id_dosen`),
  ADD KEY `status_pengajuan` (`status_pengajuan`),
  ADD KEY `id_periode` (`id_periode`);

--
-- Indexes for table `t_penghasilan`
--
ALTER TABLE `t_penghasilan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pengumuman`
--
ALTER TABLE `t_pengumuman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tujuan` (`id_tujuan`);

--
-- Indexes for table `t_penilaian`
--
ALTER TABLE `t_penilaian`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_pilih_matkul_2` (`id_pilih_matkul`),
  ADD KEY `id_pilih_matkul` (`id_pilih_matkul`);

--
-- Indexes for table `t_periode_lkd`
--
ALTER TABLE `t_periode_lkd`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tanggal_awal` (`tanggal_awal`,`tanggal_akhir`);

--
-- Indexes for table `t_pertemuan`
--
ALTER TABLE `t_pertemuan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `judul` (`judul`),
  ADD KEY `keterangan` (`keterangan`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_kelas_matkul` (`id_kelas_matkul`),
  ADD KEY `id_jadwal_kelas` (`id_jadwal_kelas`),
  ADD KEY `id_ruangan` (`id_ruangan`);

--
-- Indexes for table `t_pilihan_prodi`
--
ALTER TABLE `t_pilihan_prodi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_camaba` (`id_camaba`,`id_prodi`),
  ADD KEY `id_prodi` (`id_prodi`);

--
-- Indexes for table `t_pilih_matkul_mhs`
--
ALTER TABLE `t_pilih_matkul_mhs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_kelas_matkul` (`id_kelas_matkul`,`id_krs_mhs`),
  ADD KEY `id_krs_mhs` (`id_krs_mhs`);

--
-- Indexes for table `t_plot_matkul`
--
ALTER TABLE `t_plot_matkul`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_jurusan` (`id_jurusan`,`id_tingkat`,`id_matkul`,`id_semester`),
  ADD KEY `id_matkul` (`id_matkul`),
  ADD KEY `id_tingkat` (`id_tingkat`),
  ADD KEY `t_plot_matkul_ibfk_3` (`id_semester`),
  ADD KEY `id_dosen_koor` (`id_dosen_koor`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indexes for table `t_prestasi`
--
ALTER TABLE `t_prestasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tingkat_prestasi` (`id_tingkat_prestasi`),
  ADD KEY `id_mahasiswa` (`id_mahasiswa`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_provinsi`
--
ALTER TABLE `t_provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_rektor`
--
ALTER TABLE `t_rektor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pegawai` (`id_pegawai`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_riwayat_login`
--
ALTER TABLE `t_riwayat_login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_status` (`id_status`),
  ADD KEY `id_user` (`id_akun`),
  ADD KEY `id_status_2` (`id_status`);

--
-- Indexes for table `t_role_admin`
--
ALTER TABLE `t_role_admin`
  ADD PRIMARY KEY (`id_admin`,`id_role`),
  ADD KEY `id_tipe` (`id_role`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_role_dekan`
--
ALTER TABLE `t_role_dekan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dekan` (`id_dekan`),
  ADD KEY `id_role` (`id_role`),
  ADD KEY `id_fakultas` (`id_fakultas`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_role_jurusan`
--
ALTER TABLE `t_role_jurusan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_jurusan` (`id_jurusan`,`id_kelas`),
  ADD KEY `id_kelas` (`id_kelas`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_role_kaprodi`
--
ALTER TABLE `t_role_kaprodi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kaprodi` (`id_kajur`),
  ADD KEY `id_role` (`id_role`),
  ADD KEY `id_prodi` (`id_jurusan`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_role_rektor`
--
ALTER TABLE `t_role_rektor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rektor` (`id_rektor`),
  ADD KEY `id_role` (`id_role`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_ruangan`
--
ALTER TABLE `t_ruangan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_gedung` (`id_gedung`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_sekolah`
--
ALTER TABLE `t_sekolah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_akun` (`id_akun`),
  ADD KEY `id_tipe` (`id_tipe`),
  ADD KEY `id_akreditasi` (`id_akreditasi`),
  ADD KEY `id_tipe_2` (`id_tipe`),
  ADD KEY `id_akun_2` (`id_akun`),
  ADD KEY `id_kabupaten` (`id_kabupaten`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_semester`
--
ALTER TABLE `t_semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_semester_ajaran`
--
ALTER TABLE `t_semester_ajaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_semester` (`id_semester`),
  ADD KEY `id_tahun` (`id_tahun`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `t_status_absensi`
--
ALTER TABLE `t_status_absensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_camaba`
--
ALTER TABLE `t_status_camaba`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_dosen`
--
ALTER TABLE `t_status_dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_jurusan`
--
ALTER TABLE `t_status_jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_krs`
--
ALTER TABLE `t_status_krs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_lkd`
--
ALTER TABLE `t_status_lkd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_login`
--
ALTER TABLE `t_status_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_lulus`
--
ALTER TABLE `t_status_lulus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_mahasiswa`
--
ALTER TABLE `t_status_mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_pengajuan_sekolah`
--
ALTER TABLE `t_status_pengajuan_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_pernikahan`
--
ALTER TABLE `t_status_pernikahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_pertemuan`
--
ALTER TABLE `t_status_pertemuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_pns`
--
ALTER TABLE `t_status_pns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_serdos`
--
ALTER TABLE `t_status_serdos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_status_user`
--
ALTER TABLE `t_status_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tahun`
--
ALTER TABLE `t_tahun`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `t_tahun_pmb`
--
ALTER TABLE `t_tahun_pmb`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kode` (`kode`);

--
-- Indexes for table `t_tingkat`
--
ALTER TABLE `t_tingkat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tingkat_jurusan`
--
ALTER TABLE `t_tingkat_jurusan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_jurusan` (`id_jurusan`,`id_tingkat`),
  ADD KEY `id_angkatan` (`id_tingkat`);

--
-- Indexes for table `t_tingkat_prestasi`
--
ALTER TABLE `t_tingkat_prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tipe_admin`
--
ALTER TABLE `t_tipe_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tipe_dekan`
--
ALTER TABLE `t_tipe_dekan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tipe_kaprodi`
--
ALTER TABLE `t_tipe_kaprodi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tipe_pendaftaran`
--
ALTER TABLE `t_tipe_pendaftaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tipe_rektor`
--
ALTER TABLE `t_tipe_rektor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tipe_sekolah`
--
ALTER TABLE `t_tipe_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tujuan_pengumuman`
--
ALTER TABLE `t_tujuan_pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_ujian_pmb`
--
ALTER TABLE `t_ujian_pmb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_ruangan` (`id_ruangan`),
  ADD KEY `id_batch` (`id_batch`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_jabatan_dosen`
--
ALTER TABLE `m_jabatan_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_status_kegiatan_lkd`
--
ALTER TABLE `m_status_kegiatan_lkd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `m_unit_kerja`
--
ALTER TABLE `m_unit_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_absensi`
--
ALTER TABLE `t_absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_admin_fakultas`
--
ALTER TABLE `t_admin_fakultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_admin_jurusan`
--
ALTER TABLE `t_admin_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_akreditasi_sekolah`
--
ALTER TABLE `t_akreditasi_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_akun`
--
ALTER TABLE `t_akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1161;

--
-- AUTO_INCREMENT for table `t_akun_pmb`
--
ALTER TABLE `t_akun_pmb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `t_batch_pmb`
--
ALTER TABLE `t_batch_pmb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `t_biaya_pendaftaran`
--
ALTER TABLE `t_biaya_pendaftaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `t_camaba`
--
ALTER TABLE `t_camaba`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `t_config_kampus`
--
ALTER TABLE `t_config_kampus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `t_config_lkd`
--
ALTER TABLE `t_config_lkd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `t_config_nilai`
--
ALTER TABLE `t_config_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_config_option`
--
ALTER TABLE `t_config_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_config_rekening_pmb`
--
ALTER TABLE `t_config_rekening_pmb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_dekan`
--
ALTER TABLE `t_dekan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_detail_lkd`
--
ALTER TABLE `t_detail_lkd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_detail_nilai`
--
ALTER TABLE `t_detail_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_dosen`
--
ALTER TABLE `t_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_fakultas`
--
ALTER TABLE `t_fakultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `t_gedung`
--
ALTER TABLE `t_gedung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_hari`
--
ALTER TABLE `t_hari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_jadwal_kelas`
--
ALTER TABLE `t_jadwal_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_jadwal_krs`
--
ALTER TABLE `t_jadwal_krs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_jenis_pertemuan`
--
ALTER TABLE `t_jenis_pertemuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_jenjang_jurusan`
--
ALTER TABLE `t_jenjang_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_jurusan`
--
ALTER TABLE `t_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `t_jurusan_sekolah`
--
ALTER TABLE `t_jurusan_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_kandidat_dosen`
--
ALTER TABLE `t_kandidat_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_kaprodi`
--
ALTER TABLE `t_kaprodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_kategori_kegiatan_lkd`
--
ALTER TABLE `t_kategori_kegiatan_lkd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_kegiatan_lkd`
--
ALTER TABLE `t_kegiatan_lkd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_kelas`
--
ALTER TABLE `t_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_kelas_jurusan`
--
ALTER TABLE `t_kelas_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_kelas_matkul`
--
ALTER TABLE `t_kelas_matkul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_komponen_nilai`
--
ALTER TABLE `t_komponen_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_krs_mahasiswa`
--
ALTER TABLE `t_krs_mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_lkd_harian`
--
ALTER TABLE `t_lkd_harian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_mahasiswa`
--
ALTER TABLE `t_mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `t_matkul`
--
ALTER TABLE `t_matkul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `t_matkul_jurusan`
--
ALTER TABLE `t_matkul_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_pangkat_dosen`
--
ALTER TABLE `t_pangkat_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `t_pegawai`
--
ALTER TABLE `t_pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=398;

--
-- AUTO_INCREMENT for table `t_pengajuan_bulanan_lkd`
--
ALTER TABLE `t_pengajuan_bulanan_lkd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_pengajuan_lkd`
--
ALTER TABLE `t_pengajuan_lkd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_pengumuman`
--
ALTER TABLE `t_pengumuman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_penilaian`
--
ALTER TABLE `t_penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_periode_lkd`
--
ALTER TABLE `t_periode_lkd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_pertemuan`
--
ALTER TABLE `t_pertemuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_pilihan_prodi`
--
ALTER TABLE `t_pilihan_prodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_pilih_matkul_mhs`
--
ALTER TABLE `t_pilih_matkul_mhs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_plot_matkul`
--
ALTER TABLE `t_plot_matkul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_prestasi`
--
ALTER TABLE `t_prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_rektor`
--
ALTER TABLE `t_rektor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_riwayat_login`
--
ALTER TABLE `t_riwayat_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;

--
-- AUTO_INCREMENT for table `t_role_dekan`
--
ALTER TABLE `t_role_dekan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_role_jurusan`
--
ALTER TABLE `t_role_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_role_kaprodi`
--
ALTER TABLE `t_role_kaprodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_role_rektor`
--
ALTER TABLE `t_role_rektor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `t_ruangan`
--
ALTER TABLE `t_ruangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_sekolah`
--
ALTER TABLE `t_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `t_semester`
--
ALTER TABLE `t_semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_semester_ajaran`
--
ALTER TABLE `t_semester_ajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `t_status_dosen`
--
ALTER TABLE `t_status_dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_status_jurusan`
--
ALTER TABLE `t_status_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_status_login`
--
ALTER TABLE `t_status_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_status_mahasiswa`
--
ALTER TABLE `t_status_mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_status_pernikahan`
--
ALTER TABLE `t_status_pernikahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_status_pertemuan`
--
ALTER TABLE `t_status_pertemuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_status_user`
--
ALTER TABLE `t_status_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_tahun`
--
ALTER TABLE `t_tahun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_tahun_pmb`
--
ALTER TABLE `t_tahun_pmb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_tingkat_jurusan`
--
ALTER TABLE `t_tingkat_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_tingkat_prestasi`
--
ALTER TABLE `t_tingkat_prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_tipe_admin`
--
ALTER TABLE `t_tipe_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_tipe_dekan`
--
ALTER TABLE `t_tipe_dekan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_tipe_kaprodi`
--
ALTER TABLE `t_tipe_kaprodi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_tipe_pendaftaran`
--
ALTER TABLE `t_tipe_pendaftaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_tipe_rektor`
--
ALTER TABLE `t_tipe_rektor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_tipe_sekolah`
--
ALTER TABLE `t_tipe_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_tujuan_pengumuman`
--
ALTER TABLE `t_tujuan_pengumuman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_ujian_pmb`
--
ALTER TABLE `t_ujian_pmb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_absensi`
--
ALTER TABLE `t_absensi`
  ADD CONSTRAINT `t_absensi_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `t_status_absensi` (`id`),
  ADD CONSTRAINT `t_absensi_ibfk_2` FOREIGN KEY (`id_pertemuan`) REFERENCES `t_pertemuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_absensi_ibfk_3` FOREIGN KEY (`id_pilih_matkul`) REFERENCES `t_pilih_matkul_mhs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD CONSTRAINT `t_admin_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `t_pegawai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_admin_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_admin_fakultas`
--
ALTER TABLE `t_admin_fakultas`
  ADD CONSTRAINT `t_admin_fakultas_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `t_admin` (`id`),
  ADD CONSTRAINT `t_admin_fakultas_ibfk_2` FOREIGN KEY (`id_fakultas`) REFERENCES `t_fakultas` (`id`);

--
-- Constraints for table `t_admin_jurusan`
--
ALTER TABLE `t_admin_jurusan`
  ADD CONSTRAINT `t_admin_jurusan_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `t_admin` (`id`),
  ADD CONSTRAINT `t_admin_jurusan_ibfk_2` FOREIGN KEY (`id_jurusan`) REFERENCES `t_jurusan` (`id`);

--
-- Constraints for table `t_akun`
--
ALTER TABLE `t_akun`
  ADD CONSTRAINT `t_akun_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_batch_pmb`
--
ALTER TABLE `t_batch_pmb`
  ADD CONSTRAINT `t_batch_pmb_ibfk_1` FOREIGN KEY (`status`) REFERENCES `t_status_user` (`id`),
  ADD CONSTRAINT `t_batch_pmb_ibfk_2` FOREIGN KEY (`id_tahun`) REFERENCES `t_tahun_pmb` (`id`);

--
-- Constraints for table `t_biaya_pendaftaran`
--
ALTER TABLE `t_biaya_pendaftaran`
  ADD CONSTRAINT `t_biaya_pendaftaran_ibfk_1` FOREIGN KEY (`id_jurusan`) REFERENCES `t_jurusan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_biaya_pendaftaran_ibfk_2` FOREIGN KEY (`id_tipe_pendaftaran`) REFERENCES `t_tipe_pendaftaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_camaba`
--
ALTER TABLE `t_camaba`
  ADD CONSTRAINT `t_camaba_ibfk_1` FOREIGN KEY (`id_akun`) REFERENCES `t_akun_pmb` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_camaba_ibfk_2` FOREIGN KEY (`id_tipe_pendaftaran`) REFERENCES `t_tipe_pendaftaran` (`id`),
  ADD CONSTRAINT `t_camaba_ibfk_3` FOREIGN KEY (`agama`) REFERENCES `t_agama` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `t_camaba_ibfk_4` FOREIGN KEY (`jenis_kelamin`) REFERENCES `t_jenis_kelamin` (`id`),
  ADD CONSTRAINT `t_camaba_ibfk_5` FOREIGN KEY (`kewarganegaraan`) REFERENCES `t_kewarganegaraan` (`id`),
  ADD CONSTRAINT `t_camaba_ibfk_6` FOREIGN KEY (`id_batch`) REFERENCES `t_batch_pmb` (`id`),
  ADD CONSTRAINT `t_camaba_ibfk_7` FOREIGN KEY (`id_status`) REFERENCES `t_status_camaba` (`id`),
  ADD CONSTRAINT `t_camaba_ibfk_8` FOREIGN KEY (`id_ujian`) REFERENCES `t_ujian_pmb` (`id`),
  ADD CONSTRAINT `t_camaba_ibfk_9` FOREIGN KEY (`pilihan_lulus`) REFERENCES `t_jurusan` (`id`);

--
-- Constraints for table `t_config_kampus`
--
ALTER TABLE `t_config_kampus`
  ADD CONSTRAINT `t_config_kampus_ibfk_1` FOREIGN KEY (`option_type`) REFERENCES `t_config_option` (`id`);

--
-- Constraints for table `t_config_lkd`
--
ALTER TABLE `t_config_lkd`
  ADD CONSTRAINT `t_config_lkd_ibfk_1` FOREIGN KEY (`id_jabatan`) REFERENCES `m_jabatan_dosen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_config_lkd_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `t_kategori_kegiatan_lkd` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_config_nilai`
--
ALTER TABLE `t_config_nilai`
  ADD CONSTRAINT `t_config_nilai_ibfk_1` FOREIGN KEY (`id_lulus`) REFERENCES `t_status_lulus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_config_nilai_ibfk_2` FOREIGN KEY (`id_prodi`) REFERENCES `t_jurusan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_config_nilai_ibfk_3` FOREIGN KEY (`id_semester`) REFERENCES `t_semester_ajaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_dekan`
--
ALTER TABLE `t_dekan`
  ADD CONSTRAINT `t_dekan_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `t_pegawai` (`id`),
  ADD CONSTRAINT `t_dekan_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_detail_lkd`
--
ALTER TABLE `t_detail_lkd`
  ADD CONSTRAINT `t_detail_lkd_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `t_kegiatan_lkd` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_detail_lkd_ibfk_2` FOREIGN KEY (`id_lkd_harian`) REFERENCES `t_lkd_harian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_detail_nilai`
--
ALTER TABLE `t_detail_nilai`
  ADD CONSTRAINT `t_detail_nilai_ibfk_1` FOREIGN KEY (`id_komponen`) REFERENCES `t_komponen_nilai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_detail_nilai_ibfk_2` FOREIGN KEY (`id_penilaian`) REFERENCES `t_penilaian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_dosen`
--
ALTER TABLE `t_dosen`
  ADD CONSTRAINT `t_dosen_ibfk_1` FOREIGN KEY (`id_pangkat`) REFERENCES `t_pangkat_dosen` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_dosen_ibfk_2` FOREIGN KEY (`id_jabatan`) REFERENCES `m_jabatan_dosen` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_dosen_ibfk_3` FOREIGN KEY (`id_unit_kerja`) REFERENCES `m_unit_kerja` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_dosen_ibfk_4` FOREIGN KEY (`id_pegawai`) REFERENCES `t_pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_dosen_ibfk_5` FOREIGN KEY (`id_fakultas`) REFERENCES `t_fakultas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_dosen_ibfk_6` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`),
  ADD CONSTRAINT `t_dosen_ibfk_7` FOREIGN KEY (`id_matkul`) REFERENCES `t_matkul` (`id`),
  ADD CONSTRAINT `t_dosen_ibfk_8` FOREIGN KEY (`serdos`) REFERENCES `t_status_serdos` (`id`),
  ADD CONSTRAINT `t_dosen_ibfk_9` FOREIGN KEY (`status_aktif`) REFERENCES `t_status_dosen` (`id`);

--
-- Constraints for table `t_gedung`
--
ALTER TABLE `t_gedung`
  ADD CONSTRAINT `t_gedung_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_jadwal_kelas`
--
ALTER TABLE `t_jadwal_kelas`
  ADD CONSTRAINT `t_jadwal_kelas_ibfk_1` FOREIGN KEY (`id_ruangan`) REFERENCES `t_ruangan` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `t_jadwal_kelas_ibfk_2` FOREIGN KEY (`id_hari`) REFERENCES `t_hari` (`id`),
  ADD CONSTRAINT `t_jadwal_kelas_ibfk_3` FOREIGN KEY (`id_kelas_matkul`) REFERENCES `t_kelas_matkul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_jadwal_krs`
--
ALTER TABLE `t_jadwal_krs`
  ADD CONSTRAINT `t_jadwal_krs_ibfk_1` FOREIGN KEY (`id_semester`) REFERENCES `t_semester_ajaran` (`id`),
  ADD CONSTRAINT `t_jadwal_krs_ibfk_2` FOREIGN KEY (`id_jurusan`) REFERENCES `t_jurusan` (`id`);

--
-- Constraints for table `t_jurusan`
--
ALTER TABLE `t_jurusan`
  ADD CONSTRAINT `t_jurusan_ibfk_1` FOREIGN KEY (`id_fakultas`) REFERENCES `t_fakultas` (`id`),
  ADD CONSTRAINT `t_jurusan_ibfk_2` FOREIGN KEY (`id_jenjang`) REFERENCES `t_jenjang_jurusan` (`id`),
  ADD CONSTRAINT `t_jurusan_ibfk_3` FOREIGN KEY (`id_status`) REFERENCES `t_status_jurusan` (`id`),
  ADD CONSTRAINT `t_jurusan_ibfk_4` FOREIGN KEY (`id_semester_mulai`) REFERENCES `t_semester_ajaran` (`id`);

--
-- Constraints for table `t_kabupaten`
--
ALTER TABLE `t_kabupaten`
  ADD CONSTRAINT `regencies_province_id_foreign` FOREIGN KEY (`id_provinsi`) REFERENCES `t_provinsi` (`id`);

--
-- Constraints for table `t_kandidat_dosen`
--
ALTER TABLE `t_kandidat_dosen`
  ADD CONSTRAINT `t_kandidat_dosen_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_kandidat_dosen_ibfk_3` FOREIGN KEY (`id_matkul`) REFERENCES `t_matkul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_kaprodi`
--
ALTER TABLE `t_kaprodi`
  ADD CONSTRAINT `t_kaprodi_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `t_pegawai` (`id`),
  ADD CONSTRAINT `t_kaprodi_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_kegiatan_lkd`
--
ALTER TABLE `t_kegiatan_lkd`
  ADD CONSTRAINT `t_kegiatan_lkd_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `t_kategori_kegiatan_lkd` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `t_kegiatan_lkd_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `m_status_kegiatan_lkd` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `t_kelas`
--
ALTER TABLE `t_kelas`
  ADD CONSTRAINT `t_kelas_ibfk_1` FOREIGN KEY (`id_jenis_kelas`) REFERENCES `t_jenis_kelas` (`id`),
  ADD CONSTRAINT `t_kelas_ibfk_2` FOREIGN KEY (`id_jurusan`) REFERENCES `t_jurusan` (`id`),
  ADD CONSTRAINT `t_kelas_ibfk_3` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`),
  ADD CONSTRAINT `t_kelas_ibfk_4` FOREIGN KEY (`id_tahun`) REFERENCES `t_tahun` (`id`),
  ADD CONSTRAINT `t_kelas_ibfk_5` FOREIGN KEY (`id_doswal`) REFERENCES `t_dosen` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `t_kelas_matkul`
--
ALTER TABLE `t_kelas_matkul`
  ADD CONSTRAINT `t_kelas_matkul_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `t_kelas_matkul_ibfk_2` FOREIGN KEY (`id_plot`) REFERENCES `t_plot_matkul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_kelas_matkul_ibfk_3` FOREIGN KEY (`id_kelas`) REFERENCES `t_kelas` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `t_komponen_nilai`
--
ALTER TABLE `t_komponen_nilai`
  ADD CONSTRAINT `t_komponen_nilai_ibfk_1` FOREIGN KEY (`id_kelas_matkul`) REFERENCES `t_kelas_matkul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_krs_mahasiswa`
--
ALTER TABLE `t_krs_mahasiswa`
  ADD CONSTRAINT `t_krs_mahasiswa_ibfk_1` FOREIGN KEY (`id_mahasiswa`) REFERENCES `t_mahasiswa` (`id`),
  ADD CONSTRAINT `t_krs_mahasiswa_ibfk_2` FOREIGN KEY (`id_semester`) REFERENCES `t_semester_ajaran` (`id`),
  ADD CONSTRAINT `t_krs_mahasiswa_ibfk_3` FOREIGN KEY (`id_status`) REFERENCES `t_status_krs` (`id`),
  ADD CONSTRAINT `t_krs_mahasiswa_ibfk_4` FOREIGN KEY (`id_izin`) REFERENCES `t_izin_krs` (`id`);

--
-- Constraints for table `t_lkd_harian`
--
ALTER TABLE `t_lkd_harian`
  ADD CONSTRAINT `t_lkd_harian_ibfk_1` FOREIGN KEY (`id_pengajuan`) REFERENCES `t_pengajuan_lkd` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_mahasiswa`
--
ALTER TABLE `t_mahasiswa`
  ADD CONSTRAINT `t_mahasiswa_ibfk_1` FOREIGN KEY (`id_akun`) REFERENCES `t_akun` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_mahasiswa_ibfk_10` FOREIGN KEY (`alat_transportasi`) REFERENCES `t_alat_transportasi` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_11` FOREIGN KEY (`pekerjaan_ayah`) REFERENCES `t_pekerjaan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_12` FOREIGN KEY (`pekerjaan_ibu`) REFERENCES `t_pekerjaan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_13` FOREIGN KEY (`pekerjaan_wali`) REFERENCES `t_pekerjaan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_14` FOREIGN KEY (`pendidikan_ayah`) REFERENCES `t_pendidikan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_15` FOREIGN KEY (`pendidikan_ibu`) REFERENCES `t_pendidikan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_16` FOREIGN KEY (`pendidikan_wali`) REFERENCES `t_pendidikan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_17` FOREIGN KEY (`penghasilan_ayah`) REFERENCES `t_penghasilan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_18` FOREIGN KEY (`penghasilan_ibu`) REFERENCES `t_penghasilan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_19` FOREIGN KEY (`penghasilan_wali`) REFERENCES `t_penghasilan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_2` FOREIGN KEY (`jenis_kelamin`) REFERENCES `t_jenis_kelamin` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_20` FOREIGN KEY (`id_prodi`) REFERENCES `t_jurusan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_21` FOREIGN KEY (`id_status`) REFERENCES `t_status_mahasiswa` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_22` FOREIGN KEY (`s_jurusan`) REFERENCES `t_jurusan_sekolah` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_23` FOREIGN KEY (`s_sekolah`) REFERENCES `t_tipe_sekolah` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_24` FOREIGN KEY (`id_kelas`) REFERENCES `t_kelas` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `t_mahasiswa_ibfk_25` FOREIGN KEY (`id_dosenpa`) REFERENCES `t_dosen` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_3` FOREIGN KEY (`id_jalur_masuk`) REFERENCES `t_jalur_masuk` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_4` FOREIGN KEY (`kewarganegaraan`) REFERENCES `t_kewarganegaraan` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_5` FOREIGN KEY (`jenis_pendaftaran`) REFERENCES `t_jenis_pendaftaran` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_6` FOREIGN KEY (`mulai_semester`) REFERENCES `t_semester_ajaran` (`id`),
  ADD CONSTRAINT `t_mahasiswa_ibfk_9` FOREIGN KEY (`jenis_tinggal`) REFERENCES `t_jenis_tinggal` (`id`);

--
-- Constraints for table `t_matkul`
--
ALTER TABLE `t_matkul`
  ADD CONSTRAINT `t_matkul_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`),
  ADD CONSTRAINT `t_matkul_ibfk_2` FOREIGN KEY (`id_jenis`) REFERENCES `t_jenis_matkul` (`id`),
  ADD CONSTRAINT `t_matkul_ibfk_3` FOREIGN KEY (`id_prodi`) REFERENCES `t_jurusan` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `t_matkul_jurusan`
--
ALTER TABLE `t_matkul_jurusan`
  ADD CONSTRAINT `t_matkul_jurusan_ibfk_1` FOREIGN KEY (`id_jurusan`) REFERENCES `t_jurusan` (`id`),
  ADD CONSTRAINT `t_matkul_jurusan_ibfk_2` FOREIGN KEY (`id_matkul`) REFERENCES `t_matkul` (`id`);

--
-- Constraints for table `t_pangkat_dosen`
--
ALTER TABLE `t_pangkat_dosen`
  ADD CONSTRAINT `t_pangkat_dosen_ibfk_1` FOREIGN KEY (`id_pns`) REFERENCES `t_status_pns` (`id`);

--
-- Constraints for table `t_pegawai`
--
ALTER TABLE `t_pegawai`
  ADD CONSTRAINT `t_pegawai_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pegawai_ibfk_2` FOREIGN KEY (`id_akun`) REFERENCES `t_akun` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pegawai_ibfk_3` FOREIGN KEY (`jenis_kelamin`) REFERENCES `t_jenis_kelamin` (`id`),
  ADD CONSTRAINT `t_pegawai_ibfk_4` FOREIGN KEY (`kewarganegaraan`) REFERENCES `t_kewarganegaraan` (`id`);

--
-- Constraints for table `t_pengajuan_bulanan_lkd`
--
ALTER TABLE `t_pengajuan_bulanan_lkd`
  ADD CONSTRAINT `t_pengajuan_bulanan_lkd_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pengajuan_bulanan_lkd_ibfk_2` FOREIGN KEY (`status_pengajuan`) REFERENCES `t_status_lkd` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `t_pengajuan_lkd`
--
ALTER TABLE `t_pengajuan_lkd`
  ADD CONSTRAINT `t_pengajuan_lkd_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `t_dosen` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pengajuan_lkd_ibfk_2` FOREIGN KEY (`status_pengajuan`) REFERENCES `t_status_lkd` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pengajuan_lkd_ibfk_3` FOREIGN KEY (`id_periode`) REFERENCES `t_periode_lkd` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `t_pengumuman`
--
ALTER TABLE `t_pengumuman`
  ADD CONSTRAINT `t_pengumuman_ibfk_1` FOREIGN KEY (`id_tujuan`) REFERENCES `t_tujuan_pengumuman` (`id`);

--
-- Constraints for table `t_penilaian`
--
ALTER TABLE `t_penilaian`
  ADD CONSTRAINT `t_penilaian_ibfk_1` FOREIGN KEY (`id_pilih_matkul`) REFERENCES `t_pilih_matkul_mhs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_pertemuan`
--
ALTER TABLE `t_pertemuan`
  ADD CONSTRAINT `t_pertemuan_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `t_jenis_pertemuan` (`id`),
  ADD CONSTRAINT `t_pertemuan_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `t_status_pertemuan` (`id`),
  ADD CONSTRAINT `t_pertemuan_ibfk_3` FOREIGN KEY (`id_kelas_matkul`) REFERENCES `t_kelas_matkul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pertemuan_ibfk_5` FOREIGN KEY (`id_ruangan`) REFERENCES `t_ruangan` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `t_pertemuan_ibfk_6` FOREIGN KEY (`id_jadwal_kelas`) REFERENCES `t_jadwal_kelas` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `t_pilihan_prodi`
--
ALTER TABLE `t_pilihan_prodi`
  ADD CONSTRAINT `t_pilihan_prodi_ibfk_1` FOREIGN KEY (`id_camaba`) REFERENCES `t_camaba` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pilihan_prodi_ibfk_2` FOREIGN KEY (`id_prodi`) REFERENCES `t_jurusan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_pilih_matkul_mhs`
--
ALTER TABLE `t_pilih_matkul_mhs`
  ADD CONSTRAINT `t_pilih_matkul_mhs_ibfk_1` FOREIGN KEY (`id_kelas_matkul`) REFERENCES `t_kelas_matkul` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_pilih_matkul_mhs_ibfk_2` FOREIGN KEY (`id_krs_mhs`) REFERENCES `t_krs_mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_plot_matkul`
--
ALTER TABLE `t_plot_matkul`
  ADD CONSTRAINT `t_plot_matkul_ibfk_1` FOREIGN KEY (`id_jurusan`) REFERENCES `t_jurusan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_plot_matkul_ibfk_2` FOREIGN KEY (`id_matkul`) REFERENCES `t_matkul` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `t_plot_matkul_ibfk_3` FOREIGN KEY (`id_semester`) REFERENCES `t_semester_ajaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_plot_matkul_ibfk_4` FOREIGN KEY (`id_tingkat`) REFERENCES `t_tingkat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_plot_matkul_ibfk_5` FOREIGN KEY (`id_dosen_koor`) REFERENCES `t_dosen` (`id`),
  ADD CONSTRAINT `t_plot_matkul_ibfk_6` FOREIGN KEY (`id_jenis`) REFERENCES `t_jenis_matkul` (`id`);

--
-- Constraints for table `t_prestasi`
--
ALTER TABLE `t_prestasi`
  ADD CONSTRAINT `t_prestasi_ibfk_1` FOREIGN KEY (`id_mahasiswa`) REFERENCES `t_mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_prestasi_ibfk_2` FOREIGN KEY (`id_tingkat_prestasi`) REFERENCES `t_tingkat_prestasi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_rektor`
--
ALTER TABLE `t_rektor`
  ADD CONSTRAINT `t_rektor_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `t_pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_rektor_ibfk_2` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_riwayat_login`
--
ALTER TABLE `t_riwayat_login`
  ADD CONSTRAINT `t_riwayat_login_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `t_status_login` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `t_riwayat_login_ibfk_2` FOREIGN KEY (`id_akun`) REFERENCES `t_akun` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `t_role_admin`
--
ALTER TABLE `t_role_admin`
  ADD CONSTRAINT `t_role_admin_ibfk_1` FOREIGN KEY (`id_admin`) REFERENCES `t_admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_role_admin_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `t_tipe_admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_role_admin_ibfk_3` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_role_dekan`
--
ALTER TABLE `t_role_dekan`
  ADD CONSTRAINT `t_role_dekan_ibfk_1` FOREIGN KEY (`id_dekan`) REFERENCES `t_dekan` (`id`),
  ADD CONSTRAINT `t_role_dekan_ibfk_2` FOREIGN KEY (`id_fakultas`) REFERENCES `t_fakultas` (`id`),
  ADD CONSTRAINT `t_role_dekan_ibfk_3` FOREIGN KEY (`id_role`) REFERENCES `t_tipe_dekan` (`id`),
  ADD CONSTRAINT `t_role_dekan_ibfk_4` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_role_jurusan`
--
ALTER TABLE `t_role_jurusan`
  ADD CONSTRAINT `t_role_jurusan_ibfk_1` FOREIGN KEY (`id_jurusan`) REFERENCES `t_jurusan` (`id`),
  ADD CONSTRAINT `t_role_jurusan_ibfk_2` FOREIGN KEY (`id_kelas`) REFERENCES `t_kelas_jurusan` (`id`),
  ADD CONSTRAINT `t_role_jurusan_ibfk_3` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_role_kaprodi`
--
ALTER TABLE `t_role_kaprodi`
  ADD CONSTRAINT `t_role_kaprodi_ibfk_1` FOREIGN KEY (`id_kajur`) REFERENCES `t_kaprodi` (`id`),
  ADD CONSTRAINT `t_role_kaprodi_ibfk_2` FOREIGN KEY (`id_jurusan`) REFERENCES `t_jurusan` (`id`),
  ADD CONSTRAINT `t_role_kaprodi_ibfk_3` FOREIGN KEY (`id_role`) REFERENCES `t_tipe_kaprodi` (`id`),
  ADD CONSTRAINT `t_role_kaprodi_ibfk_4` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_role_rektor`
--
ALTER TABLE `t_role_rektor`
  ADD CONSTRAINT `t_role_rektor_ibfk_1` FOREIGN KEY (`id_rektor`) REFERENCES `t_rektor` (`id`),
  ADD CONSTRAINT `t_role_rektor_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `t_tipe_rektor` (`id`),
  ADD CONSTRAINT `t_role_rektor_ibfk_3` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_ruangan`
--
ALTER TABLE `t_ruangan`
  ADD CONSTRAINT `t_ruangan_ibfk_1` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`),
  ADD CONSTRAINT `t_ruangan_ibfk_2` FOREIGN KEY (`id_gedung`) REFERENCES `t_gedung` (`id`);

--
-- Constraints for table `t_semester_ajaran`
--
ALTER TABLE `t_semester_ajaran`
  ADD CONSTRAINT `t_semester_ajaran_ibfk_1` FOREIGN KEY (`id_semester`) REFERENCES `t_semester` (`id`),
  ADD CONSTRAINT `t_semester_ajaran_ibfk_2` FOREIGN KEY (`id_tahun`) REFERENCES `t_tahun` (`id`),
  ADD CONSTRAINT `t_semester_ajaran_ibfk_3` FOREIGN KEY (`id_status`) REFERENCES `t_status_user` (`id`);

--
-- Constraints for table `t_ujian_pmb`
--
ALTER TABLE `t_ujian_pmb`
  ADD CONSTRAINT `t_ujian_pmb_ibfk_1` FOREIGN KEY (`id_batch`) REFERENCES `t_batch_pmb` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `t_ujian_pmb_ibfk_2` FOREIGN KEY (`id_ruangan`) REFERENCES `t_ruangan` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
